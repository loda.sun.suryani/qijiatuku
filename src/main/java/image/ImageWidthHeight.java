package image;

/*
 *   @copyright (c) Qeeka 2011 
 * @author YinChunhui    Oct 20, 2011 
 */

public class ImageWidthHeight {

	public int width;
	public int height;
	
	public ImageWidthHeight(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public String toString() {
		return width + "/" +height;
	}
	
}
