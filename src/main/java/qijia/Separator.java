package qijia;

/**
 * @author chi
 */
public class Separator {
    final String start;
    final String end;
    int p;

    public Separator(String start, String end) {
        this.start = start;
        this.end = end;
        p = 0;
    }

    public boolean matches(char c) {
        if (c == start.charAt(p)) {
            p++;
            if (p == start.length()) {
                p = 0;
                return true;
            }
            return false;
        } else {
            p = 0;
            return false;
        }
    }

    //MUST more tokens
    public int end(String content, int offset) {
        for (int i = offset; i < content.length(); i++) {
            if (content.startsWith(end, i)) {
                return i + 1;
            } else if (content.startsWith(start, i)) {
                return i;
            }
        }
        return content.length();
    }
}
