package qijia;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Iterator;

/**
 * Created by loda.sun on 14-6-9.
 */
public final class FileOperator {

    private static Logger logger = LoggerFactory.getLogger(FileOperator.class);

    public static Iterator<File> fetchHtmlFileIte(String path) {
        logger.info("iterate html file");
        return FileUtils.iterateFiles(new File(path), new String[]{"html"}, true);
    }

    public static Iterator<File> fetchAllFileIte(String path) {
        logger.info("fetch all file");
        return FileUtils.iterateFiles(new File(path), null, true);
    }

    private FileOperator() {

    }
}
