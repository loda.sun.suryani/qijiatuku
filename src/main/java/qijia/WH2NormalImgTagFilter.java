package qijia;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by loda.sun on 14-6-11.
 */
public class WH2NormalImgTagFilter implements TagFilter {

    private static Logger logger = LoggerFactory.getLogger(WH2NormalImgTagFilter.class);
    private static File needToBeCutImgFile;
    private static List<String> hasBeenModifyUrls;

    static {
        needToBeCutImgFile = new File(Config.NEED_TO_BE_CUT_IMG_TXT);
        try {
            hasBeenModifyUrls = FileUtils.readLines(needToBeCutImgFile);
        } catch (Exception ex) {
            logger.error("{}", TempUtils.throwableToString(ex));
        }
    }

    @Override
    public Tag newTag(Tag tag) {
        String heightStr = tag.attr("height");
        String widthStr = tag.attr("width");
        if (!TempUtils.isEmpty(heightStr) && !(TempUtils.isEmpty(widthStr))) {
            for (String hasBeenModifyUrl : hasBeenModifyUrls) {
                if (hasBeenModifyUrl.equals(tag.attr("src"))) {
                    return tag;
                }
            }
            logger.debug("old tag : " + tag.content());
            int height = 0;
            int width = 0;
            try {
                height = Integer.parseInt(heightStr);
                width = Integer.parseInt(widthStr);
            } catch (Exception ex) {
                logger.error("can't get height/width : " + tag.content());
                logger.error("{}", TempUtils.throwableToString(ex));
            }
            String src = tag.attr("src");
            if (src.indexOf(".") == -1) {
                logger.debug("not a legal url : " + src);
                return tag;
            }
            tag.attr("src", String.format("%s.%dX%d%s", src.substring(0, src.lastIndexOf(".")), width, height, src.substring(src.lastIndexOf("."))));
            try {
                hasBeenModifyUrls.add(tag.attr("src"));
                List<String> lines = new ArrayList<>();
                lines.add(tag.attr("src"));
                FileUtils.writeLines(needToBeCutImgFile, lines, true);
            } catch (Exception ex) {
                logger.error("{}", TempUtils.throwableToString(ex));
            }
            logger.debug("new tag : " + tag.content());
        }
        return tag;
    }

}
