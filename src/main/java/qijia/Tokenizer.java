package qijia;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author chi
 */
public class Tokenizer {
    final List<Separator> separators;

    public Tokenizer(Separator... separators) {
        this.separators = Lists.newArrayList(separators);
    }

    public List<Token> parse(String content) {
        List<Token> tokens = Lists.newArrayList();
        int lastMatched = 0;
        for (int i = 0; i < content.length(); i++) {
            char c = content.charAt(i);
            for (Separator separator : separators) {
                if (separator.matches(c)) {
                    int start = i + 1 - separator.start.length();
                    if (lastMatched < start) {
                        tokens.add(new Token(content, lastMatched, start));
                    }
                    int end = separator.end(content, start + 1);
                    tokens.add(new Token(content, start, end));
                    lastMatched = end;
                }
            }
        }
        if (lastMatched < content.length()) {
            tokens.add(new Token(content, lastMatched, content.length()));
        }
        return tokens;
    }


}
