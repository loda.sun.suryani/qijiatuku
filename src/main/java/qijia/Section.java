package qijia;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author chi
 */
public abstract class Section {
    public final Token token;
    protected final List<Segment> segments = Lists.newArrayList();

    public Section(Token token) {
        this.token = token;
    }

    public Token token() {
        return token;
    }

    public abstract void parse();

    public char charAt(int i) {
        return content().charAt(i);
    }

    public String content() {
        StringBuilder b = new StringBuilder();
        for (Segment segment : segments) {
            b.append(segment.content());
        }
        return b.toString();
    }

    public class Segment {
        protected final int start;
        protected int end;
        protected String value;

        public Segment(int start) {
            this.start = start;
        }

        public Segment setEnd(int end) {
            this.end = end;
            return this;
        }

        @Override
        public String toString() {
            return content();
        }

        public String content() {
            if (value == null) {
                return token.substring(start, end);
            }
            return value;
        }
    }
}
