import image.ImgCompress;
import org.junit.Test;
import qijia.TempUtils;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by loda.sun on 14-6-10.
 */
public class ImgCompressTest {

    @Test
    public void compressTest() throws Exception {
        long start = System.nanoTime();
        File waterPrint = new File("src/testSource/image/waterPrint.jpg");
        File sourceFile = new File("src/testSource/image/test.jpg");
        File destFile = new File("src/testSource/image/" + UUID.randomUUID().toString() + "."  + TempUtils.filePostfix(sourceFile));
        ImgCompress imgCom = new ImgCompress(sourceFile, destFile, waterPrint);
        imgCom.resizeWithWater(400, 400);
        System.out.println("during : " + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start) + " ms");
    }

}
