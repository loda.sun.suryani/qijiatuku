<?php
set_time_limit ( 10 );
ini_set ( "max_execution_time", 10 );
$city_arr = array (
		'beijing' => '北京', 
		'changsha' => '长沙', 
		'chengdu' => '成都', 
		'chongqing' => '重庆', 
		'changzhou' => '常州', 
		'hangzhou' => '杭州', 
		'dalian' => '大连', 
		'guangzhou' => '广州', 
		'haerbin' => '哈尔滨', 
		'huizhou' => '惠州', 
		'huzhou' => '湖州', 
		'hefei' => '合肥', 
		'nanjing' => '南京', 
		'jinan' => '济南', 
		'qingdao' => '青岛', 
		'kunming' => '昆明', 
		'ningbo' => '宁波', 
		'kunshan' => '昆山', 
		'nantong' => '南通', 
		'nanning' => '南宁', 
		'jinhua' => '金华', 
		'nanchang' => '南昌', 
		'suzhou' => '苏州', 
		'wuxi' => '无锡', 
		'wuhan' => '武汉', 
		'tianjin' => '天津', 
		'shenyang' => '沈阳', 
		'shenzhen' => '深圳', 
		'xian' => '西安', 
		'yangzhou' => '扬州', 
		'shijiazhuang' => '石家庄', 
		'shanghai' => '上海' );
$city_url_arr = array (
		"shenzhen" => "http://www.jia.com/11/shenzhen.html", 
		"shenyang" => "http://www.jia.com/11/shenyang.html", 
		"suzhou" => "http://www.jia.com/11/suzhou.html", 
		"tianjin" => "http://www.jia.com/11/tianjin.html", 
		"wuxi" => "http://www.jia.com/11/wuxi.html", 
		"wuhan" => "http://www.jia.com/11/wuhan.html", 
		"chongqing" => "http://www.jia.com/11/chongqing.html", 
		"beijing" => "http://www.jia.com/11/beijing.html", 
		"changsha" => "http://www.jia.com/11/changsha.html", 
		"chengdu" => "http://www.jia.com/11/chengdu.html", 
		"dalian" => "http://www.jia.com/11/dalian.html", 
		"hangzhou" => "http://www.jia.com/11/hangzhou.html", 
		"nanjing" => "http://www.jia.com/11/nanjing.html", 
		"qingdao" => "http://www.jia.com/11/qingdao.html", 
		'default' => 'http://www.jia.com/11/default.html' );

$city_id = null;
{
	//cookie city id 没有设置,先识别出来,再设置cookie
	if (getenv ( 'HTTP_CDN_SRC_IP' ) && strcasecmp ( getenv ( 'HTTP_CDN_SRC_IP' ), 'unknown' )) {
		$onlineip = getenv ( 'HTTP_CDN_SRC_IP' );
	} elseif (getenv ( 'HTTP_CLIENT_IP' ) && strcasecmp ( getenv ( 'HTTP_CLIENT_IP' ), 'unknown' )) {
		$onlineip = getenv ( 'HTTP_CLIENT_IP' );
	} elseif (getenv ( 'HTTP_X_FORWARDED_FOR' ) && strcasecmp ( getenv ( 'HTTP_X_FORWARDED_FOR' ), 'unknown' )) {
		$onlineip = getenv ( 'HTTP_X_FORWARDED_FOR' );
	} elseif (getenv ( 'REMOTE_ADDR' ) && strcasecmp ( getenv ( 'REMOTE_ADDR' ), 'unknown' )) {
		$onlineip = getenv ( 'REMOTE_ADDR' );
	} elseif (isset ( $_SERVER ['REMOTE_ADDR'] ) && $_SERVER ['REMOTE_ADDR'] && strcasecmp ( $_SERVER ['REMOTE_ADDR'], 
		'unknown' )) {
		$onlineip = $_SERVER ['REMOTE_ADDR'];
	}
	preg_match ( '/[\d\.]{7,15}/', $onlineip, $onlineipmatches );
	$onlineip = $onlineipmatches [0] ? $onlineipmatches [0] : '';
	$area = file ( "http://10.10.10.29:8080/ipserver/city/$onlineip/" );
	$go_cityname = $area [0];
	
	if (strpos ( "1" . $go_cityname, "北京" )) {
		$city_id = "beijing";
	} elseif (strpos ( "1" . $go_cityname, "长沙" )) {
		$city_id = "changsha";
	} elseif (strpos ( "1" . $go_cityname, "成都" )) {
		$city_id = "chengdu";
	} elseif (strpos ( "1" . $go_cityname, "重庆" )) {
		$city_id = "chongqing";
	} elseif (strpos ( "1" . $go_cityname, "常州" )) {
		$city_id = "changzhou";
	} elseif (strpos ( "1" . $go_cityname, "杭州" )) {
		$city_id = "hangzhou";
	} elseif (strpos ( "1" . $go_cityname, "大连" )) {
		$city_id = "dalian";
	} elseif (strpos ( "1" . $go_cityname, "广州" )) {
		$city_id = "guangzhou";
	} elseif (strpos ( "1" . $go_cityname, "哈尔滨" )) {
		$city_id = "haerbin";
	} elseif (strpos ( "1" . $go_cityname, "惠州" )) {
		$city_id = "huizhou";
	} elseif (strpos ( "1" . $go_cityname, "湖州" )) {
		$city_id = "huzhou";
	} elseif (strpos ( "1" . $go_cityname, "合肥" )) {
		$city_id = "hefei";
	} elseif (strpos ( "1" . $go_cityname, "南京" )) {
		$city_id = "nanjing";
	} elseif (strpos ( "1" . $go_cityname, "济南" )) {
		$city_id = "jinan";
	} elseif (strpos ( "1" . $go_cityname, "青岛" )) {
		$city_id = "qingdao";
	} elseif (strpos ( "1" . $go_cityname, "昆明" )) {
		$city_id = "kunming";
	} elseif (strpos ( "1" . $go_cityname, "宁波" )) {
		$city_id = "ningbo";
	} elseif (strpos ( "1" . $go_cityname, "昆山" )) {
		$city_id = "kunshan";
	} elseif (strpos ( "1" . $go_cityname, "南通" )) {
		$city_id = "nantong";
	} elseif (strpos ( "1" . $go_cityname, "南宁" )) {
		$city_id = "nanning";
	} elseif (strpos ( "1" . $go_cityname, "金华" )) {
		$city_id = "jinhua";
	} elseif (strpos ( "1" . $go_cityname, "南昌" )) {
		$city_id = "nanchang";
	} elseif (strpos ( "1" . $go_cityname, "苏州" )) {
		$city_id = "suzhou";
	} elseif (strpos ( "1" . $go_cityname, "无锡" )) {
		$city_id = "wuxi";
	} elseif (strpos ( "1" . $go_cityname, "武汉" )) {
		$city_id = "wuhan";
	} elseif (strpos ( "1" . $go_cityname, "天津" )) {
		$city_id = "tianjin";
	} elseif (strpos ( "1" . $go_cityname, "沈阳" )) {
		$city_id = "shenyang";
	} elseif (strpos ( "1" . $go_cityname, "深圳" )) {
		$city_id = "shenzhen";
	} elseif (strpos ( "1" . $go_cityname, "西安" )) {
		$city_id = "xian";
	} elseif (strpos ( "1" . $go_cityname, "扬州" )) {
		$city_id = "yangzhou";
	} elseif (strpos ( "1" . $go_cityname, "石家庄" )) {
		$city_id = "shijiazhuang";
	} elseif (strpos ( "1" . $go_cityname, "上海" )) {
		$city_id = "shanghai";
	} else {
		$city_id = "default";
	}
	@setcookie ( "city_id", $city_id, time () + 360000, "/" );
}


if (array_key_exists ( $city_id, $city_url_arr )) {
	header ( "Location: " . $city_url_arr [$city_id] );
} else {
	header ( "Location: " . $city_url_arr ['default'] );
}
exit ();
//end.