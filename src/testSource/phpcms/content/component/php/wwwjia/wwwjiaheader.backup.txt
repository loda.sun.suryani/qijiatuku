 <div id="header">
		<div class="wrap">
			<h1 class="logo">
				<a href="http://www.jia.com/" target="_top">
					<img src="http://cms.tg.com.cn/content/public/resource/image/logo/logo_jia.png" width="143" height="57" alt="家网首页" />
				</a>
			</h1>
			<h2 class="site-slogan">
				<img src="http://cms.tg.com.cn/content/public/resource/image/logo/slogan.png" width="196" height="19" alt="让装修像喝茶一样轻松" />
			</h2>
			<div id="region-wrap" class="region-wrap">
			</div>
			<!-- 城市选择结束 -->
			<div id="top-nav" role="navigation" class="top-nav">

			</div>
			<!-- 全站吊顶结束 -->
			<div id="main-search">
				<div class="search-panel">
					<form id ="Search-form" action="http://mall.jia.com/goods/goods_list/shop_goods_list" method="post" name="form_search" target="_blank">
						<div class="search-area" id="J_search">
							<div class="search-tabs">
								<span class="place-holder"><em class="J_placeHolder">商品</em><b></b></span>
								<ul id="J_searchWay">
									<li>商品</li>
									<li>装修公司</li>
									<li>案例图库</li>
									<li>装修资讯</li>
								</ul>
							</div>
							<input type="text" x-webkit-grammar="builtin:search" x-webkit-speech="" name="keyword" id="search">
					    </div>
					  <button type="submit">搜索</button>
					</form>
				</div>
				<p class="search-hot"> 
	<em>热门搜索：</em><a href="#">橱柜</a> <a href="#">地板</a> <a href="#">家具</a> <a href="#">床</a> <a href="#">木门</a> <a href="#">沙发</a> <a href="#">瓷砖</a> <a href="#">吊顶</a> <a href="#">热水器</a> 
</p>
			</div>
			<!-- 搜索面板结束 -->
		</div>
	</div>
	<!-- 全站通用导航 -->
	<div id="main-nav">
		<div class="wrap">
			<ul class="nav">
				<li class="nav-item select"><a class="nav-menu first" href="http://www.jia.com/">首页</a></li>
				<li class="nav-item"><a class="nav-menu" href="http://zhuangxiu.jia.com/">找装修公司</a>
					<ul class="sub-nav nav-fitment">
						<li><a href="http://zhuangxiu.jia.com/index.php?c=main&m=shopList&tag_content_id=">找装修公司</a></li>
						<li><a href="http://zhuangxiu.jia.com/comment/company">装修点评</a></li>
						<li><a href="http://zhuangxiu.jia.com/zixun/">装修经验</a></li>
						<li><a href="http://zhuangxiu.jia.com/apply/index">装修服务</a></li>
					</ul>
				</li>
				<li class="nav-item"><a class="nav-menu" href="http://mall.jia.com/">买建材家居</a>
					<ul class="sub-nav nav-shop">
						<li class="J_mallCate"><a class="cate-hd" href="javascript:;">全部商品分类<i></i></a>
 							<ul id="mallCate">
								<li class="first-item"><em>基础建材：</em><a href="" target="_blank">开关</a><a href="" target="_blank">水管</a><a href="" target="_blank">电线</a><a href="" target="_blank">五金</a><a href="" target="_blank">辅料</a><a href="" target="_blank">板材</a><a href="" target="_blank">油漆涂料</a></li>
								<li><em>厨卫主材：</em><a href="" target="_blank">瓷砖</a><a href="" target="_blank">卫浴</a><a href="" target="_blank">淋浴房</a><a href="" target="_blank">龙头</a><a href="" target="_blank">水槽</a><a href="" target="_blank">橱柜</a><a href="" target="_blank">吊顶</a><a href="" target="_blank">净水器</a><a href="" target="_blank">厨房电器</a></li>
								<li><em>客卧主材：</em><a href="" target="_blank">地板</a><a href="" target="_blank">房门</a><a href="" target="_blank">防盗门</a><a href="" target="_blank">移门衣柜</a><a href="" target="_blank">灯具</a><a href="" target="_blank">晾衣架</a><a href="" target="_blank">阳台窗</a><a href="" target="_blank">防盗窗</a></li>
								<li><em>住宅家具：</em><a href="" target="_blank">床</a><a href="" target="_blank">床垫</a><a href="" target="_blank">沙发</a><a href="" target="_blank">柜类</a><a href="" target="_blank">桌类</a><a href="" target="_blank">几类</a><a href="" target="_blank">架类</a><a href="" target="_blank">椅</a><a href="" target="_blank">凳</a></li>
								<li><em>家用电器：</em><a href="" target="_blank">厨房电器</a><a href="" target="_blank">热水器</a><a href="" target="_blank">浴霸</a><a href="" target="_blank">生活电器</a><a href="" target="_blank">影音电器</a><a href="" target="_blank">小家电</a></li>
								<li><em>家居软装：</em><a href="" target="_blank">墙纸</a><a href="" target="_blank">窗帘</a><a href="" target="_blank">床品</a><a href="" target="_blank">地毯</a><a href="" target="_blank">家居用品</a><a href="" target="_blank">家居饰品</a></li>
								<li><em>智能家居：</em><a href="" target="_blank">地暖</a><a href="" target="_blank">智能布线</a><a href="" target="_blank">中央空调</a><a href="" target="_blank">空气净化</a><a href="" target="_blank">保洁</a></li>
							</ul>

						</li>
						<li class="promote-hot"><a href="http://mall.jia.com/hot/">促销热卖<i class="nav-hot"></i></a></li>
						<li><a href="http://www.jia.com/help/0004.html">消费者保障</a></li>
					</ul>
				</li>
				<li class="nav-item"><a class="nav-menu" href="http://zhuangxiu.jia.com/pic/">看装修效果</a></li>
			</ul>
		</div>
	</div>
	<script src="http://i10.tg.com.cn/ui/jquery.js"></script>
	<script>
		$(document).ready(function(){

			//导航
			var _timer;
			var idx = $("#main-nav li.select").index("#main-nav li.nav-item");
			$(".nav-item").bind("mouseenter",function(){
			
				var $this = $(this);
				if( $this.hasClass("select") ){
					return;
				}else{
					$(".nav-item").eq(idx).removeClass("select");
					_timer = setTimeout(function(){
						$this.addClass("hover");
					},100);
				}
				
			}).bind("mouseleave",function(){
				clearTimeout(_timer);
				$(this).removeClass("hover");
				$(".nav-item").eq(idx).addClass("select");
			});
			$(".J_mallCate").bind("mouseenter",function(){
				$(this).find("#mallCate").slideDown(150);
			}).bind("mouseleave",function(){
				$(this).find("#mallCate").hide();
			})
			$("#mallCate").delegate("li","mouseenter",function(){
				$(this).addClass("m-select")
			});
			$("#mallCate").delegate("li","mouseleave",function(){
				$(this).removeClass("m-select")
			});
			
			//城市列表
			$(".region-label").bind("click",function(){
				var $regionList = $(this).next(".region-list");
				$(this).toggleClass("label-active");
				$regionList.slideToggle(150);
			});
			$(".region-list dl").delegate("a","click",function(e){
				e.preventDefault();
				$("#J_region").text($(this).text());
				$(".region-list").hide();
				$(".region-label").removeClass("label-active");
			});
			$(document).bind("click",function(e){
				var ele = $(e.target);
				if( ele.is(".region-list,.region-list dl,.region-list dt,.city-more,.region-label-name,#J_region,.arrow") ){
					return false;
				}else{
					$(".region-list").hide();
					$(".region-label").removeClass("label-active");
				}
			});
			
			//搜索框
			$(".search-tabs").bind("mouseenter",function(){
				$(this).find("#J_searchWay").show();
			}).bind("mouseleave",function(){
				$(this).find("#J_searchWay").hide();
			});
			$("#J_searchWay").delegate("li","click",function(){
				$(".J_placeHolder").text($(this).text());
				$("#J_searchWay").hide();
				var num = $("#J_searchWay li").index($(this));
		        var $J_placeHolder = $('.J_placeHolder');
                var $Search_form = $('#Search-form');
				var $search     = $('#search');
		        var postUrl = ["http://mall.jia.com/goods/goods_list/shop_goods_list",
					           "http://zhuangxiu.jia.com/index.php?c=index&m=search_result",
					           "http://zhuangxiu.jia.com/index.php?c=index&m=search_result",
					           "http://zhuangxiu.jia.com/index.php?c=index&m=search_result"];
                var postName = ["keyword",
					            "search",
					            "search",
					            "search"];									   
                $Search_form.attr('action',postUrl[num]);
                $search.attr('name',postName[num]);				
			});
			$(".search-tabs").delegate("li","mouseenter",function(){
				$(this).addClass("hover");
			}).delegate("li","mouseleave",function(){
				$(this).parent().children().removeClass("hover");
			});
			
			//公共吊顶
			$(".myorder,.mini-cart").hover(function () {
				$(this).find(".menu-hd").addClass("hover");
				$(this).find(".menu-bd").removeClass("hide");
			}, function () {
				$(this).find(".menu-hd").removeClass("hover");
				$(this).find(".menu-bd").addClass("hide");
			});
			$(".mini-cart .cart-bd li").hover(function () {
				$(this).toggleClass("item-highlight");
			});
		});
	</script>