<?php
@session_start ();
//var_dump($_COOKIE);
$city_arr = array (
		'beijing' => '北京', 
		'changsha' => '长沙', 
		'chengdu' => '成都', 
		'chongqing' => '重庆', 
		'changzhou' => '常州', 
		'hangzhou' => '杭州', 
		'dalian' => '大连', 
		'guangzhou' => '广州', 
		'haerbin' => '哈尔滨', 
		'huizhou' => '惠州', 
		'huzhou' => '湖州', 
		'hefei' => '合肥', 
		'nanjing' => '南京', 
		'jinan' => '济南', 
		'qingdao' => '青岛', 
		'kunming' => '昆明', 
		'ningbo' => '宁波', 
		'kunshan' => '昆山', 
		'nantong' => '南通', 
		'nanning' => '南宁', 
		'jinhua' => '金华', 
		'nanchang' => '南昌', 
		'suzhou' => '苏州', 
		'wuxi' => '无锡', 
		'wuhan' => '武汉', 
		'tianjin' => '天津', 
		'shenyang' => '沈阳', 
		'shenzhen' => '深圳', 
		'xian' => '西安', 
		'yangzhou' => '扬州', 
		'shijiazhuang' => '石家庄', 
		'shanghai' => '上海' );
$city_id = null;
if (empty ( $_COOKIE ['city_id'] )) {
	//cookie city id 没有设置,先识别出来,再设置cookie
	if (getenv ( 'HTTP_CDN_SRC_IP' ) && strcasecmp ( getenv ( 'HTTP_CDN_SRC_IP' ), 'unknown' )) {
		$onlineip = getenv ( 'HTTP_CDN_SRC_IP' );
	} elseif (getenv ( 'HTTP_CLIENT_IP' ) && strcasecmp ( getenv ( 'HTTP_CLIENT_IP' ), 'unknown' )) {
		$onlineip = getenv ( 'HTTP_CLIENT_IP' );
	} elseif (getenv ( 'HTTP_X_FORWARDED_FOR' ) && strcasecmp ( getenv ( 'HTTP_X_FORWARDED_FOR' ), 'unknown' )) {
		$onlineip = getenv ( 'HTTP_X_FORWARDED_FOR' );
	} elseif (getenv ( 'REMOTE_ADDR' ) && strcasecmp ( getenv ( 'REMOTE_ADDR' ), 'unknown' )) {
		$onlineip = getenv ( 'REMOTE_ADDR' );
	} elseif (isset ( $_SERVER ['REMOTE_ADDR'] ) && $_SERVER ['REMOTE_ADDR'] && strcasecmp ( 
		$_SERVER ['REMOTE_ADDR'], 'unknown' )) {
		$onlineip = $_SERVER ['REMOTE_ADDR'];
	}
	preg_match ( "/[\d\.]{7,15}/", $onlineip, $onlineipmatches );
	$onlineip = $onlineipmatches [0] ? $onlineipmatches [0] : '';
	$area = file ( "http://10.10.10.29:8080/ipserver/city/$onlineip/" );
	$go_cityname = $area [0];
	
	if (strpos ( "1" . $go_cityname, "北京" )) {
		$city_id = "beijing";
	} elseif (strpos ( "1" . $go_cityname, "长沙" )) {
		$city_id = "changsha";
	} elseif (strpos ( "1" . $go_cityname, "成都" )) {
		$city_id = "chengdu";
	} elseif (strpos ( "1" . $go_cityname, "重庆" )) {
		$city_id = "chongqing";
	} elseif (strpos ( "1" . $go_cityname, "常州" )) {
		$city_id = "changzhou";
	} elseif (strpos ( "1" . $go_cityname, "杭州" )) {
		$city_id = "hangzhou";
	} elseif (strpos ( "1" . $go_cityname, "大连" )) {
		$city_id = "dalian";
	} elseif (strpos ( "1" . $go_cityname, "广州" )) {
		$city_id = "guangzhou";
	} elseif (strpos ( "1" . $go_cityname, "哈尔滨" )) {
		$city_id = "haerbin";
	} elseif (strpos ( "1" . $go_cityname, "惠州" )) {
		$city_id = "huizhou";
	} elseif (strpos ( "1" . $go_cityname, "湖州" )) {
		$city_id = "huzhou";
	} elseif (strpos ( "1" . $go_cityname, "合肥" )) {
		$city_id = "hefei";
	} elseif (strpos ( "1" . $go_cityname, "南京" )) {
		$city_id = "nanjing";
	} elseif (strpos ( "1" . $go_cityname, "济南" )) {
		$city_id = "jinan";
	} elseif (strpos ( "1" . $go_cityname, "青岛" )) {
		$city_id = "qingdao";
	} elseif (strpos ( "1" . $go_cityname, "昆明" )) {
		$city_id = "kunming";
	} elseif (strpos ( "1" . $go_cityname, "宁波" )) {
		$city_id = "ningbo";
	} elseif (strpos ( "1" . $go_cityname, "昆山" )) {
		$city_id = "kunshan";
	} elseif (strpos ( "1" . $go_cityname, "南通" )) {
		$city_id = "nantong";
	} elseif (strpos ( "1" . $go_cityname, "南宁" )) {
		$city_id = "nanning";
	} elseif (strpos ( "1" . $go_cityname, "金华" )) {
		$city_id = "jinhua";
	} elseif (strpos ( "1" . $go_cityname, "南昌" )) {
		$city_id = "nanchang";
	} elseif (strpos ( "1" . $go_cityname, "苏州" )) {
		$city_id = "suzhou";
	} elseif (strpos ( "1" . $go_cityname, "无锡" )) {
		$city_id = "wuxi";
	} elseif (strpos ( "1" . $go_cityname, "武汉" )) {
		$city_id = "wuhan";
	} elseif (strpos ( "1" . $go_cityname, "天津" )) {
		$city_id = "tianjin";
	} elseif (strpos ( "1" . $go_cityname, "沈阳" )) {
		$city_id = "shenyang";
	} elseif (strpos ( "1" . $go_cityname, "深圳" )) {
		$city_id = "shenzhen";
	} elseif (strpos ( "1" . $go_cityname, "西安" )) {
		$city_id = "xian";
	} elseif (strpos ( "1" . $go_cityname, "扬州" )) {
		$city_id = "yangzhou";
	} elseif (strpos ( "1" . $go_cityname, "石家庄" )) {
		$city_id = "shijiazhuang";
	} elseif (strpos ( "1" . $go_cityname, "上海" )) {
		$city_id = "shanghai";
	} else {
		$city_id = "shanghai";
	}
	@setcookie ( "city_id", $city_id, time () + 360000, "/" );
} else {
	//cookie city id 已经设置
	$city_id = $_COOKIE ['city_id'];
}

$region_content = <<<EOD
<span>{$city_arr[$city_id]}</span>
<span class="city_select"> 
	<a target="_blank" href="javascript:void(0);" onmouseout="onMouseOutbox('Citylayer');" onmouseover="onMouseOverbox('Citylayer');" class="other">[切换城市]</a> 
<div onmouseout="onMouseOutbox('Citylayer');" style="display:none;" onmouseover="onMouseOverbox('Citylayer');" id="Citylayer">
			<div style="z-index:1000;" id="City">
				<dl id="x-cities">
					<dd class="qeeka_zone">
						[全国商城]
					</dd>
					<dt class="qeeka_city">
						<a target="_blank" href="http://other.tg.com.cn">全国商城</a> 
					</dt>
					<dd class="qeeka_zone">
						[A-C]
					</dd>
					<dt class="qeeka_city">
						<a target="_blank" href="http://beijing.tg.com.cn">北京</a> <a target="_blank" href="http://changsha.tg.com.cn">长沙</a> <a target="_blank" href="http://chengdu.tg.com.cn">成都</a> <a target="_blank" href="http://chongqing.tg.com.cn">重庆</a> <a target="_blank" href="http://changzhou.tg.com.cn">常州</a> 
					</dt>
					<dd class="qeeka_zone">
						[D-H]
					</dd>
					<dt class="qeeka_city">
						<a target="_blank" href="http://hangzhou.tg.com.cn">杭州</a> <a target="_blank" href="http://dalian.tg.com.cn">大连</a> <a target="_blank" href="http://guangzhou.tg.com.cn">广州</a> <a target="_blank" href="http://haerbin.tg.com.cn">哈尔滨</a> <a target="_blank" href="http://huizhou.tg.com.cn">惠州</a> <a target="_blank" href="http://huzhou.tg.com.cn">湖州</a> <a target="_blank" href="http://hefei.tg.com.cn">合肥</a> 
					</dt>
					<dd class="qeeka_zone">
						[J-Q]
					</dd>
					<dt class="qeeka_city">
						<a target="_blank" href="http://nanjing.tg.com.cn">南京</a> <a target="_blank" href="http://jinan.tg.com.cn">济南</a> <a target="_blank" href="http://qingdao.tg.com.cn">青岛</a> <a target="_blank" href="http://kunming.tg.com.cn">昆明</a> <a target="_blank" href="http://ningbo.tg.com.cn">宁波</a> <a target="_blank" href="http://kunshan.tg.com.cn">昆山</a> <a target="_blank" href="http://nantong.tg.com.cn">南通</a> <a target="_blank" href="http://nanning.tg.com.cn">南宁</a> <a target="_blank" href="http://jinhua.tg.com.cn">金华</a> <a target="_blank" href="http://nanchang.tg.com.cn">南昌</a> 
					</dt>
					<dd class="qeeka_zone">
						[S-Y]
					</dd>
					<dt class="qeeka_city">
						<a target="_blank" href="http://suzhou.tg.com.cn">苏州</a> <a target="_blank" href="http://wuxi.tg.com.cn">无锡</a> <a target="_blank" href="http://wuhan.tg.com.cn">武汉</a> <a target="_blank" href="http://tianjin.tg.com.cn">天津</a> <a target="_blank" href="http://shenyang.tg.com.cn">沈阳</a> <a target="_blank" href="http://shenzhen.tg.com.cn">深圳</a> <a target="_blank" href="http://xian.tg.com.cn">西安</a> <a target="_blank" href="http://yangzhou.tg.com.cn">扬州</a> <a target="_blank" href="http://shijiazhuang.tg.com.cn">石家庄</a> 
					</dt>
				</dl>
			</div>
		</div>
</span>
EOD;

//$cookie_city = $_COOKIE ['city_id'];


//登陆状态区
$user_name = '';
$return = 0;
if ($_COOKIE) {
	//新商城页面使用
	if (array_key_exists ( 'www_jia_user_name', $_COOKIE )) {
		$user_name = $_COOKIE ['www_jia_user_name'];
	}
	if (array_key_exists ( 'www_jia_user_id', $_COOKIE )) {
		$user_id = $_COOKIE ['www_jia_user_id'];
	}
	//旧页面使用的
	if (array_key_exists ( 'TG_loginuser', $_COOKIE )) {
		$user_name = $_COOKIE ['TG_loginuser'];
	}
	if (array_key_exists ( 'TG_loginuserid', $_COOKIE )) {
		$user_id = $_COOKIE ['TG_loginuserid'];
	}
	if (array_key_exists ( 'TG_checkKey', $_COOKIE )) {
		$cookie_verify = $_COOKIE ['TG_checkKey'];
	}
	$check_key = 'xiaqishitiancai';
	$corret_verify = substr ( md5 ( $user_name . $check_key . $user_id ), 0, 16 );
	
	if ($cookie_verify == $corret_verify) {
		$return = 1;
	}
}
if ($return) {
	$login_content = "
<div id='siteBar'>
  <div class='htmlbody clearfix'>
    <div id='user' class='fl'>
            您好，欢迎来到齐家网！<a href='http://shop.tg.com.cn/user/index.php?c=passport/passport' target='_blank' rel='nofollow'>[{$user_name}]</a>
            &nbsp;&nbsp;[<a href='http://shop.tg.com.cn/user/index.php?c=passport/passport&m=logout'>退出登录</a>]
           </div>

    <ul>
	    <!--<li><a class='cart' href='http://mall.tg.com.cn/shanghai/index.php?c=topic/fy&page=1' rel='nofollow' target='_blank'><font color='red'>齐家风云榜</font></a></li>-->
		 <li><a href='http://mobile.tg.com.cn/'><img src='http://shop.tg.com.cn/home/image/mobile_logo.png' style='margin-top:5px'title='手机齐家网正式发布啦，轻松报名，优惠到手'></a>&nbsp;&nbsp;</li>
        <li><a class='cart' href='http://jc.sh.tg.com.cn/index.php?c=cart/cart&m=viewcart' target='_blank'><span id='shopping_car'>购物车</span></a></li>
        <li><a href='http://i.jia.com/order/order_list.htm' target='_blank'>我的订单<span id='my_order'></span></a></li>
        <li><a href='http://i.jia.com/points/list.htm?t=1' target='_blank'>我的积分</a></li>
        <li><a href='http://i.jia.com/' target='_blank'>我的齐家</a></li>
        <li><a href='http://www.tg.com.cn/help' target='_blank'>帮助中心</a></li>
    </ul>
  </div>
</div>
	";
} else {
	//$jump_js = 'window.location.href="http://mall.tg.com.cn/shanghai/index.php?c=example&m=new_login";';
	$login_content = "
<div id='siteBar'>
  <div class='htmlbody clearfix'>
    <div id='user' class='fl'>
            您好，欢迎来到齐家网！<a href='http://mall.tg.com.cn/shanghai/index.php?c=example&m=new_login' target='_blank' rel='nofollow'>[请登录]</a>&nbsp;&nbsp;
            <a class='orange' href='http://mall.tg.com.cn/shanghai/index.php?c=example&m=new_regsiter' target='_blank' rel='nofollow'>[免费注册]</a>
           </div>

    <ul>
	    <!--<li><a class='cart' href='http://mall.tg.com.cn/shanghai/index.php?c=topic/fy&page=1' rel='nofollow' target='_blank'><font color='red'>齐家风云榜</font></a></li>-->
		 <li><a href='http://mobile.tg.com.cn/'><img src='http://shop.tg.com.cn/home/image/mobile_logo.png' style='margin-top:5px' title='手机齐家网正式发布啦，轻松报名，优惠到手'></a>&nbsp;&nbsp;</li>
        <li><a class='cart' href='http://jc.sh.tg.com.cn/index.php?c=cart/cart&m=viewcart' target='_blank'><span id='shopping_car'>购物车</span></a></li>
        <li><a href='http://shop.tg.com.cn/user/index.php?c=order/order_search' target='_blank'>我的订单<span id='my_order'></span></a></li>
        <li><a href='http://shop.tg.com.cn/user/index.php?c=point/point' target='_blank'>我的积分</a></li>
        <li><a href='http://shop.tg.com.cn/user/index.php' target='_blank'>我的齐家</a></li>
        <li><a href='http://www.tg.com.cn/help' target='_blank'>帮助中心</a></li>
    </ul>
  </div>
</div>
";
}

$js_content = '
<script type="text/javascript">
function jia_header_change_city(cname,ccityname){
	var expCook  = new Date();    
	expCook.setTime(expCook.getTime() + 30*24*60*60*1000);
	document.cookie = ("city_id="+cname+";path=/; domain=.tg.com.cn;expires="+expCook.toGMTString());
	document.cookie = ("city_name="+encodeURI(ccityname)+";path=/; domain=.tg.com.cn;expires="+expCook.toGMTString());
	location.reload();
}
</script>
';

//$region_content ==> [div id="region-wrap" class="region-wrap"]
// $login_content,$entry_menu_content,$js_content ==>   [div id="top-nav" role="navigation" class="top-nav"]


$result = array ();

$result ['success'] = true;
$result ['region_content'] = $region_content;
$result ['login_content'] = $login_content;
$result ['js_content'] = $js_content;

echo "\n\n";
echo "var tgheaderjson = ", json_encode ( $result ), ";";
?>


$(document).ready(
	function(){
		var $region_wrap = $('#city_region-wrap');
		var $top_nav = $('#tg_top-nav');
		
		if(tgheaderjson){
			var all_content = tgheaderjson.login_content  + tgheaderjson.js_content;
			$region_wrap.html(tgheaderjson.region_content);
			$top_nav.html(all_content);
			mall_Cate();
		}
	}
);

