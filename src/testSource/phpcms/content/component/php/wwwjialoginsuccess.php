<?php
//有了负载均衡之后, 用session就可能产生问题了.
//所以这里用同一台memcached来做session
ini_set ( "session.save_handler", "memcache" );
ini_set ( "session.save_path", "tcp://10.10.20.149:12000" );

session_start ();
$from = "http://mall.jia.com/index.html";
if (! empty ( $_SERVER ['HTTP_REFERER'] )) {
	$from = $_SERVER ['HTTP_REFERER'];
	$from = strtolower ( $from );
	if (strpos ( "_" . $from, "cas" ) < 1) {
		if (strpos ( "_" . $from, "login" ) < 1) {
			if (strpos ( "_" . $from, "passport" ) < 1) {
				$_SESSION ['from'] = $from;
			}
		}
	}
	if (array_key_exists ( "self", $_GET )) {
		$_SESSION ['from'] = $from;
	}
}

if (array_key_exists ( "from", $_GET )) {
	$pos = strpos ( $_SERVER ['QUERY_STRING'], "from" ) + 5;
	$from = substr ( $_SERVER ['QUERY_STRING'], $pos );
	$from = str_replace ( 'ticket=', 't=', $from );
	$_SESSION ['from'] = $from;
}
require_once '../casapi/casapi.php';
CasAPI::setDebug ( false );
$ret = CasAPI::checkAppLogin ();

if ($ret) {
	$user_id = CasAPI::getUserId ();
	$user_name = CasAPI::getUser ( 'login_name' );
	setcookie ( 'www_jia_user_id', $user_id, time () + 3600, '/', ".jia.com" );
	setcookie ( 'www_jia_user_name', $user_name, time () + 3600, '/', ".jia.com" );
	setcookie ( 'JIA_user_name', $user_name, 0, '/', ".jia.com" );
	
	//var_dump($user_id);var_dump($user_name);
	echo (__LINE__ . microtime ());
	flush ();
} else {
	//需要跳转到passport去拿一次cookie,passport之后会自动把service指定的url加ticket参数后跳回来.
	//cas再通过ticket去拿到用户名等信息.
	header ( 
		"Location: https://passport.jia.com/cas/login?app_id=301&service=http://cms.jia.com/content/component/php/wwwjialoginsuccess.php" );
	return;
}

//end.