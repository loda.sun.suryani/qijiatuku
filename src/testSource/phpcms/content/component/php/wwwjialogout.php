<?php
//有了负载均衡之后, 用session就可能产生问题了.
//所以这里用同一台memcached来做session
ini_set("session.save_handler", "memcache");
ini_set("session.save_path", "tcp://10.10.20.149:12000");
ini_set('session.name', 'PHPSESSID');

$debug = $_COOKIE['LSH_DEBUG'];
if(!$debug){
	session_start ();
}else{
	echo session_name();
}

require_once '../casapi/casapi.php';
CasAPI::setDebug ( false );
$ret = CasAPI::checkAppLogin ();
setcookie ( 'www_jia_user_id', 0, time () - 36000, '/', ".jia.com" );
setcookie ( 'www_jia_user_name', 0, time () - 36000, '/', ".jia.com" );
//CasAPI::logout ();
unset($_SESSION['phpCAS']);

if (! empty ( $_GET ['self'] )) {
	if (! empty ( $_SERVER ['HTTP_REFERER'] )) {
		$_SESSION ['from'] = $_SERVER ['HTTP_REFERER'];
		setcookie ( "logoutfrom", base64_encode ( $_SERVER ['HTTP_REFERER'] ), 0, '/', 
			".jia.com" );
	}
}

if ($ret) {
	header ( 
		"Location:https://passport.jia.com/cas/logout?app_id=301&service=http://cms.jia.com/content/component/php/wwwjialogout.php" );
} else {
	if (! empty ( $_SESSION ['from'] )) {
		$url = $_SESSION ['from'];
		//unset ( $_SESSION ['from'] );
		header ( "Location: $url" );
		flush ();
		return;
	}
	if (! empty ( $_COOKIE ['loginoutfrom'] )) {
		header ( "Location: " . base64_decode ( $_COOKIE ['logoutfrom'] ) );
		return;
	}
	header ( "Location: http://www.jia.com/" );
	flush ();
}

function my_debug($v = NULL, $var_name = NULL, $method = 'dump', $backtrace = false) {
	$display_none = '';
	if (defined ( 'FRONTPAGE' )) {
		$display_none = "display:none;";
	}
	print 
		("<div style=\"background-color:#AAAAAA;padding:5px;border:solid 1px #FF0000;margin:5px;$display_none\">") ;
	$call_stack = debug_backtrace ();
	printf ( 'file:%s ,%s(),<font color="red">line:%s</font><br>', $call_stack [0] ['file'], 
		$call_stack [1] ['function'], $call_stack [0] ['line'] );
	if (! $var_name) {
		//取$v在源代码中的变量名字.
		$v_line = file ( $call_stack [0] ['file'] );
		$f_line = $v_line [$call_stack [0] ['line'] - 1];
		$match = array ();
		preg_match ( '#my_debug.*[(].*#', $f_line, $match );
		if (count ( $match )) {
			$var_name = $match [0];
		}
	}
	if ($var_name) {
		printf ( 'debug: <font color="blue">%s</font>', $var_name );
	}
	//xdebug_var_dump ( $v );
	print ('<pre style="border:solid 1px #0000CC;padding:5px;">') ;
	if ($method == 'dump') {
		var_dump ( $v );
	} else {
		print_r ( $v );
	}
	print ("</pre>") ;
	if ($backtrace) {
		foreach ( debug_backtrace () as $v ) {
			echo ($v ['file'] . ":" . $v ['line'] . "," . $v ['class'] . "," . $v ['function'] . "()\n<br>\n");
		}
	}
	print ('</div>') ;
	print ("\n") ;
}
