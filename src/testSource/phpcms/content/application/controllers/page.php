<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Page extends CI_Controller {
	public $page_url;
	public $tpl_vars;
	function __construct() {
		parent::__construct ();
		$this->tpl_css_arr = array ();
		$this->tpl_vars = array ();
		$this->multi_database = array ();
		if (! defined ( 'FRONTPAGE' )) {
			$this->multi_database ['default'] = $this->load->database ( 'default', true );
			$this->database_default = &$this->multi_database ['default'];
			if ($this->input->get ( "static" )) {
				define ( 'FRONTPAGE', '1' );
			}
		}
		$this->load->helper ( 'tpl' );
		$this->load->config ( 'smarty' );
		$this->load->config ( 'compress_pageids' );
		$this->load->library ( 'MySmarty', '', 'smarty' );
		$this->load->library ( 'CommonCache', '', 'cache' );
		$this->smarty->registerPlugin ( "function", "make_url", "tpl_make_url" );
		$this->smarty->registerPlugin ( "function", "var_dump", "tpl_var_dump" );
		$this->smarty->registerPlugin ( "function", "format_time", "tpl_format_time" );
		$this->smarty->registerPlugin ( "function", "make_menu", "tpl_make_menu" );
		$this->smarty->registerPlugin ( "function", "make_crumbs", "tpl_make_crumbs" );
		$this->smarty->registerPlugin ( "function", "include_code", "tpl_include_code" );
		$this->smarty->registerPlugin ( "function", "get_time", "tpl_get_time" );
		$this->smarty->registerPlugin ( "function", "set_var", "tpl_set_var" );
		$this->smarty->registerPlugin ( "function", "get_var", "tpl_get_var" );
		$this->smarty->registerPlugin ( "function", "fetch_url", "tpl_fetch_url" );
		$this->smarty->registerPlugin ( "function", "get_block_id", "tpl_get_block_id" );
		$this->smarty->registerPlugin ( "function", "get_block", "tpl_get_block" );
		$this->smarty->registerPlugin ( "function", "get_parent_menu_name", "tpl_get_parent_menu_name" );
		$this->smarty->registerPlugin ( "function", "get_parent_menu_url", "tpl_get_parent_menu_url" );
		$this->smarty->registerPlugin ( "function", "make_trace", "tpl_make_trace" );
		$this->smarty->registerPlugin ( "function", "fetch_page", "tpl_fetch_page" );
		$this->smarty->registerPlugin ( "function", "get_page_id", "tpl_get_page_id" );
		//$this->smarty->registerPlugin ( "function", "seo_api", "tpl_seo_api" );
	//$this->assign ();
	}
	
	private function assign() {
		$this->smarty->assign ( 'resource_url', 
			$this->config->item ( 'cms_site_url' ) . base_url ( 'public/resource' ) );
		$this->smarty->assign ( 'base_url', base_url () );
		$this->smarty->assign ( 'base_path', FCPATH );
		$this->smarty->assign ( '_GET', $_GET );
		$this->smarty->assign ( '_page_city', $this->page_city );
		$this->smarty->assign ( '_page_site', $this->page_site );
	}
	function index() {
		$page_id = $this->input->get ( "page_id" );
		$compress_pageids = $this->config->item ( 'compress_pageids' );
		if($compress_pageids && in_array($page_id, $compress_pageids)) {
			$this->compress();
		} else {
			$this->original();
		}
	}
	
	function preview() {
		error_reporting ( E_ALL ^ E_NOTICE );
		echo $this->process_page ( 0 );
	}
	function editable() {
		error_reporting ( E_ALL ^ E_NOTICE );
		echo $this->process_page ( 1 );
	}
	function original() {
		error_reporting ( E_ALL ^ E_NOTICE );
		echo $this->process_page ( 0 );
	}
	
	private function process_page($editable = 0) {
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		$page_info = null;
		$url = $_SERVER ['REQUEST_URI'];
		$url = str_replace ( '"', "", $url );
		$url = str_replace ( "'", "", $url );
		$page_site = trim ( $_SERVER ['HTTP_HOST'] );
		if ($page_site == 'jia.com') {
			$page_site = 'www.jia.com';
		}
		if (defined ( 'FRONTPAGE' )) {
			$this->database_default = $this->load->database ( "online", true );
		}
		if ($this->input->get ( 'static' )) {
			$this->database_default = $this->load->database ( "online", true );
		}
		
		$page_info = null;
		$path = null;
		if ($url) {
			$parts = parse_url ( $url );
			if (is_array ( $parts )) {
				$path = $parts ['path'];
			}
		}
		$this->page_cache_time = (60 * 60 * 24);
		if (! $page_id) {
			//根据当前url从数据库从查找$page_id
			if (is_array ( $parts )) {
				$path = $parts ['path'];
				$path_rev = strrev ( $path );
				if ('/' == substr ( $path_rev, 0, 1 )) {
					$path = ($path . 'index.html');
				}
				$page_info = $this->database_default->get_record_by_sql ( 
					sprintf ( "SELECT * FROM cms_page WHERE page_url='%s' AND page_site='%s' ", $path, $page_site ) );
			}
			if (! $page_info) {
				if (is_array ( $parts )) {
					$path = $parts ['path'];
					$path_rev = strrev ( $path );
					if ('/' == substr ( $path_rev, 0, 1 )) {
						$path = ($path . 'index.shtml');
					}
					$page_info = $this->database_default->get_record_by_sql ( 
						sprintf ( "SELECT * FROM cms_page WHERE page_url='%s' AND page_site='%s' ", $path, 
							$page_site ) );
				}
			}
			if (! $page_info) {
				if (is_array ( $parts )) {
					$path = $parts ['path'];
					$path_rev = strrev ( $path );
					if ('lmth' !== substr ( $path_rev, 0, 4 )) {
						if ('/' !== substr ( $path_rev, 0, 1 )) {
							$path = ($path . '/index.html');
						}
					}
					$page_info = $this->database_default->get_record_by_sql ( 
						sprintf ( "SELECT * FROM cms_page WHERE page_url='%s' AND page_site='%s' ", $path, 
							$page_site ) );
				}
			}
			if (! $page_info) {
				if (DEBUGMODE) {
					//header ( 'HTTP/1.1 404 Not Found' );
					//header ( 'status: 404 Not Found' );
					echo ("404 NOT FOUND.(CMS:[$page_site]$path)");
				}
				return null;
			}
			$page_id = $page_info ['page_id'];
		} else {
			$page_info = $this->database_default->get_record_by_field ( "cms_page", 'page_id', $page_id );
		}
		$this->page_id = $page_id;
		$this->page_url = $page_info ['page_url'];
		$this->page_site = $page_info ['page_site'];
		$this->page_city = $page_info ['page_city'];
		$this->page_tpl_id = $page_info ['page_tpl_id'];
		//my_debug($page_info);
		$this->tpl_vars ['site_name'] = trim ( strtolower ( $page_info ['page_site'] ) );
		
		$page_tpl_info = $this->database_default->get_record_by_field ( "cms_page_tpl", 'page_tpl_id', 
			$page_info ['page_tpl_id'] );
		$page_tpl_content = $page_tpl_info ['tpl_content'];
		$page_tpl_content = html_entity_decode ( $page_tpl_content, ENT_QUOTES, 'UTF-8' );
		$page_block_arr = $this->database_default->get_rows_by_sql ( 
			"SELECT * FROM cms_page_block WHERE page_id='{$page_id}' ORDER BY order_num ASC" );
		//my_debug($page_tpl_info);
		//my_debug ( $page_block_arr );
		$this->assign ();
		$area_content = array ();
		foreach ( $page_block_arr as $page_block ) {
			if ($page_block ['is_hidden']) {
				continue;
			}
			
			$k = "area_" . $page_block ['area_id']; // area_1,area_2,area_3,...
			if (! array_key_exists ( $k, $area_content )) {
				$area_content [$k] = '';
			}
			
			$block_content = ($this->process_block ( $page_block ['block_id'] ));
			$this->block_id = null;
			
			if ($editable) {
				$edit_link = site_url ( 'c=createblock&block_id=' . $page_block ['block_id'] );
				$edit_up_link = modify_build_url ( 
					array (
							'c' => 'createpage', 
							'm' => 'pageblock_move_up', 
							'pageblock_id' => $page_block ['auto_id'] ), site_url () );
				$edit_down_link = modify_build_url ( 
					array (
							'c' => 'createpage', 
							'm' => 'pageblock_move_down', 
							'pageblock_id' => $page_block ['auto_id'] ), site_url () );
				$edit_delete_link = modify_build_url ( 
					array (
							'c' => 'createpage', 
							'm' => 'pageblock_delete', 
							'pageblock_id' => $page_block ['auto_id'] ), site_url () );
				$public_css_url = base_url ( 'public/css' );
				$block_content = trim ( $block_content );
				$block_div_flag = false;
				if (strlen ( $block_content )) {
					$matches = null;
					if (preg_match ( '/<[^<\s]+[\s>]{1}/', $block_content, $matches, PREG_OFFSET_CAPTURE ) > 0) {
						mb_language ( 'uni' );
						$tag_head = trim ( $matches [0] [0] );
						$tag_head = trim ( $tag_head, '>' );
						$spos = $matches [0] [1];
						if (mb_strpos ( $tag_head, 'script' ) > 0) {
						} elseif (mb_strpos ( $tag_head, '#include' ) > 0) {
						} elseif (mb_strpos ( $tag_head, 'br' ) > 0) {
						} else {
							if ($spos !== false) {
								$half_b = mb_substr ( $block_content, $spos + mb_strlen ( $tag_head ) );
								$block_content = ($tag_head . " block='block' 
										edit_link='$edit_link' 
										up_link='$edit_up_link' 
										down_link='$edit_down_link' 
										delete_link='$edit_delete_link'  " . $half_b);
							}
							$block_div_flag = true;
						}
					}
				}
				if (! $block_div_flag) {
					$block_content = "<div block='block' 
										edit_link='$edit_link'  
										up_link='$edit_up_link' 
										down_link='$edit_down_link'  
										delete_link='$edit_delete_link'>
									$block_content</div>";
				}
				$area_content [$k] .= ($block_content);
			} else {
				$area_content [$k] .= ($block_content);
			}
		}
		foreach ( $area_content as $k => $v ) {
			$this->smarty->assign ( "$k", $v );
		}
		$this->smarty->assign ( 'meta_keyword', $page_info ['page_keyword'] );
		$this->smarty->assign ( 'meta_description', $page_info ['page_description'] );
		$this->smarty->assign ( 'page_title', $page_info ['page_title'] );
		if (count ( $this->tpl_css_arr )) {
			$css_code = implode ( "\n", $this->tpl_css_arr );
			$this->smarty->assign ( 'area_css', "<style type='text/css'>\n$css_code\n</style>" );
		}
		try {
			//$this->smarty->registerPlugin ( "function", "get_block", "tpl_get_block" );
			$page_tpl_content = str_replace ( '&nbsp;', ' ', $page_tpl_content );
			$page_tpl_content = $this->smarty->fetch ( "string: $page_tpl_content" );
			//$this->smarty->unregisterPlugin ( "function", "get_block" );
		} catch ( Exception $e ) {
			if (DEBUGMODE) {
				my_debug ( $e->getMessage (), 'Smarty Exception', '', '' );
			}
		}
		$page_tpl_content = preg_replace ( '/{{\$.*?}}/', '', $page_tpl_content );
		//my_debug ();
		

		if ($editable) {
			$page_tpl_content = preg_replace ( '#<\s*/\s*body\s*>#', 
				sprintf ( 
					"<script src='%s'></script>
				<script src='%spublic/js/jquery.min.js'></script>
				<script src='%spublic/js/dialog/lhgdialog.js'></script>
				</body>", 
					modify_build_url ( array ('c' => 'javascript', 'm' => 'edit_block' ), 
						$this->config->item ( 'cms_site_url' ) . base_url () ), 
					$this->config->item ( 'cms_site_url' ) . base_url (), 
					$this->config->item ( 'cms_site_url' ) . base_url () ), $page_tpl_content );
		}
		$page_tpl_content = str_ireplace ( ' src="/content', ' src="http://cms.tg.com.cn/content', $page_tpl_content );
		
		if (defined ( 'FRONTPAGE' )) {
			//oneimg*: cdn域名替换
			$page_tpl_content = preg_replace_callback ( '#//cms.tg.com.cn/content/public/resource#', 
				'replace_img_server', $page_tpl_content );
		}
		
		/*解决图片路径问题*/
		$page_tpl_content = str_replace ( "\"/content/resource", 
			"\"" . $this->config->item ( 'cms_site_url' ) . "/content/resource", $page_tpl_content );
		$city = $this->page_city;
		if (! $city) {
			$city = "shanghai";
		}
		$page_tpl_content = $this->filter_url ( $city, $page_tpl_content );
		
		if ($page_tpl_content) {
			if ($this->input->get ( 'static' )) {
				$this->database_default->where ( 'page_id', $page_id );
				$this->database_default->update ( 'cms_page', 
					array ('refresh_period' => $this->page_cache_time, 'page_static_time' => time ()/*最后一次静态化的时间*/ ) );
				$file_name = ("/var/www/html/330cms/sitestatic/" . $page_info ['page_site'] . $page_info ['page_url']);
				$dir_name = dirname ( $file_name );
				create_dir ( $dir_name );
				if (file_exists ( $file_name )) {
					chmod ( $file_name, 0777 );
					unlink ( $file_name );
				}
				//file_put_contents ( $file_name, $page_tpl_content );
				$gen_date = "<!-- ($page_id),gen:" . date ( "Y-m-d # H:i:s" ) . "-->";
				file_put_contents ( $file_name, $page_tpl_content . $gen_date );
				chmod ( $file_name, 0777 );
			}
		}
		
		if (defined ( 'FRONTPAGE' )) {
			//将页面内容丢进,X缓存
			if ($page_tpl_content) {
				$path = $parts ['path'];
				if ($path) {
					$site = $page_info ['page_site'];
					//$cache_key = ($site . $page_info ['page_url']);
					$cache_key = ($site . $path);
					//所谓动态页,则不缓存
					if (! $page_info ['is_dynamic']) {
						/*
					$page_tpl_content = preg_replace_callback ( 
						'#//cms.tg.com.cn/content/public/resource#', 'replace_img_server', 
						$page_tpl_content );
					*/
						$this->cache->set ( $cache_key, base64_encode ( $page_tpl_content ), 0, 
							$this->page_cache_time );
					}
				}
				if ($page_info ['ab_page_id']) {
					$page_b_id = $page_info ['ab_page_id'];
					$page_b_info = $this->database_default->get_record_by_field ( 'cms_page', 'page_id', $page_b_id );
					if ($page_b_info) {
						$ab_cache_key = "abtest_$cache_key";
						$ab_conf = array (
								'ab_page_id' => $page_info ['ab_page_id'], 
								'ab_page_url' => $page_b_info ['page_url'], 
								'ab_valve' => $page_info ['ab_valve'] );
						$this->cache->set ( $ab_cache_key, $ab_conf, 0, $this->page_cache_time + 60 );
					}
				
				}
			}
		}
		return $page_tpl_content;
	}
	
	function compress() {
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		$page_info = null;
		$url = $_SERVER ['REQUEST_URI'];
		$url = str_replace ( '"', "", $url );
		$url = str_replace ( "'", "", $url );
		$page_site = trim ( $_SERVER ['HTTP_HOST'] );
		if ($page_site == 'jia.com') {
			$page_site = 'www.jia.com';
		}
		if (defined ( 'FRONTPAGE' )) {
			$this->database_default = $this->load->database ( "online", true );
		}
		if ($this->input->get ( 'static' )) {
			$this->database_default = $this->load->database ( "online", true );
		}
	
		$page_info = null;
		$path = null;
		if ($url) {
			$parts = parse_url ( $url );
			if (is_array ( $parts )) {
				$path = $parts ['path'];
			}
		}
		$this->page_cache_time = (60 * 60 * 24);
		if (! $page_id) {
			//根据当前url从数据库从查找$page_id
			if (is_array ( $parts )) {
				$path = $parts ['path'];
				$path_rev = strrev ( $path );
				if ('/' == substr ( $path_rev, 0, 1 )) {
					$path = ($path . 'index.html');
				}
				$page_info = $this->database_default->get_record_by_sql (
						sprintf ( "SELECT * FROM cms_page WHERE page_url='%s' AND page_site='%s' ", $path, $page_site ) );
			}
			if (! $page_info) {
				if (is_array ( $parts )) {
					$path = $parts ['path'];
					$path_rev = strrev ( $path );
					if ('/' == substr ( $path_rev, 0, 1 )) {
						$path = ($path . 'index.shtml');
					}
					$page_info = $this->database_default->get_record_by_sql (
							sprintf ( "SELECT * FROM cms_page WHERE page_url='%s' AND page_site='%s' ", $path,
									$page_site ) );
				}
			}
			if (! $page_info) {
				if (is_array ( $parts )) {
					$path = $parts ['path'];
					$path_rev = strrev ( $path );
					if ('lmth' !== substr ( $path_rev, 0, 4 )) {
						if ('/' !== substr ( $path_rev, 0, 1 )) {
							$path = ($path . '/index.html');
						}
					}
					$page_info = $this->database_default->get_record_by_sql (
							sprintf ( "SELECT * FROM cms_page WHERE page_url='%s' AND page_site='%s' ", $path,
									$page_site ) );
				}
			}
			if (! $page_info) {
				if (DEBUGMODE) {
					//header ( 'HTTP/1.1 404 Not Found' );
					//header ( 'status: 404 Not Found' );
					echo ("404 NOT FOUND.(CMS:[$page_site]$path)");
				}
				return null;
			}
			$page_id = $page_info ['page_id'];
		} else {
			$page_info = $this->database_default->get_record_by_field ( "cms_page", 'page_id', $page_id );
		}
		$file_name = ("/var/www/html/330cms/sitestatic/" . $page_info ['page_site'] . $page_info ['page_url']);
		$compress_pageids = $this->config->item ( 'compress_pageids' );
		if(!$compress_pageids || !in_array($page_id, $compress_pageids)) {
			$file_name = $file_name . ".compress";
		}
		$content = file_get_contents("http://d.jia.com/minify/page.php?url=http://cms.tg.com.cn/content/index.php%3Fc=page%26page_id=".$page_id."%26m=original%26static=1");
		
		if ($content) {
			if ($this->input->get ( 'static' )) {
				$content .= "<!-- ($page_id),gen:" . date ( "Y-m-d # H:i:s" ) . "-->";/*添加静态化的时间*/
			}
		}		
		file_put_contents ( $file_name, $content );
		chmod ( $file_name, 0777 );
		echo $content;
	}
	
	private function set_page_cache_time($t) {
		$this->page_cache_time = (60 * 60 * 24);
	}
	
	function process_block($block_id) {
		$this->smarty->clearAllAssign ();
		$this->assign ();
		$this->set_page_cache_time ( 3600 );
		$this->database_default->reconnect ();
		$block_info = $this->database_default->get_record_by_field ( "cms_block", "block_id", $block_id );
		
		if (! $block_info) {
			my_debug ( "碎片不存在($block_id)" );
			//xdebug_print_function_stack ();
			return null;
		}
		$block_tpl_info = $this->database_default->get_record_by_field ( "cms_block_tpl", 'block_tpl_id', 
			$block_info ['block_tpl_id'] );
		
		//tpl css code
		if ($block_tpl_info ['block_css_code']) {
			$this->tpl_css_arr [$block_tpl_info ['block_tpl_id']] = $block_tpl_info ['block_css_code'];
		}
		
		//$this->smarty->unregisterPlugin ( "function", "get_block" );
		$this->block_id = $block_id;
		$this->block_tpl_id = $block_info ['block_tpl_id'];
		
		$this->smarty->assign ( 'user_var_1', $block_info ['user_var_1'] );
		/*
		if ('html' == $block_info ['ds_type']) {
			//return $page_block_info ['ds_html'];
			$block_info ['ds_html'] = str_replace ( '&nbsp;', ' ', 
				$block_info ['ds_html'] );
			return $this->smarty->fetch ( "string: " . $block_info ['ds_html'] );
		}*/
		//my_debug($page_block_info);
		if (4 == $block_info ['refresh_mode']) {
			//$this->page_cache_time = 3600;
			return $block_info ['block_content'];
		}
		//refresh_mode==3,意思是不刷新
		if (3 == $block_info ['refresh_mode']) {
			//$this->page_cache_time = 3600;
			$this->set_page_cache_time ( 3600 );
			//return $block_info ['block_content'];
		}
		//指定了过期时间,周期: x天x小时x分x秒
		if (2 == $block_info ['refresh_mode']) {
			//$this->page_cache_time = 3600;
			$this->set_page_cache_time ( 3600 );
		}
		//不缓存
		if (1 == $block_info ['refresh_mode']) {
			//$this->page_cache_time = 3600;
			$this->set_page_cache_time ( 
				3600 * 24 * intval ( $block_info ['cache_day'] ) + 3600 * intval ( $block_info ['cache_hour'] ) + 60 * intval ( 
					$block_info ['cache_minute'] ) + intval ( $block_info ['cache_second'] ) );
		}
		
		//======组合使用=====start=============
		$sql = sprintf ( "SELECT * FROM cms_datasource WHERE block_id='%s' AND ds_combine_used='1'", 
			$block_id );
		$ds_arr = $this->database_default->get_rows_by_sql ( $sql );
		if (1 == count ( $ds_arr )) {
			$ds = $ds_arr [0];
		}
		if (count ( $ds_arr ) > 1) {
			foreach ( $ds_arr as $v ) {
				$time_now = time ();
				$time_start = $time_now;
				$time_end = $time_now;
				if ($v ['time_start']) {
					$time_start = strtotime ( $v ['time_start'] );
				}
				if ($v ['time_end']) {
					$time_end = strtotime ( $v ['time_end'] );
				}
				if ($time_start < $time_now) {
					if ($time_now < $time_end) {
						//找到了当前时段有效的ds,则跳出循环
						$ds = $v;
						break;
					}
				}
			}
			if (! $ds) {
				$ds = $ds_arr [0];
			}
		}
		if ($ds) {
			if ('form' == $ds ['ds_type']) {
				//$rows = $this->database_default->get_rows_by_field ( "cms_form_record", "ds_id", $ds ['ds_id'] );
				$rows = $this->database_default->get_rows_by_sql ( 
					sprintf ( 
						"SELECT * FROM cms_form_record WHERE ds_id='%s'  ORDER BY order_main ASC,order_num ASC", 
						$ds ['ds_id'] ) );
				if ($rows) {
					
					$block_tpl_content = $block_tpl_info ['tpl_content'];
					$block_tpl_content = html_entity_decode ( $block_tpl_content, ENT_QUOTES, 'UTF-8' );
					$block_tpl_content = str_replace ( '&nbsp;', ' ', $block_tpl_content );
					$index = 0;
					$record_all = array ();
					foreach ( $rows as $row ) {
						$index += 1;
						for($i = 1; $i <= 45; $i ++) {
							$value = trim ( $row ["field_$i"] );
							if (0 === strpos ( $value, 'public/resource' )) {
								$value = ($this->config->item ( 'cms_site_url' ) . base_url () . $value);
								if ($this->input->get ( 'editable' )) {
									$value = ($value . "?t=" . time ());
								}
							} else {
								$base_url = $this->config->item ( 'base_url' );
								if (preg_match ( "#\"$base_url/public/resource(.*)(png|jpg|gif|bmp|jpeg)#", 
									$value )) {
									$value = str_replace ( "$base_url/public/resource", 
										$this->config->item ( 'cms_site_url' ) . base_url () . "public/resource", 
										$value );
								}
							}
							$this->smarty->assign ( sprintf ( 'combine%s_key%s', $index, $i ), $value );
							$record_one ["key$i"] = $value;
						}
						$record_all [] = $record_one;
					}
					if (count ( $record_all )) {
						$this->smarty->assign ( 'combine_all', $record_all );
						//my_debug ( $record_all );
						$record_all = null;
					}
				}
			}
		}
		//======组合使用=====end=============
		

		$ds = null;
		$ds_arr = null;
		//block_datasource_mode==1,意思是"支持轮播"
		if ('1' == $block_info ['block_datasource_mode']) {
			//ds_enable==1,意思是支持轮播情况下的启用的ds
			$sql = sprintf ( "SELECT * FROM cms_datasource WHERE block_id='%s' AND ds_enable='1'", 
				$block_id );
			$ds_arr = $this->database_default->get_rows_by_sql ( $sql );
			if (! $ds_arr) {
				if (DEBUGMODE) {
					my_debug ( "请检查数据源设置id:$block_id" );
				}
				return;
			}
			foreach ( $ds_arr as $v ) {
				$time_now = time ();
				$time_start = $time_now;
				$time_end = $time_now;
				if ($v ['time_start']) {
					$time_start = strtotime ( $v ['time_start'] );
				}
				if ($v ['time_end']) {
					$time_end = strtotime ( $v ['time_end'] );
				}
				if ($time_start < $time_now) {
					if ($time_now < $time_end) {
						//找到了当前时段有效的ds,则跳出循环
						$ds = $v;
						break;
					}
				}
			}
			if (! $ds) {
				$ds = $ds_arr [0];
			}
			if (! $ds) {
				if (DEBUGMODE) {
					my_debug ( "请检查数据源设置(支持轮播,但当前时段没有可用的源)id:$block_id" );
				}
			}
		} else {
			//不支持轮播
			//ds_single_used指明了选定的ds
			$sql = sprintf ( 
				"SELECT * FROM cms_datasource WHERE block_id='%s' ORDER BY ds_single_used DESC", $block_id );
			$ds = $this->database_default->get_record_by_sql ( $sql );
			if (! $ds) {
				if (DEBUGMODE) {
					my_debug ( "请检查数据源设置(不支持轮播情况下)id:$block_id" );
				}
			}
			//my_debug ( $ds );
		}
		
		//到此,$ds就是不前时段应该使用的ds(数据源)
		

		//html类型的ds
		if ('html' == $ds ['ds_type']) {
			$ds ['ds_html'] = str_replace ( '&nbsp;', ' ', $ds ['ds_html'] );
			$ret = $this->smarty->fetch ( "string: " . $ds ['ds_html'] );
			//<img alt="" src="/content/public/resource/user_2/image/20120416092406.jpg">
			//这类地址要转换成"有域名的URL"
			$ret = str_replace ( "\"/content/resource", 
				"\"" . $this->config->item ( 'cms_site_url' ) . "/content/resource", $ret );
			$this->save_block_content ( $block_id, $ret );
			return $ret;
		}
		
		//form类型的ds
		if ('form' == $ds ['ds_type']) {
			//$rows = $this->database_default->get_rows_by_field ( "cms_form_record", "ds_id", $ds ['ds_id'] );
			$rows = $this->database_default->get_rows_by_sql ( 
				sprintf ( "SELECT * FROM cms_form_record WHERE ds_id='%s'  ORDER BY order_main ASC, order_num ASC", 
					$ds ['ds_id'] ) );
			if ($rows) {
				
				$block_tpl_content = $block_tpl_info ['tpl_content'];
				$block_tpl_content = html_entity_decode ( $block_tpl_content, ENT_QUOTES, 'UTF-8' );
				$block_tpl_content = str_replace ( '&nbsp;', ' ', $block_tpl_content );
				
				$index = 0;
				$record_all = array ();
				foreach ( $rows as $row ) {
					$index += 1;
					$record_one = array ();
					for($i = 1; $i <= 45; $i ++) {
						$value = trim ( $row ["field_$i"] );
						if (0 === strpos ( $value, 'public/resource' )) {
							$value = ($this->config->item ( 'cms_site_url' ) . base_url () . $value);
							if ($this->input->get ( 'editable' )) {
								$value = ($value . "?t=" . time ());
							}
						} else {
							$base_url = $this->config->item ( 'base_url' );
							if (preg_match ( "#\"$base_url/public/resource(.*)(png|jpg|gif|bmp|jpeg)#", $value )) {
								$value = str_replace ( "$base_url/public/resource", 
									$this->config->item ( 'cms_site_url' ) . base_url () . "public/resource", 
									$value );
								//my_debug("$base_url/public/resource");
							//my_debug($this->config->item ( 'cms_site_url' ) . base_url () . "public/resource");
							}
						}
						$this->smarty->assign ( sprintf ( 'record%s_key%s', $index, $i ), $value );
						$record_one ["key$i"] = $value;
					}
					$record_all [] = $record_one;
				}
				if (count ( $record_all )) {
					$this->smarty->assign ( 'record_all', $record_all );
					//my_debug ( $record_all );
				}
				//$block_tpl_content = str_replace ( '&nbsp;', ' ', $block_tpl_content );
				$block_tpl_content = $this->smarty->fetch ( "string: $block_tpl_content" );
				$this->save_block_content ( $block_id, $block_tpl_content );
				return $block_tpl_content;
			}
			return null;
		}
		
		//sql类型的ds
		if ('sql' == $ds ['ds_type']) {
			//my_debug ( $this->multi_database );
			$database_name = $ds ['ds_sql_database'];
			$database = null;
			if (array_key_exists ( $database_name, $this->multi_database )) {
				$database = &$this->multi_database [$database_name];
			} else {
				$this->multi_database [$database_name] = $this->load->database ( $database_name, true );
				$database = & $this->multi_database [$database_name];
			}
			
			$block_tpl_content = $block_tpl_info ['tpl_content'];
			$block_tpl_content = html_entity_decode ( $block_tpl_content, ENT_QUOTES, 'UTF-8' );
			
			$manual_record_arr = array ();
			if (trim ( $ds ['ds_sql_manual_record'] )) {
				$manual_record_arr = explode ( ',', trim ( $ds ['ds_sql_manual_record'] ) );
			}
			
			$rows = $database->get_rows_by_sql ( $ds ['ds_sql_string'] );
			if ($rows) {
				if (count ( $manual_record_arr ) > 0) {
					//手动选择的数据,需要按保持顺序输出
					$rows_index = array ();
					$k = $ds ['ds_sql_primary_key'];
					foreach ( $rows as $row ) {
						$rows_index [$row [$k]] = $row;
					}
				}
				//字段映射
				$fields_arr = json_decode ( $ds ['ds_field_config'] );
				//my_debug ( $fields_arr );
				

				$record_all = array ();
				if (count ( $manual_record_arr ) > 0) {
					$index = 0;
					foreach ( $manual_record_arr as $kk ) {
						$index += 1;
						$row = $rows_index [$kk];
						$record_one = array ();
						foreach ( $fields_arr as $field => $field_k ) {
							$record_one ["key$field_k"] = $row [$field];
							$this->smarty->assign ( sprintf ( 'record%s_key%s', $index, $field_k ), $row [$field] );
						}
						$record_all [] = $record_one;
					}
				} else {
					$index = 0;
					foreach ( $rows as $row ) {
						$index += 1;
						$record_one = array ();
						foreach ( $fields_arr as $field => $field_k ) {
							$record_one ["key$field_k"] = $row [$field];
							$this->smarty->assign ( sprintf ( 'record%s_key%s', $index, $field_k ), $row [$field] );
						}
						$record_all [] = $record_one;
					}
				}
				if (count ( $record_all )) {
					$this->smarty->assign ( 'record_all', $record_all );
				}
				
				//echo $block_tpl_content;
				try {
					//my_debug ();
					$block_tpl_content = str_replace ( '&nbsp;', ' ', $block_tpl_content );
					$block_tpl_content = $this->smarty->fetch ( "string: $block_tpl_content" );
				} catch ( Exception $e ) {
					if (DEBUGMODE) {
						my_debug ( $e->getMessage (), 'Smarty Exception', '', '' );
					}
				}
				$this->save_block_content ( $block_id, $block_tpl_content );
				return ($block_tpl_content);
			} else {
				return null;
			}
		}
		
		//api类型的数据源
		if ('api' == $ds ['ds_type']) {
			$api_name = $ds ['ds_api_name'];
			$api_uniq = ("api" . uniqid ());
			$this->load->model ( $api_name, $api_uniq );
			$api = &$this->$api_uniq;
			if ($api) {
				$block_tpl_content = $block_tpl_info ['tpl_content'];
				$block_tpl_content = html_entity_decode ( $block_tpl_content, ENT_QUOTES, 'UTF-8' );
				$rows = $api->get_data ( $ds ['ds_id'] );
				$this->$api_uniq = null;
				if ($rows) {
					
					//字段映射
					//$fields_arr = json_decode ( $ds ['ds_api_config'] );
					//my_debug ( $fields_arr );
					

					$record_all = array ();
					$index = 0;
					//my_debug($rows);
					foreach ( $rows as $row ) {
						$fields_arr = array_keys ( $row );
						$fields_arr = array_flip ( $fields_arr );
						
						$index += 1;
						$record_one = array ();
						foreach ( $fields_arr as $field => $field_index ) {
							$field_k = $field_index + 1;
							$record_one ["key$field_k"] = $row [$field];
							$this->smarty->assign ( sprintf ( 'record%s_key%s', $index, $field_k ), $row [$field] );
							//my_debug(sprintf ( 'record%s_key%s', $index, $field_k ));
						}
						$record_all [] = $record_one;
					}
					if (count ( $record_all )) {
						$this->smarty->assign ( 'record_all', $record_all );
					}
					
					//echo $block_tpl_content;
					try {
						//my_debug ();
						$block_tpl_content = str_replace ( '&nbsp;', ' ', 
							$block_tpl_content );
						$block_tpl_content = $this->smarty->fetch ( "string: $block_tpl_content" );
					} catch ( Exception $e ) {
						if (DEBUGMODE) {
							my_debug ( $e->getMessage (), 'Smarty Exception', '', '' );
						}
					}
					$this->save_block_content ( $block_id, $block_tpl_content );
					return ($block_tpl_content);
				} else {
					return null;
				}
			}
		}
	}
	private function filter_url($city, $str) {
		if ("other" == $city) {
			return $str;
		}
		$str = str_replace ( "mall.jia.com/list", "mall.jia.com/$city/list", $str );
		$str = preg_replace ( '#jiaju.jia.com/list/(\d+)#', "jiaju.jia.com/$city/list/\${1}", $str );
		$str = str_replace ( "zhuangxiu.jia.com/company_list", "zhuangxiu.jia.com/$city/company_list", $str );
		return $str;
	}
	private function save_block_content($block_id, $block_content) {
		$this->db->reconnect ();
		$this->db->where ( 'block_id', $block_id );
		$this->db->update ( 'cms_block', array ('block_content' => $block_content ) );
	}
	function smart() {
		
		$this->smarty->assign ( 'time', microtime () );
		try {
			echo ($this->smarty->fetch ( 
				'string: 模板内容:
{{seo_api 
	seo_url="http://10.10.10.29:8080/kserver/tkd/"
	seedword="装修,齐家网"
	creative="/mall/product"
	full_url="http://mall.jia.com"
}}				
				' ));
		} catch ( Exception $e ) {
			if (DEBUGMODE) {
				my_debug ( $e->getMessage (), 'Smarty Exception', '', '' );
			}
		}
		
	//my_debug ( get_class ( $this->smarty ) );
	}
	function __destruct() {
		foreach ( $this->multi_database as $db ) {
			//my_debug(get_class_methods(get_class($db)));
			$db->close ();
		}
	}
}
//end.
