<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class AddSyncNode extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->database_online = &$this->load->database ( 'online', true );
		$this->load->library ( 'datagrid' );
		$this->load->library ( 'form_validation' );
	}
	function index() {
		//权限检查-begin.
		$success = validation_check ( $this->uid, "addsyncnode" );
		if ($success != 1) {
			msg ( "无权限：(addsyncnode)", "", "message" );
			safe_exit ();
		}
		//权限检查-end.
		$domain = trim ( $this->input->post ( 'domain' ) );
		$ip = trim ( $this->input->post ( 'ip' ) );
		if ($this->input->post ( 'submitform' )) {
			$this->add_node ( $domain, $ip );
			echo "操作已执行";
		}
		$this->load->view ( 'syncnode/add_view' );
	}
	private function add_node($a_site, $todo_ip) {
		//write_log ( __FILE__, "fuck" );
		$sql = "select auto_id,b.page_id,page_site,page_url from cms_sync_log a 
					left join  cms_page b
					on a.page_id=b.page_id 
				where 
					page_site='$a_site'
				";
		$rows = $this->database_online->get_rows_by_sql ( $sql );
		//my_debug ( $rows );
		if ($rows) {
			//$this->datagrid->reset ();
			//echo $this->datagrid->build ( 'datagrid', $rows, TRUE );
			

			$page_id_arr = array ();
			foreach ( $rows as $row ) {
				$page_id_arr [] = $row ['page_id'];
			}
			$page_id_arr = array_unique ( $page_id_arr );
			//my_debug ( $page_id_arr );
			$this->database_online->query ( "SET AUTOCOMMIT=0" );
			$this->database_online->query ( "BEGIN" );
			$this->database_online->where ( 'client_ip', $todo_ip );
			$this->database_online->delete ( "cms_sync_log" );
			foreach ( $page_id_arr as $k => $v ) {
				$this->database_online->insert ( 'cms_sync_log', 
					array ('page_id' => $v, 'client_ip' => $todo_ip, 'page_sync_time' => 0 ) );
				//write_log ( __FILE__, $this->database_publish->last_query () );
				echo $this->database_online->last_query (), "\n<br>\n";
			}
			$this->database_online->query ( "COMMIT" );
		}
	}
}

//end.
