<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Notice extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'notice' );
		if (! $success) {
			msg ( "无公告管理权限: key = notice", "", "message" );
			exit ();
		}
	}
	
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========app选择下拉框{{===========================================
		$app_select = array ();
		$sql = "SELECT*FROM data_app WHERE length(app_name)>0";
		$rows = $this->db->get_rows_by_sql ( $sql );
		$select_app [0] = '-选择应用方案-';
		foreach ( $rows as $row ) {
			$select_app [$row ['app_id']] = $row ['app_name'];
		}
		$view_data ['select_app'] = $select_app;
		//==========end}}====================================================
		

		//========站点选择下拉框{{============================================
		$select_city = array ();
		$select_city [0] = '-选择公告站点-';
		$this->load->config ( 'area_flag' );
		$city = $this->config->item ( 'area_flag' );
		foreach ( $city as $k => $v ) {
			$select_city [$k] = $v;
		}
		$view_data ['select_city'] = $select_city;
		//==========end}}====================================================		
		

		//=========列表===={{================================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		$page_num < 1 ? $page_num = 1 : $page_num = $page_num;
		
		$sql_where = "WHERE length(notice_title) > 0";
		if ($this->input->post ( 'select_app' )) {
			$sql_where = $sql_where . sprintf ( " AND app_id='%s' ", 
				trim ( $this->input->post ( 'select_app' ) ) );
		}
		if ($this->input->post ( 'select_city' ) && $this->input->post ( 'select_city' ) !== 0) {
			$sql_where = $sql_where . sprintf ( " AND city_id='%s' ", 
				trim ( $this->input->post ( 'select_city' ) ) );
		}
		if ($this->input->post ( 'find_notice' )) {
			$sql_where = $sql_where . sprintf ( " AND notice_title like '%s%s%s' ", '%', 
				strip_tags ( $this->input->post ( 'find_notice' ) ), '%' );
		}
		
		$sql_count = "SELECT count(*) as tot FROM data_notice $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM data_notice $sql_where ORDER BY notice_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['id'] = $row ['notice_id'];
				if ($row ['app_id'] > 0) {
					if (isset ( $select_app [$row ['app_id']] )) {
						
						$data [$k] ['应用方案'] = $select_app [$row ['app_id']];
					} else {
						$data [$k] ['应用方案'] = '';
					}
				} else {
					$data [$k] ['应用方案'] = '';
				}
				if ($row ['city_id']) {
					if (isset ( $select_city [$row ['city_id']] )) {
						
						$data [$k] ['发布站点'] = $select_city [$row ['city_id']];
					} else {
						$data [$k] ['发布站点'] = '';
					}
				} else {
					$data [$k] ['发布站点'] = '';
				}
				$data [$k] ['公告标题'] = $row ['notice_title'];
				$tmp = date ( "Y-m-d", $row ['create_time'] );
				$tmp .= "<font color=\"red\">";
				$tmp .= date ( " H:i:s", $row ['create_time'] );
				$tmp .= "</font>";
				$data [$k] ['创建时间'] = $tmp;
				$data [$k] ['操作'] = "<a href = \"javascript:void(0)\" onclick=\"edit_notice('{$row['notice_id']}');return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = blue>&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript:void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_notice('{$row['notice_id']}');return false;\">刪除</a>";
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'notice/notice_view', $view_data );
	}
	function edit_notice() {
		
		$view_data = array ();
		$view_data ['record'] = '';
		$view_data ['message'] = '';
		
		$notice_id = $this->input->get ( 'id' );
		if ($notice_id) {
			$notice_id = $this->input->get ( 'id' );
		} else {
			$db_ret = $this->db->insert ( 'data_notice', 
				array ('notice_title' => '', 'create_time' => time () ) );
			if ($db_ret) {
				$notice_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('id' => $notice_id ) ) );
			}
		}
		
		//=========清理数据库中无效数据========================================================
		$time_limit = time () - 60 * 60 * 2;
		$this->db->query ( 
			sprintf ( "DELETE FROM data_notice WHERE ( notice_title = '') AND create_time<%s", 
				$time_limit ) );
		//==========end}}=====================================================================
		

		//=========app选择下拉框{{===========================================
		$app_select = array ();
		$sql = "SELECT*FROM data_app WHERE length(app_name)>0";
		$rows = $this->db->get_rows_by_sql ( $sql );
		$select_app [0] = '-请选择-';
		foreach ( $rows as $row ) {
			$select_app [$row ['app_id']] = $row ['app_name'];
		}
		$view_data ['select_app'] = $select_app;
		//==========end}}====================================================
		

		//========站点选择下拉框{{============================================
		$select_city = array ();
		$select_city [0] = '-选择公告站点-';
		$this->load->config ( 'area_flag' );
		$city = $this->config->item ( 'area_flag' );
		foreach ( $city as $k => $v ) {
			$select_city [$k] = $v;
		}
		$view_data ['select_city'] = $select_city;
		//==========end}}====================================================
		

		$view_data ['record'] = $this->db->get_record_by_field ( 'data_notice', 'notice_id', 
			$notice_id );
		$view_data ['record_time'] = date('Y-m-d H:i:s',$view_data ['record']['create_time']);
		//=============加载编辑器{{=============================================
		$this->load->library ( 'editors' );
		$eddt = array (
				'id' => 'notice_content', 
				'value' => $view_data ['record'] ['notice_content'], 
				'width' => '600px', 
				'height' => '200px' );
		$view_data ['notice_content'] = $this->editors->getedit ( $eddt );
		//====================end}}=============================================
		$notice_title = strip_tags ( trim ( $this->input->post ( 'notice_title' ) ) );
		$notice_content = trim ( $this->input->post ( 'notice_content' ) );
		$app_id = intval ( $this->input->post ( 'app_id' ) );
		$city_id = trim ( $this->input->post ( 'city_id' ) );
		$notice_link = trim ( $this->input->post ( 'notice_link' ) );
		$create_time = $this->input->post('create_time');
		if($create_time){
			$create_time = strtotime($create_time);
		}else{
			$create_time = time();
		}
		
		$this->form_validation->set_rules ( 'notice_title', '公告标题', 'required' );
		$this->form_validation->set_rules ( 'app_id', '应用方案', 'required' );
		$this->form_validation->set_rules ( 'city_id', '应用站点', 'required' );
		
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () === true) {
				$this->db->where ( 'notice_id', $notice_id );
				$success = $this->db->update ( 'data_notice', 
					array (
							'notice_title' => $notice_title, 
							'notice_content' => $notice_content, 
							'app_id' => $app_id, 
							'city_id' => $city_id, 
							'notice_link' => $notice_link, 
							'create_time' => $create_time ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'notice/notice_edit_view', $view_data );
	}
	/*function city_search() {
		$this->load->helper ( 'security' );
		$this->load->config ( 'area_flag' );
		$city = $this->config->item ( 'area_flag' );
		$q = $this->input->post ( 'q' );
		$q = trim ( $q );
		$q = xss_clean ( $q );
		$q = trim($this->input->get ( 'q' ));
		
		foreach($city as $key=>$val){
			
		}
		
		//my_debug($rets);
		
		echo json_encode ( $ret );
		return;
	}*/
	function del_notice() {
		$notice_id = intval ( $this->input->get ( 'id' ) );
		$page_num = intval ( $this->input->post ( 'page_num' ) );
		
		$this->db->where ( 'notice_id', $notice_id );
		$success = $this->db->delete ( 'data_notice' );
		if ($success) {
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			msg ( "刪除失敗", modify_build_url ( array ('m' => 'index', 'page_num' => $page_num ) ) );
		}
	}
}
