<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class ReadmeList extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "ReadmeList" );
		if ($success != 1) {
			msg ( "无权限：Readme列表(ReadmeList)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE 1";
		/*
		if ($this->input->post ( 'page_column_id' )) {
			$sql_where = sprintf ( "$sql_where AND page_column_id='%s' ", 
				intval ( $this->input->post ( 'page_column_id' ) ) );
		}*/
		if ($this->input->post ( 'readme_content' )) {
			$sql_where = sprintf ( "$sql_where AND readme_content like '%s%s%s' ", '%', 
				$this->input->post ( 'readme_content' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM cms_readme $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_readme $sql_where ORDER BY readme_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "readme_id,create_time,readme_content,author_name" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['create_time'] = date ( "Y-m-d, H:i", $row ['create_time'] );
				$data [$k] ['edit'] = sprintf ( "<a href='%s' target=\"_blank\"  >编辑</a>", 
					modify_build_url ( 
						array ('c' => "editreadme", 'id' => $row ['readme_id'] ) ) );
				//这里可以对row做必要的转换,比如将时间戳格式为文本
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'readmelist_view', $view_data );
	}
	function master_delete() {
		$record_id = $this->input->get ( "id" );
		$record_id = intval ( $record_id );
		$this->db->where ( 'readme_id', $record_id );
		$this->db->delete ( 'cms_readme' );
		return;
	}
}

//end.
