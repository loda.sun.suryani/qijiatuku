<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class EntryList extends My_Controller {
	function __construct() {
		parent::__construct ();
	}
	public function index() {
		$view_data = array ();
		$this->load->view ( 'entrylist_view', $view_data );
	}
}
