<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class BlockList extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		
		//权限检查,key=blocklist
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "blocklist" );
		if ($success != 1) {
			msg ( "无权限：碎片列表(blocklist)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$view_data = array ();
		
		//$view_data ['select_pagetpl_options'] = '';
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========碎片列表===={{=================
		$page_size = 15;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE length(block_name)>0";
		if ($this->input->post ( 'block_name' )) {
			$sql_where = sprintf ( "$sql_where AND block_name like '%s%s%s' ", '%', 
				$this->input->post ( 'block_name' ), '%' );
		}
		if ($this->input->post ( 'block_id' )) {
			$sql_where = sprintf ( "$sql_where AND block_id = '%s' ", 
				intval ( $this->input->post ( 'block_id' ) ) );
		}
		$sql = '';
		$block_tags = '';
		if ($this->input->post ( 'block_tags' )) {
			$block_tags = trim ( $this->input->post ( 'block_tags' ) );
			$tags_array = explode ( ',', $block_tags );
			
			if (count ( $tags_array )) {
				foreach ( $tags_array as $v ) {
					if ($v) {
						$sql .= sprintf ( " AND block_tags like '%s%s%s' ", '%', $v, '%' );
					}
				
				}
			
			}
			//my_debug ( $sql );
			$sql_where = "$sql_where $sql";
		}
		//my_debug ( $sql_where );
		

		//=============================标签器begin{{==================================
		if ($block_tags) {
			$blocktags_array = explode ( ",", str_replace ( "c", "", $block_tags ) );
		} else {
			$blocktags_array = '';
		}
		
		//my_debug($blocktags_array);
		$this->load->model ( 'cmstags_model' );
		$data = null;
		$data .= $this->cmstags_model->build_tag_select ( 'tagselector', null, $blocktags_array, 
			'block_tags' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;} 
		.tag_dimen{color:blue;}
		#tagselector {border:1px solid #f0f0f0;padding:6px;}
		#div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
		#span{border:1px solid #909090;margin:2px;padding:1px;}
		</style>
		";
		
		$view_data ['block_tags'] = $data;
		//my_debug($data);
		//=============================标签器 }}end===================================
		

		$sql_count = "SELECT count(*) as tot FROM cms_block $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT block_id as id,block_name as `标题` FROM cms_block $sql_where ORDER BY block_id DESC ";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$data [$k] ['分配权限'] = form_button ( 'authority_' . $row ['id'], '分配权限', 
					"onclick=\"authority({$row['id']});return false;\" " );
				$data [$k] ['查看权限'] = form_button ( 'showpermsion_' . $row ['id'], '查看权限', 
					"onclick=\"showpermsion_({$row['id']});return false;\" " );
				$data [$k] ['copy'] = "<a href = \"javascript:void(0)\" onclick=\"copy_block('{$row['id']}');return false;\">复制</a>";
				$data [$k] ['edit'] = sprintf ( "<a href='%s' target=\"_blank\">编辑</a>", 
					site_url ( "c=createblock&block_id=" . $row ['id'] ) );
				$data [$k] ['view'] = sprintf ( "<a href='%s' target='_blank'>预览</a>", 
					site_url ( "c=block&block_id=" . $row ['id'] ) );
				$data [$k] ['fav'] = sprintf ( "<a href='%s' >收藏</a>", 
					modify_build_url ( 
						array ('m' => 'add_my_block', "block_id" => $row ['id'] ) ) );
				$data [$k] ['delete'] = form_button ( 'use_block_' . $k, '删除', 
					"onclick=\"if(!confirm('确定要删除?')){return false;}block_delete({$row['id']});return false;\" " );
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========碎片列表====}}=================
		

		$this->load->view ( 'blocklist_view', $view_data );
	}
	function add_my_block() {
		$block_id = $this->input->get ( "block_id" );
		$block_id = intval ( $block_id );
		$user_id = $this->uid;
		$this->db->where ( "block_id", $block_id );
		$this->db->where ( "user_id", $user_id );
		$this->db->delete ( "cms_my_block" );
		$this->db->insert ( "cms_my_block", 
			array ('block_id' => $block_id, 'user_id' => $user_id, 'create_time' => time () ) );
		redirect ( modify_build_url ( array ('c' => 'favblocklist' ), site_url () ) );
	}
	function block_delete() {
		$block_id = $this->input->get ( "block_id" );
		$block_id = intval ( $block_id );
		$this->db->where ( 'block_id', $block_id );
		$this->db->delete ( 'cms_block' );
		$del_sql = "delete t1 from cms_form_record as t1 
					left join cms_datasource as t2 on t2.ds_id=t1.ds_id  
					where block_id =" . $block_id;
		$this->db->query ( $del_sql );
		$this->db->where ( 'block_id', $block_id );
		$this->db->delete ( 'cms_datasource' );
		return;
	}
	function authority() {
		$block_id = $this->input->get ( "block_id" );
		$block_id = intval ( $block_id );
		if (! $block_id) {
			exit ( '参数错误' );
			return;
		}
		$page_perm_key = "edit_block_$block_id";
		$record = $this->db->get_record_by_field ( 'cms_permission', 'permission_key', 
			$page_perm_key );
		if (! $record) {
			//不存在 ,则插入一条
			$permission_name = "编辑碎片(key=edit_block_$block_id)";
			$db_ret = $this->db->insert ( "cms_permission", 
				array (
						'permission_key' => $page_perm_key, 
						'permission_name' => $permission_name, 
						'is_temp' => 0 ) );
			if ($db_ret) {
				$permission_id = $this->db->insert_id ();
			}
		} else {
			$permission_id = $record ['permission_id'];
		}
		
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		//return;
		redirect ( 
			modify_build_url ( 
				array (
						'c' => 'editpermission', 
						'm' => 'accredit', 
						'permission_id' => $permission_id ) ) );
	}
	function show_perment() {
		$block_id = intval ( $this->input->get ( 'block_id' ) );
		if(!$block_id){
			my_debug("block_id为空");
			exit;
		}
		
		$page_perm_key='edit_block_$block_id';
		//当碎片没插入时候
		$page_perm_key = "edit_block_$block_id";
		$record = $this->db->get_record_by_field ( 'cms_permission', 'permission_key', 
			$page_perm_key );
		if (! $record) {
			//不存在 ,则插入一条
			$permission_name = "编辑碎片(key=edit_block_$block_id)";
			$db_ret = $this->db->insert ( "cms_permission", 
				array (
						'permission_key' => $page_perm_key, 
						'permission_name' => $permission_name, 
						'is_temp' => 0 ) );
			if ($db_ret) {
				$permission_id = $this->db->insert_id ();
			}
		} else {
			$permission_id = $record ['permission_id'];
		}

		$sql = "SELECT permission_key FROM cms_permission WHERE permission_id='$permission_id'  ";
		$query = $this->db->query ( $sql );
		$key = $query->row_array ();
		$permission_id_k = null;
		
		//只对ID大于13100的新碎片检查权限(历史问题暂留)
		//my_debug ( $permission_id );
		//找直接权限绑定表
		$sql = "SELECT user_id FROM cms_user_to_permission WHERE permission_id IN($permission_id)  ";
		$query = $this->db->query ( $sql );
		$no_role = $query->result_array ();
		$user_id_array = array ();
		if (count ( $no_role )) {
			foreach ( $no_role as $vlk ) {
				$user_id_array [] = $vlk ["user_id"];
			}
		}
		//找是否已经绑定角色
		$sql = "SELECT role_id FROM cms_role_to_permission WHERE permission_id IN($permission_id)  ";
		$query = $this->db->query ( $sql );
		$no_role = $query->result_array ();
		$no_role_l = array ();
		if (count ( $no_role )) {
			foreach ( $no_role as $vlk ) {
				$no_role_l [] = $vlk ["role_id"];
			}
		}
		$no_role_id = null;
		$user_id_array1 = array ();
		if (count ( $no_role_l )) {
			$no_role_l = array_filter ( $no_role_l );
			$no_role_id = join ( ",", $no_role_l );
			$no_role_id = " role_id IN($no_role_id) ";
			$sql = "SELECT user_id FROM cms_user_to_role WHERE  $no_role_id ";
			//my_debug ( $sql );
			$query = $this->db->query ( $sql );
			$no_role1 = $query->result_array ();
			if (count ( $no_role1 )) {
				foreach ( $no_role1 as $vlk ) {
					$user_id_array1 [] = $vlk ["user_id"];
				}
			}
		}
		$user_id_SQL = "";
		
		if (count ( $user_id_array ) && count ( $user_id_array1 )) {
			$user_id_array = array_merge ( $user_id_array, $user_id_array1 );
		}
		//my_debug ( $user_id_array );
		if (count ( $user_id_array )) {
			$user_id_array = array_filter ( $user_id_array );
			$user_id_array = array_unique ( $user_id_array );
			$user_id_array = join ( ",", $user_id_array );
		}
		if ($user_id_array) {
			$user_id_SQL = " AND user_id IN($user_id_array)";
		} else {
			my_debug ( "没有找到此权限的用户" );
			exit ();
		}
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE 1 $user_id_SQL";
		if ($this->input->post ( 'permission_name' )) {
			$permission_name = trim ( $this->input->post ( 'permission_name' ) );
			$sql_where = "$sql_where  AND permission_name like '%$permission_name%' ";
		}
		$sql_count = "SELECT count(*) as tot FROM cms_user $sql_where"; //取总数,用于分页
		//my_debug ( $sql_count );
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_user $sql_where ORDER BY user_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "user_id,user_name,user_email,user_nick" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$user_id = $row ["user_id"];
				$data [$k] ['操作日志'] = " <button id='userlog$permission_id' class='td_p' onclick='userlog($user_id);return false;'>查看权限用户</button>";
				$data [$k] ['取消此权限'] = "<button id='reset' class='td_p' onclick='reset_delete($user_id,$permission_id);return false;'>取消此权限</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'listuser_view', $view_data );
	
	}
}
