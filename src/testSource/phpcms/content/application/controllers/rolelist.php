<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class RoleList extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		$this->load->helper ( 'cookie' );
		$this->load->library ( 'session' );
		
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "rolelist" );
		if ($success != 1) {
			msg ( "无权限：角色列表(rolelist)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE is_temp<1";
		/*
		if ($this->input->post ( 'page_column_id' )) {
			$sql_where = sprintf ( "$sql_where AND page_column_id='%s' ", 
				intval ( $this->input->post ( 'page_column_id' ) ) );
		}*/
		if ($this->input->post ( 'role_name' )) {
			$sql_where = sprintf ( "$sql_where AND role_name like '%s%s%s' ", '%', 
				$this->input->post ( 'role_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM cms_role $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_role $sql_where ORDER BY role_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "role_id,role_key,role_name" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$role_id = $row ['role_id'];
				//这里可以对row做必要的转换,比如将时间戳格式为文本
				$data [$k] ['delete'] = sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array (
								'c' => "rolelist", 
								'm' => "master_delete", 
								'role_id' => $role_id, 
								'page_num' => $page_num ) ) );
				$data [$k] ['Edit'] = " <button id='block_edit_$role_id' class='td_p' onclick='block_edit($role_id);return false;'>编辑</button>";
				$data [$k] ['权限绑定'] = "<button id='pb_edit_$role_id' class=td_p onclick='addpermiADD($role_id);return false;'>权限绑定</button>";
				$data [$k] ['查看用户'] = "<button id='pb_edit_$role_id' class=td_p onclick='show_user($role_id);return false;'>角色用户</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'rolelist_view', $view_data );
	}
	function listuser() {
		$role_id = intval ( $this->input->get ( 'role_id' ) );
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$no_permission_id = "";
		if ($this->input->post ( 'user_name' )) {
			$user_name = trim ( $this->input->post ( 'user_name' ) );
			$sql_where = " user_name like '%$user_name%' ";
			$sql = "SELECT user_id FROM cms_user WHERE $sql_where";
			$query = $this->db->query ( $sql );
			$no_data = $query->result_array ();
			$no_array_l = array ();
			if (count ( $no_data )) {
				foreach ( $no_data as $vlk ) {
					$no_array_l [] = $vlk ["user_id"];
				}
			}
			
			if (count ( $no_array_l )) {
				$no_array_l = array_filter ( $no_array_l );
				$no_permission_id = join ( ",", $no_array_l );
			}
			if ($no_permission_id) {
				$no_permission_id = " AND  user_id  IN($no_permission_id)";
			}
		}
		//my_debug ( $no_permission_id );
		//=========列表=====================
		$page_size = 15;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$DATABASE = "cms_user_to_role";
		$AUTO_ID = "auto_id";
		$sql_where = "WHERE role_id='$role_id' AND is_temp<1 $no_permission_id";
		
		$sql_count = "SELECT count($AUTO_ID) as tot FROM $DATABASE $sql_where"; //取总数,用于分页
		//my_debug ( $sql_count );
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM $DATABASE $sql_where ORDER BY $AUTO_ID DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "none" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$user_id = $row ["user_id"];
				$role_id = $row ["role_id"];
				$sql_count = "SELECT * FROM cms_user WHERE user_id='$user_id'"; //取总数,用于分页
				$query = $this->db->query ( $sql_count );
				$user = $query->row_array ();
				$data [$k] ['user_id'] = $user ["user_id"];
				$data [$k] ['账户名'] = $user ["user_name"];
				$data [$k] ['匿名'] = $user ["user_nick"];
				$data [$k] ['邮箱'] = $user ["user_email"];
				
				$data [$k] ['取消用户权限'] = " <button id='userlog$role_id' class='td_p' onclick='userreset($user_id,$role_id);return false;'>取消用户权限</button>";
				//$data [$k] ['操作日志'] = " <button id='userlog$permission_id' class='td_p' onclick='userlog($user_id);return false;'>查看权限用户</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'rolelistuser_view', $view_data );
	
	}
	function userreset() { //取消操作的角色用户，是的这个用户没这个角色的权限
		$role_id = $this->input->get ( 'role_id' );
		$user_id = $this->input->get ( 'user_id' );
		
		/*权限操作日志----start*/
		//用户
		$query = $this->db->query ( "SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
		$user = $query->row_array ();
		//权限
		/*$query = $this->db->query ( 
			"SELECT permission_id,permission_name FROM cms_permission WHERE permission_id ='admin_del'" );
		$psion = $query->row_array ();*/
		//角色
		$query = $this->db->query ( "SELECT role_name FROM cms_role WHERE  role_id='$role_id'" );
		$rol = $query->row_array ();
		$this->cms_grant_log ( $user_id, $user ['user_name'], 0, "取消/".$rol["role_name"]."/角色", 0, 0 );
		/*权限操作日志----end*/
		
		$this->db->where ( "role_id", $role_id );
		$this->db->where ( "user_id", $user_id );
		$s = $this->db->delete ( "cms_user_to_role" );
		//msg ( '', modify_build_url ( array ("m" => "user_p", "user_id" => $user_id ) ) );
		//echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		//return;
		echo $s;
	
	}
	function master_delete() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "rolelist_master_delete" );
		if (! $success) {
			msg ( "无权限：删除角色/[$UID]/", "", "message" );
			exit ();
		}
		
		$record_id = $this->input->get ( "role_id" );
		$page_num = $this->input->get ( "page_num" ); //分页的页码
		$record_id = intval ( $record_id );
		$this->db->where ( 'role_id', $record_id );
		$success = $this->db->delete ( 'cms_role' );
		//删除cms_role_to_permission表中的role_id权限内容
		$this->db->where ( 'role_id', $record_id );
		$rs = $this->db->delete ( 'cms_role_to_permission' );
		if ($success) {
			msg ( "", 
				modify_build_url ( array ('c' => 'rolelist', 'm' => 'index', 'page_num' => $page_num ) ) );
		}
	}

}

//end.
