<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Diaoding_tags extends MY_Controller {
	
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'security' );
	}
	
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		//$success = validation_check($UID,'diaoding_tags');//权限key = diaoding_tags
		/*if ( !$success) {
			msg ("无权限:吊顶标签管理 key = diaoding_tags","","message");
			exit ();
		}*/
		
		$view_data = array ();
		$view_data ['main_grid'] = '';
		$view_data ['pages_nav'] = '';
		
		//===============List Begin=====
		$page_size = 10; //每页显示数据条数
		$total_num = 0; //总页数
		$page_num = $this->input->post ( 'page_num' ); //当前页
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = 'WHERE 1 = 1';
		if ($this->input->post ( 'dimension_name' )) { //搜索维度
			$dimension_name = trim ( $this->input->post ( 'dimension_name' ) );
			$sql_where = "$sql_where AND dimension_name LIKE '%$dimension_name%' ";
		}
		
		$sql_count = "SELECT  count(*) as tot from diaoding_tag_dimension $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		
		$page_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		//参数($page_size,$tatal_num,$page_num,$page_size,$type)
		$view_data ['pages_nav'] = $page_obj->show_pages ();
		
		$select_limit_start = ($page_num - 1) * $page_size;
		$sql = "SELECT*FROM diaoding_tag_dimension $sql_where ORDER BY dimension_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
					//使用键名比较计算数组的交集，只返回$row 两数组的交集
				}
				$dimension_id = $row ['dimension_id'];
				$data [$k] ['维度id'] = $dimension_id;
				$data [$k] ['维度名'] = $row ['dimension_name'];
				$data [$k] ['操作'] = "<a href='javascript(0);' onclick=\"taglist($dimension_id);return false;\"  )>管理标签</a>";
				$data [$k] ['操作'] .= "&nbsp;<font color = blue>|</font>&nbsp";
				$data [$k] ['操作'] .= "<a href='javascript(0);' onclick=\"authorized($dimension_id);return false;\"  )>编辑</a>";
				$data [$k] ['操作'] .= "&nbsp;<font color = blue>|</font>&nbsp";
				$data [$k] ['操作'] .= sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array (
								'm' => "cmsd_del", 
								'did' => $dimension_id, 
								'page_num' => $page_num ) ) );
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, true );
		}
		$this->load->view ( 'ddtags/cmsd_list_view', $view_data );
	}
	
	function cmsd_del() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ($UID,'cmsd_del' );
		if ( !$success ) {
			msg ("无权限:删除维度 key = cmsd_del","","message" );
			exit ();
		}*/
		$record_id = $this->input->get ( 'did' );
		$record_id = intval ( $record_id );
		$page_num = $this->input->get ( 'page_num' );
		
		$this->db->where ( 'dimension_id', $record_id );
		$success_d = $this->db->delete ( 'diaoding_tag_dimension' );
		$this->db->where ( 'tag_dimension_id', $record_id );
		$success_t = $this->db->delete ( 'news_tag' );
		if ($success_d & $success_t) {
			msg ( "", modify_build_url ( array ('m' => 'index', 'page_num' => $page_num ) ) );
		}
	}
	//增加 ,编辑维度
	function cmsd_edit() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID,'cmsd_edit' );
		if ( !$success ) {
			msg ( "无权限:操作维度 key = cmsd_edit","","message" );
			exit () ;
		}*/
		$record_id = $this->input->get ( 'did' );
		$record_id = intval ( $record_id );
		$view_data = array ();
		$view_data ['message'] = null;
		
		$this->form_validation->set_rules ( 'dimension_name', '维度名', 
			'callback_dimension_name|required' ); //验证表单
		$this->db->where ( 'dimension_id', $record_id );
		$record_content = $this->db->get_record_by_field ( 'diaoding_tag_dimension', 
			'dimension_id', $record_id );
		if ($record_content) {
			$this->defaults = $record_content;
			if ($this->input->post ( 'submitform' )) {
				if ($this->form_validation->run ()) {
					$data = array (
							'dimension_name' => strip_tags ( 
								$this->input->post ( 'dimension_name' ) ) );
					$this->db->where ( 'dimension_id', $record_id );
					$this->db->update ( 'diaoding_tag_dimension', $data );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		} else {
			if ($this->input->post ( 'submitform' )) {
				if ($this->form_validation->run ()) {
					$data = array (
							'dimension_name' => strip_tags ( 
								$this->input->post ( 'dimension_name' ) ) );
					$this->db->insert ( 'diaoding_tag_dimension', $data );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}
		$this->load->view ( 'ddtags/cmsd_edit_view', $view_data );
	}
	//子类列表
	function cmst_list() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID,'cmst_list');
		if ( !$success ) {
			msg ( "无权限: 查看子类列表 key = cmst_list","","message");
			exit () ;
		}*/
		$record_id = $this->input->get ( 'did' );
		$record_id = intval ( $record_id );
		
		$view_data = array ();
		$view_data ['main_grid'] = '';
		$view_data ['page_nav'] = null;
		
		//==================分页============
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = "WHERE tag_dimension_id = $record_id";
		if ($this->input->post ( 'tag_name' )) {
			$tag_name = trim ( $this->input->post ( 'tag_name' ) );
			$sql_where = "$sql_where AND tag_name LIKE '%$tag_name%'";
		}
		
		$sql_count = "SELECT count(*) as tot FROM diaoding_tag $sql_where ";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		
		$page_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		
		$view_data ['pages_nav'] = $page_obj->show_pages ();
		
		$select_limit_start = ($page_num - 1) * $page_size;
		$sql = "SELECT*FROM diaoding_tag $sql_where ORDER BY tag_id DESC ";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		$dimension_sql = "SELECT*FROM diaoding_tag_dimension ";
		$dimension_data = $this->db->get_rows_by_sql ( $dimension_sql );
		foreach ( $dimension_data as $k => $v ) {
			$ret [$v ['dimension_id']] = $v ['dimension_name'];
		}
		//my_debug($ret);
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$tag_id = $row ['tag_id'];
				$dimension_id = $row ['tag_dimension_id'];
				$data [$k] ['标签id'] = $tag_id;
				$data [$k] ['标签名'] = $row ['tag_name'];
				$data [$k] ['所属维度'] = $ret [$dimension_id];
				$data [$k] ['操作'] = "<a href='javascript(0);' onclick=\"authorized($tag_id,$record_id);return false;\"  )>编辑</a>";
				$data [$k] ['操作'] .= "&nbsp;<font color = blue>|</font>&nbsp";
				$data [$k] ['操作'] .= sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array (
								'm' => "cmst_del", 
								'tid' => $tag_id, 
								'did' => $dimension_id, 
								'page_num' => $page_num ) ) );
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'ddtags/cmst_list_view', $view_data );
	}
	function cmst_del() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID,'tag_del' );
		if ( !$success ) {
			msg ( "无权限:删除tag key = tag_del","","message" );
			exit () ;
		}*/
		$record_id = $this->input->get ( 'tid' );
		$record_id = intval ( $record_id );
		$dimension_id = $this->input->get ( 'did' );
		$dimension_id = intval ( $dimension_id );
		$page_num = $this->input->get ( 'page_num' );
		
		$this->db->where ( 'tag_id', $record_id );
		$success = $this->db->delete ( 'diaoding_tag' );
		if ($success) {
			msg ( "", 
				modify_build_url ( 
					array ('m' => 'cmst_list', 'page_num' => $page_num, 'did' => $dimension_id ) ) );
		}
	}
	
	function cmst_edit() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID,'tag_edit' );
		if ( !$success ) {
			msg ( "无权限:操作维度 key = tag_edit","","message" );
			exit () ;
		}*/
		$record_id = $this->input->get ( 'tid' );
		$record_id = intval ( $record_id );
		$dimension_id = $this->input->get ( 'did' );
		$dimension_id = intval ( $dimension_id );
		$view_data = array ();
		$view_data ['message'] = null;
		
		$this->form_validation->set_rules ( 'tag_name', 'tag名', 'callback_tag_name|required' ); //验证表单
		

		$this->db->where ( 'tag_id', $record_id );
		$record_content = $this->db->get_record_by_field ( 'diaoding_tag', 'tag_id', $record_id );
		if ($record_content) {
			$this->defaults = $record_content;
			if ($this->input->post ( 'submitform' )) {
				if ($this->form_validation->run ()) {
					$data = array (
							'tag_name' => strip_tags ( $this->input->post ( 'tag_name' ) ) );
					$this->db->where ( 'tag_id', $record_id );
					$this->db->update ( 'diaoding_tag', $data );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		} else {
			if ($this->input->post ( 'submitform' )) {
				if ($this->form_validation->run ()) {
					$data = array (
							'tag_name' => strip_tags ( $this->input->post ( 'tag_name' ) ), 
							'tag_dimension_id' => $dimension_id );
					$this->db->insert ( 'diaoding_tag', $data );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}
		$this->load->view ( 'ddtags/cmst_edit_view', $view_data );
	}
	
	//表单验证规则
	function dimension_name($dimension_name) {
		$dimension_name = strip_tags ( $dimension_name );
		$dimension_name = trim ( $dimension_name );
		$count = $this->db->get_record_by_sql ( 
			"SELECT count(dimension_id) as t_count FROM diaoding_tag_dimension WHERE dimension_name ='$dimension_name' " );
		if ($count ['t_count']) {
			$this->form_validation->set_message ( 'dimension_name', 
				'维度名称：[<font color=blue>' . $dimension_name . ']</font>,已经使用' );
			return false;
		}
		return true;
	}
	
	function tag_name($tag_name) {
		$tag_name = strip_tags ( $tag_name );
		$tag_name = trim ( $tag_name );
		$tag_dimension_id = $this->input->get ( 'did' );
		$count = $this->db->get_record_by_sql ( 
			"SELECT count(tag_id) as t_count FROM diaoding_tag WHERE tag_name ='$tag_name'AND tag_dimension_id = '$tag_dimension_id' " );
		if ($count ['t_count']) {
			$this->form_validation->set_message ( 'tag_name', 
				'标签名称：[<font color=blue>' . $tag_name . ']</font>,已经使用' );
			return false;
		}
		return true;
	}
}
