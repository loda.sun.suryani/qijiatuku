<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class MallHotInfo extends CI_Controller {
	public $page_url;
	public $tpl_vars;
	function __construct() {
		parent::__construct ();
		$this->multi_database = array ();
		if (! defined ( 'FRONTPAGE' )) {
			$this->multi_database ['default'] = $this->load->database ( 'default', true );
			$this->database_default = &$this->multi_database ['default'];
		}
	}
	
	function index() {
		error_reporting ( E_ALL ^ E_NOTICE );
		$rt = $this->process_block ( 3495 );
		if (is_array ( $rt )) {
			echo json_encode ( array ("result" => $rt [0] ) );
		} else {
			echo json_encode ( array ("result" => null ) );
		}
	}
	
	function process_block($block_id) {
		$block_info = $this->database_default->get_record_by_field ( "cms_block", "block_id", 
			$block_id );
		
		if (! $block_info) {
			//my_debug ( "碎片不存在($block_id)" );
			//xdebug_print_function_stack ();
			return null;
		}
		$ds = null;
		//block_datasource_mode==1,意思是"支持轮播"
		if ('1' == $block_info ['block_datasource_mode']) {
			//ds_enable==1,意思是支持轮播情况下的启用的ds
			$sql = sprintf ( 
				"SELECT * FROM cms_datasource WHERE block_id='%s' AND ds_enable='1'", $block_id );
			$ds_arr = $this->database_default->get_rows_by_sql ( $sql );
			if (! $ds_arr) {
				if (DEBUGMODE) {
					//my_debug ( "请检查数据源设置id:$block_id" );
				}
				return null;
			}
			foreach ( $ds_arr as $v ) {
				$time_now = time ();
				$time_start = $time_now;
				$time_end = $time_now;
				if ($v ['time_start']) {
					$time_start = strtotime ( $v ['time_start'] );
				}
				if ($v ['time_end']) {
					$time_end = strtotime ( $v ['time_end'] );
				}
				if ($time_start < $time_now) {
					if ($time_now < $time_end) {
						//找到了当前时段有效的ds,则跳出循环
						$ds = $v;
						break;
					}
				}
			}
			if (! $ds) {
				$ds = $ds_arr [0];
			}
			if (! $ds) {
				if (DEBUGMODE) {
					//my_debug ( "请检查数据源设置(支持轮播,但当前时段没有可用的源)id:$block_id" );
				}
			}
		} else {
			//不支持轮播
			//ds_single_used指明了选定的ds
			$sql = sprintf ( 
				"SELECT * FROM cms_datasource WHERE block_id='%s' ORDER BY ds_single_used DESC", 
				$block_id );
			$ds = $this->database_default->get_record_by_sql ( $sql );
			if (! $ds) {
				if (DEBUGMODE) {
					//my_debug ( "请检查数据源设置(不支持轮播情况下)id:$block_id" );
				}
			}
			//my_debug ( $ds );
		}
		
		//到此,$ds就是不前时段应该使用的ds(数据源)
		

		//form类型的ds
		if ('form' == $ds ['ds_type']) {
			//$rows = $this->database_default->get_rows_by_field ( "cms_form_record", "ds_id", $ds ['ds_id'] );
			$rows = $this->database_default->get_rows_by_sql ( 
				sprintf ( 
					"SELECT * FROM cms_form_record WHERE ds_id='%s'  ORDER BY order_num ASC", 
					$ds ['ds_id'] ) );
			if ($rows) {
				return $rows;
			}
		}
	
	}
	function __destruct() {
		foreach ( $this->multi_database as $db ) {
			$db->close ();
		}
	}
}
