<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Comment_app_admin extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
	}
	
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'comment_app_admin' );
		/*if ( !$success) {
			msg ("无权限: key = comment_app_admin","","message");
			exit ();
		}*/
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表===={{==================================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE length(app_name) > 0";
		if ($this->input->post ( 'app_name' )) {
			$sql_where = $sql_where . sprintf ( " AND app_name like '%s%s%s' ", '%', 
				$this->input->post ( 'app_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM com_comment_app $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM com_comment_app $sql_where";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'app_id,app_name' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['模板id'] = $row ['app_tpl'];
				$data [$k] ['备注'] = $row ['app_memo'];
				$tmp = date ( "Y-m-d", $row ['create_time'] );
				$tmp .= "<font color=\"red\">";
				$tmp .= date ( " H:i:s", $row ['create_time'] );
				$tmp .= "</font>";
				$data [$k] ['创建时间'] = $tmp;
				$data [$k] ['编辑'] = form_button ( 'app_' . $k, '编辑', 
					"onclick=\"edit_app({$row['app_id']});return false;\" " );
				$data [$k] ['调用代码'] = form_button ( 'app_' . $k, '调用代码', 
					"onclick=\"app_js({$row['app_id']});return false;\" " );
				
				/*if ($row ['is_arbitrated'] == 1) { //此状态表示该应用内的发言需要审核才能显示
					$data [$k] ['审核'] = form_button ( 'app_' . $k, '需要', 
						"onclick=\"disable_app({$row['app_id']},'arbitrated');return false;\" " );
				}
				if ($row ['is_arbitrated'] == 0) {
					$data [$k] ['审核'] = form_button ( 'app_' . $k, '<font color=red>不需</font>', 
						"onclick=\"enable_app({$row ['app_id']},'arbitrated');return false;\" " );
				}*/
				
				/*if ($row ['is_open'] == 0) { //此状态允许游客发言
					$data [$k] ['对游客'] = form_button ( 'app_' . $k, 
						'<font color=red>已开放</font>', 
						"onclick=\"disable_app({$row['app_id']},'open');return false;\" " );
				}
				if ($row ['is_open'] == 1) {
					$data [$k] ['对游客'] = form_button ( 'app_' . $k, '未开放', 
						"onclick=\"enable_app({$row ['app_id']},'open');return false;\" " );
				}*/
				
				if ($row ['is_anonymous'] == 0) { //此状态允许匿名发表
					$data [$k] ['匿名'] = form_button ( 'app_' . $k, '<font color=red>允许</font>', 
						"onclick=\"disable_app({$row['app_id']},'anonymous');return false;\" " );
				}
				if ($row ['is_anonymous'] == 1) {
					$data [$k] ['匿名'] = form_button ( 'app_' . $k, '拒绝', 
						"onclick=\"enable_app({$row ['app_id']},'anonymous');return false;\" " );
				}
				
				$data [$k] ['删除'] = form_button ( 'app_' . $k, '删除', 
					"onclick=\"if(!confirm('确定要删除?')){return false;}del_app({$row['app_id']});return false;\" " );
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'comment_admin/comment_app_admin_view', $view_data );
	}
	function tpl_list() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check($UID,'tpl_list');
		if ( !$success) {
			msg ("无权限: key = tpl_list","","message");
			exit ();
		}*/
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表===={{==================================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE type_value = 0 ";
		if ($this->input->post ( 'tpl_name' )) {
			$sql_where = $sql_where . sprintf ( " AND tpl_name like '%s%s%s' ", '%', 
				$this->input->post ( 'tpl_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM com_comment_tpl $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM com_comment_tpl $sql_where";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'tpl_id,tpl_name' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['修改'] = form_button ( 'app_' . $k, '修改', 
					"onclick=\"edit_tpl({$row['tpl_id']});return false;\" " );
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'comment_admin/comment_tpl_view', $view_data );
	}
	
	function add_tpl() {
		$UID = $this->session->userdata ( 'UID' );
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['row'] = null;
		$tpl_name = strip_tags ( $this->input->post ( 'tpl_name' ) );
		$form_pop = $this->input->post ( 'form_pop' );
		$form_tpl = $this->input->post ( 'form_tpl' );
		$list_tpl = $this->input->post ( 'list_tpl' );
		$type_value = 0;
		$this->form_validation->set_rules ( 'tpl_name', 'tplName', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$data = array (
						'tpl_name' => $tpl_name, 
						'form_pop' => $form_pop, 
						'form_tpl' => $form_tpl, 
						'list_tpl' => $list_tpl, 
						'type_value' => $type_value );
				$this->db->insert ( 'com_comment_tpl', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'comment_admin/comment_tpl_add_view', $view_data );
	}
	function edit_tpl() {
		$UID = $this->session->userdata ( 'UID' );
		$tpl_id = intval ( $this->input->get ( 'id' ) );
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['row'] = array ();
		$tpl_name = strip_tags ( $this->input->post ( 'tpl_name' ) );
		$form_tpl = $this->input->post ( 'form_tpl' );
		$form_pop = $this->input->post ( 'form_pop' );
		$list_tpl = $this->input->post ( 'list_tpl' );
		$type_value = 0;
		
		$this->form_validation->set_rules ( 'tpl_name', 'tplName', 'required' );
		$this->db->where ( 'tpl_id', $tpl_id );
		$row = $this->db->get_record_by_field ( 'com_comment_tpl', 'tpl_id', $tpl_id );
		if (is_array ( $row )) {
			$view_data ['row'] = $row;
		}
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$data = array (
						'tpl_name' => $tpl_name, 
						'form_tpl' => $form_tpl, 
						'form_pop' => $form_pop, 
						'list_tpl' => $list_tpl, 
						'type_value' => $type_value );
				$this->db->where ( 'tpl_id', $tpl_id );
				$this->db->update ( 'com_comment_tpl', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'comment_admin/comment_tpl_add_view', $view_data );
	}
	function add_app() {
		$UID = $this->session->userdata ( 'UID' );
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['row'] = null;
		
		//====================={{模板列表begin==============
		$select_tpl_options = array ();
		$select_tpl_options [0] = '请选择应用模板';
		$sql = "SELECT*FROM com_comment_tpl WHERE type_value = ''";
		$rows = $this->db->get_rows_by_sql ( $sql );
		foreach ( $rows as $row ) {
			$select_tpl_options [$row ['tpl_id']] = $row ['tpl_name'];
		}
		$view_data ['app_tpl'] = $select_tpl_options;
		//=====================end}}=======================
		

		$this->form_validation->set_rules ( 'app_name', '应用名称', 'required' );
		$this->form_validation->set_rules ( 'app_tpl', '应用模板', 'required' );
		$this->form_validation->set_rules ( 'is_open', '是否开放', 'required' );
		$this->form_validation->set_rules ( 'is_arbitrated', '是否审核', 'required' );
		$this->form_validation->set_rules ( 'is_anonymous', '是否匿名', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$data = array (
						'app_name' => strip_tags ( $this->input->post ( 'app_name' ) ), 
						'app_memo' => strip_tags ( $this->input->post ( 'app_memo' ) ), 
						'app_tpl' => intval ( $this->input->post ( 'app_tpl' ) ), 
						'is_open' => intval ( $this->input->post ( 'is_open' ) ), 
						'is_arbitrated' => intval ( $this->input->post ( 'is_arbitrated' ) ), 
						'is_anonymous' => intval ( $this->input->post ( 'is_anonymous' ) ), 
						'create_time' => time () );
				$this->db->insert ( 'com_comment_app', $data );
				$app_id = $this->db->insert_id ();
				$db_ret = $this->db->insert ( "cms_permission", 
					array (
							'permission_key' => "edit_comment_app_{$app_id}", 
							'permission_name' => "编辑点评模板id={$app_id}", 
							'is_temp' => 0 ) );
				$perm_id = $this->db->insert_id ();
				$db_ret = $this->db->insert ( "cms_user_to_permission", 
					array ('user_id' => $UID, 'permission_id' => $perm_id, 'is_temp' => 0 ) );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'comment_admin/comment_app_admin_add_view', $view_data );
	}
	function edit_app() {
		$app_id = intval ( $this->input->get ( 'id' ) );
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "edit_comment_app_{$app_id}" );
		/*if ($success != 1) {
			msg ( "无权限：编辑页面(edit_comment_app_{$app_id})", "", "message" );
			safe_exit ();
		
		}*/
		$view_data = array ();
		$view_data ['message'] = null;
		
		//====================={{模板列表begin==============
		$select_tpl_options = array ();
		$select_tpl_options [0] = '请选择应用模板';
		$sql = "SELECT*FROM com_comment_tpl WHERE type_value = ''";
		$rows = $this->db->get_rows_by_sql ( $sql );
		foreach ( $rows as $row ) {
			$select_tpl_options [$row ['tpl_id']] = $row ['tpl_name'];
		}
		$view_data ['app_tpl'] = $select_tpl_options;
		//=====================end}}=======================
		

		$this->form_validation->set_rules ( 'app_name', '应用名称', 'required' );
		$this->form_validation->set_rules ( 'app_tpl', '应用模板', 'required' );
		$this->form_validation->set_rules ( 'is_open', '是否开放', 'required' );
		$this->form_validation->set_rules ( 'is_arbitrated', '是否审核', 'required' );
		$this->form_validation->set_rules ( 'is_anonymous', '是否匿名', 'required' );
		$this->db->where ( 'app_id', $app_id );
		$row = $this->db->get_record_by_field ( 'com_comment_app', 'app_id', $app_id );
		if ($row) {
			$view_data ['row'] = $row;
		}
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$data = array (
						'app_name' => strip_tags ( $this->input->post ( 'app_name' ) ), 
						'app_memo' => strip_tags ( $this->input->post ( 'app_memo' ) ), 
						'app_tpl' => intval ( $this->input->post ( 'app_tpl' ) ), 
						'is_open' => intval ( $this->input->post ( 'is_open' ) ), 
						'is_arbitrated' => intval ( $this->input->post ( 'is_arbitrated' ) ), 
						'is_anonymous' => intval ( $this->input->post ( 'is_anonymous' ) ), 
						'create_time' => time () );
				$this->db->where ( 'app_id', $app_id );
				$this->db->update ( 'com_comment_app', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		
		$this->load->view ( 'comment_admin/comment_app_admin_add_view', $view_data );
	}
	function enable_app() {
		$app_id = intval ( $this->input->get ( 'id' ) );
		$field = trim ( $this->input->get ( 'field' ) );
		if ($field === 'arbitrated') {
			$this->db->where ( 'app_id', $app_id );
			$this->db->update ( 'com_comment_app', array ('is_arbitrated' => 1 ) );
		}
		if ($field === 'open') {
			$this->db->where ( 'app_id', $app_id );
			$this->db->update ( 'com_comment_app', array ('is_open' => 0 ) );
		}
		if ($field === 'anonymous') {
			$this->db->where ( 'app_id', $app_id );
			$this->db->update ( 'com_comment_app', array ('is_anonymous' => 0 ) );
		}
		return;
	}
	function disable_app() {
		$app_id = intval ( $this->input->get ( 'id' ) );
		$field = trim ( $this->input->get ( 'field' ) );
		if ($field == 'arbitrated') {
			$this->db->where ( 'app_id', $app_id );
			$this->db->update ( 'com_comment_app', array ('is_arbitrated' => 0 ) );
		}
		if ($field === 'open') {
			$this->db->where ( 'app_id', $app_id );
			$this->db->update ( 'com_comment_app', array ('is_open' => 1 ) );
		}
		if ($field === 'anonymous') {
			$this->db->where ( 'app_id', $app_id );
			$this->db->update ( 'com_comment_app', array ('is_anonymous' => 1 ) );
		}
		return;
	}
	function del_app() {
		$app_id = intval ( $this->input->get ( 'id' ) );
		$this->db->where ( 'app_id', $app_id );
		$this->db->delete ( 'com_comment_app' );
		return;
	}
	function app_js() {
		$app_id = intval ( $this->input->get ( 'id' ) );
		$view_data = array ();
		$view_data ['app_id'] = '';
		$view_data ['app_id'] = $app_id;
		$this->load->view ( 'comment_admin/app_js_view', $view_data );
	}
}
