<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Diaoding_setting extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' );
		$this->load->helper ( "html" );
		
		$this->load->library ( 'tgapi' ); //上传图片API
		$this->load->helper ( "jpbridge" ); //
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->library ( 'datagrid' ); //文本控件
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		//http://10.10.10.201:9101/item/getItem?itemId=1
		

		//my_debug($content);
		//---选择器=-控制分类-
		$this->load->model ( 'diaodingtag_model' );
		$data = null;
		$data .= $this->diaodingtag_model->build_tag_select ( 'tagselector', array (13 ), null, 
			'setting_tag' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;}
		.tag_dimen{color:blue;}
		#tagselector {border:1px solid #f0f0f0;padding:6px;}
		#div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
		#span{border:1px solid #909090;margin:2px;padding:1px;}
		</style>
		";
		
		$view_data ['show_setting_tag'] = $data;
		//=============================标签器 }}end===================================
		

		$this->load->view ( 'diaoding_setting/setting_view', $view_data );
	}
	function do_post() {
		$UID = $this->session->userdata ( 'UID' );
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = " WHERE  1=1";
		if ($this->input->post ( 'setting_name' )) {
			$setting_name = trim ( $this->input->post ( 'setting_name' ) );
			$sql_where = "$sql_where  AND setting_name like '%$setting_name%' ";
		}
		if ($this->input->post ( 'setting_tag' )) {
			$setting_tag = trim ( $this->input->post ( 'setting_tag' ) );
			$sql_where = "$sql_where  AND setting_tag like '%$setting_tag%' ";
		}
		$sql_count = "SELECT count(setting_id) as tot FROM diaoding_scheme_setting $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM diaoding_scheme_setting $sql_where ORDER BY setting_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "setting_id,total,scheme_id,setting_name" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		//api图片
		$api = $this->tgapi->get_tg_api ();
		$api->load ( "upload" );
		
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				
				$img_addr = $row ['setting_img_small'];
				$url = $api->upload->get_url ( $img_addr );
				$data [$k] ['缩略图'] = "<img src=$url width=70 height=70>";
				//$data [$k] ['edit'] =  " <button id='diaoding_modify_{$row ['setting_id']}' class='td_p' onclick='diaoding_modify($row ['setting_id']);return false;'>编辑</button>";
				$data [$k] ['编辑'] = sprintf ( 
					"<a href='%s' target='_blank'>编辑</a>", 
					modify_build_url ( 
						array (
								'm' => "update", 
								'setting_id' => $row ['setting_id'], 
								"layout_id" => $row ['layout_id'], 
								"scheme_id" => $row ['scheme_id'] ) ) );
				$data [$k] ['delete'] = sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array ('m' => "delete", 'setting_id' => $row ['setting_id'] ) ) );
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		$js = '';
		$js .= jpb_replace ( '#main_grid', $this->datagrid->build ( 'datagrid', $data, TRUE ) );
		$js .= jpb_replace ( '#pages_nav', $view_data ['pages_nav'] );
		echo jpb_wrap ( $js );
	}
	function insert() {
		$UID = $this->session->userdata ( 'UID' );
		$scheme_id = $this->input->get ( 'sid' );
		$layout_id = $this->input->get ( 'lid' );
		$persist_record = $this->db->get_record_by_field ( "diaoding_scheme", 'scheme_id', 
			$scheme_id );
		$setting_tag = $this->input->post ( 'setting_tag' );
		if (! $setting_tag) {
			$setting_tag = "c" . $persist_record ['category_id'] . "c";
		}
		$shop_id = $persist_record ['shop_id'];
		
		$room_name = $this->input->post ( 'room_name' );
		$room_width = $this->input->post ( 'room_width' );
		$room_height = $this->input->post ( 'room_height' );
		$dd_images = $this->input->post ( 'dd_images' ); //吊顶图片数组赋到隐藏域中
		$dd_type = $this->input->post ( 'dd_type' );
		$pp_code = $this->input->post ( 'pp_code' );
		$tc_code = $this->input->post ( 'tc_code' );
		$dqmk_group = $this->input->post ( 'dqmk_group' );
		$element_number = $this->input->post ( 'number' );
		$yf = $this->input->post ( 'yf' ); //安装费
		

		$sbt_price = $this->input->post ( 'sbt_price' );
		$zlg_price = $this->input->post ( 'zlg_price' );
		$sjlg_price = $this->input->post ( 'sjlg_price' );
		$fltz_price = $this->input->post ( 'fltz_price' );
		$total = $this->input->post ( 'total' ); //吊顶计算出来总价      --
		$setting_img_small = $this->input->post ( 'api' ); //缩略图
		//====================新加的info_extra、info_extra_2字段 2012.10.29
		$info_highlight = $this->input->post ( 'info_highlight' );
		$info_extra = $this->input->post ( 'info_extra' );
		if ($info_extra) {
			$info_extra = substr ( $info_extra, 0, strrpos ( $info_extra, "," ) );
			$info_extra = $info_extra . "]";
		}
		//exit ( $info_extra );
		$info_extra_2 = $this->input->post ( 'info_extra_2' );
		//=======================================================
		$insert = array (
				'scheme_id' => $scheme_id, 
				'layout_id' => $layout_id, 
				'user_id' => $shop_id, 
				'setting_tag' => $setting_tag, 
				'setting_name' => $room_name, 
				'setting_width' => $room_width, 
				'setting_heigth' => $room_height, 
				'mail_price' => $yf, 
				'dd_images' => $dd_images, 
				'dqmk_group' => $dqmk_group, 
				'dd_type' => $dd_type, 
				'pp_code' => $pp_code, 
				'tc_code' => $tc_code, 
				'sbt_price' => $sbt_price, 
				'fltz_price' => $fltz_price, 
				'total' => $total, 
				'setting_img_small' => $setting_img_small, 
				'info_highlight' => $info_highlight, 
				'info_extra' => $info_extra, 
				'info_extra_2' => $info_extra_2, 
				"create_time" => time () );
		//my_debug ( $insert );
		$db_ret = $this->db->insert ( "diaoding_scheme_setting", $insert );
		$setting_id = $this->db->insert_id ();
		foreach ( $element_number as $k => $v ) {
			$this->db->insert ( 'diaoding_element_num', 
				array ('setting_id' => $setting_id, 'element_id' => $k, 'num' => $v ) );
		}
		if ($db_ret) {
			msg ( "DIY入库成功", modify_build_url ( array ('m' => 'default_add' ) ) );
		}
	}
	
	function default_add() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		
		$scheme_id = $this->input->get ( 'sid' );
		$layout_id = $this->input->get ( 'lid' );
		$shop_id = $this->input->get ( 'shop_id' );
		
		$record_bg = $this->data_list ( $scheme_id, 1 );
		$record_2 = $this->data_list ( $scheme_id, 2 );
		$record_3 = $this->data_list ( $scheme_id, 3 );
		$record_4 = $this->data_list ( $scheme_id, 4 );
		$record_5 = $this->data_list ( $scheme_id, 5 );
		
		$default_highlight = "</br>亮点尽在不言中，就等你来发现啦~</br></br>";
		//my_debug($record_4);
		$view_data = array ();
		$view_data ['element'] = NULL;
		$view_data ['element_bg'] = NULL;
		$view_data ['element_bg'] = $record_bg;
		$view_data ['element_2'] = $record_2;
		$view_data ['element_3'] = $record_3;
		$view_data ['element_4'] = $record_4;
		$view_data ['element_5'] = $record_5;
		$view_data ['default_highlight'] = $default_highlight;
		//my_debug($record_bg);
		//---选择器=-控制分类-
		$category_record = $this->db->get_record_by_field ( "diaoding_scheme", 
			'scheme_id', $scheme_id );
		$category_id = $category_record ['category_id'];
		$this->load->model ( 'diaodingtag_model' );
		$data = NULL;
		$data .= $this->diaodingtag_model->build_tag_select ( 'tagselector', array (13 ), 
			array ($category_id ), 'setting_tag' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;}
		.tag_dimen{color:blue;}
		#tagselector {border:1px solid #f0f0f0;padding:6px;}
		#div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
		#span{border:1px solid #909090;margin:2px;padding:1px;}
		</style>
		";
		$view_data ['show_setting_tag'] = $data;
		//=============================标签器 }}end===================================
		//上传控件---大缩略图---start
		$this->load->library ( 'editors' );
		$data_url_pic = modify_build_url ( 
			array (
					'm' => 'single_upload', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey ) );
		$pic = array (
				'element_id' => 'custom_file_upload', 
				'script' => $data_url_pic, 
				'lable' => "false" );
		$view_data ['upload_file'] = $this->editors->get_upload ( $pic, 'uploadify' );
		//--小缩略图---start
		$data_small_upload_file = modify_build_url ( 
			array (
					'm' => 'small_upload', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey ) );
		$pic = array (
				'element_id' => 'small_upload_file', 
				'script' => $data_small_upload_file, 
				'lable' => "false" );
		$view_data ['small_upload_file'] = $this->editors->get_upload ( $pic, 'uploadify' );
		
		//=================上传控件
		//编辑器---------------------
		/*$eddt = array (
		 'id' => 'setting_memo',
		 'value' => '',
		 'width' => '600px',
		 'height' => '200px' );
		 $view_data ['setting_memo'] = $this->editors->getedit ( $eddt );*/
		//----end 编辑器----------
		

		$this->load->view ( 'diaoding_setting/default_add_view', $view_data );
	}
	//小缩略图
	function small_upload() {
		$api = $this->tgapi->get_tg_api ();
		$api->load ( "upload" );
		$file = $_FILES ['Filedata'] ['tmp_name'];
		$new_file = $api->upload->from_files ( $file );
		$url = $api->upload->get_url ( $new_file );
		//将$new_file  保存到数据库，字段为 char(13)
		//echo "$url|$new_file"; 7237829.19
		echo "<img width=50 src=$url><input value=$new_file type=hidden name=small_api id=small_api >";
	}
	//大缩略图
	function single_upload() {
		$api = $this->tgapi->get_tg_api ();
		$api->load ( "upload" );
		$file = $_FILES ['Filedata'] ['tmp_name'];
		$new_file = $api->upload->from_files ( $file );
		$url = $api->upload->get_url ( $new_file );
		//将$new_file  保存到数据库，字段为 char(13)
		//echo "$url|$new_file"; 7237829.19
		echo "<img width=70 height=70 src=$url><input value=$new_file type=hidden name=api id=api >";
		//缩小图片
	/*$api->load ( "image" );
		 $image = $url; //要缩放的图片
		 $size = "80x80"; //要缩放到的大小，中间为小写的x
		 $new_file_suoxiao = $api->image->resize ( $image, $size );
		 echo "<img width=50 src=$new_file_suoxiao><input value=$new_file_suoxiao type=text name=image id=image >";*/
	}
	
	function update() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		
		$scheme_id = $this->input->get ( 'scheme_id' );
		$layout_id = $this->input->get ( 'layout_id' );
		$record_bg = $this->data_list ( $scheme_id, 1 );
		$record_2 = $this->data_list ( $scheme_id, 2 );
		$record_3 = $this->data_list ( $scheme_id, 3 );
		$record_4 = $this->data_list ( $scheme_id, 4 );
		$record_5 = $this->data_list ( $scheme_id, 5 );
		
		$view_data = array ();
		$view_data ['element'] = NULL;
		$view_data ['element_bg'] = NULL;
		$view_data ['element_bg'] = $record_bg;
		$view_data ['element_2'] = $record_2;
		$view_data ['element_3'] = $record_3;
		$view_data ['element_4'] = $record_4;
		$view_data ['element_5'] = $record_5;
		
		$setting_id = trim ( $this->input->get ( 'setting_id' ) );
		$setting_id = intval ( $setting_id );
		$view_data ['row'] = '';
		$persist_record = $this->db->get_record_by_field ( "diaoding_scheme_setting", 'setting_id', 
			$setting_id );
		$view_data ['num_records'] = array ();
		$element_num_records = array ();
		$element_num_records = $this->db->get_rows_by_sql ( 
			"SELECT element_id , num FROM diaoding_element_num WHERE setting_id=" . $setting_id );
		
		if ($element_num_records) {
			foreach ( $element_num_records as $k => $v ) {
				$num_records [$v ['element_id']] = $v ['num'];
			}
		} else {
			$num_records = array ();
		}
		$view_data ['num_records'] = $num_records;
		
		$default_highlight = "</br>
 亮点尽在不言中，就等你来发现啦~</br>
</br>";
		if (! $persist_record ['info_highlight']) {
			$persist_record ['info_highlight'] = $default_highlight;
		}
		$view_data ['row'] = $persist_record;
		$category_record = $this->db->get_record_by_field ( "diaoding_scheme", 'scheme_id', 
			$scheme_id );
		$category_id = "c" . $category_record ['category_id'] . "c";
		
		//---选择器=-控制分类-
		$ab_tags_array = explode ( ",", str_replace ( "c", "", $category_id ) );
		$this->load->model ( 'diaodingtag_model' );
		$data = null;
		$data .= $this->diaodingtag_model->build_tag_select ( 'tagselector', array (13 ), 
			$ab_tags_array, 'setting_tag' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;}
		.tag_dimen{color:blue;}
		#tagselector {border:1px solid #f0f0f0;padding:6px;}
		#div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
		#span{border:1px solid #909090;margin:2px;padding:1px;}
		</style>
		";
		
		$view_data ['show_setting_tag'] = $data;
		
		//上传控件---大缩略图---start
		$this->load->library ( 'editors' );
		$data_url_pic = modify_build_url ( 
			array (
					'm' => 'single_upload', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey ) );
		$pic = array (
				'element_id' => 'custom_file_upload', 
				'script' => $data_url_pic, 
				'lable' => "false" );
		$view_data ['upload_file'] = $this->editors->get_upload ( $pic, 'uploadify' );
		//应用api
		$api = $this->tgapi->get_tg_api ();
		$api->load ( "upload" );
		$img_addr = $persist_record ['setting_img_small'];
		$url = $api->upload->get_url ( $img_addr );
		$view_data ['pic'] = "<img src=$url width=70 height=70>";
		
		$this->load->view ( 'diaoding_setting/default_update_view', $view_data );
	}
	//修改吊顶预设方案
	function up_add() {
		$UID = $this->session->userdata ( 'UID' );
		$setting_id = $this->input->get ( 'setting_id' );
		$scheme_id = $this->input->get ( 'scheme_id' );
		$persist_record = $this->db->get_record_by_field ( "diaoding_scheme", 'scheme_id', 
			$scheme_id );
		$setting_tag = $this->input->post ( 'setting_tag' );
		if (! $setting_tag) {
			$setting_tag = "c" . $persist_record ['category_id'] . "c";
		}
		
		$shop_id = $persist_record ['shop_id'];
		$layout_id = $this->input->get ( 'layout_id' );
		$room_name = $this->input->post ( 'room_name' );
		$room_width = $this->input->post ( 'room_width' );
		$room_height = $this->input->post ( 'room_height' );
		$dd_images = $this->input->post ( 'dd_images' ); //吊顶图片数组赋到隐藏域中
		$dd_type = $this->input->post ( 'dd_type' );
		$pp_code = $this->input->post ( 'pp_code' );
		$tc_code = $this->input->post ( 'tc_code' );
		$dqmk_group = $this->input->post ( 'dqmk_group' );
		$yf = $this->input->post ( 'yf' ); //安装费
		$total = $this->input->post ( 'total' );
		$sbt_price = $this->input->post ( 'sbt_price' );
		$zlg_price = $this->input->post ( 'zlg_price' );
		$sjlg_price = $this->input->post ( 'sjlg_price' );
		$fltz_price = $this->input->post ( 'fltz_price' );
		$setting_img_small = $this->input->post ( 'api' ); //缩略图
		$info_highlight = $this->input->post ( 'info_highlight' );
		$info_extra = $this->input->post ( 'info_extra' );
		$element_number = $this->input->post ( 'number' );
		if ($info_extra) {
			$info_extra = substr ( $info_extra, 0, strrpos ( $info_extra, "," ) );
			$info_extra = $info_extra . "]";
		}
		//exit ( $info_extra );
		$info_extra_2 = $this->input->post ( 'info_extra_2' );
		$insert = array (
				'scheme_id' => $scheme_id, 
				'layout_id' => $layout_id, 
				'user_id' => $shop_id, 
				'setting_tag' => $setting_tag, 
				'setting_name' => $room_name, 
				'setting_width' => $room_width, 
				'setting_heigth' => $room_height, 
				'mail_price' => $yf, 
				'dd_images' => $dd_images, 
				'dd_type' => $dd_type, 
				'pp_code' => $pp_code, 
				'tc_code' => $tc_code, 
				'dqmk_group' => $dqmk_group, 
				'sbt_price' => $sbt_price, 
				'fltz_price' => $fltz_price, 
				'info_highlight' => $info_highlight, 
				'info_extra' => $info_extra, 
				'info_extra_2' => $info_extra_2, 
				'total' => $total, 
				'setting_img_small' => $setting_img_small, 
				"create_time" => time () );
		//my_debug($insert);
		$this->db->where ( 'setting_id', $setting_id );
		$db_ret = $this->db->update ( "diaoding_scheme_setting", $insert );
		$this->db->where ( 'setting_id', $setting_id );
		$this->db->delete ( 'diaoding_element_num' );
		foreach ( $element_number as $k => $v ) {
			$this->db->insert ( 'diaoding_element_num', 
				array ('setting_id' => $setting_id, 'element_id' => $k, 'num' => $v ) );
		}
		if ($db_ret) {
			msg ( "DIY修改成功", 
				modify_build_url ( array ('m' => 'update', 'setting_id' => $setting_id ) ) );
		}
		
	//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
	}
	function cover() {
		$img_id = $this->input->get ( 'img_id' );
		$setting_id = $this->input->get ( 'setting_id' );
		$img_reset = array ('img_is_cover' => 0 );
		$this->db->where ( 'img_setting_id', $setting_id );
		$db_ret = $this->db->update ( "news_image", $img_reset );
		
		$img = array ('img_is_cover' => 1 );
		$this->db->where ( 'img_id', $img_id );
		$this->db->where ( 'img_setting_id', $setting_id );
		$db_ret = $this->db->update ( "news_image", $img );
		
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
	}
	function delete() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID, "news_demension_del" );
		 if (! $success) {
			msg ( "无权限：删除权限/news_demension_del/", "", "message" );
			exit ();
			}*/
		
		$record_id = $this->input->get ( "setting_id" );
		$record_id = intval ( $record_id );
		$this->db->where ( 'setting_id', $record_id );
		$success = $this->db->delete ( 'diaoding_scheme_setting' );
		$this->db->where ( 'setting_id', $record_id );
		$success = $this->db->delete ( 'diaoding_element_num' );
		if ($success) {
			msg ( "", modify_build_url ( array ("m" => "index" ) ) );
		}
	}
	
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			//echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
	private function data_list($scheme_id, $element_type) {
		$record_bg = $this->db->get_rows_by_sql ( 
			"SELECT * FROM diaoding_element WHERE scheme_id='$scheme_id' AND element_type='$element_type'" );
		if (count ( $record_bg )) {
			foreach ( $record_bg as $k => $v ) {
				$record_bg [$k] ['qeekaPrice'] = 0;
				if ($v ['good_price']) {
					$record_bg [$k] ['qeekaPrice'] = $v ['good_price'];
				}
				
				$record_bg [$k] ['attributeValue'] = "0,0";
				$good_size = $v ['good_size'];
				if ($good_size) {
					$record_bg [$k] ['attributeValue'] = str_replace ( "*", ",", $good_size );
				}
				/*$good_id = $v ['good_id'];
				 $file = file_get_contents ( "http://10.10.21.126:9101/item/getItem?itemId=$good_id" );
				 $content = json_decode ( $file, true );
				 $record_bg [$k] ['attributeValue'] = "0,0";
				 $record_bg [$k] ['qeekaPrice'] = 0;
				 if (isset ( $content ['result'] )) {
					if ($content ['result'] ['itemBase'] ['qeekaPrice']) {
					$record_2 [$k] ['qeekaPrice'] = $content ['result'] ['itemBase'] ['qeekaPrice'] / 100;
					$content1 = json_decode ( $content ['result'] ['itemBase'] ['attributeJson'],
					true );

					if (isset ( $content1 [3] ['attributeValue'] ) && strpos (
					$content1 [3] ['attributeValue'], "*" ) !== false) {
					$record_2 [$k] ['attributeValue'] = str_replace ( "*", ",",
					$content1 [3] ['attributeValue'] );
					}
					}
					}*/
			
			}
		
		}
		return $record_bg;
	}
}


//end.
