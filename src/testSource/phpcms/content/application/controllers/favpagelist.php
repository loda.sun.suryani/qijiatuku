<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class FavPageList extends MY_Controller {
	function __construct() {
		parent::__construct ();
		
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		$this->load->model ( 'tree_model' );
		
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "pagelist" );
		if ($success != 1) {
			msg ( "无权限：页面列表(pagelist)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$user_id = $this->uid;
		$view_data = array ();
		//$view_data ['select_pagetpl_options'] = '';
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		$view_data ['page_column_id_select'] = '';
		$view_data ['page_site_select'] = null;
		
		//----------------{{栏目选择项start--------------------------
		$paths = $this->tree_model->paths;
		$paths_select ['0'] = '----------请选择----------';
		foreach ( $paths as $k => $v ) {
			$v = trim ( $v, '/' );
			$v_arr = explode ( '/', $v );
			$count = count ( $v_arr );
			$v = str_repeat ( '├-', $count - 1 );
			if ($count > 1) {
				$v .= '├';
			}
			$v .= $v_arr [$count - 1];
			$paths_select [$k] = $v;
		}
		$view_data ['page_column_id_select'] = $paths_select;
		//-------------------栏目选择项end}}-------------------------
		

		//=========列表===={{=================
		$page_size = 15;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE a.user_id='$user_id' ";
		if ($this->input->post ( 'page_column_id' )) {
			$sql_where = $sql_where . sprintf ( " AND page_column_id='%s' ", 
				intval ( $this->input->post ( 'page_column_id' ) ) );
		}
		if ($this->input->post ( 'page_name' )) {
			$sql_where = $sql_where . sprintf ( " AND page_name like '%s%s%s' ", '%', 
				$this->input->post ( 'page_name' ), '%' );
		}
		if ($this->input->post ( 'page_url' )) {
			$sql_where = $sql_where . sprintf ( " AND page_url like '%s%s%s' ", '%', 
				$this->input->post ( 'page_url' ), '%' );
		}
		if ($this->input->post ( 'page_site' )) {
			$sql_where = $sql_where . sprintf ( " AND page_site = '%s' ", 
				$this->input->post ( 'page_site' ) );
		}
		$sql_count = "
		SELECT count(*) as tot 
			FROM cms_my_page a 
				LEFT JOIN cms_page b 
				ON a.page_id=b.page_id 
			$sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT a.page_id as id,page_name,page_url,page_site,modify_time,publish_time 
			FROM cms_my_page a 
				LEFT JOIN cms_page b 
				ON a.page_id=b.page_id 
			$sql_where 
			ORDER BY a.create_time DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if (! array_key_exists ( 'publish_time', $row )) {
					$row ['publish_time'] = 0;
				}
				if (! array_key_exists ( 'modify_time', $row )) {
					$row ['modify_time'] = 0;
				} //删除，权限，copy放在更多
				$data [$k] ['publish'] = sprintf ( "<a href='%s' target='_blank'>发布</a>", 
					modify_build_url ( array ('c' => 'publish', "page_id" => $row ['id'] ) ) );
				if (($row ['publish_time'] <= $row ['modify_time'])) {
					//发布时间比较大, 说明没有必要再发布
					$data [$k] ['publish'] .= '(有更新)';
				}
				unset ( $data [$k] ['modify_time'] );
				unset ( $data [$k] ['publish_time'] );
				$data [$k] ['id'] = sprintf ( 
					"<a href='http://%s%s' target='_blank'>" . $row ['id'] . "</a>", 
					$row ['page_site'], $row ['page_url'] );
				if ($row ['page_url']) {
					$data [$k] ['page_url'] = sprintf ( 
						"<a href='http://%s%s' target='_blank'>" . $row ['page_url'] . "</a>", 
						$row ['page_site'], $row ['page_url'] );
				
				}
				/*$data [$k] ['refresh'] = sprintf ( 
					"<button onclick=\"javascript:ajax_then_reload('%s');return false;\" >刷新</button>", 
					modify_build_url ( array ('c' => 'page', 'page_id' => $row ['id'] ) ) );*/
				$data [$k] ['edit'] = sprintf ( "<a href='%s' target=\"_blank\" >编辑</a>", 
					site_url ( "c=createpage&page_id=" . $row ['id'] ) );
				$data [$k] ['preview'] = sprintf ( "<a href='%s' target='_blank'>预览</a>", 
					modify_build_url ( 
						array (
								'c' => 'page', 
								"page_id" => $row ['id'], 
								'm' => 'preview', 
								'editable' => '1' ) ) );
				$data [$k] ['editable'] = sprintf ( "<a href='%s' target='_blank'>编辑模式查看</a>", 
					modify_build_url ( 
						array (
								'c' => 'page', 
								"page_id" => $row ['id'], 
								'm' => 'editable', 
								'editable' => '1' ) ) );
				$data [$k] ['more'] = "<div class=\"editBlock_div\"><a href = '###'>更多</a>";
				$data [$k] ['more'] .= "<ul class='editBlock_ulist'>";
				$data [$k] ['more'] .= "<li>" . sprintf ( "<a href='%s' target='_blank'>权限</a>", 
					modify_build_url ( 
						array (
								'c' => 'pagelist', 
								'm' => 'accredit_page_perm', 
								"page_id" => $row ['id'] ) ) ) . "</li>";
				$data [$k] ['more'] .= "<li><A onclick=\"if(!confirm('确定要复制?')){return false;}copy({$row['id']});return false;\" >copy</A></li>";
				$data [$k] ['more'] .= "<li><A onclick=\"if(!confirm('确定要删除?')){return false;}page_delete({$row['id']});return false;\">删除</A></li>";
				$data [$k] ['more'] .= "</ul>";
				$data [$k] ['more'] .= "</div>";
				$data [$k] ['delete'] = "<A onclick=\"if(!confirm('确定要移除收藏?')){return false;}page_unset({$row['id']});return false;\"> 移除</A>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表====}}=================
		

		$this->load->config ( 'publish' );
		$view_data ['page_site_select'] = array_merge ( array ('0' => '---------请选择-----------' ), 
			$this->config->item ( 'publish' ) );
		
		$view_data ['user_id_select'] = array ("0" => "显示所有", $this->uid => "仅显示我的" );
		$this->load->view ( 'favpagelist_view', $view_data );
	}
	function page_unset(){
		$page_id = $this->input->get('id');
		$page_id = intval($page_id);
		$this->db->where('page_id',$page_id);
		$this->db->delete('cms_my_page');
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>"; 
	}
}

//end.
