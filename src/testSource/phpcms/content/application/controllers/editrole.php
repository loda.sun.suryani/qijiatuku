<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class EditRole extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
	}
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editrole_index" );
		if (! $success) {
			msg ( "无权限：编辑角色editrole_index/[$UID]/", "", "message" );
			exit ();
		}
		//创建一个空的记录,进入编辑
		$record_id = $this->input->get ( "id" );
		$record_id = intval ( $record_id );
		if (! $record_id) {
			$db_ret = $this->db->insert ( "cms_role", array ('is_temp' => 1 ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('c' => 'editrole', 'id' => $insert_id ) ) );
			}
		}
		
		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_role", 'role_id', 
			$record_id );
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		
		$view_data = array ();
		$view_data ['message'] = null;
		//表单验证规则
		$this->form_validation->set_rules ( 'role_name', '角色名', 
			"callback_role_name|required" );
		
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$this->db->where ( 'role_id', $record_id );
				$this->db->update ( 'cms_role', 
					array (
							'role_name' => trim ( $this->input->post ( 'role_name' ) ), 
							'role_description' => trim ( 
								$this->input->post ( 'role_description' ) ), 
							'is_temp' => '0' ) );
				if ($this->db->affected_rows ()) {
					//$view_data['message']= ("已经写入数据库.".time());
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					$view_data ['message'] = ("没有更新任何内容," . microtime ());
				}
				//redirect ( site_url ( "c=formlist" ) );
				//关闭界面
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				//return;
			}
		}
		$this->load->view ( 'editrole_view', $view_data );
	}
	
	//引用规则
	function role_name($role_name) {
		$role_name = trim ( $role_name );
		$count = $this->db->get_record_by_sql ( 
			"SELECT count(role_id) as t_count FROM cms_role WHERE role_name ='$role_name' " );
		if ($count ['t_count']) {
			$this->form_validation->set_message ( 'role_name', 
				'您的角色名：[<font color=blue>' . $role_name . ']</font>,已经使用' );
			return false;
		}
		return true;
	}

}


//end.
