<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Cmspage extends MY_Controller {
	
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'url' );
		$this->load->helper ( "toolkit" );
		$this->load->library ( 'session' );
		$this->load->library ( 'editors' );
		$this->load->model ( 'Cmspage_model' );
		$this->load->database ();
		//装载表单
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_message ( 'numeric', '必须为数字' );
		$this->url = "public/uploadpic/cmspage";
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 记录列表
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 显示cms_page_tpl表中的内容
	*/
	public function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cmspage_index' );
		if ($success != 1) {
			msg ( "无权限：页面模版显示/key=cmspage_index/", "", "message" );
			safe_exit ();
		}
		
		/*分页列表*/
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$t_count = $this->Cmspage_model->row_array ();
		$t_first = ($page - 1) * $count_page;
		$list = $this->Cmspage_model->result_array ( $t_first, $count_page );
		if (count ( $list )) {
			foreach ( $list as $key => $v ) {
				$user_id = $v ['user_id'];
				$list [$key] ['user_name'] = '';
				$sql_u = "SELECT user_name FROM cms_user WHERE user_id='$user_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				$list [$key] ['user_name'] = $row ['user_name'];
			}
		
		}
		$a ['c'] = "cmspage";
		$a ['m'] = "index";
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		$date ['t_count'] = $t_count;
		$date ['list'] = $list;
		$edit_edit = validation_check ( $UID, 'cms_page_eidtor' ); //控制当用户user_id不等于自己的uid时候的控制编辑
		$date ['edit_edit'] = $edit_edit;
		$date ['page'] = $page;
		$date ['uid'] = $UID;
		$date ['page_tpl_name'] = '';
		$date ['pic_path'] = $this->url;
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		$urlarray = array ('all' => '全部', '1' => '已开启', '2' => '已使用', '0' => '已禁用' );
		
		$variable = form_dropdown ( 'type', $urlarray, 'all' );
		$date ['variable'] = $variable;
		
		$this->load->view ( 'cmspage/cmspage_view', $date );
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 查询列表
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 根据查询显示cms_page_tpl表中的内容
	*/
	public function page_search() {
		$UID = $this->session->userdata ( 'UID' );
		$page_tpl_name = $this->input->get_post ( "page_tpl_name" );
		$type = $this->input->get_post ( "type" );
		/*分页*/
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$t_count = $this->Cmspage_model->row_array_search ( $page_tpl_name, $type ); //统计记录总数
		$t_first = ($page - 1) * $count_page;
		$list = $this->Cmspage_model->result_array_search ( $page_tpl_name, $type, $t_first, $count_page );
		if (count ( $list )) {
			foreach ( $list as $key => $v ) {
				$user_id = $v ['user_id'];
				$list [$key] ['user_name'] = '';
				$sql_u = "SELECT user_name FROM cms_user WHERE user_id='$user_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				$list [$key] ['user_name'] = $row ['user_name'];
			}
		
		}
		$a ['c'] = "cmspage";
		$a ['m'] = "page_search";
		$a ['page_tpl_name'] = $page_tpl_name;
		$a ['type'] = $type;
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		$date ['t_count'] = $t_count;
		$date ['list'] = $list;
		$date ['page'] = $page;
		$edit_edit = validation_check ( $UID, 'cms_page_eidtor' ); //控制当用户user_id不等于自己的uid时候的控制编辑
		$date ['edit_edit'] = $edit_edit;
		$date ['uid'] = $UID;
		$date ['page_tpl_name'] = $page_tpl_name;
		$date ['type'] = $type;
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = "";
		}
		$urlarray = array ('all' => '全部', '1' => '已开启', '2' => '已使用', '3' => '已禁用' );
		
		$variable = form_dropdown ( 'type', $urlarray, $type );
		$date ['variable'] = $variable;
		$date ['pic_path'] = $this->url;
		
		$this->load->view ( 'cmspage/cmspage_view', $date );
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 添加数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 
	*/
	public function cmspage_add() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$success = validation_check ( $UID, "cmspage_add" );
		if (! $success) {
			msg ( "无权限：CMS添加页面模板/cmspage_add/", "", "message" );
			safe_exit ();
		}
		
		$create_time = time ();
		//删除create_time=0的临时
		//创建递归创建目录
		create_dir ( $this->url );
		//---------删除create_time=0的临时------------------------
		if (! $this->input->get ( 'page_tpl_id' )) {
			$linshi_del = $this->Cmspage_model->linshi ();
			$data = array (
					'area_count' => "0", 
					'page_tpl_name' => "", 
					'create_time' => "0", 
					'modify_time' => "0", 
					'demo_pic_id' => "", 
					'user_id' => $UID, 
					'tpl_content' => "" );
			$page_tpl_id = $this->Cmspage_model->insert ( $data );
			$a ['c'] = "cmspage";
			$a ['m'] = "cmspage_add";
			$a ['page_tpl_id'] = "$page_tpl_id";
			if ($page_tpl_id) {
				msg ( "", url_glue ( $a ) );
			}
		} else {
			$page_tpl_id = $this->input->get ( 'page_tpl_id' );
		}
		
		//echo $page_tpl_id;
		/*验证*/
		$this->form_validation->set_rules ( 'page_tpl_name', '页面模板名称', 'callback_check_page_name|required' );
		$this->form_validation->set_rules ( 'area_count', '区域', 'required|numeric' );
		$this->form_validation->set_rules ( 'author_id', '选择设计人', 'required|numeric' );
		$this->form_validation->set_rules ( 'tpl_content', '内容', 'required' );
		
		/*是否点击提交按钮验证*/
		if ($this->form_validation->run () == TRUE) {
			$page_tpl_name = $this->Cmspage_model->replace ( $this->input->post ( "page_tpl_name" ) );
			$tpl_content = $this->Cmspage_model->replace ( $this->input->post ( "tpl_content" ) );
			
			/*图片的上传*/
			$demo_pic_id = $this->input->post ( "demo_pic_id" );
			$layout_pic_id = $this->input->post ( "layout_pic_id" );
			
			$area_count = $this->input->post ( "area_count" );
			$author_id = $this->input->post ( "author_id" );
			$create_time = time ();
			$modify_time = time ();
			/*验证入库*/
			$up = array (
					'area_count' => "$area_count", 
					'page_tpl_name' => $page_tpl_name, 
					'demo_pic_id' => $demo_pic_id, 
					'layout_pic_id' => $layout_pic_id, 
					'create_time' => $create_time, 
					'modify_time' => $modify_time, 
					'user_id' => $UID, 
					'author_id' => $author_id, 
					'tpl_content' => $tpl_content );
			//print_r ( $up );
			//print_r ( $page_tpl_id );
			$success = $this->Cmspage_model->update ( $up, $page_tpl_id );
			$a ['c'] = "cmspage";
			$a ['m'] = "index";
			if ($success) {
				msg ( "", url_glue ( $a ) );
			}
		}
		
		$date = array ();
		$date ['page_tpl_id'] = '';
		$date ['pic_path'] = $this->url;
		$date ['page_tpl_id'] = $page_tpl_id;
		//上传图片控件
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'cmsdesign', 
					'm' => 'Upload', 
					'id' => $page_tpl_id, 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'folder' => $this->url, 
					'page' => 'demo_pic_id', 
					'input' => 'demo_pic_id' ) );
		$pic = array ('element_id' => 'demo_pic_id', 'script' => $data_url_pic, 'lable' => "false" );
		$date ['demo_pic_id'] = $this->editors->get_upload ( $pic );
		
		$data_url_id = modify_build_url ( 
			array (
					'c' => 'cmsdesign', 
					'm' => 'Upload', 
					'id' => $page_tpl_id, 
					'TG_loginuserid' => $UID, 
					'folder' => $this->url, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'page' => 'layout_pic_id', 
					'input' => 'layout_pic_id' ) );
		$psd = array (
				'element_id' => 'layout_pic_id', 
				'script' => $data_url_id, 
				'queue_id' => 'custom-queue1', 
				'label' => "false" );
		$date ['layout_pic_id'] = $this->editors->get_upload ( $psd );
		
		$this->load->view ( 'cmspage/cmspage_add_view', $date );
	}
	function tags_search() {
		$this->load->helper ( 'security' );
		$ret = array ();
		$q = $this->input->post ( 'q' );
		$q = trim ( $q );
		$q = xss_clean ( $q );
		$sql_where = null;
		if (! $q) {
			$sql_where = " WHERE 1";
		} else {
			$sql_where = " WHERE user_name like '%{$q}%' OR user_nick like '%{$q}%'";
		}
		$sql = "SELECT * FROM cms_user $sql_where LIMIT 5";
		$rows = $this->db->get_rows_by_sql ( $sql );
		if ($rows) {
			foreach ( $rows as $row ) {
				if ($row ['user_nick']) {
					$page_name = $row ['user_nick'];
				} else {
					$page_name = $row ['user_name'];
				}
				$ret [] = array ('key' => $row ['user_id'], 'value' => $page_name );
			}
		}
		echo json_encode ( $ret );
		return;
	}
	public function yulan_show() {
		
		$page_tpl_id = $this->input->get ( "page_tpl_id" );
		$campage_array = $this->Cmspage_model->selectlist ( $page_tpl_id );
		$tmp = parse_url ( base_url () );
		$relative_url = $tmp ['path'];
		
		$vegetables = str_replace ( '{{$resource_url}}', $relative_url . "public/resource", 
			$campage_array ['tpl_content'] );
		$date = array ();
		$date ['tpl_content'] = html_entity_decode ( $vegetables );
		$this->load->view ( 'cmspage/cmspage_yulan_show', $date );
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 修改记录
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 修改cms_page_tpl表中的内容，并直接上传文件到$this->url制定目录中去
	*/
	public function edit() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$success = validation_check ( $UID, "cmspage_edit" );
		if (! $success) {
			msg ( "无权限：CMS编辑页面模板/cmspage_edit/", "", "message" );
			safe_exit ();
		}
		
		//创建递归创建目录
		create_dir ( $this->url );
		$page_tpl_id = $this->input->get ( "page_tpl_id" );
		$page = $this->input->get ( "page" );
		$campage_array = $this->Cmspage_model->selectlist ( $page_tpl_id );
		/*验证*/
		$this->form_validation->set_rules ( 'page_tpl_name', '页面模板名称', 'callback_check_page_name|required' );
		$this->form_validation->set_rules ( 'area_count', '区域', 'required|numeric' );
		$this->form_validation->set_rules ( 'tpl_content', 'tpl_content', 'required' );
		/*是否点击提交按钮验证*/
		if ($this->input->post ( "submit" )) {
			$page_tpl_name = $this->Cmspage_model->replace ( $this->input->post ( "page_tpl_name" ) );
			$tpl_content = $this->Cmspage_model->replace ( $this->input->post ( "tpl_content" ) );
			
			/*图片的上传*/
			$demo_pic_id = $this->input->post ( "demo_pic_id" ); //substr ( $demo_pic_id_1, strpos ( $demo_pic_id_1, "YL_" ) );
			$layout_pic_id = $this->input->post ( "layout_pic_id" );
			
			$area_count = $this->input->post ( "area_count" );
			$author_id = $this->input->post ( "author_id" );
			$is_locked = intval($this->input->post ( "is_locked" ));
			$create_time = time ();
			$modify_time = time ();
			/*验证入库*/
			if ($this->form_validation->run () == TRUE) {
				//模板记录备份表======================start==================================
				if ($campage_array ['disable'] == 2) {
					if (count ( $campage_array )) {
						$campage_array ['history_time'] = time ();
						//my_debug($campage_array);
						$bak_success = $this->db->insert ( 
							"cms_page_tpl_history", $campage_array );
						//my_debug($bak_success);
					}
				}
				//模板记录备份表======================end======================================
				$up = array (
						'area_count' => "$area_count", 
						'page_tpl_name' => $page_tpl_name, 
						'demo_pic_id' => $demo_pic_id, 
						'layout_pic_id' => $layout_pic_id, 
						'create_time' => $create_time, 
						'modify_time' => $modify_time, 
						//'user_id' => $UID,
						'author_id' => $author_id,
						'is_locked' => $is_locked, 
						'tpl_content' => $tpl_content );
				//my_debug($up);
				$success = $this->Cmspage_model->update ( $up, $page_tpl_id );
				if ($success) {
					$a ['c'] = "cmspage";
					$a ['m'] = "submit";
					$a ['page_tpl_id'] = $page_tpl_id;
					msg ( "更新成功", url_glue ( $a ) );
				}
			}
		}
		$date ['user_id'] = $campage_array ['user_id'];
		$date ['uid'] = $UID;
		$date ['page_tpl_id'] = $page_tpl_id;
		
		if (count ( $campage_array )) {
			$user_id = $campage_array ['user_id'];
			$author_id = $campage_array ['author_id'];
			$campage_array ['user_name'] = '';
			$campage_array ['author_name'] = '';
			$sql_u = "SELECT * FROM cms_user WHERE user_id='$user_id' ";
			$row = $this->db->get_record_by_sql ( $sql_u );
			if ($row ['user_nick']) {
				$page_name = $row ['user_nick'];
			} else {
				$page_name = $row ['user_name'];
			}
			$campage_array ['user_name'] = $page_name;
			$sql_u = "SELECT * FROM cms_user WHERE user_id='$author_id' ";
			$row = $this->db->get_record_by_sql ( $sql_u );
			if ($row ['user_nick']) {
				$page_name = $row ['user_nick'];
			} else {
				$page_name = $row ['user_name'];
			}
			$campage_array ['author_name'] = $page_name;
		}
		
		$date ['campage_array'] = $campage_array;
		$date ['page'] = $page;
		$date ['pic_path'] = $this->url;
		//上传图片控件
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'cmspage', 
					'm' => 'Upload', 
					'id' => $page_tpl_id, 
					'TG_loginuserid' => $UID, 
					'folder' => $this->url, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'page' => 'demo_pic_id', 
					'input' => 'demo_pic_id' ) );
		$pic = array ('element_id' => 'demo_pic_id', 'script' => $data_url_pic, 

		'lable' => "false" );
		//my_debug($pic);
		$date ['demo_pic_id'] = $this->editors->get_upload ( $pic );
		
		$data_url_id = modify_build_url ( 
			array (
					'c' => 'cmspage', 
					'm' => 'Upload', 
					'id' => $page_tpl_id, 
					'folder' => $this->url, 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'page' => 'layout_pic_id', 
					'input' => 'layout_pic_id' ) );
		$psd = array (
				'element_id' => 'layout_pic_id', 
				'script' => $data_url_id, 
				'queue_id' => 'custom-queue1', 
				'label' => "false" );
		$date ['layout_pic_id'] = $this->editors->get_upload ( $psd );
		
		$edit_edit = validation_check ( $UID, 'cms_page_eidtor' ); //控制当用户user_id不等于自己的uid时候的控制编辑
		$date ['edit_edit'] = $edit_edit;
		$this->load->view ( 'cmspage/cmspage_edit_view', $date );
	}
	//引用规则
	function check_page_name($page_tpl_name) {
		$page_tpl_name = trim ( $page_tpl_name );
		$record = $this->Cmspage_model->row_select ( $page_tpl_name, $this->input->get ( "page_tpl_id" ) );
		$count = strlen ( $page_tpl_name );
		if ($record) {
			$this->form_validation->set_message ( 'check_page_name', 
				'名称：[<font color=blue>' . $page_tpl_name . ']</font>,已经使用' );
			return false;
		} elseif ($count > 100) {
			$this->form_validation->set_message ( 'check_page_name', '字符长度不超过100个字符' );
			return false;
		}
		return true;
	}
	
public function bak_edit() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		//创建递归创建目录
		//create_dir ( $this->url );
		$page_tpl_id = $this->input->get ( "page_tpl_id" );
		$auto_id = $this->input->get ( "auto_id" );
		$page = $this->input->get ( "page" );
		//$campage_array = $this->Cmspage_model->selectlist ( $page_tpl_id );
		$campage_array = $this->db->get_record_by_sql ( 
			"SELECT * FROM cms_page_tpl_history WHERE auto_id='$auto_id' " );
		$date ['user_id'] = $campage_array ['user_id'];
		$date ['uid'] = $UID;
		$date ['page_tpl_id'] = $page_tpl_id;
		
		if (count ( $campage_array )) {
			$user_id = $campage_array ['user_id'];
			$author_id = $campage_array ['author_id'];
			$campage_array ['user_name'] = '';
			$campage_array ['author_name'] = '';
			$sql_u = "SELECT * FROM cms_user WHERE user_id='$user_id' ";
			$row = $this->db->get_record_by_sql ( $sql_u );
			if ($row ['user_nick']) {
				$page_name = $row ['user_nick'];
			} else {
				$page_name = $row ['user_name'];
			}
			$campage_array ['user_name'] = $page_name;
			$sql_u = "SELECT * FROM cms_user WHERE user_id='$author_id' ";
			$row = $this->db->get_record_by_sql ( $sql_u );
			if ($row ['user_nick']) {
				$page_name = $row ['user_nick'];
			} else {
				$page_name = $row ['user_name'];
			}
			$campage_array ['author_name'] = $page_name;
		}
		
		$date ['campage_array'] = $campage_array;
		$this->load->view ( 'cmspage/cmspage_bakedit_view', $date );
	}
	public function bak() {
		$date = array ();
		$page_tpl_id = $this->input->get ( 'page_tpl_id' );
		
		$sql = " WHERE page_tpl_id='$page_tpl_id'";
		
		/*分页列表*/
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$sql_count = "SELECT count(auto_id) as tot FROM cms_page_tpl_history $sql"; //取总数,用于分页
		$tcount = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $tcount [0];
		$t_first = ($page - 1) * $count_page;
		$sql = "SELECT * FROM cms_page_tpl_history $sql order by auto_id desc LIMIT $t_first,$count_page";
		$list = $this->db->get_rows_by_sql ( $sql );
		if (count ( $list )) {
			foreach ( $list as $key => $v ) {
				$user_id = $v ['user_id'];
				$list [$key] ['user_name'] = '';
				$sql_u = "SELECT user_name FROM cms_user WHERE user_id='$user_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				$list [$key] ['user_name'] = $row ['user_name'];
				$list [$key] ['history_time'] = date("Y-m-d H:i:s",$v ['history_time']);
				
			}
		
		}
		$getpageinfo = toolkit_pages_com ( $page, $t_count, modify_build_url ( array ('page' => '' ) ), 
			$count_page, 8 );
		
		$date ['t_count'] = $t_count;
		$date ['list'] = $list;
		if ($t_count != 0) {
			$date ['pagecode'] = $getpageinfo ['pagecode'];
		} else {
			$date ['pagecode'] = '';
		}
		$date ['pic_path'] = $this->url;
		$this->load->view ( 'cmspage/cmspage_bak_view', $date );
	
	}
	
	public function submit() {
		$page_tpl_id = $this->input->get ( "page_tpl_id" );
		$campage_array = $this->Cmspage_model->selectlist ( $page_tpl_id );
		$date = array ();
		$date ['page_tpl_id'] = $page_tpl_id;
		$date ['campage_array'] = $campage_array;
		/*模板*/
		$this->load->view ( 'cmspage/cmspage_submit_view', $date );
	}
	public function view() {
		$page_tpl_id = $this->input->get ( "page_tpl_id" );
		$campage_array = $this->Cmspage_model->selectlist ( $page_tpl_id );
		$act = $this->input->get ( "act" );
		$date = array ();
		$date ['page_tpl_id'] = $page_tpl_id;
		$url = base_url () . $this->url . '/' . $campage_array ['demo_pic_id'];
		$banshi = base_url () . $this->url . '/' . $campage_array ['layout_pic_id'];
		if ($act == 'a') {
			$date ['url'] = $url;
		} elseif ($act == 'b') {
			$date ['url'] = $banshi;
		}
		
		$date ['campage_array'] = $campage_array;
		$date ['act'] = $act;
		/*模板*/
		$this->load->view ( 'cmspage/cmspage_view_view', $date );
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 删除记录
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 删除cms_page_tpl表中的内容，并同时删除上传文件到$this->url中的对应图片

	*/
	public function cmsdel() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmspage_del" );
		if ($success != 1) {
			msg ( "无权限：CMS删除页面模板/cmspage_del/", "", "message" );
			safe_exit ();
		}
		
		$page_tpl_id = $this->input->get_post ( "page_tpl_id" );
		$page = $this->input->get_post ( "page" );
		$success = $this->Cmspage_model->delete ( $page_tpl_id, $this->url . "/" );
		if ($success) {
			$a ['c'] = "cmspage";
			$a ['m'] = "index";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 控制已禁用操作
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 
	*/
	public function disable() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmspage_disable" );
		if ($success != 1) {
			msg ( "无权限：CMS禁用页面模板/cmspage_disable/", "", "message" );
			safe_exit ();
		}
		
		$page_tpl_id = $this->input->get_post ( "page_tpl_id" );
		$page = $this->input->get_post ( "page" );
		$up = array ('disable' => '0' );
		$success = $this->Cmspage_model->update_disable ( $up, $page_tpl_id );
		if ($success) {
			$a ['c'] = "cmspage";
			$a ['m'] = "index";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 控制开启操作
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 
	*/
	public function enable() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmspage_enable" );
		if ($success != 1) {
			msg ( "无权限：CMS开启页面模板/cmspage_enable/", "", "message" );
			safe_exit ();
		}
		
		$page_tpl_id = $this->input->get_post ( "page_tpl_id" );
		$page = $this->input->get_post ( "page" );
		$up = array ('disable' => '1' );
		$success = $this->Cmspage_model->update_disable ( $up, $page_tpl_id );
		if ($success) {
			$a ['c'] = "cmspage";
			$a ['m'] = "index";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	}
	
	public function Upload() {
		$page = $_REQUEST ['page'];
		$id = $_REQUEST ['id'];
		
		if ($_REQUEST ['input']) {
			$input = $_REQUEST ['input']; //控制文本框内容
		} else {
			$input = "inputIMG";
		}
		$tempFile = $_FILES ['Filedata'] ['tmp_name'];
		$targetPath = $_REQUEST ['folder'] . "/"; //
		$targetFile = str_replace ( '//', '/', $targetPath ) . $_FILES ['Filedata'] ['name'];
		$file_name = $_FILES ['Filedata'] ['name'];
		
		$fileTypes = str_replace ( '*.', '', '*.jpg;*.gif;*.png;*psd;' );
		$fileTypes = str_replace ( ';', '|', $fileTypes );
		$typesArray = explode ( '|', $fileTypes ); //split
		$fileParts = pathinfo ( $_FILES ['Filedata'] ['name'] );
		if (in_array ( $fileParts ['extension'], $typesArray )) {
			$targetFile1 = $targetPath . $page . "_" . $id . substr ( $file_name, strlen ( $file_name ) - 4, 
				4 );
			move_uploaded_file ( $tempFile, $targetFile1 );
			$targetFile = @end ( explode ( '/', $targetFile1 ) );
			echo "<input id=$input name=$input type=hidden value=$targetFile>";
			echo "<img src=$targetFile1 width=50 height=50 border=0/>";
		
		} else {
			echo $page . 'page';
		}
	
	}

}







/* End of file cmspage.php */
/* Location: ./application/controllers/cmspage.php */
