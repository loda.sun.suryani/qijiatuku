<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class AdminLogList extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		//删除2小时前的临时page
		//$time_now = time ();
		//$this->db->query ( 
		//	sprintf ( "DELETE FROM cms_admin_log WHERE access_time<%s", $time_now - 3600 * 2 ) );
		//=============================删除过期的临时数据,}}end============================
		//=========列表=====================
		$page_size = 30;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE length(c) > 0";
		/*
		if ($this->input->post ( 'page_column_id' )) {
			$sql_where = sprintf ( "$sql_where AND page_column_id='%s' ", 
				intval ( $this->input->post ( 'page_column_id' ) ) );
		}*/
		if ($this->input->post ( 'user_name' )) { //根据用户名模糊搜索
			$str = str_replace ( array ("'", "\"", "’", "‘", "”", "“" ), '', 
				trim ( $this->input->post ( 'user_name' ) ) );
			$sql_where = $sql_where . sprintf ( " AND user_name like '%s%s%s' ", '%', $str, '%' );
		}
		if ($this->input->post ( 'query_str' )) {
			$str = str_replace ( array ("'", "\"", "’", "‘", "”", "“" ), '', 
				trim ( $this->input->post ( 'query_str' ) ) );
			$sql_where = $sql_where . sprintf ( " AND query_string like '%s%s%s' ", '%', $str, '%' );
		}
		//my_debug ( $sql_where );
		$sql_count = "SELECT count(*) as tot FROM cms_admin_log $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_admin_log $sql_where ORDER BY auto_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "user_id,user_name,ip_addr,access_time,c,m,query_string" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$time_p1 = date ( 'Y-m-d', $row ['access_time'] );
				$time_p2 = date ( 'H:i:s', $row ['access_time'] );
				$data [$k] ['access_time'] = "$time_p1 , <b>$time_p2</b>";
				$data [$k] ['query_string'] = str_replace ( '&', '<font color="red"><b>&</b></font>', 
					$row ['query_string'] );
			
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'adminloglist_view', $view_data );
	}
	function master_delete() {
		$record_id = $this->input->get ( "id" );
		$record_id = intval ( $record_id );
		$this->db->where ( 'auto_id', $record_id );
		$this->db->delete ( 'cms_admin_log' );
		return;
	}
	
	
function cms_grant_log() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		//删除2小时前的临时page
		//$time_now = time ();
		//$this->db->query ( 
		//	sprintf ( "DELETE FROM cms_admin_log WHERE access_time<%s", $time_now - 3600 * 2 ) );
		//=============================删除过期的临时数据,}}end============================
		//=========列表=====================
		$page_size = 20;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE 1";
		/*
		if ($this->input->post ( 'page_column_id' )) {
			$sql_where = sprintf ( "$sql_where AND page_column_id='%s' ", 
				intval ( $this->input->post ( 'page_column_id' ) ) );
		}*/
		if ($this->input->post ( 'user_name' )) { //根据用户名模糊搜索
			$str = str_replace ( array ("'", "\"", "’", "‘", "”", "“" ), '', 
				trim ( $this->input->post ( 'user_name' ) ) );
			$sql_where = $sql_where . " AND (admin_user_name like '%$str%' OR user_name like '%$str%') ";
		}
		if ($this->input->post ( 'query_str' )) {
			$str = str_replace ( array ("'", "\"", "’", "‘", "”", "“" ), '', 
				trim ( $this->input->post ( 'query_str' ) ) );
			$sql_where = $sql_where . sprintf ( " AND permission_name like '%s%s%s' ", '%', $str, '%' );
		}
		//my_debug ( $sql_where );
		$sql_count = "SELECT count(*) as tot FROM cms_grant_log $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_grant_log $sql_where ORDER BY auto_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "auto_id" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				
				$data [$k] ['操作人'] = $row ['admin_user_name'];
				$data [$k] ['被分配人'] = $row ['user_name'];
				$data [$k] ['权限'] = $row ['permission_name'];
				$data [$k] ['页面'] = $row ['page_id'];
				$data [$k] ['碎片'] = $row ['block_id'];
				$time_p1 = date ( 'Y-m-d', $row ['grant_time'] );
				$time_p2 = date ( 'H:i:s', $row ['grant_time'] );
				$data [$k] ['操作日期'] = "$time_p1 , <b>$time_p2</b>";
			
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'adminloglist_view', $view_data );
	}
}

//end.
