<?php
class WaterMark extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'wartermarker' );
		/*if (! $success) {
			msg ( "无权限: key = wartermarker", "", "message" );
			exit ();
		}*/
	}
	
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = null;
		$view_data ['main_grid'] = null;
		
		//=======================水印列表================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "";
		$sql_count = "SELECT count(*) as tot from cms_watermark $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_watermark $sql_where ORDER BY mark_id ASC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['ID'] = $row ['mark_id'];
				$data [$k] ['名称'] = $row ['mark_name'];
				$data [$k] ['水印图片'] = "<img src=\"{$row['mark_url']}\"/>";
				$data [$k] ['操作'] = "";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"edit_watermark({$row['mark_id']});return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_watermark({$row['mark_id']});return false;\"\">刪除</a>";
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$this->load->view ( 'watermark/watermark_list_view', $view_data );
	}
	
	function edit_watermark() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$mark_id = intval ( $this->input->get ( 'id' ) );
		$view_data = array ();
		$view_data ['record'] = '';
		if (! $mark_id) {
			$this->db->insert ( 'cms_watermark', 
				array ('create_time' => time (), 'is_temp' => 1 ) );
			$mark_id = $this->db->insert_id ();
			redirect ( modify_build_url ( array ('id' => $mark_id ) ) );
		}
		//=============================删除过期的临时数据,begin{{==========================
		$time_now = time ();
		//删除2小时前的临时
		$this->db->query ( 
			sprintf ( "DELETE FROM cms_watermark WHERE is_temp = 1 AND create_time<%s", 
				$time_now - 3600 * 2 ) );
		//=============================删除过期的临时数据,}}end============================
		$record = $this->db->get_record_by_field ( 'cms_watermark', 'mark_id', 
			$mark_id );
		$view_data ['record'] = $record;
		//=============上传控件====================
		//上传控件
		$mark_pic = modify_build_url ( 
			array (
					'c' => 'watermark', 
					'm' => 'upload', 
					'mark_id' => $mark_id, 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey ) );
		$pic = array (
				'element_id' => 'watermark', 
				'queue_id' => 'custom_queue', 
				'script' => $mark_pic, 
				'lable' => "false" );
		$view_data ['mark_upload'] = $this->editors->get_upload ( $pic );
		//==================================================
		//处理提交表单 更新数据
		if ($this->input->post ( 'submit' )) {
			$this->db->where ( 'mark_id', $mark_id );
			$success = $this->db->update ( 'cms_watermark', 
				array ('mark_name' => $this->input->post ( 'mark_name' ), 'is_temp' => 0 ) );
			if ($success) {
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			} else {
				msg ( "数据更新错误", modify_build_url () );
			}
		}
		$this->load->view ( 'watermark/watermark_edit_view', $view_data );
	}
	function del_watermark() {
		$mark_id = intval ( $this->input->get ( 'id' ) );
		$record_info = $this->db->get_record_by_field ( 'cms_watermark', 'mark_id', $mark_id );
		$old_mark = (FCPATH . $record_info ['mark_url']);
		
		if ($record_info ['mark_url']) {
			$old_mark = (FCPATH . $record_info ['mark_url']);
			if (file_exists ( $old_mark ) == true) {
				unlink ( $old_mark );
			}
		}
		$this->db->where ( 'mark_id', $mark_id );
		$this->db->delete ( 'cms_watermark' );
		return;
	}
	
	function upload() {
		//my_debug($_FILES);
		

		if (count ( $_FILES ) < 1) {
			my_debug ( "没有发现上传文件" );
			return;
		}
		if (! array_key_exists ( 'Filedata', $_FILES )) {
			my_debug ( "非法的上传操作" );
			return;
		}
		$file_name = $_FILES ['Filedata'] ['name'];
		$file_name_parts = explode ( '.', $file_name );
		if (count ( $file_name_parts ) < 2) {
			my_debug ( "非法的文件名" );
			return;
		}
		$len = count ( $file_name_parts );
		$file_ext_name = $file_name_parts [$len - 1];
		$file_ext_name = strtolower ( $file_ext_name );
		if (! $file_ext_name) {
			my_debug ( "非法的文件扩展名" );
			return;
		}
		if (! in_array ( $file_ext_name, array ('gif', 'jpg', 'jpeg', 'png' ) )) {
			my_debug ( "只支持这些扩展名:gif,jpg,jpeg,png" );
			return;
		}
		$mark_id = $this->input->get ( 'mark_id' );
		$file_name = $_FILES ['Filedata'] ['name'];
		$temp_name = $_FILES ['Filedata'] ['tmp_name'];
		//磁盘上的物理path
		$path = sprintf ( "%spublic/resource/%s/%s/%s/", FCPATH, "watermark", $this->uid, 
			date ( "Y/m", time () ) );
		//数据库中保存的path
		$path2 = sprintf ( "public/resource/%s/%s/%s/", "watermark", $this->uid, 
			date ( "Y/m", time () ) );
		create_dir ( $path ); //创建文件保存路径
		//如有旧文件，删除
		if ($mark_id) {
			$record = $this->db->get_record_by_field ( 'cms_watermark', 'mark_id', $mark_id );
			$old_path = '';
			if ($record ['mark_url']) {
				$old_path = (FCPATH . $record ['mark_url']);
				if (file_exists ( $old_path ) === true) {
					unlink ( $old_path );
				}
			}
		}
		$save_name = ($mark_id . "_" . time () . ".$file_ext_name");
		$targetfile = $path . $save_name;
		$targetfile1 = $path2 . $save_name;
		move_uploaded_file ( $temp_name, $targetfile );
		$this->db->where ( 'mark_id', $mark_id );
		$this->db->update ( 'cms_watermark', array ('mark_url' => $targetfile1 ) );
	}
}
