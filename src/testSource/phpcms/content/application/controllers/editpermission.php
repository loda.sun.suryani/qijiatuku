<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class EditPermission extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'cookie' );
		$this->load->library ( 'session' );
		$this->load->helper ( 'html' );
	}
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editpermission_index" );
		
		if (! $success) {
			msg ( "无权限：已经封闭/editpermission_index/", "", "message" );
			exit ();
		}
		//创建一个空的记录,进入编辑
		$record_id = $this->input->get ( "id" );
		$record_id = intval ( $record_id );
		if (! $record_id) {
			$db_ret = $this->db->insert ( "cms_permission", array ('is_temp' => 1 ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('c' => 'EditPermission', 'id' => $insert_id ) ) );
			}
		}
		
		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_permission", 'permission_id', 
			$record_id );
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		
		$view_data = array ();
		
		$view_data ['user_name'] = null;
		$view_data ['message'] = null;
		//表单验证规则
		$this->form_validation->set_rules ( 'permission_key', '键名', 
			"callback_permission_key|required" );
		$this->form_validation->set_rules ( 'permission_name', '权限名/说明', "required" );
		
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$this->db->where ( 'permission_id', $record_id );
				$this->db->update ( 'cms_permission', 
					array (
							'permission_key' => trim ( $this->input->post ( 'permission_key' ) ), 
							'permission_name' => trim ( $this->input->post ( 'permission_name' ) ), 
							'is_temp' => '0' ) );
				if ($this->db->affected_rows ()) {
					$view_data ['message'] = ("已经写入数据库." . time ());
					//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					$view_data ['message'] = ("没有更新任何内容," . microtime ());
				}
				//redirect ( site_url ( "c=formlist" ) );
				//关闭界面
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				//return;
			}
		}
		$this->load->view ( 'editpermission_view', $view_data );
	}
	
	//引用规则
	function permission_key($permission_key) {
		$permission_key = trim ( $permission_key );
		$count = $this->db->get_record_by_sql ( 
			"SELECT count(permission_id) as t_count FROM cms_permission WHERE permission_key ='$permission_key' " );
		if ($count ['t_count']) {
			$this->form_validation->set_message ( 'permission_key', 
				'您的键名：[<font color=blue>' . $permission_key . ']</font>,已经添加' );
			return false;
		}
		return true;
	}
	//添加权限并同时可以分配给用户或者角色
	function add_authorized() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editpermission_add_authorized" );
		if (! $success) {
			msg ( "无权限：添加权限并同时可以分配给用户或者角色/editpermission_add_authorized/", "", "message" );
			exit ();
		}
		
		$view_data = array ();
		$view_data ['role'] = null;
		$view_data ['user'] = null;
		$view_data ['UID'] = $UID;
		$view_data ['permission_key'] = null;
		$view_data ['permission_name'] = null;
		$view_data ['roleshow'] = null;
		$view_data ['usershow'] = null;
		
		//角色列表---- ------start--------
		$sql_where = 'is_temp<1';
		//查询角色名称
		if ('' != $this->input->post ( 'role_name' )) {
			$user_name = $this->input->post ( 'role_name' );
			$sql_where = "$sql_where AND role_name like '%$user_name%'";
		}
		$r = "SELECT * FROM cms_role  WHERE $sql_where ORDER BY role_id DESC";
		$role = $this->db->get_rows_by_sql ( $r );
		if (count ( $role )) {
			foreach ( $role as $k => $v ) {
				$role [$k] ['role_check'] = 0;
			}
		}
		//角色列表---- ------end--------
		//用户列表-------start--------------------------------
		$sql_where = ' user_disable!=1 ';
		//可以查询--------------------------------------
		$user_name = $this->input->post ( 'user_name' );
		if ('' != $user_name) {
			$user_name = $this->input->post ( 'user_name' );
			$sql_where = "$sql_where AND user_name like '%$user_name%'";
		}
		$u = "SELECT * FROM cms_user WHERE $sql_where ORDER BY user_id DESC ";
		$use_cms = $this->db->get_rows_by_sql ( $u );
		if (count ( $use_cms )) {
			foreach ( $use_cms as $k => $v ) {
				$use_cms [$k] ['check'] = 0;
			}
		}
		$view_data ['user_name'] = $user_name;
		//用户列表-------end--------------------------------
		

		$view_data ['message'] = '';
		$view_data ['role'] = $role;
		$view_data ['user'] = $use_cms; //用户列表，如果用户有权限打勾
		$formInput = array (
				'name' => 'user_p_id', 
				'id' => 'user_p_id', 
				'size' => '200', 
				'style' => 'display:none;', 
				'value' => '' );
		$role_p_id = form_input ( $formInput );
		$view_data ['form_input'] = $role_p_id;
		//角色权限集合
		$formInput_role = array (
				'name' => 'role_p_id', 
				'id' => 'role_p_id', 
				'size' => '200', 
				'style' => 'display:none;', 
				'value' => '' );
		$formInput_role = form_input ( $formInput_role );
		$view_data ['form_role'] = $formInput_role;
		$permission_key = $this->input->post ( 'permission_key' );
		$permission_name = $this->input->post ( 'permission_name' );
		//表单验证规则
		$this->form_validation->set_rules ( 'permission_key', '键名', 
			"callback_permission_key|required" );
		$this->form_validation->set_rules ( 'permission_name', '权限名/说明', "required" );
		
		//文本框
		$permission_key = $this->input->post ( 'permission_key' );
		$permission_name = $this->input->post ( 'permission_name' );
		$formInput = array (
				'name' => 'permission_key', 
				'style' => 'width:50%', 
				'id' => 'permission_key', 
				'value' => $permission_key );
		$view_data ['permission_key'] = form_input ( $formInput );
		$formInput_name = array (
				'name' => 'permission_name', 
				'id' => 'permission_name', 
				'style' => 'width:50%', 
				'value' => $permission_name );
		$view_data ['permission_name'] = form_input ( $formInput_name );
		
		//提交入库操作
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$user_p_id = $this->input->post ( 'user_p_id' );
				$role_p_id = $this->input->post ( 'role_p_id' );
				$permission_key = $this->input->post ( 'permission_key' );
				$permission_name = $this->input->post ( 'permission_name' );
				$db_ret = $this->db->insert ( "cms_permission", 
					array (
							'permission_key' => $permission_key, 
							'permission_name' => $permission_name, 
							'is_temp' => 0 ) );
				if ($db_ret) {
					$permission_id = $this->db->insert_id ();
				}
				//用户更新入库
				$user_p_id = substr ( $user_p_id, 0, strlen ( $user_p_id ) - 1 );
				if (count ( explode ( ",", $user_p_id ) ) && '' != $user_p_id) {
					foreach ( explode ( ",", $user_p_id ) as $value ) {
						$c_u_to_p = $this->db->get_record_by_sql ( 
							"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$value' AND permission_id='$permission_id'" );
						//my_debug ( $c_u_to_p ['t_count'] );
						if ($c_u_to_p ['t_count'] == 0 && '' != $value) {
							//插入用户直接权限记录
							$db_ret = $this->db->insert ( 
								"cms_user_to_permission", 
								array (
										'permission_id' => $permission_id, 
										'user_id' => $value, 
										'is_temp' => 0 ) );
						}
					
					}
				}
				//角色更新入库
				$role_p_id = substr ( $role_p_id, 0, strlen ( $role_p_id ) - 1 );
				if (count ( explode ( ",", $role_p_id ) ) && '' != $role_p_id) {
					foreach ( explode ( ",", $role_p_id ) as $value ) {
						$count = $this->db->get_record_by_sql ( 
							"SELECT count(auto_id) as t_count FROM cms_role_to_permission WHERE permission_id ='$permission_id' AND role_id='$value'" );
						if ($count ['t_count'] == 0 && '' != $value) {
							$db_ret = $this->db->insert ( "cms_role_to_permission", 
								array (
										'role_id' => $value, 
										'permission_id' => $permission_id, 
										'is_temp' => 0 ) );
						}
					}
				
				}
				//关闭界面
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				//return;
			} //run
		

		}
		
		$this->load->view ( 'addpermission_view', $view_data );
	}
	
	function user_accredit() {
		$UID = $this->session->userdata ( 'UID' );
		
		$view_data = array ();
		$view_data ['role'] = null;
		$view_data ['user'] = null;
		$view_data ['permission_key'] = null;
		$view_data ['permission_name'] = null;
		$view_data ['roleshow'] = null;
		$view_data ['usershow'] = null;
		
		$permission_id = $this->input->get_post ( 'permission_id' );
		$permission_id = intval ( $permission_id );
		$page_id = $this->input->get ( 'page_id' );
		$page_id = intval ( $page_id );
		
		//my_debug($permission_id);
		if ($permission_id) {
			$sql = "SELECT * FROM cms_permission  WHERE permission_id='$permission_id' ";
			$per = $this->db->get_record_by_sql ( $sql );
			$permission_key = $per ['permission_key'];
			$permission_name = $per ['permission_name'];
		}
		
		//文本框
		$formInput = array (
				'name' => 'permission_key', 
				'style' => 'width:50%', 
				'id' => 'permission_key', 
				'value' => $permission_key );
		$view_data ['permission_key'] = form_input ( $formInput );
		$formInput_name = array (
				'name' => 'permission_name', 
				'style' => 'width:50%', 
				'id' => 'permission_name', 
				'value' => $permission_name );
		$view_data ['permission_name'] = form_input ( $formInput_name );
		//判断是否有此权限-----递归页面列表------------start-----------
		//查找角色表
		$role = $this->db->get_rows_by_sql ( "SELECT * FROM cms_role" );
		$flag = 0;
		if (count ( $role )) {
			foreach ( $role as $v ) {
				$role_id = $v ['role_id'];
				//根据角色id和权限ID确定角色ID
				$r_to_p = $this->db->get_record_by_sql ( 
					"SELECT role_id FROM cms_role_to_permission WHERE role_id='$role_id' AND permission_id='$permission_id' limit 1" );
				if ($r_to_p ['role_id']) {
					//根据角色ID和用户ID确立是否绑定次用户上面
					$u_to_r = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as Tcount FROM cms_user_to_role WHERE role_id='$role_id' AND user_id='$UID'" );
					if ($u_to_r ['Tcount']) {
						$flag = 1;
					}
				}
			}
		}
		$sql = "SELECT count(auto_id) as Tcount FROM cms_user_to_permission  WHERE permission_id='$permission_id' AND user_id='$UID' ";
		$accdet = $this->db->get_record_by_sql ( $sql );
		$success = $accdet ['Tcount'] + $flag;
		//以上是判断是否有权限======$success>0有权限========下面是判断递归
		if ($success < 1) {
			//通过栏目去寻找"继承的权限"
			//权限检查
			$persist_record = $this->db->get_record_by_field ( "cms_page", 
				'page_id', $page_id );
			$this->load->model ( 'tree_model' );
			$column = $this->tree_model->keys;
			$page_column_id = $persist_record ['page_column_id'];
			$edit_column = $column [$page_column_id];
			//my_debug ( $edit_column );
			//字符处理
			$str = str_replace ( "['", "", $edit_column );
			$str = str_replace ( "]", "]'", $str );
			$column_array = array_filter ( explode ( "']'", $str ) );
			//my_debug ( $column_array );
			//循环array
			if (is_array ( $column_array )) {
				foreach ( $column_array as $k => $item ) {
					$edit_column_suc = "edit_column_" . $item;
					//my_debug ( $edit_column_suc );
					//认证是否有目录权限
					$check = validation_check ( $UID, 
						$edit_column_suc );
					if ($check == 1) {
						//my_debug ( $edit_column_suc );
						$success = 1;
						break;
					}
				}
			}
		}
		if ($success < 1) {
			msg ( "您无权授权此权限,因为您没有此权限(edit_page_{$page_id})", "", "message" );
			//my_debug ( $sql );
			safe_exit ();
		}
		//判断是否有此权限-----递归页面列表------------end-----------
		

		//用户列表-------start--------------------------------
		$sql_where = ' user_disable!=1 ';
		//获取已经有此权限集合
		//可以查询--------------------------------------
		if ('' != $this->input->post ( 'user_name' )) {
			$user_name = $this->input->post ( 'user_name' );
			$sql_where = "$sql_where  AND user_name like '%$user_name%'";
		}
		$u = "SELECT * FROM cms_user WHERE $sql_where  ORDER BY user_id DESC";
		$use_cms = $this->db->get_rows_by_sql ( $u );
		if (count ( $use_cms )) {
			foreach ( $use_cms as $k => $v ) {
				$use_cms [$k] ['check'] = 0;
				$use_cms [$k] ['disabled'] = '';
				$user_id = $v ['user_id'];
				//提取perm_source的组合
				$sql = "SELECT perm_source FROM cms_user_to_permission WHERE perm_source like '%[$UID]%' AND permission_id='$permission_id' AND user_id='$user_id'";
				$sources = $this->db->get_record_by_sql ( $sql );
				$perm_sources = $sources ['perm_source'];
				if ($perm_sources != '') {
					$perm_sources = $perm_sources . ",[$UID]";
				}
				//my_debug ( count ( $perm_sources ) );
				$sql = "SELECT permission_id FROM cms_user_to_permission  WHERE permission_id='$permission_id' AND user_id='$user_id' AND is_temp<1";
				$u_to_p = $this->db->get_record_by_sql ( $sql );
				if ($u_to_p ['permission_id'] == $permission_id) {
					$use_cms [$k] ['check'] = 1;
				}
				if (! count ( $perm_sources ) && $u_to_p ['permission_id'] == $permission_id) {
					$use_cms [$k] ['disabled'] = 'disabled';
				}
			}
		}
		
		$view_data ['message'] = '';
		$view_data ['user'] = $use_cms; //用户列表，如果用户有权限打勾
		

		//提交入库操作
		if ($this->input->post ( 'submitform' )) {
			//if ($this->form_validation->run ()) {
			$page_id = $this->input->get ( 'page_id' );
			$this->db->where ( 'permission_id', $permission_id );
			$this->db->update ( 'cms_permission', 
				array (
						'permission_key' => trim ( $this->input->post ( 'permission_key' ) ), 
						'permission_name' => trim ( $this->input->post ( 'permission_name' ) ), 
						'is_temp' => '0' ) );
			//插入数据库记录
			$user_p_id = trim ( $this->input->post ( 'user_p_id' ) );
			$user_p_id = substr ( $user_p_id, 0, strlen ( $user_p_id ) - 1 );
			if (count ( explode ( ",", $user_p_id ) ) && '' != $user_p_id) {
				foreach ( explode ( ",", $user_p_id ) as $value ) {
					$c_u_to_p = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$value' AND  permission_id='$permission_id'" );
					//my_debug ( $c_u_to_p );
					if ($c_u_to_p ['t_count'] == 0 && '' != $value) {
						//插入用户直接权限记录,提取perm_source的组合,获取UID组合
						$sources = $this->db->get_record_by_sql ( 
							"SELECT perm_source FROM cms_user_to_permission WHERE user_id ='$UID' AND permission_id='$permission_id'" );
						$perm_sources = $sources ['perm_source'];
						
						if ($perm_sources == '') {
							$perm_sources = "[$UID]";
						} else {
							$perm_sources = $perm_sources . ",[$UID]";
						}
						$db_ret = $this->db->insert ( "cms_user_to_permission", 
							array (
									'permission_id' => $permission_id, 
									'user_id' => $value, 
									'perm_source' => "$perm_sources", 
									'is_temp' => 0 ) );
					
					}
				
				}
			}
			//删除取消的权限选择
			$del_user_id = trim ( $this->input->post ( 'del_user_id' ) );
			$del_user_id = substr ( $del_user_id, 0, strlen ( $del_user_id ) - 1 );
			//my_debug(explode ( ",", $del_user_id ) );
			if (count ( explode ( ",", $del_user_id ) ) && '' != $del_user_id) {
				foreach ( explode ( ",", $del_user_id ) as $value ) {
					if ($value) {
						$sources = $this->db->get_record_by_sql ( 
							"SELECT perm_source FROM cms_user_to_permission WHERE user_id ='$value' AND permission_id='$permission_id'" );
						$perm_sources = $sources ['perm_source'];
						
						$c_u_to_p = $this->db->get_record_by_sql ( 
							"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$value' AND perm_source='$perm_sources' AND permission_id='$permission_id'" );
						//my_debug ( $c_u_to_p );
						if ($c_u_to_p ['t_count'] >= 1 && '' != $value) {
							$this->db->where ( 'permission_id', $permission_id );
							$this->db->where ( 'user_id', $value );
							$this->db->where ( 'perm_source', $perm_sources );
							$this->db->delete ( 'cms_user_to_permission' );
							//获取perm_source子集合权限列表并删除
							$perm_sources = $perm_sources . ",[$user_id]";
							$perm = $this->db->get_rows_by_sql ( 
								"SELECT auto_id FROM cms_user_to_permission WHERE  perm_source like '$perm_sources%' AND permission_id='$permission_id'" );
							if (count ( $perm )) {
								foreach ( $perm as $v_auto_id ) {
									$this->db->where ( 'auto_id', $v_auto_id ['auto_id'] );
									$this->db->delete ( 'cms_user_to_permission' );
								}
							
							}
							//my_debug ( $perm );
						}
					
					}
				
				}
			}
			
			//关闭界面
			

			echo "<font color=red>Success</font>";
			msg ( "", 
				site_url ( 
					"c=editpermission&m=user_accredit&page_id=$page_id&permission_id=$permission_id" ) );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//return;
		//}//run
		

		}
		
		$this->load->view ( 'management/editpermission_user_view', $view_data );
	}
	//shouquan
	function accredit() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editpermission_shouquan" );
		//my_debug($success);
		if (! $success) {
			msg ( "无权限：编辑权限并同时可以分配给用户或者角色/editpermission_shouquan/", "", "message" );
			exit ();
		}
		
		$view_data = array ();
		$userdata = $this->session->userdata ( "s_doc_id_$UID" );
		$view_data ['UID'] = $UID;
		//my_debug ( $userdata );
		$view_data ['userdata'] = $userdata;
		
		$view_data ['json_encode'] = '';
		if ($userdata) {
			$sort1 = explode ( ",", $userdata );
			$sort = array_filter ( $sort1 );
			$body = join ( ",", $sort );
			$u = "SELECT user_id,user_name FROM cms_user WHERE user_id IN($body)";
			$query = $this->db->query ( $u );
			$list = $query->result_array ();
			$ret = array ();
			if (count ( $list )) {
				foreach ( $list as $k => $v ) {
					$ret [$k] ['user_id'] = $v ["user_id"];
					$ret [$k] ['UID'] = $UID;
					$ret [$k] ['user_name'] = $v ["user_name"];
				}
			}
			//$ret = $this->sysSortArray ( $ret, "asc", "SORT_ASC" );
			//my_debug($ret);
			$view_data ['json_encode'] = json_encode ( $ret );
		}
		
		$view_data ['role'] = null;
		$view_data ['user'] = null;
		$view_data ['permission_key'] = null;
		$view_data ['permission_name'] = null;
		$view_data ['roleshow'] = null;
		$view_data ['usershow'] = null;
		
		$permission_id = $this->input->get_post ( 'permission_id' );
		$permission_id = intval ( $permission_id );
		
		if ($permission_id) {
			$sql = "SELECT * FROM cms_permission  WHERE permission_id='$permission_id' ";
			$per = $this->db->get_record_by_sql ( $sql );
			$permission_key = $per ['permission_key'];
			$permission_name = $per ['permission_name'];
		}
		//my_debug($per);
		//文本框
		$formInput = array (
				'name' => 'permission_key', 
				'style' => 'width:50%', 
				'id' => 'permission_key', 
				'value' => $permission_key );
		$view_data ['permission_key'] = form_input ( $formInput );
		$formInput_name = array (
				'name' => 'permission_name', 
				'style' => 'width:50%', 
				'id' => 'permission_name', 
				'value' => $permission_name );
		$view_data ['permission_name'] = form_input ( $formInput_name );
		
		//用户列表-------start--------------------------------
		$sql_where = ' user_disable!=1 ';
		//获取已经有此权限集合
		

		//可以查询--------------------------------------
		if ('' != $this->input->post ( 'user_name' )) {
			$user_name = $this->input->post ( 'user_name' );
			$sql_where = "$sql_where AND user_name like '%$user_name%'";
		}
		$u = "SELECT * FROM cms_user WHERE $sql_where ORDER BY user_id DESC LIMIT 20";
		$use_cms = $this->db->get_rows_by_sql ( $u );
		if (count ( $use_cms )) {
			foreach ( $use_cms as $k => $v ) {
				$use_cms [$k] ['check'] = 0;
				$user_id = $v ['user_id'];
				$sql = "SELECT permission_id FROM cms_user_to_permission  WHERE permission_id='$permission_id' AND user_id='$user_id' AND is_temp<1";
				$u_to_p = $this->db->get_record_by_sql ( $sql );
				if ($u_to_p ['permission_id'] == $permission_id) {
					$use_cms [$k] ['check'] = 1;
					//unset ( $use_cms [$k] );
				

				}
			}
		}
		
		//$js = 'id="user_id" onChange="change_page(this.value);"';
		//$u_user = form_dropdown ( 'user_id', $u_arr, 0, $js );
		//用户列表-------end--------------------------------
		

		$view_data ['message'] = '';
		//$view_data ['role'] = $role;
		$view_data ['user'] = $use_cms; //用户列表，如果用户有权限打勾
		

		//表单验证规则
		//$this->form_validation->set_rules ( 'permission_key', '用户', "callback_user_id" );
		//$this->form_validation->set_rules ( 'permission_key', '角色', "callback_role_id" );
		//提交入库操作
		if ($this->input->post ( 'submitform' )) {
			//if ($this->form_validation->run ()) {
			$page_id = $this->input->get ( 'page_id' );
			$user_p_id = $this->input->post ( 'user_id_list' ); //权限的用户ID
			/*$this->db->where ( 'permission_id', $permission_id );
			$this->db->update ( 'cms_permission', 
				array (
						'permission_key' => trim ( $this->input->post ( 'permission_key' ) ), 
						'permission_name' => trim ( $this->input->post ( 'permission_name' ) ), 
						'is_temp' => '0' ) );*/
			
			//用户更新入库
			//$user_p_id = substr ( $user_p_id, 0, strlen ( $user_p_id ) - 1 );
			if (count ( explode ( ",", $user_p_id ) ) && '' != $user_p_id) {
				foreach ( explode ( ",", $user_p_id ) as $value ) {
					$c_u_to_p = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$value' AND permission_id='$permission_id'" );
					//my_debug ( $c_u_to_p ['t_count'] );
					if ($c_u_to_p ['t_count'] == 0 && '' != $value) {
						/*权限操作日志----start*/
						$query = $this->db->query ( 
							"SELECT user_name FROM cms_user WHERE user_id ='$value'" );
						$user = $query->row_array ();
						$this->cms_grant_log ( $value, $user ['user_name'], $permission_id, 
							$permission_name, $page_id, 0 );
						/*权限操作日志----end*/
						
						//插入用户直接权限记录
						$db_ret = $this->db->insert ( 
							"cms_user_to_permission", 
							array (
									'permission_id' => $permission_id, 
									'user_id' => $value, 
									'is_temp' => 0 ) );
					
					}
					//my_debug ( $permission_id );
					if ($value) {
						$explode = explode ( ",", $user_p_id );
						$ids = array_flip ( $explode );
						$unique = array_unique ( explode ( ",", $user_p_id ) );
						
						if (in_array ( $value, $unique )) {
							//my_debug ( $ids [$value] );
							unset ( $ids [$value] );
						}
						$ids = array_flip ( $ids );
						$ids = implode ( ",", $ids );
					}
					$this->session->unset_userdata ( "s_doc_id_$UID", $ids );
				
				}
			
			}
			
			echo "<font color=red>Success</font>";
			msg ( "", 
				site_url ( "c=editpermission&m=accredit&page_id=$page_id&permission_id=$permission_id" ) );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//return;
		//}//run
		

		}
		
		$this->load->view ( 'addpermission_view', $view_data );
	}
	//给碎片批量添加权限
	function blockid_accredit() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editpermission_shouquan" );
		//my_debug($success);
		if (! $success) {
			msg ( "无权限：编辑权限并同时可以分配给用户或者角色/editpermission_shouquan/", "", "message" );
			exit ();
		}
		
		$page_id = $this->input->get ( 'page_id' );
		$view_data = array ();
		
		$view_data ['user'] = null;
		$view_data ['blockidlist'] = null;
		$view_data ['permission_id'] = null;
		$view_data ['block'] = null;
		//$page_id = $this->input->get ( 'page_id' );
		//获取提交过来的block——id
		$permission_id = array ();
		if ($this->input->post ( 'submit_form' )) {
			$block_id_list = $this->input->post ( 'block_id_list' );
			$block_id = $this->input->post ( 'block_id' );
			if (count ( $block_id )) {
				foreach ( $block_id as $id ) {
					$sql = "SELECT * FROM cms_permission  WHERE permission_key='edit_block_$id' AND is_temp<1";
					//my_debug ( $sql );
					$u_to_p = $this->db->get_record_by_sql ( $sql );
					if ($u_to_p ['permission_id']) {
						$permission_id [$id] = $u_to_p ['permission_id'];
					}
				}
			}
			//my_debug ( $permission_id );
			$u = "SELECT * FROM cms_block WHERE block_id IN($block_id_list) ";
			$block = $this->db->get_rows_by_sql ( $u );
			$view_data ['block'] = $block;
			$view_data ['blockidlist'] = $block_id_list;
		}
		if (count ( $permission_id )) {
			$view_data ['permission_id'] = json_encode ( $permission_id );
		}
		
		//用户列表-------start--------------------------------
		$sql_where = ' user_disable!=1 ';
		//获取已经有此权限集合
		

		//可以查询--------------------------------------
		if ('' != $this->input->post ( 'user_name' )) {
			$user_name = $this->input->post ( 'user_name' );
			$sql_where = "$sql_where AND user_name like '%$user_name%'";
		}
		$u = "SELECT * FROM cms_user WHERE $sql_where ORDER BY user_id DESC LIMIT 5";
		$use_cms = $this->db->get_rows_by_sql ( $u );
		if (count ( $use_cms )) {
			foreach ( $use_cms as $k => $v ) {
				$use_cms [$k] ['check'] = 0;
			}
		}
		
		//$js = 'id="user_id" onChange="change_page(this.value);"';
		//$u_user = form_dropdown ( 'user_id', $u_arr, 0, $js );
		//用户列表-------end--------------------------------
		

		$view_data ['message'] = '';
		//$view_data ['role'] = $role;
		$view_data ['user'] = $use_cms; //用户列表，如果用户有权限打勾
		

		//$list_id = $this->session->userdata ( "s_doc_id_$page_id" . $UID );
		//my_debug ( $list_id );
		

		//表单验证规则
		$this->form_validation->set_rules ( 'block_id_list', 'block_id', "required" );
		$this->form_validation->set_rules ( 'permission_id_list', '权限id', "required" );
		//提交入库操作
		

		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				
				$block_id_list = $this->input->post ( 'block_id_list' ); //block_id
				$permission_id_list = $this->input->post ( 'permission_id_list' ); //权限ID
				$user_id = $this->input->post ( 'user_id' ); //用户
				if ($permission_id_list) {
					$permission_id_list = json_decode ( $permission_id_list );
				
				}
				//my_debug ( $block_id_list );
				//my_debug ( $permission_id_list );
				

				//用户权限更新入库
				if ($permission_id_list) {
					foreach ( $permission_id_list as $block_id => $value ) {
						if (count ( $user_id )) { //循环用户ID
							foreach ( $user_id as $u ) {
								if ('' != $value && '' != $u) {
									$c_u_to_p = $this->db->get_record_by_sql ( 
										"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$u' AND permission_id='$value'" );
									if ($c_u_to_p ['t_count'] == 0) {
										/*权限操作日志----start*/
										$query = $this->db->query ( 
											"SELECT user_name FROM cms_user WHERE user_id ='$u'" );
										$user = $query->row_array ();
										$query = $this->db->query ( 
											"SELECT permission_name FROM cms_permission WHERE permission_id ='$value'" );
										$psion = $query->row_array ();
										$this->cms_grant_log ( $u, $user ['user_name'], $value, 
											$psion ['permission_name'], $page_id, $block_id );
										/*权限操作日志----end*/
										//插入用户直接权限记录
										$db_ret = $this->db->insert ( 
											"cms_user_to_permission", 
											array (
													'permission_id' => $value, 
													'user_id' => $u, 
													'is_temp' => 0 ) );
									
									}
								}
							
							}
						
						}
						//my_debug ( $block_id );
						//循环获取permission_id中键名block_id，一旦$block_id已经存在就直接删除session
						if ($block_id) {
							$explode = explode ( ",", $block_id_list );
							$ids = array_flip ( $explode );
							//$unique = array_unique ( explode ( ",", $block_id_list ) );
							if (in_array ( $block_id, $ids )) {
								//my_debug ( $ids [$value] );
								unset ( $ids [$block_id] );
							}
							$ids = array_flip ( $ids );
							$ids = implode ( ",", $ids );
						}
						$this->session->unset_userdata ( "s_doc_id_$page_id" . $UID, $ids );
					
					}
				
				}
				
				echo "<font color=red>Success</font>";
				msg ( "", site_url ( "c=createpage&page_id=$page_id" ) );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				//return;
			} //run
		

		}
		
		$this->load->view ( 'blockid_accredit_view', $view_data );
	}
	
	public function ajax_callback() {
		$view_data = array ();
		$user_id = $this->input->get_post ( 'user_id' ); //
		$sql_where = '';
		if ('' != $user_id) {
			$explode = explode ( "|", $user_id );
			$id = array_filter ( $explode );
			$id = join ( ",", $id );
			$sql_where = " WHERE user_id IN ($id) AND user_disable!=1";
			$u = "SELECT * FROM cms_user  $sql_where";
			$use_cms = $this->db->get_rows_by_sql ( $u );
			$view_data ["user"] = $use_cms;
		} else {
			$view_data ["user"] = null;
		}
		
		$content = $this->load->view ( 'management/ajax_callback_view', $view_data, true );
		echo $content;
	}
	public function change_database_ajax() {
		$view_data = array ();
		$sql_where = '';
		if ('' != $this->input->get_post ( 'user_name' )) {
			$user_name = $this->input->get_post ( 'user_name' );
			$sql_where = " WHERE user_name like '%$user_name%'  AND user_disable!=1";
		}
		$u = "SELECT * FROM cms_user  $sql_where ORDER BY user_id DESC LIMIT 5";
		$use_cms = $this->db->get_rows_by_sql ( $u );
		$view_data ["user"] = $use_cms;
		$content = $this->load->view ( 'management/change_database_ajax_view', $view_data, true );
		echo $content;
	}
	public function sesssion() {
		$docID = $this->input->get ( 'user_id' );
		$block_id = $this->input->get ( 'UID' );
		$list_id = $this->session->userdata ( "s_doc_id_$block_id" );
		$ids = trim ( $list_id . "," . $docID );
		$ids = trim ( $list_id . "," . $docID, "," );
		if ($ids) {
			$explode = explode ( ",", $ids );
			$ids = array_flip ( $explode );
			
			//my_debug($explode);
			//$unique = array_unique ( explode ( ",", $ids ) );
			//if (in_array ( $docID, $explode )) {
			//	unset ( $ids [$docID] );
			//}
			$ids = array_flip ( 
				$ids );
			$ids = implode ( ",", $ids );
		}
		//$s_doc_id=json_encode(array_filter($a[$block_id]));
		$this->session->set_userdata ( "s_doc_id_$block_id", $ids );
		//my_debug($ids);
		

		echo "由于调用API接口，反应速度特慢，请耐心等待.....";
		echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
	public function sesssion_del() {
		$docID = $this->input->get ( 'user_id' );
		$block_id = $this->input->get ( 'UID' );
		$list_id = $this->session->userdata ( "s_doc_id_$block_id" );
		if ($list_id) {
			$explode = explode ( ",", $list_id );
			$ids = array_flip ( $explode );
			$unique = array_unique ( explode ( ",", $list_id ) );
			if (in_array ( $docID, $unique )) {
				unset ( $ids [$docID] );
			}
			$ids = array_flip ( $ids );
			$ids = implode ( ",", $ids );
		}
		//$s_doc_id=json_encode(array_filter($a[$block_id]));
		$this->session->set_userdata ( "s_doc_id_$block_id", $ids );
		
		echo "由于调用API接口，反应速度特慢，请耐心等待.....";
		echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
	
	public function perssion_del() {
		$permission_id = $this->input->get ( 'permission_id' );
		$page_id = $this->input->get ( 'page_id' );
		$user_id = $this->input->get ( 'user_id' );
		
		/*权限操作日志----start*/
		$query = $this->db->query ( "SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
		$user = $query->row_array ();
		$permission_name = "取消此权限";
		if ($permission_id) {
			$query = $this->db->query ( 
				"SELECT permission_name FROM cms_permission WHERE permission_id ='$permission_id'" );
			$psion = $query->row_array ();
			if (isset ( $psion ['permission_name'] )) {
				$permission_name = "取消此权限" . $psion ['permission_name'];
			}
		
		}
		
		$this->cms_grant_log ( $permission_id, $user ['user_name'], $permission_id, $permission_name, 
			$page_id, 0 );
		/*权限操作日志----end*/
		
		$this->db->where ( "permission_id", $permission_id );
		$this->db->where ( "user_id", $user_id );
		$this->db->delete ( "cms_user_to_permission" );
		msg ( '', "index.php?c=editpermission&m=accredit&page_id=$page_id&permission_id=$permission_id" );
		//echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
}


//end.
