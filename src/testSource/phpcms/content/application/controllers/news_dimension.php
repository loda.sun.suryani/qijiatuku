<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class News_dimension extends MY_Controller {

	function __construct () {
		parent::__construct() ;
		$this->load->library ( 'form_validation' );//表单验证类
		$this->load->library ( 'datagrid' );//文本控件
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' );//session类
		$this->load->helper ('pagenav');//分页类
		$this->load->helper ('security');
	}
	
	function index () {
	
		$UID = $this->session->userdata('UID');
		/*$success = validation_check($UID,'news_dimension_index');//权限key = news_dimension_index
		if ( !$success) {
			msg ("无权限:维度管理 key = news_dimension_index","","message");
			exit ();
		}*/
		
		$view_data = array ();
		$view_data['main_grid'] = '';
		$view_data['pages_nav'] = '';
		

		//===============List Begin=====
		$page_size = 10 ; //每页显示数据条数
		$total_num = 0 ; //总页数
		$page_num = $this->input->post('page_num'); //当前页
		if( $page_num <1 ) {
			$page_num = 1 ;
		}
		
		$sql_where = 'WHERE 1 = 1' ;
		if( $this->input->post('dimension_name') ) {//搜索维度
			$dimension_name = trim($this->input->post('dimension_name'));
				$sql_where = "$sql_where AND dimension_name LIKE '%$dimension_name%' ";
		}
		
		$sql_count = "SELECT  count(*) as tot from news_dimension $sql_where";
		$row = $this->db->get_record_by_sql ($sql_count,'num');
		$total_num = $row[0] ;
		
		$page_obj = new PageNav ( $page_size,$total_num,$page_num,10,2);
		//参数($page_size,$tatal_num,$page_num,$page_size,$type)
		$view_data['pages_nav'] = $page_obj->show_pages ();
		
		$select_limit_start = ($page_num - 1)*$page_size ;
		$sql = "SELECT*FROM news_dimension $sql_where ORDER BY dimension_id asc";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ($sql);
		$field_list = trim ('dimension_id','dimension_name');
		$field_arr = null ;
		if ($field_list) {
			$field_arr = explode (',',$field_list);
			$field_arr = array_flip($field_arr);
		}
		if ( count ($data)) {
			foreach ( $data as $k => $row) {
				if ( $field_arr ) {
				$data[$k] = array_intersect_key ( $row,$field_arr);
				//使用键名比较计算数组的交集，只返回$row 两数组的交集
				}
				$dimension_id = $row ['dimension_id'];
				$data [$k] ['delete'] = sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array (
								'c' => "news_dimension", 
								'm' => "dimension_del", 
								'did' => $row ['dimension_id'],
								'page_num' => $page_num ) ) );
				$data [$k]['编辑'] = "<button id = authorized_$dimension_id class = 'td_p' onclick = 'authorized($dimension_id);return false;'>编辑</button>";
				$data [$k]['查看子类'] = "<button id = taglist_$dimension_id class ='td_p' onclick = 'taglist($dimension_id);return false;'>查看子类</button>";			
			}
			$this->datagrid->reset ();
			$view_data['main_grid'] = $this->datagrid->build ('datagrid',$data,true);
		}	
		$this->load->view ( 'tuku/news_dimension_view',$view_data );
	}
	
	function dimension_del () {
		$UID = $this->session->userdata( 'UID' );
		$success = validation_check ($UID,'dimension_del' );
		if ( !$success ) {
			msg ("无权限:删除维度 key = dimension_del","","message" );
			exit ();
		}
		
		$record_id = $this->input->get( 'did' );
		$record_id = intval ( $record_id );
		$page_num = $this->input->get( 'page_num' );
		
		$this->db->where ( 'dimension_id',$record_id );
		$success_d = $this->db->delete ('news_dimension');
		$this->db->where ('tag_dimension_id',$record_id);
		$success_t = $this->db->delete ('news_tag');
		if ( $success_d & $success_t) {
			msg ( "",
			modify_build_url ( array ('c' => 'news_dimension','m' => 'index','page_num'=>$page_num)));
		}
	}
	//增加 ,编辑维度
	function dimension_edit () {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID,'dimension_del' );
		if ( !$success ) {
			msg ( "无权限:操作维度 key = dimension_edit","","message" );
			exit () ;
		}*/
		
		$record_id = $this->input->get( 'did' );
		$record_id = intval ( $record_id );
		$view_data = array ();
		$view_data ['message'] = null ;

		$this->form_validation->set_rules ( 'dimension_name','维度名','callback_dimension_name|required');//验证表单
		
		$this->db->where( 'dimension_id',$record_id );
		$record_content = $this->db->get_record_by_field ( 'news_dimension','dimension_id',$record_id );
		
		if ( $record_content ) {
			$this->defaults = $record_content ;
			if ( $this->input->post( 'submitform' ) ) {
				if ( $this->form_validation->run () ) {
					$data = array ( 'dimension_name'=>strip_tags($this->input->post( 'dimension_name' )) );
					$this->db->where( 'dimension_id',$record_id );
					$this->db->update( 'news_dimension',$data );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}else{
			if ( $this->input->post( 'submitform' ) ) {
				if ( $this->form_validation->run () ) {
					$data = array ( 'dimension_name'=>strip_tags($this->input->post ( 'dimension_name' )) );
					$this->db->insert ( 'news_dimension',$data );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}
		$this->load->view ( 'tuku/news_dimension_edit_view',$view_data);
	}
	//子类列表
	function tag_list () {
		$UID = $this->session->userdata( 'UID' );
		/*$success = validation_check ( $UID,'tag_list');
		if ( !$success ) {
			msg ( "无权限: 查看子类列表 key = tag_list","","message");
			exit () ;
		}*/
		
		$record_id = $this->input->get( 'did' );
		$record_id = intval ( $record_id );
		
		$view_data = array () ;
		$view_data['main_grid'] = '' ; 
		$view_data['page_nav'] = null;

		//==================分页============
		$page_size = 15 ;
		$total_num = 0 ;	
		$page_num = $this->input->post ('page_num');
		if ( $page_num < 1 ) {
			$page_num = 1 ;
		}
		
		$sql_where = "WHERE tag_dimension_id = $record_id";
		if ( $this->input->post( 'tag_name' )) {
			$tag_name = trim($this->input->post( 'tag_name' ));
			$sql_where = "$sql_where AND tag_name LIKE '%$tag_name%'";
		}
		
		$sql_count = "SELECT count(*) as tot FROM news_tag $sql_where ";
		$row = $this->db->get_record_by_sql($sql_count,'num');
		$total_num = $row[0];
		
		$page_obj = new PageNav ( $page_size,$total_num,$page_num,10,2 ) ;
		
		$view_data['pages_nav'] = $page_obj->show_pages();
		
		$select_limit_start = ($page_num - 1)*$page_size;
		$sql = "SELECT*FROM news_tag $sql_where ORDER BY tag_id asc ";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim("tag_dimension_id,tag_id,tag_name");
		$field_arr = null ;
		if ( $field_list ) {
			$field_arr = explode (',',$field_list) ;
			$field_arr = array_flip ( $field_arr );
		}
		if (count($data)) {
			foreach ($data as $k => $row) {
				if ( $field_arr ) {
					$data[$k] = array_intersect_key ($row,$field_arr);
				}
				$tag_id = $row['tag_id'];
				$dimension_id = $row['tag_dimension_id'] ;
				$data [$k] ['delete'] = sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array (
								'c' => "news_dimension", 
								'm' => "tag_del", 
								'tid' => $row ['tag_id'],
								'did' => $row ['tag_dimension_id'],
								'page_num' => $page_num ) ) );
				$data [$k] ['编辑'] = " <button id='authorized_$tag_id' class='td_p' onclick='authorized($tag_id,$record_id);return false;'>编辑</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ('tuku/news_tag_list_view',$view_data);
	}
	function tag_del () {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID,'tag_del' );
		if ( !$success ) {
			msg ( "无权限:删除tag key = tag_del","","message" );
			exit () ;
		}
		$record_id = $this->input->get('tid');
		$record_id = intval ($record_id);
		$dimension_id = $this->input->get('did');
		$dimension_id = intval ($dimension_id);
		$page_num = $this->input->get('page_num');
		
		$this->db->where('tag_id',$record_id);
		$success = $this->db->delete('news_tag');
		if ( $success ) {
			msg ("",
				modify_build_url ( array ( 'c' => 'news_dimension',
										'm' => 'tag_list',
										'page_num' => $page_num,
										'did' => $dimension_id ) ) );
	
		}
		
	
	}
		
	function tag_edit () {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID,'tag_edit' );
		if ( !$success ) {
			msg ( "无权限:操作维度 key = tag_edit","","message" );
			exit () ;
		}*/
		
		$record_id = $this->input->get( 'tid' );
		$record_id = intval ( $record_id );
		$dimension_id = $this->input->get ('did');
		$dimension_id = intval ($dimension_id);
		$view_data = array ();
		$view_data ['message'] = null ;
		
		$this->form_validation->set_rules ( 'tag_name','tag名','callback_tag_name|required');//验证表单
		
		$this->db->where( 'tag_id',$record_id );
		$record_content = $this->db->get_record_by_field ( 'news_tag','tag_id',$record_id );
		if ( $record_content ) {
			$this->defaults = $record_content ;
			if ( $this->input->post( 'submitform' ) ) {
				if ( $this->form_validation->run () ) {
					$data = array ( 'tag_name'=>strip_tags($this->input->post( 'tag_name' )) );
					$this->db->where( 'tag_id',$record_id );
					$this->db->update( 'news_tag',$data );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}else{
			if ( $this->input->post( 'submitform' ) ) {
				if ( $this->form_validation->run () ) {
					$data = array ( 'tag_name'=> strip_tags($this->input->post ( 'tag_name' )),
									'tag_dimension_id'=> $dimension_id);
					$this->db->insert ( 'news_tag',$data );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";		
				}
			}
		}
		$this->load->view( 'tuku/news_tag_edit_view',$view_data);
	}
	
	//表单验证规则
	
	function dimension_name($dimension_name) {
			$dimension_name = strip_tags($dimension_name);
			$dimension_name = trim ( $dimension_name );
			$count = $this->db->get_record_by_sql ( 
				"SELECT count(dimension_id) as t_count FROM news_dimension WHERE dimension_name ='$dimension_name' " );
			if ($count ['t_count']) {
				$this->form_validation->set_message ( 'dimension_name', 
					'维度名：[<font color=blue>' . $dimension_name . ']</font>,已经使用' );
				return false;
			}
			return true;
	}
	
	function tag_name ($tag_name) {
			$tag_name = strip_tags($tag_name);
			$tag_name = trim ( $tag_name );
			$tag_dimension_id = $this->input->get('did');
			$count = $this->db->get_record_by_sql ( 
				"SELECT count(tag_id) as t_count FROM news_tag WHERE tag_name ='$tag_name'AND tag_dimension_id = '$tag_dimension_id' " );
			if ($count ['t_count']) {
				$this->form_validation->set_message ( 'tag_name', 
					'tag名：[<font color=blue>' . $tag_name . ']</font>,已经使用' );
				return false;
			}
			return true;
	}

}
