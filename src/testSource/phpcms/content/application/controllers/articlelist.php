<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class ArticleList extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE is_temp<1";
		if ($this->input->post ( 'article_title' )) {
			$sql_where = sprintf ( "$sql_where AND article_title like '%s%s%s' ", '%', 
				$this->input->post ( 'article_title' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM news_article $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		if ($page_num > ceil ( $total_num / $page_size ) && $total_num != 0) {
			$page_num = ceil ( $total_num / $page_size );
		}
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM news_article $sql_where ORDER BY article_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "article_title,create_time,article_tags" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$article_id = $row ['article_id'];
				$tmp = date ( "Y-m-d", $row ['create_time'] );
				$tmp .= "<font color=\"red\">";
				$tmp .= date ( " H:i:s", $row ['create_time'] );
				$tmp .= "</font>";
				$data [$k] ['create_time'] = $tmp;
				
				$tag_name = '';
				$img_tags = str_replace ( "c", "", $row ['article_tags'] );
				if (! empty ( $img_tags )) {
					$tag = "SELECT tag_name FROM news_tag WHERE tag_id IN($img_tags)"; //取总数,用于分页
					$name = $this->db->get_rows_by_sql ( $tag );
					if (count ( $name )) {
						foreach ( $name as $n ) {
							$tag_name .= $n ['tag_name'] . ",";
						}
					
					}
					$data [$k] ['article_tags'] = substr ( $tag_name, 0, 
						strlen ( $tag_name ) - 1 );
				} else {
					$data [$k] ['article_tags'] = null;
				}
				
				$data [$k] ['编辑'] = "<a href = \"javascript void(0)\" onclick=\"addpermi({$row['article_id']});return false;\">编辑</a>";
				$data [$k] ['delete'] = "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}masterdel({$row['article_id']});return false;\"\">刪除</a>";
			
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'article/articlelist_view', $view_data );
	}
	function add() {
		//创建一个空的记录,进入编辑
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$record_id = intval ( $this->input->get ( "id" ) );
		if (! $record_id) {
			$db_ret = $this->db->insert ( "news_article", 
				array ('is_temp' => 1, 'create_time' => time () ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( 
					modify_build_url ( array ('c' => 'articlelist', 'id' => $insert_id ) ) );
			}
		}
		//=============================删除过期的临时数据,begin{{==========================
		$time = time () - 3600 * 2;
		//删除2小时前的临时
		$thumb_sql = "select article_thumb FROM news_article WHERE  length(article_thumb)>0 AND is_temp = 1 AND create_time < $time";
		$thumb = $this->db->get_rows_by_sql ( $thumb_sql );
		foreach ( $thumb as $k => $v ) {
			if (file_exists ( FCPATH . $v ['article_thumb'] ) === true) {
				unlink ( FCPATH . $v ['article_thumb'] );
			}
		}
		$this->db->query ( 
			sprintf ( "DELETE FROM news_article WHERE is_temp = 1 AND create_time<%s", $time ) );
		//=============================删除过期的临时数据,}}end============================
		
		//从数据库中取出该记录
		$view_data = array ();
		$persist_record = $this->db->get_record_by_field ( "news_article", 'article_id', 
			$record_id );
		$view_data ['row'] = $persist_record;
		$view_data ['message'] = null;
		//表单验证规则
		$this->form_validation->set_rules ( 'article_title', '文章标题', "required" );
		$this->form_validation->set_rules ( 'article_description', '文章简介', "required" );
		if ($this->input->post ( 'submit' )) {
			if ($this->form_validation->run ()) {
				$this->db->where ( 'article_id', $record_id );
				$this->db->update ( 'news_article', 
					array (
							'article_title' => strip_tags ( 
								trim ( $this->input->post ( 'article_title' ) ) ), 
							'article_author_name' => strip_tags ( 
								trim ( $this->input->post ( 'article_author_name' ) ) ), 
							'article_source' => trim ( 
								$this->input->post ( 'article_source' ) ), 
							'article_editor' => strip_tags ( 
								trim ( $this->input->post ( 'article_editor' ) ) ), 
							'article_description' => trim ( 
								$this->input->post ( 'article_description' ) ), 
							'article_content' => trim ( 
								$this->input->post ( 'article_content' ) ), 
							'article_tips' => trim ( $this->input->post ( 'article_tips' ) ), 
							'publish_time' => time (), 
							'modify_time' => time (), 
							'article_author_id' => $UID, 
							'article_tags' => trim ( $this->input->post ( 'article_tags' ) ), 
							'is_temp' => '0' ) );
				if ($this->db->affected_rows ()) {
					$view_data ['message'] = ("已经写入数据库." . time ());
				} else {
					$view_data ['message'] = ("没有更新任何内容," . microtime ());
				}
				//redirect ( site_url ( "c=formlist" ) );
				//关闭界面
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		
		//---选择器=-控制分类-
		$ab_tags_array = explode ( ",", 
			str_replace ( "c", "", $persist_record ['article_tags'] ) );
		$this->load->model ( 'tag_model' );
		$data = null;
		$data .= $this->tag_model->build_tag_select ( 'tagselector', array (16 ), 
			! empty ( $ab_tags_array ) ? $ab_tags_array : null, 'article_tags' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;} 
		.tag_dimen{color:blue;}
		#tagselector {border:1px solid #f0f0f0;padding:6px;}
		#div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
		#span{border:1px solid #909090;margin:2px;padding:1px;}
		</style>";
		$view_data ['data_tag'] = $data;
		
		//上传控件
		$article_pic = modify_build_url ( 
			array (
					'c' => 'articlelist', 
					'm' => 'upload', 
					'article_id' => $record_id, 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'input' => 'article_pic' ) );
		$pic = array (
				'element_id' => 'article_pic', 
				'queue_id' => 'custom_queue', 
				'script' => $article_pic, 
				'lable' => "false" );
		$view_data ['article_picture'] = $this->editors->get_upload ( $pic );
		$this->load->view ( 'article/editarticle_view', $view_data );
	}
	function masterdel() {
		$record_id = $this->input->get ( "id" );
		$page_num = $this->input->get ( "page_num" );
		$record_id = intval ( $record_id );
		$thumb = $this->db->get_record_by_sql ( 
			"select*from news_article where length(article_thumb)>0 AND article_id = $record_id" );
		if ($thumb ['article_thumb']) {
			if (file_exists ( FCPATH . $thumb ['article_thumb'] )) {
				unlink ( FCPATH . $thumb ['article_thumb'] );
			}
		}
		$this->db->where ( 'article_id', $record_id );
		$success = $this->db->delete ( 'news_article' );
		/*if ($success) {
			msg ( "", 
				modify_build_url ( 
					array ('c' => 'articlelist', 'm' => 'index', 'page_num' => $page_num ) ) );
		
		}*/
		return;
	}
	function dao() {
		$UID = $this->session->userdata ( 'UID' );
		$news = $this->load->database ( 'news', TRUE );
		
		$yi = $news->get_rows_by_sql ( "select * from cms_tagcategory" );
		if (count ( $yi )) {
			foreach ( $yi as $k => $vm ) {
				$category_id = $vm ['tagcategory_id'];
				$article_tags = $vm ['id'];
				$persist = $news->get_rows_by_sql ( 
					"select * from cms_news_info_content where category_id=$category_id" );
				foreach ( $persist as $k => $v ) {
					$content_id = $v ['content_id'];
					$p = $news->get_record_by_field ( "cms_news_info_content_expand", 
						'content_id', $content_id );
					$info_content = $p ['info_content'];
					$info_summary = $p ['info_summary'];
					$data = array (
							'article_title' => $v ['info_title'], 
							'info_summary' => $info_summary, 
							'article_content' => $info_content, 
							'article_author_id' => $UID, 
							'article_tags' => "c" . $article_tags . "c", 
							'publish_time' => time (), 
							'create_time' => time (), 
							'is_temp' => 0 );
					if ($info_content) {
						$ret = $this->db->insert ( "news_article", $data );
						//my_debug ( $data );
					}
				
				}
			
			}
		}
	}
	
	function upload() {
		if (count ( $_FILES ) < 1) {
			my_debug ( "没有发现上传文件" );
			return;
		}
		if (! array_key_exists ( 'Filedata', $_FILES )) {
			my_debug ( "非法的上传操作" );
			return;
		}
		$file_name = $_FILES ['Filedata'] ['name'];
		$file_name_parts = explode ( '.', $file_name );
		if (count ( $file_name_parts ) < 2) {
			my_debug ( "非法的文件名" );
			return;
		}
		$len = count ( $file_name_parts );
		$file_ext_name = $file_name_parts [$len - 1];
		$file_ext_name = strtolower ( $file_ext_name );
		if (! $file_ext_name) {
			my_debug ( "非法的文件扩展名" );
			return;
		}
		if (! in_array ( $file_ext_name, array ('jpg', 'jpeg', 'png' ) )) {
			my_debug ( "只支持这些扩展名:jpg,jpeg,png" );
			return;
		}
		$article_id = $this->input->get ( 'article_id' );
		$file_name = $_FILES ['Filedata'] ['name'];
		$temp_name = $_FILES ['Filedata'] ['tmp_name'];
		$type = $this->input->get ( 'input' );
		//确定保存的文件路径
		if ($this->input->get ( 'input' )) {
			//磁盘上的物理path
			$path = sprintf ( "%spublic/resource/%s/%s/%s/", FCPATH, $type, 
				$this->uid, date ( "Y/m", time () ) );
			//数据库中保存的path
			$path2 = sprintf ( "public/resource/%s/%s/%s/", $type, $this->uid, 
				date ( "Y/m", time () ) );
		}
		create_dir ( $path ); //创建文件保存路径
		//如有旧文件，删除
		if ($article_id) {
			$record = $this->db->get_record_by_field ( 'news_article', 'article_id', $article_id );
			$old_path = '';
			if ($record ['article_thumb']) {
				$old_path = (FCPATH . $record ['article_thumb']);
			}
			if (file_exists ( $old_path ) === true) {
				unlink ( $old_path );
			}
		}
		
		$save_name = ($article_id . "_" . time () . ".$file_ext_name");
		$targetfile = $path . $save_name;
		$targetfile1 = $path2 . $save_name;
		$path_thumb = $path2 . ($article_id . "_" . time () . "_000.jpg");
		//my_debug($path_thumb);
		$is_copied = move_uploaded_file ( $temp_name, $targetfile );
		if ($is_copied) {
			if (in_array ( $file_ext_name, array ('jpg', 'png', 'jpeg' ) )) {
				//生成缩略图
				# Constants
				define ( "THUMB_MAX_WIDTH", 150 );
				define ( "THUMB_MAX_HEIGHT", 150 );
				# Get image location
				$image_path = ($path . $save_name);
				# Load image
				$img = null;
				$ext = strtolower ( end ( explode ( '.', $image_path ) ) );
				$img_type = exif_imagetype ( $image_path );
				if ($img_type == IMAGETYPE_JPEG) {
					$img = imagecreatefromjpeg ( $image_path );
				} else if ($img_type == IMAGETYPE_PNG) {
					$img = imagecreatefrompng ( $image_path );
					# Only if your version of GD includes GIF support
				} else if ($img_type == IMAGETYPE_GIF) {
					$img = imagecreatefromgif ( $image_path );
				}
				# If an image was successfully loaded, test the image for size
				if ($img) {
					# Get image size and scale ratio
					$width = imagesx ( $img );
					$height = imagesy ( $img );
					$scale = min ( THUMB_MAX_WIDTH / $width, THUMB_MAX_HEIGHT / $height );
					# If the image is larger than the max shrink it
					if ($scale < 1) {
						$new_width = floor ( $scale * $width );
						$new_height = floor ( $scale * $height );
						
						# Create a new temporary image
						$tmp_img = imagecreatetruecolor ( 
							$new_width, $new_height );
						
						# Copy and resize old image into new image
						imagecopyresized ( $tmp_img, $img, 0, 0, 
							0, 0, $new_width, $new_height, $width, $height );
						imagedestroy ( $img );
						$img = $tmp_img;
						//my_debug ( $path . $path_thumb );
						if ($img) {
							touch ( FCPATH . $path_thumb );
							//imageFilter ( $img, IMG_FILTER_GRAYSCALE );//变为类度图像
							imagejpeg ( $img, 
								FCPATH . $path_thumb, 50 );
						}
						imagedestroy ( $img );
					}
				}
			} else {
				$path_thumb = null;
			}
			if ($path_thumb) {
				if (file_exists ( FCPATH . $path_thumb ) === true) {
				
				} else {
					rename ( FCPATH . $targetfile1, FCPATH . $path_thumb );
					$path_thumb = $path_thumb;
				}
			} else {
				$path_thumb = null;
			}
			if (file_exists ( FCPATH . $targetfile1 ) === true) {
				unlink ( FCPATH . $targetfile1 );
			}
			$this->db->where ( 'article_id', $article_id );
			$this->db->update ( 'news_article', array ('article_thumb' => $path_thumb ) );
			echo "<input id=$type name=$type type=hidden value=$path_thumb>";
			echo "<img src=$path_thumb  border=0/>";
		}
	}
}
