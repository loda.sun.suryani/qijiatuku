<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Cmsblock extends MY_Controller {
	
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'cookie' );
		$this->load->helper ( 'url' );
		$this->load->library ( 'session' );
		$this->load->helper ( "toolkit" );
		$this->load->library ( 'editors' );
		$this->load->model ( 'Cmsblock_model' );
		$this->load->database ();
		//装载表单
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_message ( 'required', '信息不能为空' );
		$this->form_validation->set_message ( 'numeric', '必须输入数字or选择分类' );
		$this->url = "public/uploadpic/";
	
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 显示
	 * Author : aothor
	 * Date : 2012.01.16
	 * History : 显示cms_block_tpl表中内容列表带分页
	*/
	public function index() {
		
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cmsblock_index' );
		if ($success != 1) {
			msg ( "无权限：碎片模版显示/key=cmsblock_index/", "", "message" );
			safe_exit ();
		}
		
		//页面分页功能
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		/*统计记录总数*/
		$t_count = $this->Cmsblock_model->row_array ();
		$t_first = ($page - 1) * $count_page;
		/*获取记录集合*/
		$list = $this->Cmsblock_model->result_array ( $t_first, $count_page );
		if (count ( $list )) {
			foreach ( $list as $key => $v ) {
				$user_id = $v ['user_id'];
				$author_id = $v ['author_id'];
				$list [$key] ['user_name'] = '';
				$list [$key] ['author_name'] = '';
				$sql_u = "SELECT * FROM cms_user WHERE user_id='$user_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				if ($row ['user_nick']) {
					$page_name = $row ['user_nick'];
				} else {
					$page_name = $row ['user_name'];
				}
				$list [$key] ['user_name'] = $page_name;
				$sql_u = "SELECT * FROM cms_user WHERE user_id='$author_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				if ($row ['user_nick']) {
					$page_name = $row ['user_nick'];
				} else {
					$page_name = $row ['user_name'];
				}
				$list [$key] ['author_name'] = $page_name;
			}
		
		}
		$a ['c'] = "cmsblock";
		$a ['m'] = "index";
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		// print_r(url_get("page"));
		

		/*文本框*/
		$input = array ('name' => 'block_tpl_name', 'id' => 'block_tpl_name', 'value' => '' );
		$date ['block_tpl_name_from'] = form_input ( $input );
		/*状态select选择框子*/
		$control_array = array ('all' => '全部', '1' => '已开启', '2' => '已使用', '0' => '已禁用' );
		$control_from = form_dropdown ( 'control', $control_array, 'all' );
		$date ['control_from'] = $control_from;
		/*模板select选择框子*/
		$category = $this->Cmsblock_model->tpl_category_array (); /*模板分类*/
		$tpl_category_id_from = form_dropdown ( 'tpl_category_id', $category, 0 );
		$date ['tpl_category_id_from'] = $tpl_category_id_from;
		/*记录数select选择框子,0代码默认显示第一个*/
		$number = $this->Cmsblock_model->tpl_category_number (); /*记录数*/
		$number_from = form_dropdown ( 'number', $number, 'all' );
		$date ['number_from'] = $number_from;
		
		/*内容输出到页面*/
		
		$date ['t_count'] = $t_count; //统计
		$date ['list'] = $list;
		
		$date ['page'] = $page;
		$edit_edit = validation_check ( $UID, 'cms_block_eidtor' ); //控制当用户user_id不等于自己的uid时候的控制编辑
		$date ['edit_edit'] = $edit_edit;
		$date ['uid'] = $UID;
		$date ['page_tpl_name'] = '';
		$date ['type'] = '';
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		$date ['pic_path'] = $this->url;
		/*模板*/
		$this->load->view ( 'cmsblock/cmsblock_view', $date );
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 查询显示
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 根据条件查询显示cms_block_tpl表中内容列表带分页
	*/
	public function block_search() {
		$UID = $this->session->userdata ( 'UID' );
		$block_tpl_name = trim ( $this->input->get_post ( "block_tpl_name" ) );
		$control = trim ( $this->input->get_post ( "control" ) );
		$tpl_category_id = trim ( $this->input->get_post ( "tpl_category_id" ) );
		$number_x = trim ( $this->input->get_post ( "number" ) );
		/*文本框*/
		$input = array ('name' => 'block_tpl_name', 'id' => 'block_tpl_name', 'value' => "$block_tpl_name" );
		$date ['block_tpl_name_from'] = form_input ( $input );
		$date ['block_tpl_name'] = $block_tpl_name;
		/*状态select选择框子*/
		$control_array = array ('all' => '全部', '1' => '已开启', '2' => '已使用', '0' => '已禁用' );
		$control_from = form_dropdown ( 'control', $control_array, $control );
		$date ['control_from'] = $control_from;
		/*模板select选择框子*/
		$category = $this->Cmsblock_model->tpl_category_array (); /*模板分类*/
		$tpl_category_id_from = form_dropdown ( 'tpl_category_id', $category, $tpl_category_id );
		$date ['tpl_category_id_from'] = $tpl_category_id_from;
		/*记录数select选择框子,带自动选择功能*/
		$number = $this->Cmsblock_model->tpl_category_number (); /*记录数*/
		$number_from = form_dropdown ( 'number', $number, $number_x );
		$date ['number_from'] = $number_from;
		
		//页面分页功能
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$t_count = $this->Cmsblock_model->row_array_search ( $block_tpl_name, $control, $tpl_category_id, 
			$number_x ); //统计记录总数
		$t_first = ($page - 1) * $count_page;
		/*获取记录集合*/
		$list = $this->Cmsblock_model->result_array_search ( $block_tpl_name, $control, $tpl_category_id, 
			$number_x, $t_first, $count_page );
		if (count ( $list )) {
			foreach ( $list as $key => $v ) {
				$user_id = $v ['user_id'];
				$author_id = $v ['author_id'];
				$list [$key] ['user_name'] = '';
				$list [$key] ['author_name'] = '';
				$sql_u = "SELECT * FROM cms_user WHERE user_id='$user_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				if ($row ['user_nick']) {
					$page_name = $row ['user_nick'];
				} else {
					$page_name = $row ['user_name'];
				}
				$list [$key] ['user_name'] = $page_name;
				$sql_u = "SELECT * FROM cms_user WHERE user_id='$author_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				if ($row ['user_nick']) {
					$page_name = $row ['user_nick'];
				} else {
					$page_name = $row ['user_name'];
				}
				$list [$key] ['author_name'] = $page_name;
			}
		
		}
		//print_r($list);
		$a ['c'] = "cmsblock";
		$a ['m'] = "block_search";
		$a ['block_tpl_name'] = $block_tpl_name;
		$a ['control'] = $control;
		$a ['tpl_category_id'] = $tpl_category_id;
		$a ['number'] = $number_x;
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		/*输出到页面*/
		$date ['t_count'] = $t_count; //统计
		$date ['list'] = $list;
		$date ['page'] = $page;
		$edit_edit = validation_check ( $UID, 'cms_block_eidtor' ); //控制当用户user_id不等于自己的uid时候的控制编辑
		$date ['edit_edit'] = $edit_edit;
		$date ['page_tpl_name'] = '';
		$date ['number'] = $number;
		$date ['uid'] = $UID;
		$date ['category'] = $category;
		$date ['type'] = '';
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		$date ['pic_path'] = $this->url;
		
		/*模板*/
		$this->load->view ( 'cmsblock/cmsblock_view', $date );
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 添加数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 添加cms_block_tpl表中内容，并上传图片到$this->url知道的文件夹中去，同时按照年月新建一个新的文件夹
	*/
	
	public function cmsblock_add() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$success = validation_check ( $UID, "cmsblock_add" );
		if (! $success) {
			msg ( "无权限：CMS添加碎片模版cmsblock_add", "", "message" );
			safe_exit ();
		}
		
		/*图片的上传,新建目录*/
		$tpl_path = date ( "Ym" );
		//创建递归创建目录
		create_dir ( $this->url . $tpl_path );
		if (! file_exists ( $this->url . $tpl_path )) {
			mkdir ( $this->url . $tpl_path, 0777 );
		}
		
		if (! $this->input->get ( 'block_tpl_id' )) {
			$tpl_category_id = '0';
			$create_time = time ();
			//删除create_time=0的临时
			$linshi_del = $this->Cmsblock_model->linshi ();
			//---------删除create_time=0的临时------------------------
			$insert = array (
					'tpl_category_id' => "$tpl_category_id", 
					'block_tpl_name' => "", 
					'tpl_path' => "$tpl_path", 
					'create_time' => "0", 
					'modify_time' => "0", 
					'size_width' => "", 
					'size_height' => "", 
					'size_row' => "", 
					'demo_pic_id' => "", 
					'size_col' => "", 
					'user_id' => $UID, 
					'tpl_content' => "" );
			$block_tpl_id = $this->Cmsblock_model->cms_block_insert ( $insert );
			$a ['c'] = "cmsblock";
			$a ['m'] = "cmsblock_add";
			$a ['block_tpl_id'] = $block_tpl_id;
			if ($block_tpl_id) {
				msg ( "", url_glue ( $a ) );
			}
		} else {
			$block_tpl_id = $this->input->get ( 'block_tpl_id' );
		}
		//echo $block_tpl_id;
		$block_array = $this->Cmsblock_model->selectlist ( $block_tpl_id );
		$tpl_category_id = $this->input->get_post ( 'tpl_category_id' );
		/*验证表单提交内容*/
		
		$this->form_validation->set_rules ( 'tpl_category_id', 'tpl_category_id', 'required|numeric' );
		$this->form_validation->set_rules ( 'block_tpl_name', '碎片模板名称', 'callback_block_tpl_name|required' );
		$this->form_validation->set_rules ( 'size_width', 'size_width', 'required|numeric' );
		$this->form_validation->set_rules ( 'size_height', 'size_height', 'required|numeric' );
		$this->form_validation->set_rules ( 'size_row', 'size_row', 'required|numeric' );
		$this->form_validation->set_rules ( 'size_col', 'size_col', 'required|numeric' );
		$this->form_validation->set_rules ( 'author_id', '选择设计人', 'required|numeric' );
		$this->form_validation->set_rules ( 'tpl_content', 'tpl_content', 'required' );
		/*验证入库*/
		if ($this->form_validation->run () == TRUE) {
			$block_tpl_name = $this->Cmsblock_model->replace ( $this->input->post ( "block_tpl_name" ) );
			$tpl_content = $this->Cmsblock_model->replace ( $this->input->post ( "tpl_content" ) );
			$size_width = trim ( $this->input->post ( "size_width" ) );
			$size_height = trim ( $this->input->post ( "size_height" ) );
			$size_row = trim ( $this->input->post ( "size_row" ) );
			$size_col = trim ( $this->input->post ( "size_col" ) );
			$tpl_category_id = $this->input->post ( "tpl_category_id" );
			$tpl_path = $this->input->post ( "tpl_path" );
			$block_css = $this->input->post ( "block_css" );
			$author_id = $this->input->post ( "author_id" );
			$tpl_description = $this->input->post ( "tpl_description" );
			$block_css_code = trim ( $this->input->post ( "block_css_code" ) );
			/*图片的上传*/
			$demo_pic_id_1 = $this->input->post ( "demo_pic_id" );
			$demo_pic_id = substr ( $demo_pic_id_1, strpos ( $demo_pic_id_1, "YL_" ) );
			
			$create_time = time ();
			/*验证入库*/
			$up = array (
					'tpl_category_id' => "$tpl_category_id", 
					'block_tpl_name' => "$block_tpl_name", 
					'tpl_path' => "$tpl_path", 
					'create_time' => "$create_time", 
					'modify_time' => "$create_time", 
					'size_width' => "$size_width", 
					'size_height' => "$size_height", 
					'size_row' => "$size_row", 
					'demo_pic_id' => "$demo_pic_id", 
					'size_col' => "$size_col", 
					'block_css' => "$block_css", 
					'user_id' => $UID, 
					'block_css_code' => $block_css_code, 
					'author_id' => $author_id, 
					'tpl_content' => "$tpl_content", 
					'tpl_description' => $tpl_description );
			$success = $this->Cmsblock_model->update ( $up, $block_tpl_id );
			if ($success) {
				/*当修改内容为空时候提示*/
				$a ['c'] = "cmsblock";
				$a ['m'] = "index"; //blockedit
				//$a ['block_tpl_id'] = $block_tpl_id;
				msg ( "添加成功", url_glue ( $a ) );
			}
		}
		$date = array ();
		/*模板分类     文本*/
		$category = $this->Cmsblock_model->tpl_category_array (); /*模板分类*/
		if ($tpl_category_id) {
			$d = $tpl_category_id;
		} else {
			$d = 0;
		}
		$date ['tpl_category_id_from'] = form_dropdown ( 'tpl_category_id', $category, $d );
		/*$input= array('name'=>'block_tpl_name','id'=>'block_tpl_name','value'=>'$block_tpl_name');
		$date['block_tpl_name_from']= form_input($input);
		*/
		$date ['block_tpl_id'] = '';
		$date ['pic_path'] = $this->url . $tpl_path;
		$date ['block_tpl_id'] = $block_tpl_id;
		$date ['block_array'] = $block_array;
		$date ['pic_path'] = $this->url;
		//编辑器
		$eddt = array (
				'css' => '', 
				'id' => 'tpl_content', 
				'value' => '', 
				'width' => '600px', 
				'height' => '300px' );
		$date ['plugins'] = $this->editors->getedit ( $eddt );
		$eddt_c = array (
				'css' => '', 
				'id' => 'tpl_description', 
				'value' => '', 
				'width' => '600px', 
				'height' => '100px', 
				'items' => 'simple' );
		//my_debug($eddt_c);
		$date ['tpl_description'] = $this->editors->getedit ( $eddt_c );
		//上传图片控件
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'cmsdesign', 
					'm' => 'Upload', 
					'id' => $block_tpl_id, 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'folder' => $this->url . $tpl_path, 
					'page' => 'demo_pic_id', 
					'input' => 'demo_pic_id' ) );
		$pic = array ('element_id' => 'demo_pic_id', 'script' => $data_url_pic, 'lable' => "false" );
		$date ['demo_pic_id_upload'] = $this->editors->get_upload ( $pic );
		/*模板*/
		$this->load->view ( 'cmsblock/cmsblock_add_view', $date );
	
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 修改数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 修改cms_block_tpl表中内容，并上传图片到$this->url.$tpl_path的文件夹中去
	*/
	public function blockedit() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$success = validation_check ( $UID, "cmsblock_blockedit" );
		if (! $success) {
			msg ( "无权限：CMS编辑碎片模版cmsblock_blockedit", "", "message" );
			safe_exit ();
		}
		
		/*插入记录*/
		$block_tpl_id = $this->input->get_post ( "block_tpl_id" );
		$page = $this->input->get_post ( "page" );
		$block_array = $this->Cmsblock_model->selectlist ( $block_tpl_id );
		/*图片的上传，检查是否已经新建目录了*/
		$tpl_path = $block_array ['tpl_path']; //指定已经存在的文件夹
		//$tpl_path = date ( "Ym" );
		//创建递归创建目录
		create_dir ( $this->url . $tpl_path );
		if (! file_exists ( $this->url . $tpl_path )) {
			mkdir ( $this->url . $tpl_path, 0777 );
		}
		
		if (url_get ( 'tpl_category_id' ) != '') {
			$tpl_category_id = url_get ( 'tpl_category_id' );
		} elseif ($block_array ['tpl_category_id'] != '0' && url_get ( 'tpl_category_id' ) == '') {
			$tpl_category_id = $this->Cmsblock_model->select_tpl_category_id ( $block_tpl_id );
		} else {
			$tpl_category_id = 0;
		}
		
		/*验证*/
		$this->form_validation->set_rules ( 'block_tpl_name', '碎片模板名称', 'callback_block_tpl_name|required' );
		$this->form_validation->set_rules ( 'size_width', 'size_width', 'required|numeric' );
		$this->form_validation->set_rules ( 'size_height', 'size_height', 'required|numeric' );
		$this->form_validation->set_rules ( 'size_row', 'size_row', 'required|numeric' );
		$this->form_validation->set_rules ( 'size_col', 'size_col', 'required|numeric' );
		$this->form_validation->set_rules ( 'tpl_content', 'tpl_content', 'required' );
		/*验证入库*/
		if ($this->form_validation->run () == TRUE) {
			$block_tpl_name = $this->Cmsblock_model->replace ( $this->input->post ( "block_tpl_name" ) );
			$tpl_content = $this->Cmsblock_model->replace ( $this->input->post ( "tpl_content" ) );
			$block_css_code = trim ( $this->input->post ( "block_css_code" ) );
			$size_width = trim ( $this->input->post ( "size_width" ) );
			$size_height = trim ( $this->input->post ( "size_height" ) );
			$size_row = trim ( $this->input->post ( "size_row" ) );
			$size_col = trim ( $this->input->post ( "size_col" ) );
			$tpl_category_id = $this->input->post ( "tpl_category_id" );
			$tpl_path = $this->input->post ( "tpl_path" );
			$author_id = $this->input->post ( "author_id" );
			$block_css = $this->input->post ( "block_css" );
			$tpl_description = $this->input->post ( "tpl_description" );
			$is_locked = intval($this->input->post ( "is_locked" ));
			/*图片的上传*/
			$demo_pic_id_1 = $this->input->post ( "demo_pic_id" );
			$demo_pic_id = substr ( $demo_pic_id_1, strpos ( $demo_pic_id_1, "YL_" ) );
			
			$create_time = time ();
			/*验证入库*/
			if ($size_width > 0 && $size_height > 0 && $size_row > 0 && $size_col > 0) {
				//碎片模板记录备份表======================start==================================
				if ($block_array ['control'] == 2) {
					if (count ( $block_array )) {
						$block_array ['history_time'] = time ();
						//my_debug($campage_array);
						$bak_success = $this->db->insert ( 
							"cms_block_tpl_history", $block_array );
						//my_debug($bak_success);
					}
				}
				//碎片模板记录备份表======================end======================================
				

				$up = array (
						'tpl_category_id' => "$tpl_category_id", 
						'block_tpl_name' => "$block_tpl_name", 
						'tpl_path' => "$tpl_path", 
						'create_time' => "$create_time", 
						'modify_time' => "$create_time", 
						'size_width' => "$size_width", 
						'size_height' => "$size_height", 
						'size_row' => "$size_row", 
						'demo_pic_id' => "$demo_pic_id", 
						'size_col' => "$size_col", 
						'block_css' => "$block_css", 
						'tpl_content' => "$tpl_content", 
						'block_css_code' => $block_css_code, 
						'author_id' => $author_id,
						'is_locked' => $is_locked,  
						'tpl_description' => "$tpl_description" );
				//my_debug($up);
				$success = $this->Cmsblock_model->update ( $up, $block_tpl_id );
				if ($success) {
					/*当修改内容为空时候提示*/
					$a ['c'] = "cmsblock";
					$a ['m'] = "submit"; //blockedit
					$a ['block_tpl_id'] = $block_tpl_id;
					msg ( "success", url_glue ( $a ) );
				}
			} else {
				$a ['c'] = "cmsblock";
				$a ['m'] = "blockedit"; //
				$a ['block_tpl_id'] = $block_tpl_id;
				msg ( "示例尺寸,记录数必须大于0", url_glue ( $a ) );
			}
		}
		/*输出到页面*/
		$date ['block_tpl_id'] = $block_tpl_id;
		if (count ( $block_array )) {
			$user_id = $block_array ['user_id'];
			$author_id = $block_array ['author_id'];
			$block_array ['user_name'] = '';
			$block_array ['author_name'] = '';
			$sql_u = "SELECT * FROM cms_user WHERE user_id='$user_id' ";
			$row = $this->db->get_record_by_sql ( $sql_u );
			if ($row ['user_nick']) {
				$page_name = $row ['user_nick'];
			} else {
				$page_name = $row ['user_name'];
			}
			$block_array ['user_name'] = $page_name;
			$sql_u = "SELECT * FROM cms_user WHERE user_id='$author_id' ";
			$row = $this->db->get_record_by_sql ( $sql_u );
			if ($row ['user_nick']) {
				$page_name = $row ['user_nick'];
			} else {
				$page_name = $row ['user_name'];
			}
			$block_array ['author_name'] = $page_name;
		}
		
		$date ['block_array'] = $block_array;
		$date ['user_id'] = $block_array ['user_id'];
		$date ['uid'] = $UID;
		//print_r($block_array);
		$date ['page'] = $page;
		$category = $this->Cmsblock_model->tpl_category_array_noall (); /*模板分类下拉框*/
		$date ['category'] = $category;
		$tpl_category_id_from = form_dropdown ( 'tpl_category_id', $category, $tpl_category_id );
		$date ['tpl_category_id_from'] = $tpl_category_id_from;
		$date ['pic_path'] = $this->url;
		
		//编辑器
		$tmp = parse_url ( base_url () );
		$relative_url = $tmp ['path'];
		$contents_Css = '';
		if (strpos ( $block_array ['block_css'], 'resource_url' ) > 1) {
			$contents_Css = str_replace ( '.css', '.css?time=' . time (), 
				str_replace ( '{{$resource_url}}', $relative_url . "public/resource", 
					$block_array ['block_css'] ) );
		}
		$eddt = array (
				'css' => "$contents_Css", 
				'id' => 'tpl_content', 
				'value' => $block_array ['tpl_content'], 
				'width' => '600px', 
				'height' => '300px' );
		$date ['plugins'] = $this->editors->getedit ( $eddt );
		//说明
		$eddt_c = array (
				'css' => '', 
				'id' => 'tpl_description', 
				'value' => $block_array ['tpl_description'], 
				'width' => '600px', 
				'height' => '100px', 
				'items' => 'simple' );
		//my_debug($eddt_c);
		$date ['tpl_description'] = $this->editors->getedit ( $eddt_c );
		//上传图片控件
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'cmsblock', 
					'm' => 'Upload', 
					'id' => $block_tpl_id, 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'folder' => $this->url . $tpl_path, 
					'page' => 'demo_pic_id', 
					'input' => 'demo_pic_id' ) );
		$pic = array ('element_id' => 'demo_pic_id', 'script' => $data_url_pic, 'lable' => "false" );
		$date ['demo_pic_id_upload'] = $this->editors->get_upload ( $pic );
		/*模板*/
		$edit_edit = validation_check ( $UID, 'cms_block_eidtor' ); //控制当用户user_id不等于自己的uid时候的控制编辑
		$date ['edit_edit'] = $edit_edit;
		$this->load->view ( 'cmsblock/cmsblock_edit_view', $date );
	}
	//引用规则
	function block_tpl_name($block_tpl_name) {
		$block_tpl_name = trim ( $block_tpl_name );
		$count = strlen ( $block_tpl_name );
		$record = $this->Cmsblock_model->row_select ( $block_tpl_name, $this->input->get ( "block_tpl_id" ) );
		if ($record) {
			$this->form_validation->set_message ( 'block_tpl_name', 
				'名称：[<font color=blue>' . $block_tpl_name . ']</font>,已经使用' );
			return false;
		} elseif ($count > 100) {
			$this->form_validation->set_message ( 'block_tpl_name', '字符长度不超过100个字符' );
			return false;
		}
		return true;
	}
	public function submit() {
		$block_tpl_id = $this->input->get ( "block_tpl_id" );
		$block_array = $this->Cmsblock_model->selectlist ( $block_tpl_id );
		$date = array ();
		$date ['block_tpl_id'] = $block_tpl_id;
		$date ['block_array'] = $block_array;
		/*模板*/
		$this->load->view ( 'cmsblock/cmsblock_submit_view', $date );
	}
	public function view() {
		$block_tpl_id = $this->input->get ( "block_tpl_id" );
		$act = $this->input->get ( "act" );
		$action = $this->input->get ( "action" );
		$block_array = $this->Cmsblock_model->selectlist ( $block_tpl_id );
		$date = array ();
		//print_r($block_array);
		$url = base_url () . $this->url . '/' . $block_array ['tpl_path'] . '/' . $block_array ['demo_pic_id'];
		$date ['url'] = $url;
		$date ['block_array'] = $block_array;
		$date ['block_tpl_id'] = $block_tpl_id;
		$date ['act'] = $act;
		/*模板*/
		$this->load->view ( 'cmsblock/cmsblock_view_view', $date );
	}
	
	/*控制已禁用操作*/
	public function disable() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmsblock_disable" );
		if (! $success) {
			msg ( "无权限：CMS禁用碎片模版/cmsblock_disable/", "", "message" );
			safe_exit ();
		}
		
		$block_tpl_id = $this->input->get_post ( "block_tpl_id" );
		$page = $this->input->get_post ( "page" );
		$up = array ('control' => '0' );
		$success = $this->Cmsblock_model->update_disable ( $up, $block_tpl_id );
		if ($success) {
			/*当修改内容为空时候提示*/
			$a ['c'] = "cmsblock";
			$a ['m'] = "index";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	}
	/*控制开启操作*/
	public function enable() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmsblock_enable" );
		if (! $success) {
			msg ( "无权限：CMS开启碎片模版/cmsblock_enable/", "", "message" );
			safe_exit ();
		}
		
		$block_tpl_id = $this->input->get_post ( "block_tpl_id" );
		$page = $this->input->get_post ( "page" );
		$up = array ('control' => '1' );
		$success = $this->Cmsblock_model->update_disable ( $up, $block_tpl_id );
		if ($success) {
			/*当修改内容为空时候提示*/
			$a ['c'] = "cmsblock";
			$a ['m'] = "index";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 删除数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 删除cms_block_tpl表中内容，并上传图片到$this->url的文件夹中的图片文件同时给予删除
	*/
	public function cmsblockdel() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmsblockdel" );
		if (! $success) {
			msg ( "无权限：CMS删除碎片模版cmsblockdel", "", "message" );
			safe_exit ();
		}
		
		$block_tpl_id = $this->input->get_post ( "block_tpl_id" );
		$page = $this->input->get_post ( "page" );
		$success = $this->Cmsblock_model->cmsblockdel ( $block_tpl_id, $this->url );
		if ($success) {
			/*当修改内容为空时候提示*/
			$a ['c'] = "cmsblock";
			$a ['m'] = "index";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 添加分类数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 
	*/
	public function cms_category() {
		
		$this->form_validation->set_rules ( 'tpl_category', 'tpl_category', 'required' );
		$block_tpl_id = url_get ( 'block_tpl_id' );
		$act = url_get ( 'act' );
		if ($this->input->post ( "submit" )) {
			$tpl_category = $this->input->post ( "tpl_category" );
			if ($this->form_validation->run () == TRUE) {
				$data = array ('tpl_category' => "$tpl_category" );
				$success = $this->Cmsblock_model->tpl_category_insert ( $data );
				/*当修改内容为空时候提示*/
				$a ['c'] = "cmsblock";
				$a ['m'] = "blockedit";
				$a ['block_tpl_id'] = $block_tpl_id;
				$a ['tpl_category_id'] = $success;
				msg ( "", url_glue ( $a ) );
			} else {
				/*当修改内容为空时候提示*/
				$a ['c'] = "cmsblock";
				$a ['m'] = "blockedit";
				$a ['block_tpl_id'] = $block_tpl_id;
				msg ( "fail", url_glue ( $a ) );
			
			}
		}
		$display ['act'] = "$act";
		$display ['block_tpl_id'] = "$block_tpl_id";
		$this->load->view ( 'cmsblock/cms_categoryAdd_view', $display );
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 分类数据列表显示带分页
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 
	*/
	public function classification() {
		
		//页面分页功能
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		//统计记录总数
		$t_count = $this->Cmsblock_model->countclassification ();
		$t_first = ($page - 1) * $count_page;
		/*获取记录集合*/
		$list = $this->Cmsblock_model->classification_array ( $t_first, $count_page );
		$a ['c'] = "cmsblock";
		$a ['m'] = "classification";
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		/*内容输出到模板*/
		$date ['t_count'] = $t_count; //统计
		$date ['list'] = $list;
		$date ['page'] = $page;
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		/*模板*/
		$this->load->view ( 'cmsblock/classification_view', $date );
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 修改分类数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 
	*/
	public function classup() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmsblock_classup" );
		if (! $success) {
			msg ( "无权限：CMS修改碎片模版分类/cmsblock_classup/", "", "message" );
			safe_exit ();
		}
		
		/*表单验证，当不用thicbox的时候会自己动验证*/
		$this->form_validation->set_rules ( 'tpl_category', 'tpl_category', 'required' );
		/*获取Id编号*/
		$tpl_category_id = $this->input->get_post ( "tpl_category_id" );
		/*获取内容*/
		$categroy = $this->Cmsblock_model->classup ( $tpl_category_id );
		/*当提交的时候判断处理*/
		if ($this->input->post ( "submit" )) {
			$tpl_category = $this->input->post ( "tpl_category" );
			/*表单验证通过后进行更新SQL*/
			if ($this->form_validation->run () == TRUE) {
				$data = array ('tpl_category' => "$tpl_category" );
				$success = $this->Cmsblock_model->classup_update ( $data, $tpl_category_id );
				$a ['c'] = "cmsblock";
				$a ['m'] = "classification";
				//$a['page']=$page;
				msg ( "", url_glue ( $a ) ); //前面为空不提示，直接转向页面
			} else {
				/*当修改内容为空时候提示*/
				$a ['c'] = "cmsblock";
				$a ['m'] = "classification";
				msg ( "fail", url_glue ( $a ) );
			
			}
		}
		
		/*内容输出到页面去*/
		$date ['categroy'] = $categroy;
		$date ['tpl_category_id'] = $tpl_category_id;
		/*调用模板*/
		$this->load->view ( 'cmsblock/cms_categoryEdit_view', $date );
	
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 删除分类数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 
	*/
	public function classdel() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmsblock_classdel" );
		if (! $success) {
			msg ( "无权限：CMS删除碎片模版分类/cmsblock_classdel/", "", "message" );
			safe_exit ();
		}
		
		$tpl_category_id = $this->input->get_post ( "tpl_category_id" );
		$page = $this->input->get_post ( "page" );
		$success = $this->Cmsblock_model->classdel ( $tpl_category_id );
		if ($success) {
			/*当修改内容为空时候提示*/
			$a ['c'] = "cmsblock";
			$a ['m'] = "classification";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	}
	
	public function Upload() {
		$page = $_REQUEST ['page'];
		$id = $_REQUEST ['id'];
		
		if ($_REQUEST ['input']) {
			$input = $_REQUEST ['input']; //控制文本框内容
		} else {
			$input = "inputIMG";
		}
		$tempFile = $_FILES ['Filedata'] ['tmp_name'];
		$targetPath = $_REQUEST ['folder'] . "/"; //
		$targetFile = str_replace ( '//', '/', $targetPath ) . $_FILES ['Filedata'] ['name'];
		$file_name = $_FILES ['Filedata'] ['name'];
		
		$fileTypes = str_replace ( '*.', '', '*.jpg;*.gif;*.png;*psd;' );
		$fileTypes = str_replace ( ';', '|', $fileTypes );
		$typesArray = explode ( '|', $fileTypes ); //split
		$fileParts = pathinfo ( $_FILES ['Filedata'] ['name'] );
		if (in_array ( $fileParts ['extension'], $typesArray )) {
			$targetFile1 = $targetPath . $page . "_" . $id . substr ( $file_name, strlen ( $file_name ) - 4, 
				4 );
			move_uploaded_file ( $tempFile, $targetFile1 );
			$targetFile = @end ( explode ( '/', $targetFile1 ) );
			echo "<input id=$input name=$input type=hidden value=$targetFile>";
			echo "<img src=$targetFile1 width=50 height=50 border=0/>";
		
		} else {
			echo $page . 'page';
		}
	
	}
	
	public function bak_edit() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		/*插入记录*/
		$block_tpl_id = $this->input->get_post ( "block_tpl_id" );
		$auto_id = $this->input->get_post ( "auto_id" );
		$page = $this->input->get_post ( "page" );
		//$block_array = $this->Cmsblock_model->selectlist ( $block_tpl_id );
		$block_array = $this->db->get_record_by_sql ( 
			"SELECT * FROM cms_block_tpl_history WHERE auto_id='$auto_id' " );
		/*图片的上传，检查是否已经新建目录了*/
		$tpl_path = $block_array ['tpl_path']; //指定已经存在的文件夹
		//$tpl_path = date ( "Ym" );
		//创建递归创建目录
		create_dir ( $this->url . $tpl_path );
		if (! file_exists ( $this->url . $tpl_path )) {
			mkdir ( $this->url . $tpl_path, 0777 );
		}
		
		if (url_get ( 'tpl_category_id' ) != '') {
			$tpl_category_id = url_get ( 'tpl_category_id' );
		} elseif ($block_array ['tpl_category_id'] != '0' && url_get ( 'tpl_category_id' ) == '') {
			$tpl_category_id = $this->Cmsblock_model->select_tpl_category_id ( $block_tpl_id );
		} else {
			$tpl_category_id = 0;
		}
		
		/*验证*/
		$this->form_validation->set_rules ( 'block_tpl_name', '碎片模板名称', 'callback_block_tpl_name|required' );
		$this->form_validation->set_rules ( 'size_width', 'size_width', 'required|numeric' );
		$this->form_validation->set_rules ( 'size_height', 'size_height', 'required|numeric' );
		$this->form_validation->set_rules ( 'size_row', 'size_row', 'required|numeric' );
		$this->form_validation->set_rules ( 'size_col', 'size_col', 'required|numeric' );
		$this->form_validation->set_rules ( 'tpl_content', 'tpl_content', 'required' );
		/*验证入库*/
		if ($this->form_validation->run () == TRUE) {
		
		}
		/*输出到页面*/
		$date ['block_tpl_id'] = $block_tpl_id;
		if (count ( $block_array )) {
			$user_id = $block_array ['user_id'];
			$author_id = $block_array ['author_id'];
			$block_array ['user_name'] = '';
			$block_array ['author_name'] = '';
			$sql_u = "SELECT * FROM cms_user WHERE user_id='$user_id' ";
			$row = $this->db->get_record_by_sql ( $sql_u );
			if ($row ['user_nick']) {
				$page_name = $row ['user_nick'];
			} else {
				$page_name = $row ['user_name'];
			}
			$block_array ['user_name'] = $page_name;
			$sql_u = "SELECT * FROM cms_user WHERE user_id='$author_id' ";
			$row = $this->db->get_record_by_sql ( $sql_u );
			if ($row ['user_nick']) {
				$page_name = $row ['user_nick'];
			} else {
				$page_name = $row ['user_name'];
			}
			$block_array ['author_name'] = $page_name;
		}
		
		$date ['block_array'] = $block_array;
		$date ['user_id'] = $block_array ['user_id'];
		$date ['uid'] = $UID;
		//print_r($block_array);
		$date ['page'] = $page;
		$category = $this->Cmsblock_model->tpl_category_array_noall (); /*模板分类下拉框*/
		$date ['category'] = $category;
		$tpl_category_id_from = form_dropdown ( 'tpl_category_id', $category, $tpl_category_id );
		$date ['tpl_category_id_from'] = $tpl_category_id_from;
		$date ['pic_path'] = $this->url;
		
		//编辑器
		$tmp = parse_url ( base_url () );
		$relative_url = $tmp ['path'];
		$contents_Css = '';
		if (strpos ( $block_array ['block_css'], 'resource_url' ) > 1) {
			$contents_Css = str_replace ( '.css', '.css?time=' . time (), 
				str_replace ( '{{$resource_url}}', $relative_url . "public/resource", 
					$block_array ['block_css'] ) );
		}
		$eddt = array (
				'css' => "$contents_Css", 
				'id' => 'tpl_content', 
				'value' => $block_array ['tpl_content'], 
				'width' => '600px', 
				'height' => '300px' );
		$date ['plugins'] = $this->editors->getedit ( $eddt );
		//说明
		$eddt_c = array (
				'css' => '', 
				'id' => 'tpl_description', 
				'value' => $block_array ['tpl_description'], 
				'width' => '600px', 
				'height' => '100px', 
				'items' => 'simple' );
		//my_debug($eddt_c);
		$date ['tpl_description'] = $this->editors->getedit ( $eddt_c );
		//上传图片控件
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'cmsblock', 
					'm' => 'Upload', 
					'id' => $block_tpl_id, 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'folder' => $this->url . $tpl_path, 
					'page' => 'demo_pic_id', 
					'input' => 'demo_pic_id' ) );
		$pic = array ('element_id' => 'demo_pic_id', 'script' => $data_url_pic, 'lable' => "false" );
		$date ['demo_pic_id_upload'] = $this->editors->get_upload ( $pic );
		/*模板*/
		$edit_edit = validation_check ( $UID, 'cms_block_eidtor' ); //控制当用户user_id不等于自己的uid时候的控制编辑
		$date ['edit_edit'] = $edit_edit;
		$this->load->view ( 'cmsblock/cmsblock_bakedit_view', $date );
	}
	public function bak() {
		$date = array ();
		$block_tpl_id = $this->input->get ( 'block_tpl_id' );
		
		$sql = " WHERE block_tpl_id='$block_tpl_id'";
		
		/*分页列表*/
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$sql_count = "SELECT count(auto_id) as tot FROM cms_block_tpl_history $sql"; //取总数,用于分页
		$tcount = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $tcount [0];
		$t_first = ($page - 1) * $count_page;
		$sql = "SELECT * FROM cms_block_tpl_history $sql order by auto_id desc LIMIT $t_first,$count_page";
		$list = $this->db->get_rows_by_sql ( $sql );
		if (count ( $list )) {
			foreach ( $list as $key => $v ) {
				$user_id = $v ['user_id'];
				$author_id = $v ['author_id'];
				$list [$key] ['user_name'] = '';
				$list [$key] ['author_name'] = '';
				$sql_u = "SELECT * FROM cms_user WHERE user_id='$user_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				if ($row ['user_nick']) {
					$page_name = $row ['user_nick'];
				} else {
					$page_name = $row ['user_name'];
				}
				$list [$key] ['user_name'] = $page_name;
				$sql_u = "SELECT * FROM cms_user WHERE user_id='$author_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				if ($row ['user_nick']) {
					$page_name = $row ['user_nick'];
				} else {
					$page_name = $row ['user_name'];
				}
				$list [$key] ['author_name'] = $page_name;
				$tpl_category_id = $v ['tpl_category_id'];
				$f = $this->db->query ( 
					"SELECT tpl_category FROM cms_tpl_category WHERE tpl_category_id='$tpl_category_id'" );
				$f = $f->row_array ();
				$list [$key] ['tpl_category'] = $f ['tpl_category'];
				$list [$key] ['history_time'] = date ( "Y-m-d H:i:s", $v ['history_time'] );
			
			}
		
		}
		$getpageinfo = toolkit_pages_com ( $page, $t_count, modify_build_url ( array ('page' => '' ) ), 
			$count_page, 8 );
		
		$date ['t_count'] = $t_count;
		$date ['list'] = $list;
		if ($t_count != 0) {
			$date ['pagecode'] = $getpageinfo ['pagecode'];
		} else {
			$date ['pagecode'] = '';
		}
		$date ['pic_path'] = $this->url;
		$this->load->view ( 'cmsblock/cmsblock_bak_view', $date );
	
	}

}







/* End of file cmsblock.php */
/* Location: ./application/controllers/cmsblock.php */
