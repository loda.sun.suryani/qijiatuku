<?php
class DiaoDingScheme extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'diaodingscheme' );
		/*if (! $success) {
			msg ( "无权限: key = diaodingscheme", "", "message" );
			exit ();
		}*/
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$view_data ['num'] = '';
		//=======================方案列表================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE length(scheme_name) > 0";
		if ($this->input->post ( 'scheme_name' )) {
			$sql_where = $sql_where . sprintf ( " AND scheme_name like '%s%s%s' ", '%', 
				$this->input->post ( 'scheme_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM diaoding_scheme $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM diaoding_scheme $sql_where ORDER BY scheme_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		//my_debug($data);
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['方案ID'] = $row ['scheme_id'];
				$data [$k] ['方案名称'] = $row ['scheme_name'];
				$data [$k] ['操作'] = sprintf ( "<a href='%s'>空间列表</a>", 
					modify_build_url ( 
						array ('m' => 'layout_list', 'sid' => $row ['scheme_id'] ) ) );
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"edit_scheme({$row['scheme_id']});return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_scheme({$row['scheme_id']});return false;\"\">刪除</a>";
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$this->load->view ( 'diaoding/scheme_list_view', $view_data );
	
	}
	
	function edit_scheme() {
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data = array ();
		$view_data ['record'] = null;
		
		if (! $scheme_id) {
			$success = $this->db->insert ( 'diaoding_scheme', 
				array ('scheme_name' => '', 'create_time' => time () ) );
			if ($success) {
				$scheme_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('sid' => $scheme_id ) ) );
			}
		}
		//===================清理无效数据{{=====================================
		$time_limit = time () - 60 * 60 * 2;
		$this->db->query ( 
			sprintf ( "DELETE FROM diaoding_scheme WHERE ( scheme_name = '') AND create_time<%s", 
				$time_limit ) );
		//==========end}}=====================================================================
		//=================={{品牌列表=================================================
		$category_list = $this->db->get_rows_by_sql ( 
			"select*from diaoding_tag where tag_dimension_id=13" );
		$pinpai_arr [0] = "-请选择品牌-";
		foreach ( $category_list as $k => $v ) {
			$pinpai_arr [$v ['tag_id']] = $v ['tag_name'];
		}
		$view_data ['pinpai'] = $pinpai_arr;
		//===================================)).end================================
		$view_data ['record'] = $this->db->get_record_by_field ( 'diaoding_scheme', 
			'scheme_id', $scheme_id );
		
		$scheme_name = trim ( $this->input->post ( 'scheme_name' ) );
		$category_id = intval ( $this->input->post ( 'category_id' ) );
		$shop_id = intval ( $this->input->post ( 'shop_id' ) );
		$scheme_memo = trim ( $this->input->post ( 'scheme_memo' ) );
		
		$this->form_validation->set_rules ( 'scheme_name', '方案名称', 'required' );
		$this->form_validation->set_rules ( 'category_id', '品牌id', 'required|is_natural_no_zero' );
		$this->form_validation->set_rules ( 'shop_id', '店铺id', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$this->db->where ( 'scheme_id', $scheme_id );
				$success = $this->db->update ( 'diaoding_scheme', 
					array (
							'scheme_name' => $scheme_name, 
							'scheme_memo' => $scheme_memo, 
							'category_id' => $category_id, 
							'shop_id' => $shop_id, 
							'create_time' => time () ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'diaoding/scheme_edit_view', $view_data );
	}
	function layout_list() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data ['scheme_id'] = $scheme_id;
		//=======================方案列表================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE scheme_id ='$scheme_id' AND length(layout_name) > 0";
		if ($this->input->post ( 'layout_name' )) {
			$sql_where = $sql_where . sprintf ( " AND layout_name like '%s%s%s' ", '%', 
				$this->input->post ( 'layout_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM diaoding_layout $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM diaoding_layout $sql_where ORDER BY layout_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		//my_debug($data);
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['空间ID'] = $row ['layout_id'];
				$data [$k] ['空间名称'] = $row ['layout_name'];
				$data [$k] ['操作'] = sprintf ( "<a href='%s' >空间预设</a>", 
					modify_build_url ( 
						array (
								'c' => "diaoding_setting", 
								'm' => "default_add", 
								'sid' => $row ['scheme_id'], 
								'lid' => $row ['layout_id'] ) ) );
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"element_list({$row['layout_id']},{$row['scheme_id']});return false;\">素材列表</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"edit_layout({$row['layout_id']},{$scheme_id});return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_layout({$row['layout_id']});return false;\"\">刪除</a>";
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$this->load->view ( 'diaoding/layout_list_view', $view_data );
	
	}
	function edit_layout() {
		$layout_id = intval ( $this->input->get ( 'id' ) );
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data = array ();
		$view_data ['record'] = null;
		if (! $layout_id) {
			$default_data = array (
					'layout_name' => '', 
					'scheme_id' => $scheme_id, 
					/*'layout_width' => 70,  //默认单元格宽度
					'layout_height' => 70,  //默认单元格高度
					'layout_product_width' => 140,  //默认产品宽度
					'layout_product_height' => 60,  //默认产品高度*/
					'create_time' => time () );
			$success = $this->db->insert ( 'diaoding_layout', $default_data );
			if ($success) {
				$layout_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('id' => $layout_id ) ) );
			}
		}
		//===================清理无效数据{{=====================================
		$time_limit = time () - 60 * 60 * 2;
		$this->db->query ( 
			sprintf ( "DELETE FROM diaoding_layout WHERE ( layout_name = '') AND create_time<%s", 
				$time_limit ) );
		//==========end}}=====================================================================
		$view_data ['record'] = $this->db->get_record_by_field ( 'diaoding_layout', 
			'layout_id', $layout_id );
		
		$layout_name = trim ( $this->input->post ( 'layout_name' ) );
		$layout_memo = trim ( $this->input->post ( 'layout_memo' ) );
		/*$layout_width = intval ( $this->input->post ( 'layout_width' ) );
		$layout_height = intval ( $this->input->post ( 'layout_height' ) );
		$layout_num = intval ( $this->input->post ( 'layout_num' ) );
		$layout_total = intval ( $this->input->post ( 'layout_total' ) );
		$layout_product_width = intval ( $this->input->post ( 'layout_product_width' ) );
		$layout_product_height = intval ( $this->input->post ( 'layout_product_height' ) );*/
		
		$this->form_validation->set_rules ( 'layout_name', '空间名称', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$this->db->where ( 'layout_id', $layout_id );
				$success = $this->db->update ( 'diaoding_layout', 
					array (
							'layout_name' => $layout_name, 
							'layout_memo' => $layout_memo, 
							/*'layout_width' => $layout_width, 
							'layout_height' => $layout_height, 
							'layout_num' => $layout_num, 
							'layout_total' => $layout_total, 
							'layout_product_width' => $layout_product_width, 
							'layout_product_height' => $layout_product_height,*/ 
							'create_time' => time () ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'diaoding/layout_edit_view', $view_data );
	
	}
	function element_list() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$layout_id = intval ( $this->input->get ( 'lid' ) );
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data ['layout_id'] = $layout_id;
		$view_data ['scheme_id'] = $scheme_id;
		//=======================方案列表================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE layout_id ='$layout_id' AND length(element_name) > 0";
		if ($this->input->post ( 'element_name' )) {
			$sql_where = $sql_where . sprintf ( " AND element_name like '%s%s%s' ", '%', 
				$this->input->post ( 'element_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM diaoding_element $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM diaoding_element $sql_where ORDER BY element_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		//my_debug($data);
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['素材ID'] = $row ['element_id'];
				$data [$k] ['素材名称'] = $row ['element_name'];
				if ($row ['element_type'] == 1) {
					$data [$k] ['类型'] = '背景';
				}
				if ($row ['element_type'] == 2) {
					$data [$k] ['类型'] = '照明';
				}
				if ($row ['element_type'] == 3) {
					$data [$k] ['类型'] = '取暖';
				}
				if ($row ['element_type'] == 4) {
					$data [$k] ['类型'] = '排气';
				}
				if ($row ['element_type'] == 5) {
					$data [$k] ['类型'] = '多功能';
				}
				$data [$k] ['商品id'] = $row ['good_id'];
				/*$data [$k] ['图片宽'] = $row ['lay_width'];
				$data [$k] ['图片高'] = $row ['lay_heigth'];*/
				$data [$k] ['商品图片'] = "<img src={$row['lay_img_path']} height=44 border=0/>";
				$data [$k] ['操作'] = "<a href = \"javascript void(0)\" onclick=\"edit_element({$row['element_id']},0,0);return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_layout({$row['element_id']});return false;\"\">刪除</a>";
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$this->load->view ( 'diaoding/element_list_view', $view_data );
	
	}
	function edit_element() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$element_id = intval ( $this->input->get ( 'id' ) );
		$layout_id = intval ( $this->input->get ( 'lid' ) );
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data = array ();
		$view_data ['record'] = null;
		if (! $element_id) {
			$default_data = array (
					'element_name' => '', 
					'layout_id' => $layout_id, 
					'scheme_id' => $scheme_id, 
					'create_time' => time () );
			$success = $this->db->insert ( 'diaoding_element', $default_data );
			if ($success) {
				$element_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('id' => $element_id ) ) );
			}
		}
		//===================清理无效数据{{=====================================
		$time_limit = time () - 60 * 60 * 2;
		$this->db->query ( 
			sprintf ( 
				"DELETE FROM diaoding_element WHERE ( element_name = '') AND create_time<%s", 
				$time_limit ) );
		//==========end}}=====================================================================
		$element_select_data = array ();
		$element_select_data ['0'] = '-选择类型-';
		$element_select_data ['1'] = '背景';
		$element_select_data ['2'] = '照明';
		$element_select_data ['3'] = '取暖';
		$element_select_data ['4'] = '排气';
		$element_select_data ['5'] = '多功能';
		$view_data ['element_select'] = $element_select_data;
		
		$view_data ['record'] = $this->db->get_record_by_field ( 'diaoding_element', 'element_id', 
			$element_id );
		
		$element_name = trim ( $this->input->post ( 'element_name' ) );
		$element_type = intval ( $this->input->post ( 'element_type' ) );
		$qb_type = intval ( $this->input->post ( 'qb_type' ) );
		$good_id = trim ( $this->input->post ( 'good_id' ) );
		$good_price = $this->input->post ( 'good_price' );
		$good_size = $this->input->post ( 'good_size' );
		//============================{{图片上传begin==============================
		$element_pic = modify_build_url ( 
			array (
					'c' => 'diaodingscheme', 
					'm' => 'upload', 
					'element_id' => $element_id, 
					'page' => 'element_pic', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'input' => 'element_pic' ) );
		$pic = array (
				'element_id' => 'element_pic', 
				'queue_id' => 'custom_queue', 
				'script' => $element_pic, 
				'lable' => "false" );
		$view_data ['element_picture'] = $this->editors->get_upload ( $pic );
		//=============================.end}}======================================
		

		//my_debug($element_type);
		$this->form_validation->set_message ( 'is_natural_no_zero', '素材类型必须选择' );
		$this->form_validation->set_rules ( 'element_name', '素材名称', 'required' );
		$this->form_validation->set_rules ( 'good_id', '商品id', 'numeric|required' );
		$this->form_validation->set_rules ( 'good_price', '商品价格', 'required' );
		$this->form_validation->set_rules ( 'good_size', '商品规格', 'required' );
		$this->form_validation->set_rules ( 'element_type', '素材类型', 'is_natural_no_zero|required' );
		$this->form_validation->set_rules ( 'qb_type', '是否对称', 'required' );
		if ($this->input->post ( 'submit' )) {
			if ($this->form_validation->run () == true) {
				$this->db->where ( 'element_id', $element_id );
				$success = $this->db->update ( 'diaoding_element', 
					array (
							'element_name' => $element_name, 
							'element_type' => $element_type, 
							'good_id' => $good_id, 
							'good_price' => $good_price, 
							'good_size' => $good_size, 
							'qb_type' => $qb_type, 
							'create_time' => time () ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'diaoding/element_edit_view', $view_data );
	}
	function del_element() {
		$element_id = $this->input->get ( 'id' );
		$record_info = $this->db->get_record_by_field ( 'diaoding_element', 'element_id', 
			$element_id );
		if ($record_info ['lay_img_path']) {
			$old_img = (FCPATH . $record_info ['lay_img_path']);
			if (file_exists ( $old_img ) == true) {
				unlink ( $old_img );
			}
		}
		$this->db->where ( 'element_id', $element_id );
		$this->db->delete ( 'diaoding_element' );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function del_layout() {
		$layout_id = intval ( $this->input->get ( 'lid' ) );
		$sql = "select (lay_img_path) from diaoding_element where layout_id = $layout_id";
		$img_rows = $this->db->get_rows_by_sql ( $sql );
		if (count ( $img_rows ) > 0) {
			foreach ( $img_rows as $k => $v ) {
				$old_img = (FCPATH . $v ['lay_img_path']);
				if (file_exists ( $old_img ) == true) {
					unlink ( $old_img );
				}
			}
		}
		$this->db->where ( 'layout_id', $layout_id );
		$this->db->delete ( 'diaoding_element' );
		$this->db->where ( 'layout_id', $layout_id );
		$this->db->delete ( 'diaoding_layout' );
		return;
	}
	function del_scheme() {
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$sql = "select (lay_img_path) from diaoding_element where scheme_id = $scheme_id";
		$img_rows = $this->db->get_rows_by_sql ( $sql );
		if (count ( $img_rows ) > 0) {
			foreach ( $img_rows as $k => $v ) {
				$old_img = (FCPATH . $v ['lay_img_path']);
				if (file_exists ( $old_img ) == true) {
					unlink ( $old_img );
				}
			}
		}
		$this->db->where ( 'scheme_id', $scheme_id );
		$this->db->delete ( 'diaoding_element' );
		$this->db->where ( 'scheme_id', $scheme_id );
		$this->db->delete ( 'diaoding_layout' );
		$this->db->where ( 'scheme_id', $scheme_id );
		$this->db->delete ( 'diaoding_scheme' );
		return;
	}
	function upload() {
		//my_debug($_FILES);		
		if (count ( $_FILES ) < 1) {
			my_debug ( "没有发现上传文件" );
			return;
		}
		if (! array_key_exists ( 'Filedata', $_FILES )) {
			my_debug ( "非法的上传操作" );
			return;
		}
		$file_name = $_FILES ['Filedata'] ['name'];
		$file_name_parts = explode ( '.', $file_name );
		if (count ( $file_name_parts ) < 2) {
			my_debug ( "非法的文件名" );
			return;
		}
		$len = count ( $file_name_parts );
		$file_ext_name = $file_name_parts [$len - 1];
		$file_ext_name = strtolower ( $file_ext_name );
		if (! $file_ext_name) {
			my_debug ( "非法的文件扩展名" );
			return;
		}
		if (! in_array ( $file_ext_name, array ('jpg', 'jpeg', 'png', 'bmp' ) )) {
			my_debug ( "只支持这些扩展名:jpg,jpeg,png,bmp" );
			return;
		}
		$element_id = $this->input->get ( 'element_id' );
		$file_name = $_FILES ['Filedata'] ['name'];
		$temp_name = $_FILES ['Filedata'] ['tmp_name'];
		$type = $this->input->get ( 'input' );
		//确定保存的文件路径
		if ($this->input->get ( 'input' )) {
			//磁盘上的物理path
			$path = sprintf ( "%spublic/resource/%s/", FCPATH, $type );
			//数据库中保存的path
			$path2 = sprintf ( "public/resource/%s/", $type );
		}
		create_dir ( $path ); //创建文件保存路径
		//如有旧文件，删除
		if ($element_id) {
			$record = $this->db->get_record_by_field ( 'diaoding_element', 'element_id', 
				$element_id );
			$old_path = '';
			if ($type == 'element_pic') {
				if ($record ['lay_img_path']) {
					$old_path = (FCPATH . $record ['lay_img_path']);
				}
			}
			if (file_exists ( $old_path ) === true) {
				unlink ( $old_path );
			}
		}
		$rand_num = rand ( 10000, 20000 );
		$save_name = ("element_" . $rand_num . "_" . $element_id . "_temp" . ".$file_ext_name");
		$save_name_2 = ("element_" . $rand_num . "_" . $element_id . ".$file_ext_name");
		
		$targetfile = $path . $save_name;
		$targetfile1 = $path2 . $save_name;
		$path_thumb = $path2 . $save_name_2;
		
		$is_copied = move_uploaded_file ( $temp_name, $targetfile );
		$img_info = getimagesize ( $targetfile1 );
		$width = $img_info [0];
		$heigth = $img_info [1];
		if ($is_copied) {
			if (in_array ( $file_ext_name, array ('jpg', 'png', 'jpeg' ) )) {
				//生成缩略图
				# Constants
				if ($heigth / $width == 1) {
					define ( "THUMB_MAX_WIDTH", 70 );
					define ( "THUMB_MAX_HEIGHT", 70 );
				}
				if ($heigth / $width == 2) {
					define ( "THUMB_MAX_WIDTH", 70 );
					define ( "THUMB_MAX_HEIGHT", 140 );
				}
				if ($width / $heigth == 2) {
					define ( "THUMB_MAX_WIDTH", 140 );
					define ( "THUMB_MAX_HEIGHT", 70 );
				}
			
			}
			# Get image location
			$image_path = ($path . $save_name);
			# Load image
			$img = null;
			$ext = strtolower ( end ( explode ( '.', $image_path ) ) );
			$img_type = exif_imagetype ( $image_path );
			if ($img_type == IMAGETYPE_JPEG) {
				$img = imagecreatefromjpeg ( $image_path );
			} else if ($img_type == IMAGETYPE_PNG) {
				$img = imagecreatefrompng ( $image_path );
				# Only if your version of GD includes GIF support
			} else if ($img_type == IMAGETYPE_GIF) {
				$img = imagecreatefromgif ( $image_path );
			}
			# If an image was successfully loaded, test the image for size
			if ($img) {
				# Get image size and scale ratio
				$width = imagesx ( $img );
				$height = imagesy ( $img );
				$scale = min ( THUMB_MAX_WIDTH / $width, THUMB_MAX_HEIGHT / $height );
				# If the image is larger than the max shrink it
				if ($scale < 1) {
					$new_width = floor ( $scale * $width );
					$new_height = floor ( $scale * $height );
					
					# Create a new temporary image
					$tmp_img = imagecreatetruecolor ( $new_width, 
						$new_height );
					
					# Copy and resize old image into new image
					imagecopyresized ( $tmp_img, $img, 0, 0, 0, 0, 
						$new_width, $new_height, $width, $height );
					imagedestroy ( $img );
					$img = $tmp_img;
					//my_debug ( $path . $path_thumb );
					if ($img) {
						touch ( FCPATH . $path_thumb );
						//imageFilter ( $img, IMG_FILTER_GRAYSCALE );//变为类度图像
						imagejpeg ( $img, FCPATH . $path_thumb, 
							50 );
					}
					imagedestroy ( $img );
				}
			}
		} else {
			$path_thumb = null;
		}
		if ($path_thumb) {
			if (file_exists ( FCPATH . $path_thumb ) === true) {
				$path_thumb = $path_thumb;
			} else {
				rename ( FCPATH . $targetfile1, FCPATH . $path_thumb );
				$path_thumb = $path_thumb;
			}
		} else {
			$path_thumb = null;
		}
		if (file_exists ( FCPATH . $targetfile1 ) === true) {
			unlink ( FCPATH . $targetfile1 );
		}
		if ($path_thumb) {
			$this->db->where ( 'element_id', $element_id );
			$this->db->update ( 'diaoding_element', array ('lay_img_path' => $path_thumb ) );
			echo "<img src=$path_thumb border=0/>";
		}
	}
}
