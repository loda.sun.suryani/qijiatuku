<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Tree extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'tree_model' );
				
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "edit_column" );
		if ($success != 1) {
			msg ( "无权限：管理栏目(edit_column)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$jstree_path = FCPATH . "component/jstree";
		//my_debug($jstree_path);
		include "{$jstree_path}/server.php";
	}
	function show() {
		$view_data = array ();
		$view_data ['jstree_url'] = '';
		$view_data ['public_js_url'] = '';
		$jstree_path = FCPATH . "component/jstree";
		
		$view_data ['jstree_url'] = base_url ( "component/jstree" );
		$view_data ['public_js_url'] = base_url ( 'public/js' );
		$view_data ['tree_grid'] = '';
		
		$data_arr = array ();
		$items = & $this->tree_model->items;
		//my_debug($paths,'','');
		

		$this->load->view ( 'tree_view', $view_data );
	}
	function detail() {
		//my_debug ( $_GET, '#_GET' );
		$id = $this->input->get ( 'id' );
		$id = substr ( $id, 5 );
		$id = intval ( $id );
		if (! $id) {
			if (DEBUGMODE) {
				my_debug ( "id不正确" );
			}
			return;
		}
		$item_info = $this->db->get_record_by_field ( 'cms_tree', 'id', $id );
		$data_arr = array ();
		$data_arr [0] ['attr'] = 'id';
		$data_arr [0] ['value'] = $item_info ['id'];
		$data_arr [1] ['attr'] = '目录名';
		$data_arr [1] ['value'] = $item_info ['item_name'];
		$data_arr [2] ['attr'] = 'URL';
		$data_arr [2] ['value'] = $item_info ['item_url'];
		$this->load->library ( 'datagrid' );
		$this->datagrid->reset ();
		echo $this->datagrid->build ( 'datagrid', $data_arr, TRUE );
		
		echo sprintf ( '<a href="#" onclick="show_v(\'修改\',\'%s\',\'0\',\'0\');" >修改</a>', 
			modify_build_url ( array ('m' => 'edit', 'id' => $id ) ) );
		echo "<br>";
		echo sprintf ( '<a href="#" onclick="show_v(\'授权\',\'%s\',\'0\',\'0\');" >授权</a>', 
			modify_build_url ( array ('m' => 'accredit_page_perm', 'id' => $id ) ) );
	}
	function edit() {
		$id = $this->input->get ( 'id' );
		$id = intval ( $id );
		if (! $id) {
			if (DEBUGMODE) {
				my_debug ( "id不正确" );
			}
			return;
		}
		$this->load->library ( 'form_validation' );
		
		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_tree", 'id', $id );
		$this->defaults = $persist_record;
		
		$view_data = array ();
		$this->form_validation->set_rules ( 'item_name', '目录名', 'required|alpha|max_length[100]' );
		$this->form_validation->set_rules ( 'item_url', 'URL', 'required|max_length[100]' );
		
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == TRUE) {
				//my_debug ( $_POST );
				$this->db->where ( 'id', $id );
				$this->db->update ( 'cms_tree', 
					array (
							'item_name' => trim ( $this->field ( 'item_name' ) ), 
							'item_url' => trim ( $this->field ( 'item_url' ) ), 
							'is_external' => trim ( $this->field ( 'is_external' ) ) ) );
				
				//关闭界面
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				return;
			}
		}
		
		$this->load->view ( 'tree_view_edit', $view_data );
	}
	function path() {
		header ( "Content-type: text/html; charset=utf-8" );
		//my_debug($this->tree_model->items,'','');
		//my_debug ( $this->tree_model->paths, '', '' );
		my_debug ( $this->tree_model->keys, '', '' );
		//my_debug ( $this->tree_model->tree, '', '' );
	//my_debug(get_object_vars($this->tree_model),'','');
	}
	function accredit_page_perm() {
		$id = $this->input->get ( "id" );
		$id = intval ( $id );
		if (! $id) {
			my_debug ( '参数错误' );
			return;
		}
		$perm_key = "edit_column_$id";
		$record = $this->db->get_record_by_field ( 'cms_permission', 'permission_key', $perm_key );
		if (! $record) {
			//不存在 ,则插入一条
			$permission_name = "编辑栏目(column_id=$id)";
			$db_ret = $this->db->insert ( "cms_permission", 
				array (
						'permission_key' => $perm_key, 
						'permission_name' => $permission_name, 
						'is_temp' => 0 ) );
			if ($db_ret) {
				$permission_id = $this->db->insert_id ();
			}
		} else {
			$permission_id = $record ['permission_id'];
		}
		
		redirect ( 
			modify_build_url ( 
				array (
						'c' => 'editpermission', 
						'm' => 'accredit', 
						'permission_id' => $permission_id ) ) );
	}

}


//end.
