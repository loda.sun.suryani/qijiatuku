<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class EditU2r extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
	}
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editu2r_index" );
		if (! $success) {
			msg ( "无权限：用户权限editu2r_index/[$UID]/", "", "message" );
			exit ();
		}
		
		//创建一个空的记录,进入编辑
		$record_id = $this->input->get ( "id" );
		$record_id = intval ( $record_id );
		if (! $record_id) {
			$db_ret = $this->db->insert ( "cms_user_to_role", array ('is_temp' => 1 ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('c' => 'EditU2r', 'id' => $insert_id ) ) );
			}
		}
		
		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_user_to_role", 'auto_id', 
			$record_id );
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		
		$view_data = array ();
		$view_data ['message'] = null;
		//表单验证规则
		$this->form_validation->set_rules ( 'user_id', '用户ID', "is_natural" );
		$this->form_validation->set_rules ( 'role_id', '角色ID', "is_natural" );
		
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$this->db->where ( 'auto_id', $record_id );
				$this->db->update ( 'cms_user_to_role', 
					array (
							'user_id' => trim ( $this->input->post ( 'user_id' ) ), 
							'role_id' => trim ( $this->input->post ( 'role_id' ) ), 
							'is_temp' => '0' ) );
				if ($this->db->affected_rows ()) {
					$view_data ['message'] = ("已经写入数据库." . time ());
				} else {
					$view_data ['message'] = ("没有更新任何内容," . microtime ());
				}
				//redirect ( site_url ( "c=formlist" ) );
			//关闭界面
			//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//return;
			}
		}
		$this->load->view ( 'editu2r_view', $view_data );
	}
	
	function u2r_add() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editu2r_u2r_add" );
		if (! $success) {
			msg ( "无权限：用户绑定角色权限editu2r_u2r_add/[$UID]/", "", "message" );
			exit ();
		}
		
		//创建一个空的记录,进入编辑
		if ($this->input->get ( "user_id" )) {
			$user_id = $this->input->get ( "user_id" );
		}
		$user_id = intval ( $user_id );
		//从数据库中取出该记录
		$view_data = array ();
		$view_data ['record'] = null;
		$r = "select * from  cms_role where is_temp<1 order by role_id desc";
		$role = $this->db->get_rows_by_sql ( $r );
		$view_data ['record'] = $role;
		$view_data ['message'] = null;
		$view_data ['permission'] = null;
		//表单验证规则
		//$this->form_validation->set_rules ( 'permission_id_text', '您没选择角色绑定,如需退出右上角点击关闭即可', "required" );
		if ($this->input->post ( 'submitform' )) {
			//if ($this->form_validation->run ()) {
			$permission_id_text = trim ( $this->input->post ( 'permission_id_text' ) );
			$permission_id_text = substr ( $permission_id_text, 0, strlen ( $permission_id_text ) - 1 );
			if (count ( explode ( ",", $permission_id_text ) ) && '' != $permission_id_text) {
				foreach ( explode ( ",", $permission_id_text ) as $value ) {
					$count = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t_count FROM cms_user_to_role WHERE user_id ='$user_id' AND role_id='$value'" );
					if ($count ['t_count'] == 0 && '' != $value) {
						/*权限操作日志----start*/
						//用户
						$query = $this->db->query ( 
							"SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
						$user = $query->row_array ();
						//权限
						/*$query = $this->db->query ( 
							"SELECT permission_name FROM cms_permission WHERE permission_id ='$value'" );
						$psion = $query->row_array ();*/
						//角色
						$query = $this->db->query ( 
							"SELECT role_name FROM cms_role WHERE  role_id='$value'" );
						$rol = $query->row_array ();
						$this->cms_grant_log ( $user_id, $user ['user_name'], $value, 
							$rol ['role_name']."/角色", 0, 0 );
						/*权限操作日志----end*/
						$db_ret = $this->db->insert ( "cms_user_to_role", 
							array ('role_id' => $value, 'user_id' => $user_id, 'is_temp' => 0 ) );
					}
				}
				//当role_id记录and user_id除外记录给删除掉
				/*$sql = "SELECT * FROM cms_role_to_permission WHERE role_id NOT IN($permission_id_text) AND is_temp<1  ";
				$query = $this->db->query ( $sql );
				$array = $query->result_array ();
				$del_id = array ();
				if (count ( $array )) {
					while ( list ( $k, $v ) = each ( $array ) ) {
						$del_id [] = $v ["permission_id"];
					}
					$permission_id = implode ( ",", $del_id );
					$del_sql = "permission_id IN($permission_id) AND user_id=$user_id";
					my_debug($del_sql);
					//$this->db->where ( $del_sql );
					//$this->db->delete ( 'cms_user_to_permission' );
				}*/
				//删除cms_user_to_role
				$condition = "role_id NOT IN($permission_id_text) AND user_id=$user_id";
				$this->db->where ( $condition );
				$this->db->delete ( 'cms_user_to_role' );
			} else {
				$this->db->where ( 'user_id', $user_id );
				$this->db->delete ( 'cms_user_to_role' );
			}
			
			if ($this->db->affected_rows ()) {
				$view_data ['message'] = ("已经写入数据库." . time ());
			} else {
				$view_data ['message'] = ("没有更新任何内容," . microtime ());
			}
			//redirect ( site_url ( "c=formlist" ) );
			//关闭界面
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//}if ($this->form_validation->run ()) {
		}
		
		//====权限列表------------------
		$combination = '';
		$sqlq = "SELECT * FROM cms_role  WHERE is_temp<1";
		$permission = $this->db->get_rows_by_sql ( $sqlq );
		if (count ( $permission )) {
			foreach ( $permission as $k => $v ) {
				$p_id = $v ['role_id'];
				if ($p_id) {
					$sql = "SELECT role_id FROM cms_user_to_role  WHERE role_id='$p_id' AND user_id='$user_id' AND is_temp<1";
					$role_to_p = $this->db->get_record_by_sql ( $sql );
				}
				//my_debug($role_to_p);
				if (count ( $role_to_p ) >= 1 && $role_to_p ['role_id'] == $p_id) {
					$permission [$k] ['check'] = 1;
					$combination .= $role_to_p ['role_id'] . ',';
				} else {
					$permission [$k] ['check'] = 0;
				}
			
			}
		}
		
		$view_data ['permission'] = $permission;
		$formInput = array (
				'name' => 'permission_id_text', 
				'id' => 'permission_id_text', 
				'style' => 'display:none;', 
				'value' => $combination );
		$permission_id_text = form_input ( $formInput );
		$view_data ['form_input'] = $permission_id_text;
		$this->load->view ( 'editu2r_add_view', $view_data );
	}

}


//end.
