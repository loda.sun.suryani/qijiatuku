<?php
/**
 * Class untuk test library datagrid
 *
 * @author Permana
 *
 * @property Datagrid $datagrid
 */
class Test extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('html');
        $this->load->library('datagrid');

    }

    function index()
    {
        $this->load->library('phpconsole');
        $this->phpconsole->log(__LINE__, __FILE__);
        $this->phpconsole->log(__LINE__, date('Y-m-d H:i:s', time()));
        $this->phpconsole->flush();
        $this->phpconsole->log(__LINE__, __FILE__);
        $this->phpconsole->log(__LINE__, date('Y-m-d H:i:s', time()));
        $this->phpconsole->log(__LINE__, "test(测试)...");
        exit(__FILE__ . ":" . __LINE__);
    }

    function batch_publish()
    {
        return;
        /*
         * 批量发布.
         * 这不是真的发布,只是在文件同步表中插入记录,省去手动点击.
         * 仅适用于紧急情况下的修复
         */
        $this->db_online = $this->load->database('online', true);

        $rows = $this->db_online->get_rows_by_sql(
            "SELECT page_id,page_title,page_site,page_url FROM (`cms_page`) WHERE page_site='mall.jia.com' AND is_static=1 ");
        $this->datagrid->reset();
        echo $this->datagrid->build('datagrid', $rows, TRUE);

        $this->db_online->query("SET AUTOCOMMIT=0");
        $this->db_online->query("BEGIN");
        foreach ($rows as $row) {
            $this->db_online->where('page_id', $row ['page_id']);
            $this->db_online->delete('cms_sync_log');
            $this->db_online->insert('cms_sync_log',
                array('page_id' => $row ['page_id'], 'client_ip' => '10.10.20.131'));
        }
        $this->db_online->query("COMMIT");
    }

    function form()
    {
        $data = array();
        $id = $this->input->get('id');
        $sql = "SELECT page_tpl_name FROM  cms_page_tpl WHERE page_tpl_id ='$id'";
        $result = $this->db->get_record_by_sql($sql);
        //my_debug($result);
        $data ['page_tpl_name'] = $result['page_tpl_name'];
        $this->load->view('jqgrid/from_views', $data);
    }

    public function grid_index()
    {
        $success = '';
        error_reporting(E_ALL);

        $data = array();
        //=============加载编辑器{{=============================================
        $this->load->library('editors');
        $eddt = array('id' => 'notice_content', 'value' => '', 'width' => '600px', 'height' => '200px');
        $data ['notice_content'] = $this->editors->getedit($eddt);

        $cloumn = "page_tpl_id,page_tpl_name,layout_pic_id,create_time,user_id";
        $data ['cloumn'] = $cloumn;
        $this->load->view('jqgrid/jqgrid_index_views', $data);
    }

    public function show()
    {
        $sql = "";
        if ($_GET ['_search'] == 'true') {
            $searchString = $this->input->get("searchString"); //字段的数值
            $searchOper = $this->input->get("searchOper"); //判断符号
            $searchField = $this->input->get("searchField"); //字段
            if ($searchOper == "eq") {
                $sql = " WHERE $searchField ='$searchString'";
            }

        }
        $page = $_GET ['page']; // get the requested page
        $limit = $_GET ['rows']; // get how many rows we want to have into the grid
        $sidx = $_GET ['sidx']; // get index row - i.e. user click to sort
        $sord = $_GET ['sord']; // get the direction
        if (!$sidx)
            $sidx = 1;
        $sql_count = "SELECT count(*) as count FROM cms_page_tpl $sql"; //取总数,用于分页
        $row = $this->db->get_record_by_sql($sql_count);
        $count = $row ['count']; //取得总数
        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if ($start < 0) {
            $start = 0;
        }
        $clumn = "page_tpl_id,page_tpl_name,layout_pic_id,create_time,user_id";
        $sql = "SELECT $clumn FROM  cms_page_tpl $sql ORDER BY page_tpl_id $sord LIMIT $start , $limit";
        $result = $this->db->get_rows_by_sql($sql);
        $responce = "";
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;
        $i = 0;
        if (count($result)) {
            foreach ($result as $k => $row) {
                $user_id = $row ['user_id'];
                $sql_u = "SELECT user_name FROM cms_user WHERE user_id='$user_id' ";
                $row_u = $this->db->get_record_by_sql($sql_u);
                $responce->rows [$i] ['id'] = $row ['page_tpl_id']; //行ID，是grid规定不可重复的ID标示
                $responce->rows [$i] ['cell'] = array(
                    $row ['page_tpl_id'],
                    $row ["page_tpl_name"],
                    $row ["layout_pic_id"],
                    date("Y-m-d", $row ["create_time"]),
                    $row_u ['user_name'],
                    "");
                $i++;
            }
        } else { //当积累为空的时候 ,防止js报错，赋值为空即可
            $responce->rows [$i] ['id'] = ''; //行ID，是grid规定不可重复的ID标示
            $responce->rows [$i] ['cell'] = array('', '', '', '', '', '');
        }
        echo json_encode($responce);

    }

    public function add()
    {
        //print_r ( $_REQUEST );
        $arr = $_REQUEST;
        $success = 1;
        echo $success;

    }

    public function edit()
    {
        //print_r ( $_REQUEST );
        //
        $arr = $_REQUEST;
        $insert = array('page_tpl_name' => $arr ['page_tpl_name']);
        $this->db->where("page_tpl_id", $arr ['id']);
        $success = $this->db->update("cms_page_tpl", $insert);
        echo $success;
        echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
    }

    public function del()
    {
        $id = $this->input->post("id");
        $this->db->where_in("page_tpl_id", $id);
        //$success = $this->db->delete ( "cms_page_tpl" );
        echo $id;
    }

    public function convert_csv()
    {
        $fn = FCPATH . "llg.csv";
        //var_dump($fn);
        $rows = file($fn);
        $todo = array("symbol" => "XAUUSD", "zoom" => "d1");
        foreach ($rows as $row) {
            $fields = explode(",", $row);
            //var_dump($fields);
            $todo["datetime"] = trim(str_replace(".", "", $fields[0]));
            $todo["open"] = trim($fields[2]);
            $todo["high"] = trim($fields[3]);
            $todo["low"] = trim($fields[4]);
            $todo["close"] = trim($fields[5]);
            $todo["volumn"] = trim($fields[6]);
            $this->db->insert("chart_data", $todo);
            echo ".";
        }
    }

}

//end.
