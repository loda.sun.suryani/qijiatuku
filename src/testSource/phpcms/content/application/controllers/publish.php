<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Publish extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->database_default = $this->load->database ( 'default', true );
	}
	function index() {
		//权限检查-begin.
		$success = validation_check ( $this->uid, "publish" );
		if ($success != 1) {
			msg ( "无权限：发布页面(publish)", "", "message" );
			safe_exit ();
		}
		//权限检查-end.
		

		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		$page_info = $this->database_default->get_record_by_field ( "cms_page", 
			'page_id', $page_id );
		if ($page_info ['page_url_enabled'] < 1) {
			my_debug ( "URL没有经过管理员审核!" );
			return;
		}
		$page_url = strtolower ( $page_info ['page_url'] );
		if ('/' != substr ( $page_url, 0, 1 )) {
			my_debug ( "地址非法:" . $page_url );
			return;
		}
		if ('lmth' != substr ( strrev ( $page_url ), 0, 4 )) {
			my_debug ( "地址非法:" . $page_url );
			return;
		}
		
		if ($this->input->post ( 'submitform' )) {
			set_time_limit(150);
			$time_start = time ();
			$this->database_publish = $this->load->database ( "online", true );
			/*
			$rows = $this->database_publish->get_rows_by_sql ( "show open tables" );
			$flag_in_use = false;
			if ($rows) {
				foreach ( $rows as $v ) {
					$row = array_change_key_case ( $v );
					if ($row ['database'] == 'cmsreal' and $row ['table'] == 'cms_page') {
						if ($row ['in_use'] > 0) {
							$flag_in_use = true;
						}
					}
				}
			}
			if ($flag_in_use) {
				echo "<font color='red'>另一个发布进程正在执行,请稍后重试...</font>";
				return;
			}
			*/
			
			$this->database_publish->query ( "SET AUTOCOMMIT=0" );
			$this->database_publish->query ( "BEGIN" );
			$this->process_page ();
			foreach ( array ('cms_sql' ) as $table ) {
				echo __LINE__."<br>";flush();
				$this->process_table ( $table );
			}
			$this->database_publish->query ( "COMMIT" );
			$this->database_default->where ( 'page_id', $page_id );
			$this->database_default->update ( 'cms_page', 
				array ('publish_time' => time () ) );
			//清除缓存.
			$this->load->library ( 'CommonCache', '', 'cache' );
			$this->cache->delete ( $page_info ['page_site'] . $page_info ['page_url'] );

			$time_end = time ();
			$time_cost = $time_end - $time_start;
			if ($this->input->post ( 'ping' ) == 1) {
				$this->_baidu_ping ( $page_info );
			}
			//echo "<script>alert('发布成功');</script>";
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			echo "<script>if(parent.window.close_ed){parent.window.close_ed();}</script>";
			//my_debug ( '发布成功!' );
			//echo sprintf ( "<A href=\"http://%s%s\">点此查看</A>", $page_info ['page_site'],	$page_info ['page_url'] );
			//echo "<script>alert(\"用时:{$time_cost}秒\")</script>";
			//echo "<script>document.location=\"http://cms.tg.com.cn/content/index.php?c=page&page_id=$page_id&static=1\"</script>";
			//msg ( "用时:{$time_cost}秒)","http://cms.tg.com.cn/content/index.php?c=page&page_id=$page_id&static=1" );
			msg ( 
				"<br><br><br><br><br><br>发布操作正在进行,请不要关闭本页...", 
				"http://cms.tg.com.cn/content/index.php?c=page&page_id=$page_id&static=1" );
		} else {
			$view_data = array ();
			$view_data ['server_id_select'] = null;
			$this->load->config ( 'publish' );
			$publish_config = $this->config->item ( 'publish' );
			$view_data ['page_site'] = $page_info ['page_site'];
			$this->load->view ( 'publish_view', $view_data );
		}
	}
	
	function _postUrl($url, $postvar) {
		$ch = curl_init ();
		$headers = array (
				"POST " . $url . " HTTP/1.0", 
				"Content-type: text/xml;charset=\"utf-8\"", 
				"Accept: text/xml", 
				"Content-length: " . strlen ( $postvar ) );
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $postvar );
		$res = curl_exec ( $ch );
		curl_close ( $ch );
		return $res;
	}
	
	function _baidu_ping($ping_info) {
		$baiduXML = "
		    <?xml version=\"1.0\" encoding=\"UTF-8\"?>
		    <methodCall>
		    <methodName>weblogUpdates.extendedPing</methodName>
		    <params>
		    <param><value><string>" . $ping_info ['page_title'] . "</string></value></param>
		    <param><value><string>http://" . $ping_info ['page_site'] . "</string></value></param>
		    <param><value><string>http://" . $ping_info ['page_site'] . $ping_info ['page_url'] . "</string></value></param>
		    <param><value><string></string></value></param>
		    </params>
		    </methodCall>";
		$res = $this->_postUrl ( 'http://ping.baidu.com/ping/RPC2', $baiduXML );
		//下面是返回成功与否的判断（根据百度ping的接口说明）
		if (strpos ( $res, "<int>0</int>" ))
			echo "PING成功";
		else
			echo "PING失败";
	}
	
	function pageslog_insert($info = array()) {
		$this->load->config ( 'mongodbconfig' );
		$mongodb_config_arr = $this->config->item ( 'mongodbconfig' );
		$this->load->library ( 'mongodb', $mongodb_config_arr ["default"] ); //加载mongo类
		if (count ( $info )) {
			$pagesinfo = array ();
			$cms_all_db = $this->mongodb->mongo_all->cms; //全局可用的调用mongo 适用于非封装的mongo调用（在框架内）
			$pageslogdb = $cms_all_db->pageslog;
			$mongodb_id = $this->get_autoincre_id ( "pageslog", $cms_all_db );
			$info ["_id"] = $mongodb_id;
			$pageslogdb->insert ( $info );
			$pageslog_list = array ();
			$pageslog_list ["pageslog_id"] = $mongodb_id;
			$pageslog_list ["time"] = $info ['time'];
			$pageslog_list ["page_id"] = $info ['page_id'];
			$pageslog_listdb = $cms_all_db->pageslog_list;
			$pageslog_listdb->insert ( $pageslog_list );
		
		}
	
	}
	
	private function get_autoincre_id($name, $db) {
		$update = array ('$inc' => array ("id" => 1 ) );
		$query = array ('table_name' => $name );
		$command = array (
				'findandmodify' => 'autoincre_system', 
				'update' => $update, 
				'query' => $query, 
				'new' => true, 
				'upsert' => true );
		$id = $db->command ( $command );
		return $id ['value'] ['id'];
	}
	private function process_page() {
		$pageslog = array ();
		$time_start = microtime ( true );
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		$pageslog ['page_id'] = $page_id;
		$pageslog ['time'] = time ();
		$page_info = $this->database_default->get_record_by_field ( "cms_page", 
			'page_id', $page_id );
		if ($page_info) {
			//删除旧数据:同站点下同URL的页面记录
			$this->database_publish->where ( 'page_site', 
				$page_info ['page_site'] );
			$this->database_publish->where ( 'page_url', $page_info ['page_url'] );
			$this->database_publish->delete ( 'cms_page' );
			//echo "\n<br>", __LINE__, "/\n", $this->database_publish->last_query ();
			//删除旧数据:此page_id下block关联信息
			$this->database_publish->where ( 'page_id', 
				$page_info ['page_id'] );
			$this->database_publish->delete ( 'cms_page_block' );
			//echo "\n<br>", __LINE__, "/\n", $this->database_publish->last_query ();
			

			//覆盖到目标数据库
			$this->database_publish->replace ( 'cms_page', $page_info );
			//echo "\n<br>", __LINE__, "/\n", $this->database_publish->last_query ();
		}
		$pageslog ['cms_page'] = $page_info; //page发布日志数组构造
		$this->page_id = $page_id;
		//my_debug($page_info);
		

		$this->database_default->reconnect();
		//本页用到的page tpl
		$page_tpl_info = $this->database_default->get_record_by_field ( 
			"cms_page_tpl", 'page_tpl_id', $page_info ['page_tpl_id'] );
		if ($page_tpl_info) {
			//覆盖到目标数据库
			$this->database_publish->replace ( 'cms_page_tpl', 
				$page_tpl_info );
			//echo "\n<br>", __LINE__, "/\n", $this->database_publish->last_query ();
		}
		$pageslog ['cms_page_tpl'] = $page_tpl_info; //page发布日志数组构造
		//页面模板里get_block函数用到的block,也需要复制
		$page_tpl_content = $page_tpl_info ['tpl_content'];
		$matches = array ();
		if (preg_match_all ( '/{{get_block.*block_id=(.*)[^\s]*}}/', $page_tpl_content, 
			$matches ) > 0) {
			//my_debug ( $matches );
			foreach ( $matches [1] as $block_id ) {
				echo __LINE__."<br>";flush();
				$block_id = trim ( $block_id, "'\"" );
				$block_id = trim ( $block_id );
				$block_id = intval ( $block_id );
				if ($block_id) {
					$this->process_block ( $block_id );
				}
			}
		}
		$pageslog ['cms_page_block'] = array (); //page发布日志数组构造
		//本页用到的所有block
		$page_block_arr = $this->database_default->get_rows_by_sql ( 
			"SELECT * FROM cms_page_block WHERE page_id='{$page_id}' ORDER BY order_num ASC" );
		foreach ( $page_block_arr as $page_block ) {
			echo __LINE__."<br>";flush();
			//覆盖到目标数据库
			$this->database_publish->replace ( 'cms_page_block', 
				$page_block );
			//echo "\n<br>", __LINE__, "/\n", $this->database_publish->last_query ();
			$block_content = $this->process_block ( $page_block ['block_id'] );
			$pageslog ['cms_page_block'] [] = $page_block; //page发布日志数组构造
		}
		$pageslog ['cms_page_block_info'] = $this->pageslog_block;
		/*ip,domain静态化同步问题*/
		$host = $page_info ['page_site'];
		$uri = $page_info ['page_url'];
		$this->database_publish->where ( 'page_id', $page_id );
		$this->database_publish->delete ( 'cms_sync_log' );
		//echo "\n<br>", __LINE__, "/\n", $this->database_publish->last_query ();
		$time_end = microtime ( true );
		//echo "__LINE__:", __LINE__, "用时:", ($time_end - $time_start), "\n<br>";
		

		if ($page_info ['is_static']) {
			/* */
			//先读取此页一次,以生成最新的静态内容字段
			/*
			$static_content = curl_fetch ( 
				"http://127.0.0.1/content/index.php?c=page&page_id=$page_id&static=1" );
			if (! $static_content) {
				my_debug ( "发布失败,请重试一次..." );
				safe_exit ();
			}
			*/
			//此域名对应的所有ip
			$ip_arr = $this->database_publish->get_rows_by_field ( 
				'cms_ip_domain', 'domain', $page_info ['page_site'] );
			if ($ip_arr) {
				foreach ( $ip_arr as $ip_config ) {
					echo __LINE__."<br>";flush();
					$this->database_publish->insert ( 'cms_sync_log', 
						array (
								'page_id' => $page_id, 
								'client_ip' => $ip_config ['ip'], 
								'page_sync_time' => 0 ) );
					//echo "\n<br>", __LINE__, "/\n", $this->database_publish->last_query ();
				}
			} else {
				my_debug ( "此域名没有对应的IP地址,可能是系统配置有错误." );
			}
		}
		$time_end = microtime ( true );
		//$this->pageslog_insert ( $pageslog );//写日志到mongodb
		//echo "__LINE__:", __LINE__, "用时:", ($time_end - $time_start), "\n<br>";
	}
	
	function process_block($block_id) {
		$time_start = microtime ( true );
		$cms_page_block = array ();
		$block_info = $this->database_default->get_record_by_field ( "cms_block", 
			"block_id", $block_id );
		echo "[".$block_id."]";
		if ($block_info) {
			//覆盖到目标数据库
			$this->database_publish->replace ( 'cms_block', $block_info );
			$cms_page_block ['cms_block'] = $block_info; //page发布日志数组构造
		}
		$block_tpl_id = $block_info ['block_tpl_id'];
		$block_tpl_info = $this->database_default->get_record_by_field ( "cms_block_tpl", 
			"block_tpl_id", $block_tpl_id );
		if ($block_tpl_info) {
			//覆盖到目标数据库
			$this->database_publish->replace ( 'cms_block_tpl', 
				$block_tpl_info );
			$cms_page_block ['cms_block_tpl'] = $block_tpl_info; //page发布日志数组构造
		}
		
		//block模板里get_block函数用到的block,也需要复制
		/*注意: 如果get_block循环引用 ,可能会死循环!!!!!!!!!!!!!! 潜在危险需要改进*/
		$block_tpl_content = $block_tpl_info ['tpl_content'];
		$matches = array ();
		if (preg_match_all ( '/{{get_block.*block_id=(.*)[^\s]*}}/', $block_tpl_content, 
			$matches ) > 0) {
			//my_debug ( $matches );
			foreach ( $matches [1] as $b_id) {
				echo __LINE__."<br>";flush();
				$b_id = trim ( $b_id, "'\"" );
				$b_id = trim ( $b_id );
				$b_id = intval ( $b_id );
				if ($b_id) {
					$this->process_block ( $b_id );
				}
			}
		}
		
		//取此block_id关联的所有的 ds
		$ds_arr = $this->database_default->get_rows_by_field ( 
			'cms_datasource', 'block_id', $block_id );
		
		$this->database_publish->where ( 'block_id', $block_id );
		$this->database_publish->delete ( 'cms_datasource' );
		$cms_page_block ['cms_datasource'] = array (); //page发布日志数组构造
		foreach ( $ds_arr as $ds ) {
			echo __LINE__."<br>";flush();
			//覆盖到目标数据库
			$this->database_publish->replace ( 'cms_datasource', $ds );
			$cms_page_block ['cms_datasource'] [] = $ds; //page发布日志数组构造
			$this->database_publish->where ( 'ds_id', $ds ['ds_id'] );
			$this->database_publish->delete ( 'cms_form_record' );
			$cms_page_block ['cms_form_record'] = array (); //page发布日志数组构造
			if ('form' == $ds ['ds_type']) {
				//如果是表单输入的数据, 则需要找到所有相关的记录,复制过去
				$ds_id = $ds ['ds_id'];
				$rows = $this->database_default->get_rows_by_field ( "cms_form_record", 
					'ds_id', $ds_id );
				if ($rows) {
					foreach ( $rows as $row ) {
						echo __LINE__."<br>";flush();
						//覆盖到目标数据库
						$this->database_publish->replace ( 
							'cms_form_record', $row );
						$cms_page_block ['cms_form_record'] [] = $row; //page发布日志数组构造
					}
				}
			}
		}
		$time_end = microtime ( true );
		$this->pageslog_block [$block_id] = $cms_page_block;
		//echo "__LINE__:", __LINE__, "用时:", ($time_end - $time_start), "\n<br>";
		flush ();
	}
	
	function process_table($table) {
		$time_start = microtime ( true );
		$rows = $this->database_default->get_rows_by_sql ( "SELECT * FROM $table" );
		if ($rows) {
			foreach ( $rows as $row ) {
				echo __LINE__."<br>";flush();
				//覆盖到目标数据库
				$this->database_publish->replace ( $table, $row );
			}
		}
		$time_end = microtime ( true );
		//echo "__LINE__:", __LINE__, "用时:", ($time_end - $time_start), "\n<br>";
	}
}
