<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Zixun_message extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
		$this->load->library ( 'CommonCache', '', 'cache' );
		$this->count_page = 10; //分页每页显示条数
	}
	
	function index() {
		$view_data = array ();
		$page_id = $this->input->get ( 'page_id' ); //page_id
		//$user_id = $this->input->get ( 'user_id' ); //会员ID
		//$user_name = $this->input->get ( 'user_name' ); //user_name
		$ip = $_SERVER ["REMOTE_ADDR"];
		$theverify = $this->input->get ( 'theverify' );
		$pass = "cms_fuwu.jia.com"; //这里是说好的密码
		if (isset ( $_COOKIE ["www_jia_user_name"] )) {
			$username = $_COOKIE ["www_jia_user_name"];
			$user_name = $username;
		} else {
			$user_name = "游客";
		}
		
		if (isset ( $_COOKIE ["jia_html_id"] )) {
			$user_id = $_COOKIE ["jia_html_id"];
			$user_id = $this->uid ( $user_id );
		} else {
			$user_id = 0;
		}
		
		if (! $page_id) {
			$page_id = "0";
		}
		$view_data ["page_id"] = $page_id;
		$view_data ["user_name"] = $user_name;
		$view_data ["user_id"] = $user_id;
		//$check_key = cms_inner_verify ( $user_id, $ip, $pass );
		//my_debug($check_key);
		//if ($theverify != $check_key) {
		//echo "验证不通过！";
		//exit ();
		//}
		//
		/*验证
		$this->form_validation->set_rules ( 'comment_content', '文本框', 'required' );
		$this->form_validation->set_rules ( 'code', '验证码', 'required' );
		if ($this->form_validation->run () == TRUE) {
			$code = trim ( $this->input->post ( 'code' ) );
			$verify = trim ( $this->input->post ( 'verify' ) ); //获取验证码日期间
			if ($verify && $code) {
				$verify_code = $this->db->get_record_by_sql ( 
					"SELECT count(*) as tcount FROM com_verify_code WHERE verify='$verify' AND code='$code' " );
				$tcount = $verify_code ['tcount'];
			} else {
				$tcount = 0;
			}
			$comment_content = $this->input->post ( "comment_content" ); //复制给哪个用户的user_id
			if (! $user_name) {
				$user_name = "zixun";
			}
			
			$insert = array (
					'page_id' => $page_id, 
					'user_name' => $user_name, 
					'user_id' => $user_id, 
					'comment_content' => $comment_content, 
					'is_arbitrated' => 0, 
					"create_time" => time () );
			//exit;
			if ($tcount) {
				$db_ret = $this->db->insert ( "data_iframe_comment", $insert );
				if ($db_ret) {
					//msg ( "ok",  );
					echo ('<script language="JavaScript">');
					echo ("alert('成功');");
					echo "function timeout(){";
					echo ("location.href='" . modify_build_url () . "';");
					echo "}";
					echo "setTimeout(timeout,2000);";
					echo ('</script>');
					exit ();
				} else {
					echo "<span class=error>失败</span>";
				}
			} else {
				echo "<span class=error>失败</span>";
			}
		
		}*/
		
		$view_data ['grid'] = '';
		$view_data ['getpageinfo'] = '';
		$data_base = "data_iframe_comment";
		$ORDER = "auto_id";
		//=========列表===={{=============================================================		
		$sql_where = "WHERE page_id = '$page_id' AND is_arbitrated='1' ";
		
		//my_debug($sql_where);
		if ($this->input->get_post ( 'count_page' ) != '') {
			$count_page = $this->input->get_post ( 'count_page' );
		} else {
			$count_page = $this->count_page;
		}
		if ($this->input->get_post ( 'page' ) <= 0 || $this->input->get_post ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = $this->input->get_post ( 'page' );
		}
		$sql_count = "SELECT count(*) as tot FROM $data_base $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $row [0];
		
		$p_count = ceil ( $t_count / $count_page );
		if ($page > $p_count && $p_count > 0) {
			$page = $p_count;
		}
		$t_first = ($page - 1) * $count_page;
		$sql = "SELECT * FROM $data_base $sql_where ORDER BY $ORDER DESC";
		$sql = "$sql LIMIT $t_first,$count_page";
		//my_debug($sql);
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$data [$k] ['create_time'] = date ( "Y-m-d H:i:s", $row ['create_time'] );
				$data [$k] ['arbitrate_time'] = date ( "Y-m-d H:i:s", $row ['arbitrate_time'] );
			}
		}
		
		$view_data ['grid'] = $data;
		$getpageinfo = toolkit_pages_zixun ( $page, $t_count, modify_build_url ( array ('page' => '' ) ), 
			$count_page, 8, '' );
		if ($getpageinfo) {
			$view_data ['getpageinfo'] = $getpageinfo ['pagecode'];
		}
		$contenttext = $this->load->view ( 'comment_admin/show_ajax_view', $view_data, TRUE );
		$view_data ["contenttext"] = $contenttext;
		
		$this->load->view ( 'comment_admin/zixun_index_view', $view_data );
	}
	function show_ajax() {
		$page_id = $this->input->get ( 'page_id' ); //page_id
		$user_id = $this->input->get ( 'user_id' ); //会员ID
		

		$view_data = array ();
		$view_data ['grid'] = '';
		$view_data ['getpageinfo'] = '';
		$data_base = "data_iframe_comment";
		$ORDER = "auto_id";
		//=========列表===={{=============================================================		
		$sql_where = "WHERE page_id = '$page_id' AND is_arbitrated='1' ";
		
		//my_debug($sql_where);
		if ($this->input->get_post ( 'count_page' ) != '') {
			$count_page = $this->input->get_post ( 'count_page' );
		} else {
			$count_page = $this->count_page;
		}
		if ($this->input->get_post ( 'page' ) <= 0 || $this->input->get_post ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = $this->input->get_post ( 'page' );
		}
		$sql_count = "SELECT count(*) as tot FROM $data_base $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $row [0];
		
		$p_count = ceil ( $t_count / $count_page );
		if ($page > $p_count && $p_count > 0) {
			$page = $p_count;
		}
		$t_first = ($page - 1) * $count_page;
		$sql = "SELECT * FROM $data_base $sql_where ORDER BY $ORDER DESC";
		$sql = "$sql LIMIT $t_first,$count_page";
		//my_debug($sql);
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$data [$k] ['create_time'] = date ( "Y-m-d H:i:s", $row ['create_time'] );
			}
		}
		$view_data ['grid'] = $data;
		$getpageinfo = toolkit_pages_zixun ( $page, $t_count, modify_build_url ( array ('page' => '' ) ), 
			$count_page, 8, '' );
		if ($getpageinfo) {
			$view_data ['getpageinfo'] = $getpageinfo ['pagecode'];
		}
		echo $this->load->view ( 'comment_admin/show_ajax_view', $view_data, TRUE );
	}
	function fuwu_del() {
		$comment_id = intval ( $this->input->get_post ( 'id' ) );
		$this->db->where ( 'comment_id', $comment_id );
		$success = $this->db->update ( 'com_comment', array ('is_arbitrated' => '2' ) );
		echo $success;
	}
	function addajax() {
		/*验证*/
		$page_id = $this->input->get ( 'page_id' ); //page_id
		//$user_id = $this->input->get ( 'user_id' ); //会员ID
		//$username = $this->input->get ( 'user_name' ); //user_name
		$this->form_validation->set_rules ( 'comment_content', '文本框', 'required' );
		$this->form_validation->set_rules ( 'code', '验证码', 'required' );
		/*验证入库*/
		if ($this->form_validation->run () == TRUE) {
			$code = trim ( $this->input->get_post ( 'code' ) );
			$verify = trim ( $this->input->get_post ( 'verify' ) ); //获取验证码日期间
			if ($verify && $code) {
				$verify_code = $this->db->get_record_by_sql ( 
					"SELECT count(*) as tcount FROM com_verify_code WHERE verify='$verify' AND code='$code' " );
				$tcount = $verify_code ['tcount'];
			} else {
				$tcount = 0;
			}
			$comment_content = $this->js_unescape ( $this->input->get_post ( "comment_content" ) ); //复制给哪个用户的user_id
			

			//获取cookie
			if (isset ( $_COOKIE ["www_jia_user_name"] )) {
				$username = $_COOKIE ["www_jia_user_name"];
				$user_name = $username;
			} else {
				$user_name = "游客";
			}
			
			if (isset ( $_COOKIE ["jia_html_id"] )) {
				$user_id = $_COOKIE ["jia_html_id"];
				$user_id = $this->uid ( $user_id );
			} else {
				$user_id = 0;
			}
			
			$insert = array (
					'page_id' => $page_id, 
					'user_name' => $user_name, 
					'user_id' => $user_id, 
					'comment_content' => $comment_content, 
					'is_arbitrated' => 0, 
					"create_time" => time () );
			//exit;
			if ($tcount) {
				$db_ret = $this->db->insert ( "data_iframe_comment", $insert );
				if ($db_ret) {
					$this->db->where ( 'verify', $verify );
					$this->db->where ( 'code', $code );
					$this->db->delete ( 'com_verify_code' );
					echo $db_ret;
				} else {
					echo 0;
				}
			} else {
				echo $tcount;
			}
		
		}
	
	}
	
	private function js_unescape($str) {
		$ret = '';
		$len = strlen ( $str );
		
		for($i = 0; $i < $len; $i ++) {
			if ($str [$i] == '%' && $str [$i + 1] == 'u') {
				$val = hexdec ( substr ( $str, $i + 2, 4 ) );
				
				if ($val < 0x7f)
					$ret .= chr ( $val );
				else if ($val < 0x800)
					$ret .= chr ( 0xc0 | ($val >> 6) ) . chr ( 0x80 | ($val & 0x3f) );
				else
					$ret .= chr ( 0xe0 | ($val >> 12) ) . chr ( 0x80 | (($val >> 6) & 0x3f) ) . chr ( 
						0x80 | ($val & 0x3f) );
				
				$i += 5;
			} else if ($str [$i] == '%') {
				$ret .= urldecode ( substr ( $str, $i, 3 ) );
				$i += 2;
			} else
				$ret .= $str [$i];
		}
		return $ret;
	}
	private function pass($shop_id, $theverify) {
		//服务商家
		$ip = $_SERVER ["REMOTE_ADDR"];
		$pass = "cms_diaoding.jia.com"; //这里是说好的密码
		$check_key = cms_inner_verify ( $shop_id, $ip, $pass );
		my_debug ( $check_key );
		if ($theverify != $check_key) {
			echo "验证不通过！";
			exit ();
		}
	}
	
	function validationcode() {
		$str = $this->random ( 4 ); //随机生成的字符串 
		$width = 50; //验证码图片的宽度 
		$height = 22; //验证码图片的高度 
		@header ( "Content-Type:image/png" );
		$im = imagecreate ( $width, $height );
		//背景色 
		$back = imagecolorallocate ( $im, 0xff, 0xff, 0xff );
		//模糊点颜色 
		$pix = imagecolorallocate ( $im, 255, 255, 255 ); //187,230,247//255,255,255
		//字体色 
		$font = imagecolorallocate ( $im, 41, 163, 238 );
		//绘模糊作用的点 
		mt_srand ();
		for($i = 0; $i < 1000; $i ++) {
			imagesetpixel ( $im, mt_rand ( 0, $width ), mt_rand ( 0, $height ), $pix );
		}
		imagestring ( $im, 5, 7, 5, $str, $font );
		imagerectangle ( $im, 0, 0, $width - 1, $height - 1, $font );
		imagepng ( $im );
		imagedestroy ( $im );
		//$this->session->set_userdata ( 'validationcode', $str );
		

		$time_now = time ();
		//删除1小时前的临时page
		$this->db->query ( 
			sprintf ( "DELETE FROM com_verify_code WHERE create_time<%s", $time_now - 3600 * 1 ) );
		$verify = trim ( $this->input->get ( 'nowtime' ) );
		$up = array ('verify' => $verify, 'code' => $str, 'create_time' => time () );
		$success = $this->db->insert ( "com_verify_code", $up );
		echo $str;
	}
	private function random($len) {
		$srcstr = "0123456789"; //ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 
		mt_srand ();
		$strs = "";
		for($i = 0; $i < $len; $i ++) {
			$strs .= $srcstr [mt_rand ( 0, 9 )];
		}
		return strtoupper ( $strs );
	}
	private function uid($user_id) {
		$api_server = "http://10.10.21.126:10005/user/"; //外网测试
		$api = "getDecodeUID";
		$req_body = "{'app_id':'201','encode_uid':'$user_id'}";
		$array = json_decode ( $this->do_post_api ( $api_server, $api, $req_body ), true );
		if (count ( $array )) {
			$user_id = $array ["result"];
		} else {
			$user_id = 0;
		}
		
		return $user_id;
	}
	private function do_post_api($api_server, $api, $req_body, $port = 9091) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, "$api_server$api" );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 2000 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $req_body );
		$data = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			exit ( 
				"function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
		}
		return $data;
	}

}
