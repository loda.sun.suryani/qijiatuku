<?php

class _tg_api_upload extends __tg_api{

	function __construct()
	{
		parent::__construct();

		$this->load("http");

		$this->api_server="http://api.tg.com.cn";
		$x=mt_rand(1,3);
		if($x==1)
		{
			$x="";
		}
		$this->image_server="http://ii1{$x}.tg.com.cn";
	}


	function from_files($files,$vars=array())
	{
		if(@is_array($files)){
			foreach($files as $file){
				$result[]=tg_func_read_ouput($this->http->post_files($this->api_server."/upload",$file,$vars));
			}
		}else{
				$result=tg_func_read_ouput($this->http->post_files($this->api_server."/upload",$files,$vars));
		}
		return $result;
	}



	function get_ext($ext_num)
	{
		if($ext_num==1) return "jpg";
		if($ext_num==2) return "gif";
		if($ext_num==3) return "bmp";
		if($ext_num==4) return "jpeg";
		if($ext_num==5) return "png";
		if($ext_num==6) return "txt";
		if($ext_num==7) return "doc";
		if($ext_num==8) return "swf";
		if($ext_num==20) return "flv";
		if($ext_num==19) return "tmp";
		if($ext_num==18) return "rar";
		if($ext_num==17) return "image";
		if($ext_num==16) return "xls";
		if($ext_num==15) return "application";
		if($ext_num==21) return "pdf";
		if($ext_num==22) return "error";
		if($ext_num==23) return "exe";
		if($ext_num==24) return "video";
		if($ext_num==25) return "zip";
		if($ext_num==26) return "htm";
		if($ext_num==27) return "message";
		if($ext_num==29) return "csv";
		if($ext_num==30) return "ppt";
		if($ext_num==31) return "docx";
		if($ext_num==32) return "xlsx";
		if($ext_num==33) return "pptx";
		if($ext_num==34) return "psd";
		if($ext_num==35) return "dwg";
		if($ext_num==36) return "5";
		if($ext_num==37) return "com";
		if($ext_num==38) return "tif";
        return "";
	}


	function get_url($id)
	{
        $totalid=$id;
		if(strlen($id)>20)
		{
			return $this->image_server."/upimg/view/$id";
		}
		@list($id,$ext_num)=explode(".",$id);
		$iddir=strval(floatval($id)+floatval(100000000));
		$iddir=$iddir[0].$iddir[1].$iddir[2]."/".$iddir[3].$iddir[4].$iddir[5];

        $dir=$iddir;
        $ext=$this->get_ext($ext_num);
        if($ext && $dir) {
            return "http://i1.tg.com.cn/$dir/$id.$ext";
        }
		return $this->image_server."/upimg/view/$totalid";
	}

	function get_url_rand($id)
	{
        $totalid=$id;
		if(strlen($id)>20)
		{
			return $this->image_server."/upimg/view/$id";
		}
		list($id,$ext_num)=explode(".",$id);
		$iddir=strval(floatval($id)+floatval(100000000));
		$iddir=$iddir[0].$iddir[1].$iddir[2]."/".$iddir[3].$iddir[4].$iddir[5];

        $dir=$iddir;
        $ext=$this->get_ext($ext_num);
        if($ext && $dir) {
			$last=substr(strval($id),-1);
			if($last<=3){
				$i="";
			}
			elseif($last<=6){
				$i="2";
			}else{
				$i="3";
			}
            return "http://i1{$i}.tg.com.cn/$dir/$id.$ext";
        }
		return $this->image_server."/upimg/view/$totalid";
	}


	function get_secure_url($id)
	{
		return $this->image_server."/upimg/read/$id/skey/jsi8d90xa";
	}

}
