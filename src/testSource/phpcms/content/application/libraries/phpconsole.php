<?php
class PhpConsole {
	private $params = array ();
	private $counter = 0;
	private $uniq = 0;
	function __construct() {
		$this->params = array ('[begin]' => __LINE__ .date(",Y-m-d H:i:s",time()));
		$this->uniq = (uniqid () . "/" . $_SERVER ['REMOTE_ADDR']);
	}
	function __destruct() {
		//my_debug ( $this->params );
		$this->_do_post ( 
			"http://192.168.96.171:8585/phpconsole/" . $this->uniq, $this->params );
	}
	function flush() {
		$this->_do_post ( "http://192.168.96.171:8585/phpconsole/" . $this->uniq, 
			$this->params );
		$this->params = array ('[begin]' => __LINE__ );
	}
	function log($line_num, $data) {
		$this->counter ++;
		$k = ($this->counter . "_line_$line_num");
		$content = null;
		if (is_array ( $data )) {
			$content = var_export ( $data, true );
		} else {
			$content = $data;
		}
		$this->params [$k] = $content;
	}
	private function _do_post($url, $data) {
		try {
			$ch = curl_init ( $url );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true ); // 返回字符串，而非直接输出
			curl_setopt ( $ch, CURLOPT_HEADER, false ); // 不返回header部分
			curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 2 ); // 设置socket连接超时时间
			@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 1000 );
			
			if (is_null ( $data )) {
				// GET
			} else if (is_string ( $data )) {
				curl_setopt ( $ch, CURLOPT_POST, true );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
				// POST
			} else if (is_array ( $data )) {
				// POST
				//my_debug ( $data );
				curl_setopt ( $ch, 
					CURLOPT_POST, true );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, http_build_query ( $data ) );
			}
			
			@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 20000 );
			set_time_limit ( 30 ); // 设置自己服务器超时时间
			$str = curl_exec ( $ch );
			$curl_errno = curl_errno ( $ch );
			$curl_error = curl_error ( $ch );
			curl_close ( $ch );
			if ($curl_errno > 0) {
				return null;
			}
			return $str;
		} catch ( Exception $e ) {
			return null;
		}
		return $str;
	}
}
