<?php
if (! defined ( 'DS' )) {
	define ( 'DS', DIRECTORY_SEPARATOR );
}
define ( "TG_API_BASE_PATH", dirname ( __FILE__ ) . DS );
define ( "TG_API_MODULE_DIR", "tgapi" . DS );
class __tg_api {
	function __construct() {
	}
	function load($module_list) {
		//加载模块
		$module_list = @explode ( " ", @trim ( $module_list ) );
		if (@is_array ( $module_list )) {
			foreach ( $module_list as $module ) {
				include_once TG_API_BASE_PATH . TG_API_MODULE_DIR . $module . ".php";
				$classname = "_tg_api_" . $module;
				$this->$module = new $classname ();
			}
		}
	}
}

class TgApi {
	function __construct() {
		$this->__the_tg_api = new __tg_api ();
	}
	function get_tg_api() {
		return $this->__the_tg_api;
	}

}
function tg_func_ext($f) {
	if (strpos ( $f, "." ) !== false) {
		return strtolower ( end ( explode ( ".", $f ) ) );
	}
	return "";
}

function tg_func_read_ouput($s) {
	return unserialize ( $s );
}

function tg_func_gbk2utf8($s) {
	return mb_convert_encoding ( $s, "UTF-8", "GBK" );
}

function tg_func_utf82gbk($s) {
	return mb_convert_encoding ( $s, "GBK", "UTF-8" );
}

