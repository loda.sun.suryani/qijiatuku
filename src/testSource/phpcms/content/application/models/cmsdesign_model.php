<?php
class Cmsdesign_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
		$this->table_cms_design = "cms_design";
		$this->table_cms_elements_design = "cms_elements_design";
	
	}
	function row_select($design_title, $design_id) {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_design} WHERE design_title='$design_title' and design_id!='$design_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function linshi() {
		$design_date = '0';
		$this->db->where ( 'design_date', $design_date );
		$ret = $this->db->delete ( "{$this->table_cms_design}" );
		return $ret;
	}
	function tpl_category_insert($data) {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_elements_design} WHERE design_category='" . $data ['design_category'] . "'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		if ($row ['t_count'] == 0 && $data ['design_category'] != '') {
			$ret = $this->db->insert ( "{$this->table_cms_elements_design}", $data );
			$id = $this->db->insert_id ();
			return $id;
		} else {
			return 0;
		}
	
	}
	function insert($data) {
		$ret = $this->db->insert ( "{$this->table_cms_design}", $data );
		$id = $this->db->insert_id ();
		return $id;
	}
	
	function row_array() {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_design} WHERE design_date<>0";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array($t_first, $count_page) {
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_design} WHERE design_date<>0 order by design_id desc LIMIT $t_first,$count_page" );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$array [$k] ['design_date'] = date ( "Y-m-d", $row ['design_date'] );
				
				$sql = "SELECT count(*) as t_count FROM {$this->table_cms_elements_design} WHERE design_category_id='" . $row ['design_category_id'] . "'";
				$q = $this->db->query ( $sql );
				$r = $q->row_array ();
				if ($r ['t_count'] > 0) {
					$sql = "SELECT design_category FROM {$this->table_cms_elements_design} WHERE design_category_id='" . $row ['design_category_id'] . "' ";
					$qu = $this->db->query ( $sql );
					$E = $qu->row_array ();
					$array [$k] ['design_category'] = $E ['design_category'];
				} else {
					$array [$k] ['design_category'] = "<font color='#FF0000'>此元素分类已删除</font>";
				}
			
			}
		}
		return $array;
	}
	
	function row_array_search($design_title, $design_category_id) {
		$where = " design_date<>0 AND design_title like '%$design_title%'";
		if ($design_category_id == 'all') {
			$where .= "";
		} else {
			$where .= " AND  design_category_id='$design_category_id'";
		}
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_design} WHERE $where";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array_search($design_title, $design_category_id, $t_first, $count_page) {
		$where = " design_date<>0 AND design_title like '%$design_title%'";
		if ($design_category_id == 'all') {
			$where .= "";
		} else {
			$where .= " AND  design_category_id='$design_category_id'";
		}
		$sql = "SELECT * FROM {$this->table_cms_design} WHERE $where order by design_id desc LIMIT $t_first,$count_page";
		$query = $this->db->query ( $sql );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$array [$k] ['design_date'] = date ( "Y-m-d", $row ['design_date'] );
				
				$sql = "SELECT count(*) as t_count FROM {$this->table_cms_elements_design} WHERE design_category_id='" . $row ['design_category_id'] . "'";
				$query = $this->db->query ( $sql );
				$r = $query->row_array ();
				if ($r ['t_count'] > 0) {
					$sql = "SELECT design_category FROM {$this->table_cms_elements_design} WHERE design_category_id='" . $row ['design_category_id'] . "' ";
					$query = $this->db->query ( $sql );
					$E = $query->row_array ();
					$array [$k] ['design_category'] = $E ['design_category'];
				} else {
					$array [$k] ['design_category'] = "<font color='#FF0000'>此元素分类已删除</font>";
				}
			}
		}
		return $array;
	}
	
	function update($set, $design_id) {
		$condition = "design_id ='$design_id'";
		$this->db->where ( $condition );
		//$this->db->where('design_id', $design_id);
		$success = $this->db->update ( "{$this->table_cms_design}", $set );
		return $success;
	}
	function selectlist($design_id) {
		$sql = "SELECT * FROM {$this->table_cms_design} where design_id='$design_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row;
	}
	function delete($design_id, $url) {
		if ($design_id != '') {
			$sql = "SELECT design_pic,design_psd FROM {$this->table_cms_design} where design_id='$design_id'";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			//print_r($row);
			@unlink ( $url . $row ['design_pic'] );
			@unlink ( $url . $row ['design_psd'] );
		}
		$condition = "design_id ='$design_id'";
		$this->db->where ( $condition );
		$ret = $this->db->delete ( "{$this->table_cms_design}" );
		return $ret;
	}
	
	function design_category_id_array() {
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_elements_design} order by design_category_id desc" );
		$array = $query->result_array ();
		$name['all']="-全部元素-";
		if (is_array ( $array )) {
			foreach ( $array as $k => $v ) {
				$id = $v ['design_category_id'];
				$name [$id] = $v ['design_category'];
			}
		}
		
		return $name;
	}
	
	function countclassification() {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_elements_design}";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function classification_array($t_first, $count_page) {
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_elements_design} order by design_category_id desc LIMIT $t_first,$count_page" );
		$array = $query->result_array ();
		return $array;
	}
	function classup($design_category_id) {
		$sql = "SELECT * FROM {$this->table_cms_elements_design} where design_category_id='$design_category_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row;
	}
	function classup_update($data, $design_category_id) {
		$condition = "design_category_id ='$design_category_id'";
		$this->db->where ( $condition );
		$up = $this->db->update ( "{$this->table_cms_elements_design}", $data );
		return $up;
	}
	function classdel($design_category_id) {
		$this->db->where ( 'design_category_id', $design_category_id );
		$ret = $this->db->delete ( "{$this->table_cms_elements_design}" );
		return $ret;
	}
	function replace($str) {
		
		//$str=preg_replace("#<([php?%]).*?\\1>#s", "", $str); 
		//$str=str_replace("<?", "", $str);
		//$str=str_replace("<%", "", $str);
		//$str=str_replace("<?php", "", $str);
		$str = htmlentities ( $str, ENT_NOQUOTES, "UTF-8" );
		
		return trim ( $str );
	}

}
?>
