<?php
class Cmspage_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
		$this->table_cms_page_tpl = "cms_page_tpl";
		$this->table_cms_page = "cms_page";
	
	}
	
	function insert($data) {
		$ret12 = $this->db->insert ( "{$this->table_cms_page_tpl}", $data );
		$ret = $this->db->insert_id ();
		return $ret;
	}
	function linshi() {
		$create_time = '0';
		$this->db->where ( 'create_time', $create_time );
		$ret = $this->db->delete ( "{$this->table_cms_page_tpl}" );
		return $ret;
	}
	function row_select($page_tpl_name,$page_tpl_id) {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_page_tpl} WHERE page_tpl_name='$page_tpl_name' and page_tpl_id!='$page_tpl_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function row_array() {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_page_tpl} WHERE create_time<>0";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array($t_first, $count_page) {
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_page_tpl}  WHERE create_time<>0 order by page_tpl_id desc LIMIT $t_first,$count_page" );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$array [$k] ['create_time'] = date ( "Y-m-d", $row ['create_time'] );
				$array [$k] ['modify_time'] = date ( "Y-m-d", $row ['modify_time'] );
				
				//更新入库是否已经使用-------------start--------
				$page_tpl_id = $row ['page_tpl_id'];
				$page_tpl_id_count = $this->db->query ( "SELECT count(page_tpl_id) as t_count FROM {$this->table_cms_page} WHERE page_tpl_id='$page_tpl_id' " )->row_array ();
				$array [$k] ['page_tpl_id_count'] = $page_tpl_id_count ['t_count'];
				if ($page_tpl_id_count ['t_count'] >= 1) {
					$up = array ('disable' => "2" );
					$this->db->where ( 'page_tpl_id', $page_tpl_id );
					$id = $this->db->update ( "{$this->table_cms_page_tpl}", $up );
				}
				//更新入库是否已经使用------------end---------
			}
		}
		return $array;
	}
	
	function row_array_search($page_tpl_name, $type) {
		$where = "create_time<>0 and page_tpl_name like '%$page_tpl_name%'";
		if ($type == 'all') {
			$where .= "";
		} else {
			$where .= " and  disable='$type'";
		}
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_page_tpl} WHERE $where";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array_search($page_tpl_name, $type, $t_first, $count_page) {
		$where = "create_time<>0 and page_tpl_name like '%$page_tpl_name%'";
		if ($type == 'all') {
			$where .= "";
		} else {
			$where .= " and  disable='$type'";
		}
		$sql = "SELECT * FROM {$this->table_cms_page_tpl} WHERE $where order by page_tpl_id desc LIMIT $t_first,$count_page";
		//echo $sql;
		$query = $this->db->query ( $sql );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$array [$k] ['create_time'] = date ( "Y-m-d", $row ['create_time'] );
				$array [$k] ['modify_time'] = date ( "Y-m-d", $row ['modify_time'] );
				//更新入库是否已经使用-------------start--------
				$page_tpl_id = $row ['page_tpl_id'];
				$page_tpl_id_count = $this->db->query ( "SELECT count(page_tpl_id) as t_count FROM {$this->table_cms_page} WHERE page_tpl_id='$page_tpl_id' " )->row_array ();
				$array [$k] ['page_tpl_id_count'] = $page_tpl_id_count ['t_count'];
				if ($page_tpl_id_count ['t_count'] >= 1) {
					$up = array ('disable' => "2" );
					$this->db->where ( 'page_tpl_id', $page_tpl_id );
					$id = $this->db->update ( "{$this->table_cms_page_tpl}", $up );
				}
				//更新入库是否已经使用------------end---------
			}
		}
		return $array;
	}
	
	function update($set, $page_tpl_id) {
		$condition = "page_tpl_id ='$page_tpl_id'";
		$this->db->where ( $condition );
		//$this->db->where('page_tpl_id', $page_tpl_id);
		$success = $this->db->update ( "{$this->table_cms_page_tpl}", $set );
		return $success;
	}
	function selectlist($page_tpl_id) {
		$sql = "SELECT * FROM {$this->table_cms_page_tpl} where page_tpl_id='$page_tpl_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row;
	}
	function delete($page_tpl_id, $url) {
		if ($page_tpl_id != '') {
			$sql = "SELECT demo_pic_id FROM {$this->table_cms_page_tpl} where page_tpl_id='$page_tpl_id'";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			@unlink ( $url . $row ['demo_pic_id'] );
		}
		$this->db->where ( 'page_tpl_id', $page_tpl_id );
		$ret = $this->db->delete ( "{$this->table_cms_page_tpl}" );
		return $ret;
	}
	
	function update_disable($set, $page_tpl_id) {
		$this->db->where ( 'page_tpl_id', $page_tpl_id );
		$success = $this->db->update ( "{$this->table_cms_page_tpl}", $set );
		return $success;
	}
	
	function replace($str) {
		
		/*$str=preg_replace("#<([php?%]).*?\\1>#s", "", $str); 
	   $str=str_replace("<?", "", $str);
	   $str=str_replace("<%", "", $str);
	   $str=str_replace("<?php", "", $str);*/
		$str = htmlentities ( $str, ENT_NOQUOTES, "UTF-8" );
		
		return trim ( $str );
	}

}
?>
