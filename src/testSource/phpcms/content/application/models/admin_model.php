<?php
class Admin_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
		
		$this->table_cms_user = "cms_user";
	
	}
	
	function admin_select_count($username, $password) {
		$sql = "SELECT count(*) as T_count FROM {$this->table_cms_user} where user_name='$username' and  user_password='$password' and user_disable<>1";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['T_count'];
	}
	function admin_select_row($username, $password) {
		$sql = "SELECT * FROM {$this->table_cms_user} where user_name='$username' and  user_password='$password' and user_disable<>1";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row;
	}
	function selectlist($user_id) {
		$sql = "SELECT * FROM {$this->table_cms_user} where user_id='$user_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row;
	}
	function random($len) {
		$srcstr = "0123456789"; //ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 
		mt_srand ();
		$strs = "";
		for($i = 0; $i < $len; $i ++) {
			$strs .= $srcstr [mt_rand ( 0, 9 )];
		}
		return strtoupper ( $strs );
	}
	function user_insert($data) {
		$ret12 = $this->db->insert ( "{$this->table_cms_user}", $data );
		$ret = $this->db->insert_id ();
		return $ret;
	}
	function user_update($set, $user_id) {
		$this->db->where ( 'user_id', $user_id );
		$success = $this->db->update ( "{$this->table_cms_user}", $set );
		return $success;
	}
	function user_del($user_id) {
		$this->db->where ( 'user_id', $user_id );
		$ret = $this->db->delete ( "{$this->table_cms_user}" );
		//当删除用户的时候，顺便也就把cms_user_to_role绑定角色也就给删除掉了 
		$this->db->where ( 'user_id', $user_id );
		$this->db->delete ( "cms_user_to_role" );
		//当删除用户的时候，顺便也就把cms_user_to_permission绑定权限也就给删除掉了 
		$this->db->where ( 'user_id', $user_id );
		$this->db->delete ( "cms_user_to_permission" );
		return $ret;
	}
	function row_array_count($sql_where) {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_user}  $sql_where";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array($t_first, $count_page,$sql_where) {
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_user}  $sql_where order by user_id desc LIMIT $t_first,$count_page" );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$onlineip=read_ip ( $row ['user_ip_last'] );
				$array [$k] ['user_ip_first'] = read_ip ( $row ['user_ip_first'] );
				$array [$k] ['user_ip_last'] = $onlineip;
				$area = file_get_contents ( "http://10.10.20.147:8083/ipserver/city/$onlineip/" );
				$array [$k] ['area'] = $area;
				$array [$k] ['user_time_first'] = date ( "Y-m-d", $row ['user_time_first'] );
				$array [$k] ['user_time_last'] = date ( "Y-m-d", $row ['user_time_last'] );
			}
		}
		return $array;
	}
	//跑数据
	function pao_cms_data($data,$column,$conditions) {
		$query = $this->db->query ( "SELECT $column FROM $data WHERE $conditions  " );
		$array = $query->result_array ();
		return $array;
	}
	function pao_up($data,$field,$set, $ds_id) {
		$this->db->where ( "$field", $ds_id );
		$success = $this->db->update ( "$data", $set );
		return $success;
	}

}
?>
