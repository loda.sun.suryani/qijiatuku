<?php
class AdApi extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
	}
	public function setting($ds_id = null) {
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		if ($ds_id) {
			$ci = &get_instance ();
			$page_city = $ci->page_city;
			$sql = "SELECT * FROM cms_datasource where ds_id=$ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$req_body = json_decode ( $row ['ds_api_config'], true );
			$pos_ids = $req_body ['pos_id'];
			$flip = array_flip ( $pos_ids );
			$areaflag = $req_body ['areaflag'];
			if ($page_city) {
				$areaflag = $page_city;
			}
			$ret = array ();
			foreach ( $pos_ids as $k => $v ) {
				//my_debug($v);
				$req_url = "http://gao.tg.com.cn/api/api_cms/get_publish_info?pos_id=$v&areaflag=$areaflag";
				$pos_json = $this->do_get_api ( $req_url );
				$pos_array = json_decode ( $pos_json, true );
				if (! $pos_array) {
					$pos_json = str_replace ( '\\', "/", $pos_json );
					$pos_array = json_decode ( trim($pos_json), true );
				}
				//my_debug ( $pos_array );
				//广告相关信息
				$ad_img = $pos_array ['result'] ['img']; //图片
				if (substr ( $ad_img, - 12, 12 ) == "/upimg/view/") {
					$ad_img = '';
				}
				$ad_link = $pos_array ['result'] ['link']; //链接
				if (substr ( $ad_link, - 7, 7 ) == "http://") {
					$ad_link = '';
				}
				if($pos_array ['msg']==1){
					$ad_text1 = $pos_array ['result'] ['text'] [0]; //文字说明一
					$ad_text2 = $pos_array ['result'] ['text'] [1]; //文字说明二
					$ad_text3 = $pos_array ['result'] ['text'] [2]; //文字说明三
					$goods_id = $pos_array ['result'] ['goods_id']; //商品id
				}else{
					$ad_text1 = ""; //文字说明一
					$ad_text2 = ""; //文字说明二
					$ad_text3 = ""; //文字说明三
					$goods_id = ""; //商品id
					my_debug("广告接口有问题：".$req_url);
					my_debug($pos_json);
					my_debug($pos_array);
					safe_exit();
				}
				
				if ($goods_id) {
					//通过goods_id读取商品信息
					$goods_req_url = "http://10.10.21.126:9101/item/getItemList?itemId=" . $goods_id;
					$goods_content_json = $this->do_get_api ( $goods_req_url );
					$goods_content_array = json_decode ( $goods_content_json, true );
					$goods_rows = $goods_content_array ['result']; //商品基本信息
					$goods_base = $goods_content_array ['result'] [0] ['itemBase'];
					$goods_image = $goods_content_array ['result'] [0] ['itemImage'] [0];
				}
				if ($goods_rows) {
					$thum = str_replace ( '.', '_100x100.', $goods_image ['imagePath'] );
					//========================{{调用商品评价相关数据个接口搬出去
					$pingjia_req = "{\"item_id\":{$goods_id}, \"start\":1,\"size\":1}";
					$pingjia_list_json = $this->do_post_api ( "http://10.10.21.126:9105", "/review/_listOfItem", $pingjia_req );
					$pingjia_list = json_decode ( $pingjia_list_json, true );
					$pjjia_num = $pingjia_list ['content'] ['totalCount'];
					$new_pingjia = $pingjia_list ['content'] ['list'] [0] ['content'];
					$new_rating = $pingjia_list ['content'] ['totalAvgRatingsList'] [0] ['rating'];
					//========================{{.end=========================================
					//========================{{调用店铺名称====================================
					$shopName_req_url = "http://10.10.21.126:9101/shopInfo/getShopNameAndSoldCount?shopId=" . $goods_base ['shopId'];
					$shopName_json = $this->do_get_api ( $shopName_req_url );
					$shopName_data = json_decode ( $shopName_json, true );
					//my_debug($shopName_data);
					$shopName = $shopName_data ['result'] [0] ['shopName'];
					//========================}}.end=============================================
					$ret [$k] ['key1'] = intval ( $goods_id ); //key1产品id
					$ret [$k] ['key2'] = "http://mall.jia.com/item/" . $goods_id; //key2产品地址
					$ret [$k] ['key3'] = "http://imgmall.tg.com.cn/" . $goods_image ['imagePath']; //key3图片地址
					$ret [$k] ['key4'] = $goods_base ['name']; //key4产品名称
					$ret [$k] ['key5'] = $goods_base ['name']; //key5
					$ret [$k] ['key6'] = $goods_base ['promotionId']; //key6 用于判断是否促销 1为有促销 0为无促销				
					$ret [$k] ['key7'] = $goods_base ['promotionPrice'] / 100; //key7促销价
					$ret [$k] ['key8'] = $goods_base ['marketPrice'] / 100; //key8市场价
					$ret [$k] ['key9'] = $goods_base ['qeekaPrice'] / 100; //key9齐家价
					$ret [$k] ['key10'] = $goods_base ['instoreCount']; //key10正常库存 
					$ret [$k] ['key11'] = $goods_base ['sellCount']; //key11已售出(历史销量)
					$ret [$k] ['key12'] = $pjjia_num; //key12已有评价数量
					$ret [$k] ['key13'] = $goods_base ['unitName']; //key13单位
					if ($ret [$k] ['key6'] > 0) {
						$ret [$k] ['key14'] = $goods_base ['promotionPrice'] / 100; ////key14有促销显示促销价
						$ret [$k] ['key15'] = $goods_base ['promotionName']; //key15促销方案名称
						$ret [$k] ['key16'] = $goods_base ['promotionTemplateName']; //key16促销类型名 如 ：秒杀，抢购等						
						$ret [$k] ['key17'] = $goods_base ['promotionInstoreCount']; //key17促销库存
						$ret [$k] ['key18'] = date ( "F j, Y, H:i:s ", $goods_base ['promotionStartTime'] / 1000 ); //key18促销开始时间
						$ret [$k] ['key19'] = date ( "F j, Y, H:i:s ", $goods_base ['promotionEndTime'] / 1000 ); //key19促销结束时间
						//========================{{调用促销销量===========================
						$req_body = "{\"promotionId\": \"{$goods_base['promotionId']}\",\"itemIds\": [\"{$goods_id}\"]}";
						$promotionSales = $this->do_post_api ( "http://dingdan.api.tg.local", "/order/getSalesCountForItemPro.htm", $req_body );
						$promotionSales = json_decode ( $promotionSales, true );
						if(is_array($promotionSales ['result'])){
							$ret [$k] ['key20'] = intval ( $promotionSales ['result'] [0] ['totalCount'] ); //key20促销销量
						}else{
							$ret [$k] ['key20'] = 0;
						}
						//==========================.end}}===============================================
						$ret [$k] ['key21'] = $goods_base ['shopId']; //key21店铺id
						$ret [$k] ['key22'] = "http://imgmall.tg.com.cn/" . $thum;
						$ret [$k] ['key23'] = $goods_base ['itemType']; //key23商品品类 1为a类 2为b类3为c类
						$ret [$k] ['key24'] = $new_rating; //key24商品质量评价平均分
						$ret [$k] ['key25'] = $new_pingjia; //key25商品最新一条评论
						$ret [$k] ['key26'] = $shopName; //key26店铺名称
					} else {
						$ret [$k] ['key14'] = $goods_base ['qeekaPrice'] / 100; //key14 无促销显示商城价
						$ret [$k] ['key15'] = '';
						$ret [$k] ['key16'] = '';
						$ret [$k] ['key17'] = '0';
						$ret [$k] ['key18'] = '';
						$ret [$k] ['key19'] = '';
						$ret [$k] ['key20'] = '';
						$ret [$k] ['key21'] = $goods_base ['shopId']; //key21 店铺id
						$ret [$k] ['key22'] = "http://imgmall.tg.com.cn/" . $thum; //key22百里挑一专用产品缩略图100*100
						$ret [$k] ['key23'] = $goods_base ['itemType']; //key23商品品类 1为a类 2为b类3为c类
						$ret [$k] ['key24'] = $new_rating; //key24商品质量评价平均分
						$ret [$k] ['key25'] = $new_pingjia; //key25商品最新一条评论
						$ret [$k] ['key26'] = $shopName; //key26店铺名称
					}
				} else {
					$ret [$k] ['key1'] = '';
					$ret [$k] ['key2'] = '';
					$ret [$k] ['key3'] = '';
					$ret [$k] ['key4'] = '';
					$ret [$k] ['key5'] = '';
					$ret [$k] ['key6'] = '';
					$ret [$k] ['key7'] = '';
					$ret [$k] ['key8'] = '';
					$ret [$k] ['key9'] = '';
					$ret [$k] ['key10'] = '';
					$ret [$k] ['key11'] = '';
					$ret [$k] ['key12'] = '';
					$ret [$k] ['key13'] = '';
					$ret [$k] ['key14'] = '';
					$ret [$k] ['key15'] = '';
					$ret [$k] ['key16'] = '';
					$ret [$k] ['key17'] = '';
					$ret [$k] ['key18'] = '';
					$ret [$k] ['key19'] = '';
					$ret [$k] ['key20'] = '';
					$ret [$k] ['key21'] = '';
					$ret [$k] ['key22'] = '';
					$ret [$k] ['key23'] = '';
					$ret [$k] ['key24'] = '';
					$ret [$k] ['key25'] = '';
					$ret [$k] ['key26'] = '';
				}
				$ret [$k] ['key27'] = $ad_img; //key27广告图片地址
				$ret [$k] ['key28'] = $ad_link; //key28广告链接地址
				$ret [$k] ['key29'] = $ad_text1; //key29文字说明一
				$ret [$k] ['key30'] = $ad_text2; //key30文字说明二
				$ret [$k] ['key31'] = $ad_text3; //key31文字说明三
				$ret [$k] ['key'] = $flip [$v];
			}
			$ret = $this->sysSortArray ( $ret, "key", "SORT_ASC" );
		}
		return $ret;
	}
	private function show_form() {
		$ci = &get_instance ();
		//显示配置表单
		echo form_open ( modify_build_url ( null ), array ('name' => "theform", "id" => "theform" ) );
		echo "<table><tr>";
		echo "<td>请输入广告ID号:</td>";
		echo "<td>" . form_input ( "pos_id", $ci->input->post ( "pos_id" ), "id='pos_id' size='50' " ) . "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td></td>";
		echo "<td>" . form_submit ( 'submitform', '确定', "id='submitform'" ) . "</td>";
		echo "</tr></table>";
		echo form_close ();
		echo "<br>\n";
	}
	
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$pos_id = $this->input->post ( 'pos_id' );
		$areaflag = "shanghai";
		$goods_id = '';
		$success = 1;
		$tip = '';
		$datasource_save = array ();
		if ($pos_id && preg_match ( "/^(\d+\,)*\d+$/", $pos_id )) {
			$pos_id_arr = explode ( ',', $pos_id );
			foreach ( $pos_id_arr as $k => $v ) {
				$req_url = "http://gao.tg.com.cn/api/api_cms/get_publish_info?pos_id=" . $v . "&areaflag=" . $areaflag;
				$ad_json = $this->do_get_api ( $req_url );
				//my_debug ( $ad_json );
				$ad_array = json_decode ( $ad_json, true );
				if (! $ad_array) {
					$ad_json = str_replace ( '\\', "/", $ad_json );
					$ad_array = json_decode ( $ad_json, true );
				}
				if ($ad_array ['msg'] == 1) {
					$success = 1;
				} else {
					$tip .= $v . ",";
					$success = 0;
				}
			}
		} else {
			$success = 0;
			//$tip .= "<font color = \"red\">请输入正确的广告id和城市</font>";
		}
		if ($success) {
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/ad/adapi';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['ds_api_config'] = "{\"pos_id\":[$pos_id],\"areaflag\":\"$areaflag\"}";
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			//my_debug ( "写入数据库!" );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
			if ($tip) {
				echo "<font color = \"red\">" . $tip . "广告位无效</font>";
			} else {
				echo "<font color = \"red\">请输入正确的广告位id</font>";
			}
		}
	}
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			//echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
	private function do_post_api($api_server, $api, $req_body, $port = 9091) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, "$api_server$api" );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 2000 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $req_body );
		$data = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			exit ( "function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
		}
		return $data;
	}
	// 说明：PHP中二维数组的排序方法 
	private function sysSortArray($ArrayData, $KeyName1, $SortOrder1 = "SORT_ASC", $SortType1 = "SORT_REGULAR") {
		//my_debug($ArrayData);
		if (! is_array ( $ArrayData )) {
			return $ArrayData;
		}
		// Get args number.
		$ArgCount = func_num_args ();
		// Get keys to sort by and put them to SortRule array.
		for($I = 1; $I < $ArgCount; $I ++) {
			$Arg = func_get_arg ( $I );
			//my_debug($Arg);
			if (! preg_match ( "/\bSORT\b/i", $Arg )) {
				$KeyNameList [] = $Arg;
				$SortRule [] = '$' . $Arg;
			} else {
				$SortRule [] = $Arg;
			}
		}
		// Get the values according to the keys and put them to array.
		foreach ( $ArrayData as $Key => $Info ) {
			//my_debug($KeyNameList);
			foreach ( $KeyNameList as $KeyName ) {
				${$KeyName} [$Key] = $Info [$KeyName];
			}
		}
		// Create the eval string and eval it.
		$EvalString = 'array_multisort(' . join ( ",", $SortRule ) . ',$ArrayData);';
		eval ( $EvalString );
		return $ArrayData;
	}
}


