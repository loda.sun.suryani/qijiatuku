<?php
class Mallproducts extends CI_Model {
	function __construct() {
		//my_debug();
		parent::__construct ();
		$this->load->helper ( "url" );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'form' );
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		//my_debug($ds_id);
		if ($ds_id) {
			$sql = "SELECT * FROM cms_datasource where ds_id=$ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			
			$req_body = $row ['ds_api_config'];
			//$req_body = "{\"category_id\":\"10010104\",\"local_city_name\":\"北京\",\"start\":1,\"size\":5}";
			$raw_content = $this->do_post_api ( "http://10.10.21.126:9091", "/item/_find", 
				$req_body );
			$this->load->config ( 'apiconf' );
			$apiconf = $this->config->item ( 'apiconf' );
			$img_conf = $apiconf ['img_url'];
			$product_conf = $apiconf ['product_url'];
			$content = json_decode ( $raw_content, true );
			$rows = $content ['docList'];
			if ($rows) {
				foreach ( $rows as $m => $n ) {
					$raw [$m] = $n ['docID'];
				}
				$item_list = implode ( ',', $raw );
				$raw_content = $this->do_get_api ( 
					"http://10.10.21.126:9101/item/getPromotionInstoreCountByIds?itemIds=$item_list" );
				$content = json_decode ( $raw_content, true );
				$promotionInstoreCount = $content ['result'];
				foreach ( $promotionInstoreCount as $k => $v ) {
					$promotionCount [$k] = $v ['promotionInstoreCount'];
				}
				//my_debug($promotionCount);
				$ret = array ();
				foreach ( $rows as $k => $v ) {
					$ret [$k] ['docID'] = $v ['docID']; //key1产品id
					$ret [$k] ['product_url'] = $product_conf . "/" . $v ['docID']; //key2产品地址
					$ret [$k] ['img_url'] = $img_conf . "/" . $v ['img_url']; //key3图片地址
					$ret [$k] ['name'] = $v ['name']; //key4产品名称
					$ret [$k] ['highlightedName'] = $v ['highlightedName']; //key5
					$ret [$k] ['is_promotion'] = $v ['is_promotion']; //key6 用于判断是否促销 1为有促销 0为无促销				
					$ret [$k] ['price_promotion'] = $v ['price_promotion'] / 100; //key7促销价
					$ret [$k] ['price_sc'] = $v ['price_sc'] / 100; //key8市场价
					$ret [$k] ['price_tg'] = $v ['price_tg'] / 100; //key9齐家价
					$ret [$k] ['instore_count'] = $v ['instore_count']; //key10正常库存 
					$ret [$k] ['sold_count'] = $v ['sold_count']; //key11已售出
					$ret [$k] ['review_count'] = $v ['review_count']; //key12已有评价数量
					$ret [$k] ['unit'] = $v ['unit']; //key13单位
					if ($ret [$k] ['is_promotion'] == 1) { //如果key6 = 1 进行促销 才会有k14-k19
						/*
						if (time () < $v ['promotion_start_time'] / 1000) {
							$ret [$k] ['price_show'] = $v ['price_tg'] / 100; //key14促销没开始显示商城价
						} else {
							$ret [$k] ['price_show'] = $v ['price_promotion'] / 100;
						}
						*/
						$ret [$k] ['price_show'] = $v ['price_promotion'] / 100;
						$ret [$k] ['promotion_names'] = $v ['promotion_names'] [0]; //key15促销方案名称
						$ret [$k] ['promotion_template_name'] = $v ['promotion_template_name']; //key16促销类型名 如 ：秒杀，抢购等
						$ret [$k] ['promotionInstoreCount'] = $promotionCount [$k]; //key17促销库存
						$ret [$k] ['promotion_start_time'] = date ( "F j, Y, H:i:s ", 
							$v ['promotion_start_time'] / 1000 ); //key18促销开始时间
						$ret [$k] ['promotion_end_time'] = date ( "F j, Y, H:i:s ", 
							$v ['promotion_end_time'] / 1000 ); //key19促销结束时间					
					} else {
						$ret [$k] ['price_show'] = $v ['price_tg'] / 100; //key14 无促销活动时显示商城价	
					}
				}
				//my_debug ( $ret );
				return $ret;
			}
		} else {
			return null;
		}
	}
	private function show_form() {
		$ci = &get_instance ();
		$view_data = array ();
		$view_data ['message'] = null;
		//=========local_city_name选择下拉框{{===========================================
		$local_city_name = array ();
		$this->load->config ( 'api_city' );
		$rows = $this->config->item ( 'api_city' );
		foreach ( $rows as $k => $v ) {
			$select_city [0] = '-请选择-';
			$select_city [$k] = $v;
		}
		$view_data ['local_city_name'] = $select_city;
		//==========end}}====================================================
		$this->load->view ( 'api_config_view/api_category_products_view', $view_data );
	}
	
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		
		$category_id = intval ( $ci->input->post ( "category_id" ) );
		$local_city_name = $ci->input->post ( "local_city_name" );
		$size = intval ( $ci->input->post ( "size" ) );
		if ($size == '') {
			$size = 5;
		}
		
		if ($category_id) {
			$ds_api_config = "{\"category_id\":\"$category_id\",\"local_city_name\":\"$local_city_name\",\"start\":\"1\",\"size\":\"$size\"}";
			$datasource_save = array ();
			$datasource_save ['ds_api_config'] = $ds_api_config;
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/mall_product/mallproducts';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
		}
	}
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
	private function do_post_api($api_server, $api, $req_body, $port = 9091) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, "$api_server$api" );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 2000 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $req_body );
		$data = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			exit ( 
				"function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
		}
		return $data;
	}

}


