<?php
class Mallshops extends CI_Model {
	function __construct() {
		//my_debug();
		parent::__construct ();
		$this->load->helper ( "url" );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'form' );
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		if ($ds_id) {
			$sql = "SELECT * FROM cms_datasource where ds_id=$ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$req_body = $row ['ds_api_config'];
			//$req_body = "{\"category_id\":\"110301\",\"itemTop\":1,\"index\":\"item\",\"start\":1,\"size\":5,\"isFaceted\":false}";
			$raw_content = $this->do_post_api ( "http://10.10.21.126:9091", "/top/shop", 
				$req_body );
			$this->load->config ( 'apiconf' );
			$apiconf = $this->config->item ( 'apiconf' );
			//$img_conf = $apiconf ['img_url'];
			$product_conf = $apiconf ['product_url'];
			$content = json_decode ( $raw_content, true );
			$rows = $content ['shopSet'];
			//my_debug($content);		
			if ($rows) {
				foreach ( $rows as $k => $v ) {
					$ret [$k] ['docID'] = $v ['itemList'][0] ['docID']; //key1产品id
					$ret [$k] ['highlightedName'] = $v ['itemList'][0] ['highlightedName']; //key2
					$ret [$k] ['is_promotion'] = $v ['itemList'][0] ['is_promotion']; //key3 用于判断是否促销 1为有促销 0为无促销				
					if ($ret [$k] ['is_promotion'] == 1) {
						$ret [$k] ['promotion_names'] = $v ['itemList'][0] ['promotion_names'] [0]; //key4促销方案名称						
					}else{
						$ret [$k] ['promotion_names'] = '';
					}
					$ret [$k] ['shopName'] = $v ['shopName']; //key5 商铺名字
					$ret [$k] ['shop_id'] = $v ['shop_id']; //key6商铺ID
					$ret [$k] ['category_id'] = $v ['itemList'][0]['category_id'];//key7品类id
					$ret [$k] ['category_name'] = $v ['itemList'][0]['category_name'];//key8品类名称
				}
				//my_debug ( $ret );
				return $ret;
			}
		} else {
			return null;
		}
	
	}
	private function show_form() {
		$ci = &get_instance ();
		$view_data = array ();
		$view_data ['message'] = null;
		//=========local_city_name选择下拉框{{===========================================
		/*$local_city_name = array ();
		$this->load->config ( 'api_city' );
		$rows = $this->config->item ( 'api_city' );
		foreach ( $rows as $k => $v ) {
			$select_city [0] = '-请选择-';
			$select_city [$k] = $v;
		}
		$view_data ['local_city_name'] = $select_city;*/
		//==========end}}====================================================
		$this->load->view ( 'api_config_view/api_mallshops_view', $view_data );
	}
	
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		
		$category_id = intval ( $ci->input->post ( "category_id" ) );
		$itemTop = intval ( $ci->input->post ( "itemTop" ) );
		$start = intval ( $ci->input->post ( "start" ) );
		$size = intval ( $ci->input->post ( "size" ) );
		if ($itemTop == '') {
			$itemTop = 1;
		}
		if ($start == '') {
			$start = 1;
		}
		if ($size == '') {
			$size = 5;
		}
		
		if ($category_id) {
			$ds_api_config = "{\"category_id\":\"$category_id\",\"itemTop\":\"$itemTop\",\"index\":\"item\",\"start\":\"$start\",\"size\":\"$size\",\"isFaceted\":\"false\"}";
			$datasource_save = array ();
			$datasource_save ['ds_api_config'] = $ds_api_config;
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/mall_product/mallshops';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
		}
	}
	private function do_post_api($api_server, $api, $req_body, $port = 9091) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, "$api_server$api" );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 2000 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $req_body );
		$data = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			exit ( 
				"function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
		}
		return $data;
	}

}


