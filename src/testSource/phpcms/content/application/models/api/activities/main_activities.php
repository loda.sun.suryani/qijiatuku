<?php
class Main_activities extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form (); //直接生产固定的API接口
		} else {
			$this->show_form (); //通过提交产品的iD获取api记录
		}
	}
	public function get_data($ds_id = null) {
		$this->page_cache_time = 30;
		$dabase = array ();
		if ($ds_id) {
			$ds_id = intval ( $ds_id );
			$sql = "SELECT ds_sql_manual_record FROM cms_datasource where ds_id=$ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$areaflag = $row ['ds_sql_manual_record'];
			$this->db_shop = $this->load->database ( 'shop', TRUE );
			//缓存处理
			$key = $areaflag . $ds_id . "shop";
			$dabase = $this->cache->get ( $key, null );
			if (! $dabase) {
				//$array = explode ( ",", $ds_sql_manual_record );
				//热门 `ap_id` =255
				//建材 `ap_id` =268 
				//装修  `ap_id` =269 
				//家居 `ap_id` =270 
				//婚庆`ap_id` =271 
				$time = date ( 
					"Y-m-d 0:00:00" );
				$time = strtotime ( $time );
				$where = "AND start_time <= '$time' AND end_time >= '$time'";
				
				$sql_count = "SELECT count(*) as tot FROM igg_ad WHERE `ap_id` =255 AND `disable` = 'false' AND `areaflag`='$areaflag' $where"; //取总数,用于分页
				$tcount = $this->db_shop->get_record_by_sql ( $sql_count, 'num' );
				$t_count = $tcount [0];
				if ($t_count == 0) {
					$sql = "SELECT * FROM `igg_ad` WHERE `ap_id` =255 AND `disable` = 'false' AND `areaflag`='$areaflag'  ORDER BY `igg_ad`.`ad_order` ASC";
				} else {
					$sql = "SELECT * FROM `igg_ad` WHERE `ap_id` =255 AND `disable` = 'false' AND `areaflag`='$areaflag' $where  ORDER BY `igg_ad`.`ad_order` ASC";
				}
				$data = $this->db_shop->get_rows_by_sql ( $sql );
				if (count ( $data )) {
					foreach ( $data as $k => $v ) {
						$city = '255';
						$dabase [] = array (
								'ad_title' => strip_tags ( $v ['ad_title'] ), 
								'ad_link ' => str_replace ( "act.tg.com.cn", "tg.jia.com", 
									$v ['ad_link'] ), 
								'areaflag' => $v ['areaflag'], 
								"type" => $city , 
								"start_time" => date ( "Y-m-d", $v ['start_time'] ), 
								"end_time" => date ( "Y-m-d", $v ['end_time'] ) );
					}
				
				}
				
				$sql_count = "SELECT count(*) as tot FROM igg_ad WHERE `ap_id` =268 AND `disable` = 'false' AND `areaflag`='$areaflag' $where"; //取总数,用于分页
				$tcount = $this->db_shop->get_record_by_sql ( $sql_count, 'num' );
				$t_count = $tcount [0];
				if ($t_count == 0) {
					$sql = "SELECT * FROM `igg_ad` WHERE `ap_id` =268 AND `disable` = 'false' AND `areaflag`='$areaflag'  ORDER BY `igg_ad`.`ad_order` ASC";
				} else {
					$sql = "SELECT * FROM `igg_ad` WHERE `ap_id` =268 AND `disable` = 'false' AND `areaflag`='$areaflag' $where  ORDER BY `igg_ad`.`ad_order` ASC";
				}
				$data = $this->db_shop->get_rows_by_sql ( $sql );
				if (count ( $data )) {
					foreach ( $data as $k => $v ) {
						$city = '268';
						$dabase [] = array (
								'ad_title' => strip_tags ( $v ['ad_title'] ), 
								'ad_link ' => str_replace ( "act.tg.com.cn", "tg.jia.com", 
									$v ['ad_link'] ), 
								'areaflag' => $v ['areaflag'], 
								"type" => $city, 
								"start_time" => date ( "Y-m-d", $v ['start_time'] ), 
								"end_time" => date ( "Y-m-d", $v ['end_time'] ) );
					}
				
				}
				
				$sql_count = "SELECT count(*) as tot FROM igg_ad WHERE `ap_id` =269 AND `disable` = 'false' AND `areaflag`='$areaflag' $where"; //取总数,用于分页
				$tcount = $this->db_shop->get_record_by_sql ( $sql_count, 'num' );
				$t_count = $tcount [0];
				if ($t_count == 0) {
					$sql = "SELECT * FROM `igg_ad` WHERE `ap_id` =269 AND `disable` = 'false' AND `areaflag`='$areaflag'  ORDER BY `igg_ad`.`ad_order` ASC";
				} else {
					$sql = "SELECT * FROM `igg_ad` WHERE `ap_id` =269 AND `disable` = 'false' AND `areaflag`='$areaflag' $where  ORDER BY `igg_ad`.`ad_order` ASC";
				}
				$data = $this->db_shop->get_rows_by_sql ( $sql );
				if (count ( $data )) {
					foreach ( $data as $k => $v ) {
						$city = '269';
						$dabase [] = array (
								'ad_title' => strip_tags ( $v ['ad_title'] ), 
								'ad_link ' => str_replace ( "act.tg.com.cn", "tg.jia.com", 
									$v ['ad_link'] ), 
								'areaflag' => $v ['areaflag'], 
								"type" => $city, 
								"start_time" => date ( "Y-m-d", $v ['start_time'] ), 
								"end_time" => date ( "Y-m-d", $v ['end_time'] ) );
					}
				
				}
				$sql_count = "SELECT count(*) as tot FROM igg_ad WHERE `ap_id` =270 AND `disable` = 'false' AND `areaflag`='$areaflag' $where"; //取总数,用于分页
				$tcount = $this->db_shop->get_record_by_sql ( $sql_count, 'num' );
				$t_count = $tcount [0];
				if ($t_count == 0) {
					$sql = "SELECT * FROM `igg_ad` WHERE `ap_id` =270 AND `disable` = 'false' AND `areaflag`='$areaflag'  ORDER BY `igg_ad`.`ad_order` ASC";
				} else {
					$sql = "SELECT * FROM `igg_ad` WHERE `ap_id` =270 AND `disable` = 'false' AND `areaflag`='$areaflag' $where  ORDER BY `igg_ad`.`ad_order` ASC";
				}
				$data = $this->db_shop->get_rows_by_sql ( $sql );
				if (count ( $data )) {
					foreach ( $data as $k => $v ) {
						$city = '270';
						$dabase [] = array (
								'ad_title' => strip_tags ( $v ['ad_title'] ), 
								'ad_link ' => str_replace ( "act.tg.com.cn", "tg.jia.com", 
									$v ['ad_link'] ), 
								'areaflag' => $v ['areaflag'], 
								"type" => $city, 
								"start_time" => date ( "Y-m-d", $v ['start_time'] ), 
								"end_time" => date ( "Y-m-d", $v ['end_time'] ) );
					}
				
				}
				
				//接口end---------------
				//$dabase = $this->sysSortArray ( $dabase, "asc", "SORT_ASC" );
				$this->cache->set ( $key, $dabase, 0, 
					$this->page_cache_time );
			}
		}
		//my_debug ( $dabase );
		return $dabase;
	}
	private function show_form() { /*
		$default_fields = array ();
		$default_fields [] = array ('k' => 'title', 'mapkey' => 1 );
		$default_fields [] = array ('k' => 'create_time', 'mapkey' => 2 );
		*/
		$ci = &get_instance (); /*
		if (! $ci->status ['fields_map']) {
			$ci->status ['fields_map'] = $default_fields;
		}
		*/
		//显示配置表单
		echo form_open ( modify_build_url ( null ), array ('name' => "theform", "id" => "theform" ) );
		echo "请输入现场活动城市(如:shanghai):";
		echo form_input ( "goods_ids", $ci->input->post ( "goods_ids" ), "id='goods_ids' size='40' " );
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() { //接受表单提交,做相应处理
		$ci = &get_instance ();
		$ids = $ci->input->post ( "goods_ids" );
		$ids = trim ( $ids );
		$ids = trim ( $ids, "," );
		$ids_arr = explode ( ',', $ids );
		if (count ( $ids_arr ) < 1) {
			//输入的数据不合要求,显示表单,再填!
			$this->show_form ();
		} else {
			$datasource_save = array ();
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/activities/main_activities';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['ds_sql_manual_record'] = $ids;
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			my_debug ( "写入数据库!" );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
	}
	
	private function sysSortArray($ArrayData, $KeyName1, $SortOrder1 = "SORT_ASC", $SortType1 = "SORT_REGULAR") {
		//my_debug($ArrayData);
		if (! is_array ( $ArrayData )) {
			return $ArrayData;
		}
		// Get args number.
		$ArgCount = func_num_args ();
		// Get keys to sort by and put them to SortRule array.
		for($I = 1; $I < $ArgCount; $I ++) {
			$Arg = func_get_arg ( $I );
			//my_debug($Arg);
			if (! @eregi ( "SORT", $Arg )) {
				$KeyNameList [] = $Arg;
				$SortRule [] = '$' . $Arg;
			} else {
				$SortRule [] = $Arg;
			}
		}
		// Get the values according to the keys and put them to array.
		foreach ( $ArrayData as $Key => $Info ) {
			//my_debug($KeyNameList);
			foreach ( $KeyNameList as $KeyName ) {
				${$KeyName} [$Key] = $Info [$KeyName];
			}
		}
		// Create the eval string and eval it.
		$EvalString = 'array_multisort(' . join ( ",", $SortRule ) . ',$ArrayData);';
		eval ( $EvalString );
		return $ArrayData;
	}
}


