<?php
class PjApi extends CI_Model {
	function __construct() {
		//my_debug();
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		$ret = array ();
		$ids = '';
		$this->page_cache_time = 30;
		if ($ds_id) {
			$ds_record = $this->db->get_record_by_field ( 'cms_datasource', 'ds_id', $ds_id );
			$cid = $ds_record ['ds_api_config'];
			$size = $ds_record ['ds_field_config'];
			if (! $size) {
				$size = 20;
			}
			$pj_req = "{\"score\":3,\"category_id\":$cid,\"size\":20}";
			$pj_json = $this->do_post_api ( "http://10.10.21.126:9105", "/review/_getCategoryRenewReview", $pj_req );
			$pj_content = json_decode ( $pj_json, true );
			$pj_data = $pj_content ['content'];
			//对评价进行过滤 除掉相同商品的评价
			foreach ( $pj_data as $key => $val ) {
				$i = 0;
				foreach ( $pj_data as $k => $v ) {
					$diff = json_encode ( $val ['itemID'] ) == json_encode ( $v ['itemID'] );
					if ($diff && $i == 0) {
						$i ++;
						continue;
					} else if ($diff) {
						unset ( $pj_data [$k] );
					}
				}
			}
			$ids = array ();
			if (count ( $pj_data ) > 0) {
				foreach ( $pj_data as $k => $v ) {
					$ids [$k] = $v ['itemID'];
				}
			}
			$ids = array_unique ( $ids );
			$id_vars = implode ( ',', $ids );
			$goods_req = "{\"docID\":[$id_vars]}"; //接口请求参数json格式
			$goods_json = $this->do_post_api ( "10.10.21.126:9091", "/item/_find", $goods_req );
			$goods_data = json_decode ( $goods_json, true );
			$goods_data = $goods_data ['docList'];
			if ($goods_data) {
				foreach ( $goods_data as $k => $v ) {
					$thumb [$v ['docID']] = "http://imgmall.tg.com.cn/" . str_replace ( '.', '_105x75.', $v ['img_url'] );
				}
			}
			if ($pj_data) {
				for($k = 0; $k < $size; $k ++) {
					if ($thumb [$pj_data [$k] ['itemID']]) {
						$user_req_url = "http://10.10.21.126:9005/user/getUserNameById?id=" . $pj_data [$k] ['userID'];
						$user_json = $this->do_get_api ( $user_req_url );
						$user_data = json_decode ( $user_json, true );
						$user_name = $user_data ['result'];
						$ret [$k] ['key1'] = $pj_data [$k] ['userID']; //用户id
						$ret [$k] ['key2'] = $user_name; //用户名
						$ret [$k] ['key3'] = $pj_data [$k] ['addTime']; //时间
						$ret [$k] ['key4'] = $pj_data [$k] ['score']; //最新打分
						//2013.07.26
						if ($pj_data [$k]['content']) {
							$pj_str = $pj_data [$k]['content']; // 评价内容
						} else if ($pj_data [$k]['productReview']) {
							$pj_str = $pj_data [$k]['productReview']; // 评价内容
						} else if ($pj_data [$k]['serviceReview']) {
							$pj_str = $pj_data [$k]['serviceReview']; // 评价内容
						}
						if (mb_strlen ( $pj_str ) > 40) {
							$ret [$k] ['key5'] = mb_substr ( $pj_str, 0, 40 ) . "..."; //评价内容
						} else {
							$ret [$k] ['key5'] = mb_substr ( $pj_str, 0, 40 ); //评价内容
						}
						//.end
						$ret [$k] ['key6'] = "http://mall.jia.com/item/" . $pj_data [$k] ['itemID'] . "#evaluateDetail"; //商品评价详情地址
						$ret [$k] ['key7'] = $thumb [$pj_data [$k] ['itemID']]; //商品图片缩略图 50x50
						$ret [$k] ['key8'] = $pj_data [$k]['itemName'];
					}
				}
			}
			//my_debug ( $ret );
			return $ret;
		}
	}
	private function show_form() {
		$ci = &get_instance ();
		//显示配置表单
		echo form_open ( modify_build_url ( null ), array ('name' => "theform", "id" => "theform" ) );
		echo "<table><tr>";
		echo "<td>请输入商品分类ID(例如:家居类商品,品类id为:15):</td>";
		echo "<td>" . form_input ( "catid", $ci->input->post ( "catid" ), "id='catid' size='50' " ) . "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>请输入调取数量:</td>";
		echo "<td>" . form_input ( "size", $ci->input->post ( "size" ), "id='size' size='50' " ) . "（不填则默认为20）</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td></td>";
		echo "<td>" . form_submit ( 'submitform', '确定', "id='submitform'" ) . "</td>";
		echo "</tr></table>";
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$cid = intval ( trim ( $ci->input->post ( 'catid' ) ) );
		$size = intval ( trim ( $ci->input->post ( 'size' ) ) );
		if (! $size) {
			$size = 20;
		}
		$success = 1;
		$tip = '';
		if ($cid) {
			$success = 1;
		} else {
			$success = '';
		}
		$pj_req = "{\"score\":4,\"category_id\":$cid,\"size\":20}";
		$pj_json = $this->do_post_api ( "http://10.10.21.126:9105", "/review/_getCategoryRenewReview", $pj_req );
		$pj_content = json_decode ( $pj_json, true );
		$pj_data = $pj_content ['content'];
		if ($pj_data) {
			$success = 1;
		} else {
			$success = '';
			$tip = $cid;
		}
		if ($success) {
			//=======================配置信息存入数据库=====
			$datasource_save = array ();
			$datasource_save ['ds_api_config'] = $cid;
			$datasource_save ['ds_field_config'] = $size;
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/pingjia/pjapi';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );

			$ci->db->insert ( 'cms_datasource', $datasource_save );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
			if ($tip) {
				echo "类目<font color = red>{$tip}</font>尚无符合要求的评论,请检查！";
			} else {
				echo "请检查输入格式";
			}
		}
	}
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			//echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
	private function do_post_api($api_server, $api, $req_body, $port = 9091) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, "$api_server$api" );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 2000 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $req_body );
		$data = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			exit ( "function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
		}
		return $data;
	}
}


