<?php
class Hotcourse extends CI_Model {
	function __construct() {
		//my_debug();
		parent::__construct ();
		$this->load->helper ( "url" );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'form' );
	}
	public function setting($ds_id = null) {
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		if ($ds_id) {
			$sql = "SELECT * FROM cms_datasource where ds_id=$ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			//my_debug($row ['ds_api_config']);
			$course_ids = explode ( ",", $row ['ds_api_config'] );
		}
		foreach ( $course_ids as $id ) {
			$sql = "SELECT*FROM fuwu_course WHERE course_id = $id";
			$data [] = $this->db->get_record_by_sql ( $sql );
		}
		foreach ($data as $k=>$v){
			$ret[$k]['key1'] = $v['course_title'];//课程标题
			$ret[$k]['key2'] = "http://cms.tg.com.cn/content/".$v['course_picture'];// 课程封面
			if($v['course_file']){
				$ret[$k]['key3'] = "http://cms.tg.com.cn/content/".$v['course_file'];//文件链接
			}
			if($v['course_url']){
				$ret[$k]['key3'] = "http://".$v['course_url'];//文件链接
			}			
		}
		//my_debug($ret);
		return $ret;
	}
	private function show_form() {
		$ci = &get_instance ();
		//显示配置表单
		echo form_open ( modify_build_url ( null ), 
			array ('name' => "theform", "id" => "theform" ) );
		echo "请输入课程ID号(以逗号隔开，形如:2906,203,1125):";
		echo form_input ( "course_ids", $ci->input->post ( "course_ids" ), 
			"id='course_ids' size='40' " );
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$course_ids = trim ( $ci->input->post ( 'course_ids' ) );
		if (isset ( $course_ids ) && $course_ids != '') {
			if (preg_match ( "/^(\d+\,)*\d+$/", $course_ids )) {
				//=======================配置信息存入数据库=====
				$api_config = $site;
				$datasource_save = array ();
				$datasource_save ['ds_api_config'] = $course_ids;
				$datasource_save ['ds_type'] = 'api';
				$datasource_save ['ds_api_name'] = 'api/fuwu/hotcourse';
				$datasource_save ['create_time'] = time ();
				$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
				$ci->db->insert ( 'cms_datasource', $datasource_save );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			} else {
				echo "<font size = '2' color = 'red'>请注意输入格式!</font>";
				$this->show_form ();
			}
		} else {
			echo "<font size = '2' color = 'red'>请输入课程id!</font>";
			$this->show_form ();
		}
	}

}


