<?php
class Coursecate extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	public function setting($ds_id = null) {
		$this->process_form ();
	}
	public function get_data($ds_id = null) {
		$page = 0;
		if ($_GET ['sid']) {
			$sid = intval ( $_GET ['sid'] );
		} else {
			$sid = 0;
		}
		//my_debug ( $sid );
		$sql = "SELECT*FROM fuwu_course_category ORDER BY category_id ASC";
		$rows = $this->db->get_rows_by_sql ( $sql );
		foreach ( $rows as $k => $v ) {
			$data [0] = array ('category_id' => '0', 'category_name' => '全部' );
			$data [$k + 1] = $v;
		}
		foreach ( $data as $k => $v ) {
			$url = modify_build_url ( array ('sid' => $v ['category_id'], 'page' => $page ) );
			if ($v ['category_id'] == "$sid") {
				$data [$k] ['url'] = "<li><a class = \"hover\" href ={$url} uid ={$v['category_id']}>{$v['category_name']}</a></li>";
			} else {
				$data [$k] ['url'] = "<li><a href ={$url} uid ={$v['category_id']}>{$v['category_name']}</a></li>";
			}
		
		}
		//my_debug ( $data );
		return $data;
	}
	
	private function process_form() {
		$ci = &get_instance ();
		$datasource_save = array ();
		$datasource_save ['ds_type'] = 'api';
		$datasource_save ['ds_api_name'] = 'api/fuwu/coursecate';
		$datasource_save ['create_time'] = time ();
		$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
		$ci->db->insert ( 'cms_datasource', $datasource_save );
		//my_debug ( "写入数据库!" );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		//my_debug ( $_POST );
	}
}
