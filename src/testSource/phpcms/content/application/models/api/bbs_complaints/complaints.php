<?php
class Complaints extends CI_Model {
	function __construct() {
		//my_debug();
		parent::__construct ();
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		if ($ds_id) {
			$sql = "SELECT * FROM cms_datasource where ds_id = $ds_id";
			$this->db->reconnect();
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$areaflag = $row ['ds_api_config'];
			$files = "http://bbs.jia.com/api/api_topic/get_complaints_topic?areaflag=" . $areaflag;
			//$files = "http://bbs.jia.com/get_complaints_threads.php?areaflag=" . $areaflag;
			$ret_str = '';
			$ret_str = file_get_contents ( $files );
			$ret = json_decode ( $ret_str, true );
			$arr2 = array ();
			//my_debug($ret);
			if (is_array ( $ret [$areaflag] ) && count ( $ret [$areaflag] ) > 0) {
				$more = $ret [$areaflag] ['url'];
				$row = array_splice ( $ret [$areaflag], 1 );
				foreach ( $row as $k => $v ) {
					$arr2 [$k] ['key1'] = $v ['tid']; //1帖子id
					$arr2 [$k] ['key2'] = $v ['subject']; //2帖子主题
					if ($v ['dateline']) {
						$arr2 [$k] ['key3'] = date ( "Y-m-d H:i:s", $v ['dateline'] ); //3发布时间	
					} else {
						$arr2 [$k] ['key3'] = '';
					}
					$arr2 [$k] ['key4'] = $areaflag; //4城市
					$arr2 [$k] ['key5'] = $more; //5更多帖子地址	
				}
			}
		}
		//my_debug ( $arr2 );
		return $arr2;
	}
	private function show_form() {
		$ci = &get_instance ();
		
		//显示配置表单
		echo form_open ( modify_build_url ( null ), array ('name' => "theform", "id" => "theform" ) );
		echo "请输入曝光展示城市(如:shanghai):";
		echo form_input ( "city_id", trim($ci->input->post ( "city_id" )), "id='city_id' size='40' " );
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$city_id = trim ( strip_tags ( $ci->input->post ( 'city_id' ) ) );
		$success = 1;
		$msg = '';
		if ($city_id) {
			$json = file_get_contents ( "http://bbs.jia.com/api/api_topic/get_complaints_topic?areaflag=$city_id" );
			//$json = file_get_contents ( "http://bbs.jia.com/get_complaints_threads.php?areaflag=$city_id" );
			$arr = json_decode ( $json, true );
			$error_type = $arr [$city_id];
			if ($error_type == 1) {
				$msg = "分站" . $city_id . "前台投诉板块未开启<br/>";
				$success = '';
			}
			if ($error_type == 2) {
				$msg = "分站" . $city_id . "在论坛没有投诉板块<br/>";
				$success = '';
			}
			if ($error_type == '') {
				$msg = "分站" . $city_id . "在论坛没有投诉帖子<br/>";
				$success = '';
			}
		}else{
			$success = '';
			$msg = "请填写城市拼音";
		}
		//=======================配置信息存数据库=====
		$datasource_save = array ();
		$datasource_save ['ds_api_config'] = $city_id;
		$datasource_save ['ds_type'] = 'api';
		$datasource_save ['ds_api_name'] = 'api/bbs_complaints/complaints';
		$datasource_save ['create_time'] = time ();
		$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
		
		if ($success) {
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
			echo "<font color = red>";
			echo $msg;
			echo "</font>";
		}
	}
}


