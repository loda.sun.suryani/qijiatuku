<?php
class BbsExpress extends CI_Model {
	function __construct() {
		//my_debug();
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		$this->page_cache_time = 30;
		if ($ds_id) {
			$sql = "SELECT * FROM cms_datasource where ds_id = $ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$areaflag = $row ['ds_api_config'];
			$files = "http://bbs.tg.com.cn/get_complaints.php?areaflag=" . $areaflag;
			//my_debug($files);			
			//缓存	
			$key = $areaflag . $ds_id . "bbs";
			$ret = $this->cache->get ( $key, null );
			if (! $ret) {
				$ret_json = file_get_contents ( $files );
				$ret_arr = json_decode ( $ret_json, true );
				$ret = array ();
				if ($ret_arr) {
					foreach ( $ret_arr as $num => $row ) {
						foreach ( $row as $k => $v ) {
							$v ['sort'] = $num;
							$rets [] = $v;
						}
					}
					foreach($rets as $k=>$v){
						$ret [$k] ['key1'] = intval($v['tid']);//1帖子id
						$ret [$k] ['key2'] = $v['subject'];//2帖子主题
						if($v['dateline']){
							$ret [$k] ['key3'] = date("Y-m-d H:i:s",$v['dateline']);//3发布时间	
						}else{
							$ret [$k] ['key3'] = '';
						}						
						$ret [$k] ['key4'] = $areaflag;//4城市
						$ret [$k] ['key5']	= $v['sort'];//5帖子类型 tousu 或者 biaoyang
					
					}
					$this->cache->set ( $key, $ret, 0, $this->page_cache_time );
				}
			}
			//my_debug ( $ret );
			return $ret;
		}
	}
	private function show_form() {
		$ci = &get_instance ();
		
		//显示配置表单
		echo form_open ( modify_build_url ( null ), 
			array ('name' => "theform", "id" => "theform" ) );
		echo "请输入曝光展示城市(如:shanghai):";
		echo form_input ( "city_id", $ci->input->post ( "city_id" ), "id='goods_ids' size='40' " );
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$city_id = trim ( $ci->input->post ( 'city_id' ) );
		//$city = explode ( ',', $city_id );
		//$success = '';
		$state = 1;
		$msg = '';
		
		if ($city_id) {
			$files = "http://bbs.tg.com.cn/get_complaints.php?areaflag=" . $city_id;
			$ret_str = file_get_contents ( $files );
			$ret = json_decode ( $ret_str, true );
			if (! $ret || $ret == error) {
				$state = '';
				$msg = "该城市不存在或尚未建立投诉板块";
			}
		} else {
			$state = '';
			$msg = "请输入城市拼音";
		}
		
		//=======================配置信息存数据库=====
		$ds_api_config = $city_id;
		//my_debug($ds_api_config);
		$datasource_save = array ();
		$datasource_save ['ds_api_config'] = $ds_api_config;
		$datasource_save ['ds_type'] = 'api';
		$datasource_save ['ds_api_name'] = 'api/bbs_complaints/bbsexpress';
		$datasource_save ['create_time'] = time ();
		$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
		
		if ($state) {
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
			echo "<font color = red>";
			echo $msg;
			echo "</font>";
		}
	
	}

}


