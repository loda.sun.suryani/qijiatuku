<?php
class Tree_Model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
		$this->_build_tree ();
	}
	
	//创建tree结构
	private function _build_tree() {
		function walk_tree(&$items,&$out_path, &$tree, $depth, $path = '') {
			$i = 0;
			if (array_key_exists ( '_id', $tree )) {
				//$out .= (sprintf ( "$path,%s\n", $tree ['_item'] ['id'] ));
				$out_path [$tree ['_id']] = $path;
			}
			if (count ( $tree ) > 1) {
				foreach ( $tree as $k => $v ) {
					if ('_id' == $k) {
						continue;
					}
					$i = ($i + 1);
					//$out .= ("<div id='menu-$depth-$i' class='menu_$depth' style='margin:5px;padding:5px;border:1px solid blue;'>\n");
					walk_tree ( $items,$out_path, $v, $depth + 1, "$path/".$items[$k]['title'] );
					//$out .= ("</div>\n");
				}
			}
			//$out .= ("\n");
		}
		
		$rows = $this->db->get_rows_by_sql ( "SELECT * FROM cms_tree WHERE id>1 ORDER BY level ASC,parent_id ASC" );
		//my_debug ( $rows );
		$this->load->library ( 'datagrid' );
		$this->datagrid->reset ();
		//echo $this->datagrid->build ( 'datagrid', $rows, TRUE );
		$paths = array ();
		$paths [1] = '';
		$items = array ();
		foreach ( $rows as $row ) {
			$id = $row ['id'];
			$parent_id = $row ['parent_id'];
			$items [$id] = $row;
			$parent_path = $paths [$parent_id];
			$paths [$id] = ("{$parent_path}['$id']");
			//$path_arr[$id] = ;
		}
		unset ( $paths [1] );
		//sort($paths);
		$tree = array ();
		foreach ( $paths as $k => $v ) {
			eval ( sprintf ( '$tree%s["_id"]=%s;', $v, $k ) );
		}
		//my_debug ( $items );
		//my_debug ( $paths );//到此$paths已经保存了无排序的paths
		//my_debug ( $tree, 'tree', 'print' );
		$this->keys = $paths;
		$this->tree = $tree;
		$out = array ();
		walk_tree ( $items,$out, $tree, 0, '' );
		//my_debug ( $out, '#out', 'print' );
		$this->items = $items;
		$this->paths = $out;
	}
	
	function idpath_to_namepath($idpath) {
		;
	}
	function idpath_to_titlepath($idpath) {
		;
	}
}


//end.
