<?php

$lang['required']			= "%s是必须填写的.";
$lang['isset']				= "%s必须有效.";
$lang['valid_email']		= "%s必须是合法的EMAIL地址";
$lang['valid_emails']		= "%s必须是合法的EMAIL地址";
$lang['valid_url']			= "%s必须是合法的URL地址";
$lang['valid_ip']			= "%s必须是合法IP地址.";
$lang['min_length']			= "%s长度最少为%s";
$lang['max_length']			= "%s长度最多为%s";
$lang['exact_length']		= "%s长度必须等于%s";
$lang['alpha']				= "%s只能是字母";
$lang['alpha_numeric']		= "%s只能是数字/字母";
$lang['alpha_dash']			= "%s只能是数字/字母/下划线";
$lang['numeric']			= "%s只能是数字";
$lang['is_numeric']			= "%s只能是数字";
$lang['integer']			= "%s必须是自然数";
$lang['regex_match']		= "%s格式不正确(正则表达式)";
$lang['matches']			= "%s与%s不匹配";
$lang['is_unique'] 			= "%s必须是唯一值";
$lang['is_natural']			= "%s必须是正整数";
$lang['is_natural_no_zero']	= "%s必须大于0";
$lang['decimal']			= "%s必须是十进制数";
$lang['less_than']			= "%s必须小于%s.";
$lang['greater_than']		= "%s必须大于%s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
