<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>设计元素</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/page.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/li.js" type="text/javascript"></script>
</head>
<body>
<?php  echo crumbs_nav('/设计元素管理');?>
<hr />
<form name="form1" method="post" action="index.php?c=cmsdesign&m=cmsdesign_search" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="d_fenlei">元素分类<?php echo $design_category_id_from?></td>
    <td class="d_gj">关键字
      <label>
        <input type="text" name="design_title" id="design_title" value="<?php echo trim($design_title);?>" />
      </label></td>
    <td class="d_cx"><input type="submit" name="button" id="button" value="查询" /> <input type="button" onclick="location.href='<?php echo site_url('c=cmsdesign&m=cmsdesign_add')?>';return false" value="添加" /></td>
  </tr>
</table>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_grid">
  <thead>
		<tr>
			<th >编号</th>
            <th >示例</th>
			<th >名称</th>
			<th >分类</th>
			<th >文件</th>
			<th >尺寸</th>
            
			<th >url</th>
            <th >发布日期</th>
			<th >操作</th>
		</tr>
	</thead>
  <?php  
  if(is_array($list)){
  foreach($list as $k=>$item){
		  if (! file_exists ($pic_path.'/'.$item['design_pic'] )) {
			  $directory='NO';
			}else{
			  $directory='yes';
			}
		if (! file_exists ($pic_path.'/'.$item['design_psd'] )) {
			  $psd='NO';
			}else{
			  $psd='yes';
			}
	  ?>

  <tr>
     <td class="d_xuhao"><?php echo $item['design_id'];echo strpos($item['design_pic'],'_'.$item['design_id']);?></td>
     <td class="d_shili"><?php if($directory=='yes'){?><a href="<?php echo base_url().$pic_path;?>/<?php echo $item['design_pic']?>"  class="thickbox" title="<?php echo $item['design_title']?>">示例</a><?php }else{?>无图<?php }?></td>
     <td class="d_name"><?php echo $item['design_title']?></td>
   <td class="d_fl"><?php echo $item['design_category']?></td>
    <td class="d_psd"><?php if($psd=='yes'){?><a href="<?php echo base_url().$pic_path;?>/<?php echo $item['design_psd']?>">下载</a><?php }else{?>无文件<?php }?></td>
    <td class="d_chicun"><?php echo $item['design_width']?>X<?php echo $item['design_height']?></td>
    
    <td class="d_url"><?php echo $item['design_url']?></td>
    <td class="d_fbdate"><?php echo $item['design_date']?></td>
    <td class="d_control"><a href="index.php?c=cmsdesign&m=edit&design_id=<?php echo $item['design_id']?>&page=<?php echo $page?>">修改</a> &nbsp;&nbsp;| &nbsp;&nbsp;<a href="index.php?c=cmsdesign&m=cmsdesign_del&design_id=<?php echo $item['design_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要删除记录吗?"))>删除</a></td>
  </tr>
  <?php }
  
  }?>
  <tr>
    <td colspan="9" calss='page'><?php echo $navigation?></td>
  </tr>
</table>


</body>
</html>