<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  
<title>应用编辑</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>

<table class="p_g" width="100%" border="0">
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">应用板块</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'app_name', 
		'id' => "app_name", 
		'size' => 72,
		'value' => $record['app_name'] ) );
echo form_error ( 'app_name', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td valign = "top">说明</td>
		<td>
<?php 
echo form_textarea ( array (
		'name' => 'app_memo', 
		'id' => 'app_memo', 
		"value" => $record['app_memo'] ) );
echo form_error ( 'memo', '<span class="error">', '</span>' );
?></td>
	</tr>
	<tr>
		<td valign = "top">应用模板</td>
		<td>
<?php 
echo form_dropdown ( 'tpl_id', $select_tpl, $record['app_tpl'] );
echo form_error ( 'tpl_id', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	
	<tr><td> </td><td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td></tr>
</table>
<?php 
echo form_close();
?>
</script>
</body>
</html>