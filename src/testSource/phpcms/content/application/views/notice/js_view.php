<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
textarea {
    border-bottom: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-top: 1px solid #dcdcdc;
    border-left: 1px solid #dcdcdc;
    font-size: 12px;
    width: 630px;
    line-height: 20px;
}
button {
    cursor: pointer;
    width: auto;
    color: #2953a6;
    font-size: 12px;
    height: 20px;
    line-height: 18px;
    text-align: center;
    border-bottom: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-top: 1px solid #dcdcdc;
    border-left: 1px solid #dcdcdc;
    background-color: #fff;
}

</style>
<title>app_js</title>
</head>
<body>
<?php
$js = "
<script src=\"/content/index.php?c=notice_show&m=json&app_id={$app_id}\"></script>
<div id=\"notice_list\"></div>
<div id=\"notice_pagecode\"></div>";
echo form_textarea(array('name'=>'jscode',
						 'id'=>'jscode',
						 'value'=>"$js",
						 'rows'=>'12'));
?>
<br/>
<font color = "red">*tip:</font> time=today    控制只显示当天（可选）
						         count_page=     控制每页显示条数（可选）

<br/>
<br/>
<?php
echo form_button (array( 'name'=>'copycode',
						 'id'=>'copecode',
						 'content'=>'复制到粘贴板',
						 'value'=> 'true',
						 'onclick'=>"copyCode();return false;"));
?>
</body>
<script type="text/javascript">
function copyCode(obj){
var testCode=document.getElementById("jscode").value;
if(copy2Clipboard(testCode)!=false){
	alert("生成的代码已经复制到粘贴板，你可以使用Ctrl+V 贴到需要的地方去了哦！  ");
}
}
copy2Clipboard =  function(txt){
	if(window.clipboardData){
		window.clipboardData.clearData();
		window.clipboardData.setData("Text",txt);
	}else if(navigator.userAgent.indexOf("Opera")!=-1){
		window.location=txt;
	}else if(window.netscape){
		try{netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
	}catch(e){
		alert("您的firefox安全限制限制您进行剪贴板操作，请打开’about:config’将signed.applets.codebase_principal_support’设置为true’之后重试，相对路径为firefox根目录/greprefs/all.js");
		return false;
	}
var clip=Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
if(!clip)
return;
var trans=Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
if(!trans)
return;
trans.addDataFlavor('text/unicode');
var str=new Object();var len=new Object();
var str=Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
var copytext=txt;str.data=copytext;trans.setTransferData("text/unicode",str,copytext.length*2);
var clipid=Components.interfaces.nsIClipboard;
if(!clip)
return false;
clip.setData(trans,null,clipid.kGlobalClipboard);}}
</script>
</html>

