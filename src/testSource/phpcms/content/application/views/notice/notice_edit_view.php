<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  
<title>公告编辑</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
</head>
<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>

<table class="p_g" width="100%" border="0">
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">公告标题</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'notice_title', 
		'id' => "notice_title", 
		'size' => 72,
		'value' => $record['notice_title'] ) );
echo form_error ( 'notice_title', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td valign = "top">公告内容</td>
		<td>
<?php 
/*echo form_textarea ( array (
		'name' => 'notice_content', 
		'id' => 'notice_content', 
		"value" => $record['notice_content'] ) );
echo form_error ( 'notice_content', '<span class="error">', '</span>' );*/
echo $notice_content;
?></td>
	</tr>
	<tr>
		<td valign = "top">应用方案</td>
		<td>
<?php 
echo form_dropdown ( 'app_id', $select_app, $record['app_id'] );
echo form_error ( 'app_id', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td valign = "top">发布站点</td>
		<td>
<?php 
echo form_dropdown ( 'city_id', $select_city, $record['city_id'] );
echo form_error ( 'city_id', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td>创建时间</td>
		<td>
<?php 
echo form_input (array(	'name'=>'create_time',
						'id'=>'create_time',
						'value'=>$record_time,
						'onclick'=>"WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"));
?>
<font color = "red">*tip</font>: 点击输入框选择时间 若不选默认为输入框内显示的时间
		</td>
		</tr>
	<tr>
		<td valign = "top">跳转链接</td>
		<td>
<?php 
echo form_input ( array (
		'name' => 'notice_link', 
		'id' => 'notice_link',
		'size' => 72, 
		'value' => $record['notice_link'] ) );

?></td>
	</tr>
	
	<tr><td> </td><td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td></tr>
</table>
<?php 
echo form_close();
?>
</script>
</body>
</html>