<script charset="utf-8" src="<?php echo base_url();?>public/kindeditor/kindeditor-min.js"></script>
    <script>
			var editor;
			KindEditor.ready(function(K) {
					editor = K.create('textarea[name="<?php echo $ked_id_s?>"]', {
					uploadJson : '<?php echo base_url();?>public/kindeditor/php/upload_json.php',
					fileManagerJson : '<?php echo base_url();?>public/kindeditor/php/file_manager_json.php',
					allowFileManager : true,
					tagLineMode:true,
					<?php if(!empty($ked_contentsCss_s)){?>
					cssPath : [<?php echo substr($ked_contentsCss_s,1);?>],
					<?php }else{?>
					cssPath : ['<?php echo base_url();?>public/kindeditor/css/public.css'],
					<?php }?>
					filterMode:false ,
					//urlType : 'absolute',
					newlineTag : 'br',
					resizeType : 1,
					allowPreviewEmoticons : false,
					allowImageUpload : false,

					items : [
						'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
						'insertunorderedlist', '|', 'emoticons', 'image', 'link']

				});

			});

	</script>
      <textarea name="<?php echo $ked_id_s?>"  id="<?php echo $ked_id_s?>" style="width:600px;height:200px;visibility:hidden; font-size:12px;" ><?php echo $ked_value_s?></textarea>