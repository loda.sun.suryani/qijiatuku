
<script charset="utf-8" src="<?php echo base_url();?>public/kindeditor/kindeditor-min.js"></script>
    <script>
			var editor;
			KindEditor.ready(function(K) {
					editor = K.create('textarea[name="<?php echo $ked_id?>"]', {
					uploadJson : '<?php echo base_url();?>public/kindeditor/php/upload_json.php',
					fileManagerJson : '<?php echo base_url();?>public/kindeditor/php/file_manager_json.php',
					allowFileManager : true,
					<?php if(!empty($ked_contentsCss)){?>
					cssPath : [<?php echo substr($ked_contentsCss,1);?>],
					<?php }else{?>
					cssPath : ['<?php echo base_url();?>public/kindeditor/css/public.css'],
					<?php }?>
					filterMode:false ,
					//urlType : 'absolute',
					newlineTag : 'br',
					items : ['source', '|', 'undo', 'redo', '|', 'preview', 'cut', 'copy', 'paste',
		'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
		'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
		'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
		'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
		'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image',
		'flash',  'table', 'hr', 'anchor', 'link', 'unlink']

				});

			});

	</script>
      <textarea name="<?php echo $ked_id?>"  id="<?php echo $ked_id?>" style="width:600px;height:300px;visibility:hidden; font-size:12px;" ><?php echo $ked_value?></textarea>