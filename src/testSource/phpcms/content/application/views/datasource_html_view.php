<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php $ci = get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>手动编辑</title>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>

</head>
<body id="demo_body">
<div id="container" style="height:auto;">
<?php
echo form_open ( modify_build_url(), array ('name' => "theform","id" => "theform" ) ); 

echo "使用富文本编辑器";
echo form_checkbox ( array (
		'name' => 'use_richeditor', 
		'id' => 'use_richeditor', 
		'value' => '1', 
		'onchange'=>"javascript:$('#theform').submit();",
		'checked' => ( bool ) ($this->input->post ( 'use_richeditor' )) ) );
echo "\n<br>";
echo $findeditor;
echo form_submit ( 'submitform', '保存', "id='submitform'" ); 
echo form_close ();
?>
</div>
</body>
</html>