<?php $ci= &get_instance();?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>

<title>页面</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/box.css" type="text/css" />
<style type="text/css">
#fullbg {  
	background-color:Gray;  
	left:0px;  
	opacity:0.5;  
	position:absolute;  
	top:0px;  
	z-index:9999;  
	filter:alpha(opacity=50); /* IE6 */  
	-moz-opacity:0.5; /* Mozilla */  
	-khtml-opacity:0.5; /* Safari */  
}  
#loadingpic {  
	background-color:#FFF;  
	border:1px solid #888;  
	display:none;  
	left:50%;  
	margin:-100px 0 0 -100px;  
	padding:12px;  
	position:fixed !important; /* 浮动对话框 */  
	position:absolute;  
	top:50%;  
	z-index:10000;  
} 
</style>

</head>

<body>
<div id="fullbg"></div>
<img id="loadingpic" src="<?php	echo base_url ()?>public/js/dialog/skins/icons/loading.gif" />

<?php
echo form_open ( modify_build_url(array() ), array (
		'name' => "theform", 
		"id" => "theform" ) );
?>

<div id="fn">
<?php 
//echo form_submit ( 'submitform', '完成', "id='submitform'" );
$add_record_url = modify_build_url(array('c'=>"forminput","m"=>"index" ) );
echo html_tag('A','录入记录',
		array('href'=>"javascript:show_v('录入一条记录','$add_record_url','0','0')"));

echo "&nbsp;&nbsp;&nbsp;(表单信息  [";
echo "id: ",$form_info['form_id'];
echo ", 名称: ",$form_info['form_name'];
echo "])";
?>
</div>

<div style="overflow-x:scroll;" width="100%">
<?php
echo $slave_grid; 
?>
</div>
<button onclick="if(parent.window.close_dialog){parent.window.close_dialog();}return false;">完成</button>
&nbsp;
	
<?php
echo html_tag('A','刷新',
		array('href'=>modify_build_url(array() )) );
 
echo form_close ();
?>

<script> 

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script>
</body>
</html>

<script type="text/javascript"> 
function showBg() {  
	var bh = $("body").height();  
	var bw = $("body").width();  
	$("#fullbg").css({  
		height:bh,  
		width:bw,  
		display:"block"  
	});  
	$("#loadingpic").show();  
}  
function closeBg() {  
	$("#fullbg,#loadingpic").hide();  
}
</script>    
 
<!--[if lte IE 6]> 
<script type="text/javascript"> 
$(document).ready(function() {  
	var domThis = $('#loadingpic')[0];  
	var wh = $(window).height() / 2;  
	$("body").css({  
	"background-image": "url(about:blank)",  
	"background-attachment": "fixed"  
	});  
	domThis.style.setExpression=\'#\'"   
});  
</script> 
<![endif]-->