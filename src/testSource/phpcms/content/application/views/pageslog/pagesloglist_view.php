<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"  src="<?php  echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
<script type="text/javascript"  src="<?php  echo base_url ()?>public/js/li.js"></script>
<title>日志列表</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php
echo base_url ()?>public/css/box.css"
	type="text/css" />
</head>
<body>
<?php echo crumbs_nav("/页面管理/日志列表");?>
<?php
echo form_open ( modify_build_url (), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
版本查询:
&nbsp;&nbsp;&nbsp;&nbsp;
从<input type="text" name="start_time" class="Wdate" id="d4331" size="12" onFocus="WdatePicker({doubleCalendar:true,dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'d4332\',{M:0,d:-0})||$dp.$DV(\'2070-4-3\',{M:0,d:-0})}'})" value="" />
到<input type="text" class="Wdate" name="end_time"  id="d4332" onFocus="WdatePicker({doubleCalendar:true,dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'d4331\',{M:0,d:0});}',maxDate:'2070-4-3'})" size="12" value=""/>
<?php
echo form_submit ( 'search', '查询', "id='search'" );
?>
<br />
<?php
echo $pages_nav;
?>
<br />
<?php
echo $main_grid;
?>
<br />
<br />
<br />
<?php
echo form_close ();
?>
</body>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
</script>
</html>