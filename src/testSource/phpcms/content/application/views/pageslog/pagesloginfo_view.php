<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!--<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/li.js"></script>
-->
<style type="text/css">

.red {color:red;}


</style>
<title>日志详情</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php
echo base_url ()?>public/css/box.css"
	type="text/css" />
</head>
<?php echo crumbs_nav("/页面管理/日志列表/日志详情");?>
<table id="" class="table_grid" width="98%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<th>
			page_id
		</th>
		<td>
			<?php echo $loginfo['page_id']; ?>
		</td>
	</tr>
	<tr>
		<th>
			版本时间
		</th>
		<td>
			<?php echo date("Y-m-d H:i:s",$loginfo['time']); ?>
		</td>

	</tr>
	<tr>
		<th>
			cms_page
		</th>
		<td>
			<?php if($loginfo['cms_page'])
			{
				get_array_array($loginfo['cms_page'],"");

			}
			?>
		</td>
	</tr>
	<tr>
		<th>
			cms_page_tpl
		</th>
		<td>
			<?php if($loginfo['cms_page_tpl'])
			{
				get_array_array($loginfo['cms_page_tpl'],"");
			}
			?>
		</td>
	<tr>
		<th rowspan="<?php echo count($loginfo['cms_page_block']);?>">
			cms_page_block
		</th>
			<?php if($loginfo['cms_page_block'])
			{
				foreach($loginfo['cms_page_block'] as $key=>$value)
				{
					echo "<td>"; 	
					
					get_array_array($value,"");

					if($key == count($loginfo['cms_page_block'])-1)
					{
						echo "</td></tr>";
					}
					else
					{
						echo "</td></tr><tr>";
					}
				}
			}
			else
			{
				echo "</td></tr>";
			}
			?>		
	<tr>
		<th rowspan="<?php echo count($loginfo['cms_page_block_info']);?>">
			cms_page_block_info
		</th>
			<?php if($loginfo['cms_page_block_info'])
			{
				foreach($loginfo['cms_page_block_info'] as $key=>$value)
				{
					echo "<td>"; 	
					if($value)
					{
						foreach($value as $ke1=>$valu1)
						{
							get_array_array($valu1,"");
						}
					}
					if($key == count($loginfo['cms_page_block_info'])-1)
					{
						echo "</td></tr>";
					}
					else
					{
						echo "</td></tr><tr>";
					}
				}
			}
			else
			{
				echo "</td></tr>";
			}
			?>		
	</tr>
</table>
<!--<table id="" class="" width="98%" cellspacing="0" cellpadding="0" border="1">
	<tr>
		<th>
			page_id
		</th>
		<td>
			<?php echo $loginfo['page_id']; ?>
		</td>
	</tr>
	<tr>
		<th>
			版本时间
		</th>
		<td>
			<?php echo date("Y-m-d H:i:s",$loginfo['time']); ?>
		</td>

	</tr>
	<tr>
		<th>
			cms_page
		</th>
		<td>
			<?php var_dump($loginfo['cms_page']);?>
		</td>
	</tr>
	<tr>
		<th>
			cms_page_tpl
		</th>
		<td>
			<?php var_dump($loginfo['cms_page_tpl']);?>
		</td>	
	<tr>
		<th >
			cms_page_block
		</th>
		<td>
			<?php var_dump($loginfo['cms_page_block']);?>
		</td>	
	<tr>
		<th>
			cms_page_block_info
		</th>
		<td>
			<?php var_dump($loginfo['cms_page_block_info']);?>
		</td>
	</tr>
</table>-->
<body>
<?php
	function get_array_array($array,$tab)
	{
		$tab.="&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
		if(is_array($array))
		{
			foreach($array as $key => $value)
			{
				if(is_array($value))
				{
					echo $tab.$key."&nbsp;:<br>";
					get_array_array($value,$tab);
				}
				else{
					echo $tab.$key."&nbsp;:&nbsp;<span class='red'>".htmlentities($value,ENT_NOQUOTES,"UTF-8")."</span><br>";
				}
			}

		}
	}
?>