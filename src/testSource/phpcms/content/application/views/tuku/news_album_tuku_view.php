<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>图片列表</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
</head>
<body>
<?php
$ci = &get_instance ();
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
<a href="javascript void(0)" onclick='addpermi(<?php echo $ab_id?>);return false;'>批量新增图库</a>&nbsp;&nbsp;&nbsp;
<a href = "javascript void(0)" onclick="bulkediting(<?php echo $ab_id?>);return false;">批量修改图片描述</a>
<br/>

<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>
<?php
//echo $form_input;
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function tuku_e(v){
	show_v('编辑','<?php echo site_url("c=news_album&m=edit_img")?>&img_id='+v,'0','0' );
}
function tuku_f(k,v){
	ajax_then_submit("<?php echo site_url("c=news_album&m=cover")?>&img_id="+k+"&ab_id="+v);
}
function tuku(v){
	show_v('图库列表','<?php echo site_url("c=news_album&m=tuku")?>&ab_id='+v,'0','0' );
}
function addpermi(v){
	show_v('新增图库列表','<?php echo site_url("c=news_album&m=addtuku")?>&ab_id='+v,'0','0' );
}
function bulkediting(v){
	show_v('批量修改图片描述','<?php echo site_url("c=news_album&m=bulk_edit")?>&ab_id='+v,'0','0');
}
function masterdel(k,v){
	ajax_then_submit("<?php echo site_url("c=news_album&m=tuku_delete")?>&img_id="+k+"&ab_id="+v);
}
function img_move_down(k,v){
	ajax_then_submit("<?php echo site_url("c=news_album&m=img_move_down")?>&img_id="+k+"&ab_id="+v);
}
function img_move_up(k,v){
	ajax_then_submit("<?php echo site_url("c=news_album&m=img_move_up")?>&img_id="+k+"&ab_id="+v);
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};
</script>
</body>
</html>