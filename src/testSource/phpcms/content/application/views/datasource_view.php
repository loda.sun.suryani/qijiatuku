<?php
$ci =& get_instance();
echo form_open ( modify_build_url(), array ('name' => "theform","id" => "theform" ) );
echo form_hidden ( "ischangesql", "0" );
if(!intval($ci->input->get('sql_id'))){
	echo form_dropdown ( 'sql_id', $select_sql_options, $this->input->post ( "sql_id" ), "id='sql_id' size='10'" );
	echo "\n\n";
}
if ($sql_string) {
	echo "<div class='sqlstring' >{$sql_string}</div>";
}
echo "\n\n";

?>
<table width="100%" border="0">
	<tr>
		<td valign="top">
<?php
if(is_array($form_arr)>0){
	foreach ( $form_arr as $key => $value ) {
		$field_key = "input_{$key}";
		$default_value = null;
		if (is_array ( $_POST ) and array_key_exists ( $field_key, $_POST )) {
			$default_value = $this->input->post ( $field_key );
		} else {
			$default_value = $value ['default'];
		}
		echo '<div class="clear"></div><div class="formlabel" ><b>';
		echo "\n";
		echo form_label ( "{$value['description']}($key)", $field_key );
		echo '</b></div>';
		echo "\n";
		
		if ($value ['type'] == 'text') {
			echo form_input ( array (
					'name' => $field_key, 
					'id' => "{$key}", 
					"value" => $default_value ) );
		}
		if ($value ['type'] == 'select') {
			echo form_dropdown ( $field_key, $value ['options'], $default_value );
		}
		echo "\n";
		echo form_error ( $field_key, '<span class="error">', '</span>' );
		echo "\n\n";
	}
}

if (! $this->input->post ( 'manual_mode' )) {
	//-------------------------------------------------------------------------------
	echo '<div class="clear"></div><div class="formlabel" ><b>';
	echo "\n";
	echo form_label ( '开始记录号(select_limit_start)' );
	echo '</b></div>';
	echo "\n";
	echo form_input ( array (
			'name' => 'select_limit_start', 
			'id' => "select_limit_start", 
			"value" => $this->input->post ( 'select_limit_start' ) ) );
	echo "\n";
	echo form_error ( 'select_limit_start', '<span class="error">', '</span>' );
	echo "\n\n";
	
	//-------------------------------------------------------------------------------
	echo '<div class="clear"></div><div class="formlabel" ><b>';
	echo "\n";
	echo form_label ( '记录条数(select_limit_count)' );
	echo '</b></div>';
	echo "\n";
	echo form_input ( array (
			'name' => 'select_limit_count', 
			'id' => "select_limit_count", 
			"value" => $this->input->post ( 'select_limit_count' ) ) );
	echo "\n";
	echo form_error ( 'select_limit_count', '<span class="error">', '</span>' );
}
echo '<div class="clear"></div>';
echo "<br/>\n\n";
//-------------------------------------------------------------------------------
echo form_submit ( 'submitform', '测试数据源', "id='submitform'" );
if (count ( $records_data ) > 0) {
	echo "&nbsp;";
	echo form_submit ( 'submitform', '保存', "id='submitnext'" );
}
?>
    <br />
		</td>
		<td valign="top">
<?php
if (( bool ) $this->input->post ( "manual_mode" )) {
	//已经选择的ID
	if ($selectedrecordgrid) {
		echo "已经选择的记录:<br/>";
		echo $selectedrecordgrid;
	}
	echo form_hidden ( "record_id_move_up", '' );
	echo form_hidden ( "record_id_move_down", '' );
	echo form_hidden ( "record_id_remove", '' );
}
?>
    <br />
		</td>
		<td valign="top">
<?php
if(!intval($ci->input->get('sql_id'))){
	if ($fieldgrid) {
		echo $fieldgrid;
	}
}

?>
      <br />
		</td>
		<td valign="top">
<?php
if(!intval($ci->input->get('sql_id'))){
	if ($selectedfieldgrid) {
		echo $selectedfieldgrid;
	}
}
?>
        <br />
		</td>
	</tr>
</table>
<?php

echo form_checkbox ( array (
		'name' => 'manual_mode', 
		'id' => 'manual_mode', 
		'value' => '1', 
		'onclick'=>"this.blur();",
		'checked' => ( bool ) ($this->input->post ( 'manual_mode' )) ) );
echo form_label ( "手动选择模式", "manual_mode" );
echo form_hidden ( "id_list", $id_list );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );

if ($this->input->post ( 'manual_mode' )) {
	echo '<button id="checkallrecords">选择所有/反转</button>';
}
echo $pages_nav;
?>
<div style="overflow-x:scroll;">
<?php 
echo $datagrid;
?>
</div>
<?php 
echo "<div class='sqlstring' >{$sql_real}</div>";
if ($id_list) {
	echo "\n(已经选择的记录:{$id_list})";
}

//my_debug ( $_POST );
echo form_close ();
?>