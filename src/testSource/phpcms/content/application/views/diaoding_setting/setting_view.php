<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jpbridge.js"></script>
<title>HTML应用程序</title>
<script>
<!--标签管理js引用-->
function input_tag_changed(){
	//alert('input_tag_changed!...');
	if(console){
		console.log("input_tag_changed! ...");
	}
}
</script>
</head>
<body>

<?php 
$ci =& get_instance();
echo form_open( modify_build_url ( ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>

<input type="hidden" name="setting_tag" id="setting_tag" value=""><?php echo form_error('setting_tag', '<div class="error">', '</div>'); ?><?php echo $show_setting_tag;?>
方案名称
<?php 
echo form_input ( array (
		'name' => 'setting_name', 
		'id' => "setting_name",
		'size' => 40,
		'autocomplete'=>'on',
		"value" => $this->input->post ( 'setting_name' ) ) );
echo nbs(5);
echo form_submit ( 'search', '查询', "id='search'" );?>
<div id="main_grid"></div><!--显示列表容器-->
<div id="pages_nav"></div><!--分页列表容器-->
<?php 
echo form_close ();
?>

<script>
$('#theform').ajaxForm({
	url:"<?php echo modify_build_url(array('m'=>'do_post'))?>",
    success: function(response){
    //alert(response);
    $('body').append(response);
	}
});


function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
</script>

<script> 
$(document).ready(
		function(){
			$("#theform").submit();//加载一次提交
		}
);
</script>

</body>
</html>