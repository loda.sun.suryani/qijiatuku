<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>

<title>列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
<br/>
搜索用户名
<?php 
echo form_input ( array (
		'name' => 'user_name', 
		'id' => "user_name",
		'size' => 40,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'user_name' ) ) );
echo nbs(5);
?>
&nbsp;
搜索事件
<?php
echo form_input ( array (
		'name' => 'query_str', 
		'id' => "query_str",
		'size' => 40,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'query_str' ) ) );
echo nbs(5);
echo form_submit ( 'search', '搜索', "id='search'" );
?>
<br/>

<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>

<?php
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
</script> 

</body>
</html>