<?php $ci= &get_instance();?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>

<title>输入表单</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>

<?php echo crumbs_nav("/页面管理/新增页面");?>
<hr/>
<?php
echo form_open ( modify_build_url(array('c'=>'createformfield',
										'form_id'=>$this->input->get ( 'form_id' ),
										'field_id'=>$this->input->get ( 'field_id' ))),  
				array (
		'name' => "theform", 
		"id" => "theform" ) );
?>
<div id="fn">
<?php 
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</div>
<table id="p_g" width="855" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="80" valign="top">字段</td>
		<td valign="top">
<?php 
echo form_input ( array (
		'name' => 'field_name', 
		'id' => "field_name",
		'size' => 40,  
		"value" => $ci->field ( 'field_name' ) ) );

echo form_error ( 'field_name', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
<br>只能是英文,可以不填
		</td>
	</tr>
	<tr>
		<td width="80" >字段名称</td>
		<td>
<?php 
echo form_input ( array (
		'name' => 'field_title', 
		'id' => "field_title",
		'size' => 40,  
		"value" => $ci->field ( 'field_title' ) ) );

echo form_error ( 'field_title', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
		</td>
	</tr>
	<tr>
		<td>字段类型</td>
		<td>
<?php
echo form_dropdown ( 'field_type', $field_type_select, $ci->field ( "field_type" ), "id='field_type'" );
?>
		</td>
	</tr>
	<tr>
		<td>可选项</td>
		<td>
<?php
echo form_textarea ( 'field_options',  $ci->field ( "field_options" ), "id='field_options'" );
?>
<br/>
填写格式为json,例如:
<font color="blue">
{"a":"选项01","b":"选项02","c":"选项03"}
</font>
		</td>
	</tr>
	<tr>
		<td width="80" valign="top">验证规则</td>
		<td>
<?php 
echo form_input ( array (
		'name' => 'field_rules', 
		'id' => "field_rules",
		'size' => 40,  
		"value" => $ci->field ( 'field_rules' ) ) );

echo form_error ( 'field_rules', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
<br/>
以下列表将列出可被使用的原生规则,多个规则之间用|线隔开
        <TABLE cellSpacing="1" cellPadding="0" border="1">
          <TBODY>
            <TR>
              <TH>规则</TH>
              <TH>参数</TH>
              <TH>描述</TH>
              <TH>举例</TH>
            </TR>
            <TR>
              <TD><STRONG>required</STRONG></TD>
              <TD>No</TD>
              <TD>如果元素为空，则返回FALSE</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>matches</STRONG></TD>
              <TD>Yes</TD>
              <TD>如果表单元素的值与参数中对应的表单字段的值不相等，则返回FALSE</TD>
              <TD>matches[form_item]</TD>
            </TR>
            <TR>
              <TD><STRONG>min_length</STRONG></TD>
              <TD>Yes</TD>
              <TD>如果表单元素值的字符长度少于参数中定义的数字，则返回FALSE</TD>
              <TD>min_length[6]</TD>
            </TR>
            <TR>
              <TD><STRONG>max_length</STRONG></TD>
              <TD>Yes</TD>
              <TD>如果表单元素值的字符长度大于参数中定义的数字，则返回FALSE</TD>
              <TD>max_length[12]</TD>
            </TR>
            <TR>
              <TD><STRONG>exact_length</STRONG></TD>
              <TD>Yes</TD>
              <TD>如果表单元素值的字符长度与参数中定义的数字不符，则返回FALSE</TD>
              <TD>exact_length[8]</TD>
            </TR>
            <TR>
              <TD><STRONG>alpha</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素值中包含除字母以外的其他字符，则返回FALSE</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>alpha_numeric</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素值中包含除字母和数字以外的其他字符，则返回FALSE</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>alpha_dash</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素值中包含除字母/数字/下划线/破折号以外的其他字符，则返回FALSE</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>numeric</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素值中包含除数字以外的字符，则返回 FALSE</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>integer</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素中包含除整数以外的字符，则返回FALSE</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>is_natural</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素值中包含了非自然数的其他数值 （其他数值不包括零），则返回FALSE。自然数形如：0,1,2,3....等等。</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>is_natural_no_zero</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素值包含了非自然数的其他数值 （其他数值包括零），则返回FALSE。非零的自然数：1,2,3.....等等。</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>valid_email</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素值包含不合法的email地址，则返回FALSE</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>valid_emails</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素值中任何一个值包含不合法的email地址（地址之间用英文逗号分割），则返回FALSE。</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>valid_ip</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素的值不是一个合法的IP地址，则返回FALSE。</TD>
              <TD> </TD>
            </TR>
            <TR>
              <TD><STRONG>valid_base64</STRONG></TD>
              <TD>No</TD>
              <TD>如果表单元素的值包含除了base64 编码字符之外的其他字符，则返回FALSE。</TD>
              <TD> </TD>
            </TR>
          </TBODY>
        </TABLE></td>
	</tr>
	<tr>
		<td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td>
		<td>&nbsp;</td>
	</tr>
</table>
<?php 
echo form_close ();
?>
<div class="hide_box" id="frameBox" style="width: 1000px;">
<h4><a href="javascript:void(0)" title="关闭窗口">×</a><span id="t_title"></span></h4>
<div><iframe frameborder="0" id="add_frame" scrolling="auto"
	width="100%" height="500" src=""></iframe></div>
</div>
<script> 

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script>
</body>
</html>