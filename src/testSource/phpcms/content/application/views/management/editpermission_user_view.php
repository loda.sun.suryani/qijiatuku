<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>生成的表单相关文件</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<title>列表</title>
<style type="text/css">
<!--
#user_id {
	vertical-align: -2px;
	_vertical-align: -1px;
	margin-right: 2px;
	vertical-align: middle;
}

#role_id {
	vertical-align: -2px;
	_vertical-align: -1px;
	margin-right: 2px;
	vertical-align: middle;
}
-->
</style>
<style type="text/css">
/*.page{padding:2px;font-weight:bold;font-size:12px;}
.page span{padding:0 5px 0 5px;margin:2px;background:#666;color:#fff;border:1px solid #09c;}*/
body {
	font-family: Tahoma;
	font-size: 12px;
}

.fixbox {
	border: 1px solid #ccc;
	width: 400px;
	height: 200px;
	overflow: auto;
	position: relative;
	z-index: 202;
}

.fixtable {
	width: 750px;
	z-index: 1001;
	position: relative;
	overflow: auto;
	border-spacing: 0;
	border-collapse: collapse;
	border: 0;
}

.fixtable tr {
	background-color: #fff;
}

.fixtable th {
	top: expression(this.parentElement.parentElement.parentElement.parentElement.scrollTop
		-   0);
	position: relative;
	z-index: 10;
	background: url(headbg.jpg) repeat-x top left;
	border-bottom: 1px solid #ccc;
	border-right: 1px solid #ccc;
	height: 19px;
}

.fixtable td {
	height: 20px;
	border-bottom: 1px solid #ccc;
	border-right: 1px solid #ccc;
	text-align: left;
}

th.lockcolumn {
	left: expression(this.parentElement.parentElement.parentElement.parentElement.scrollLeft
		-   0);
	z-index: 99;
	width: 80px;
}

td.lockcolumn {
	position: relative;
	left: expression(this.parentElement.parentElement.parentElement.parentElement.scrollLeft
		-   0);
}

.fullwidth {
	width: 100%;
}

.xscroll {
	overflow-x: hidden;
}
</style>
<script> 
var xmlhttp;
var kkc
function loadXMLDoc(url,cont)
{
kkc=cont
xmlhttp=null;
if (window.XMLHttpRequest)
  {// code for all new browsers
  xmlhttp=new XMLHttpRequest();
  }
else if (window.ActiveXObject)
  {// code for IE5 and IE6
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
if (xmlhttp!=null)
  {
  xmlhttp.onreadystatechange=state_Change;

  xmlhttp.open("GET",url,true);
  xmlhttp.send(null);
  }
else
  {
  alert("Your browser does not support XMLHTTP.");
  }
}

function state_Change()
{
if (xmlhttp.readyState==4)
  {// 4 = "loaded"
  if (xmlhttp.status==200)
    {// 200 = OK
    // ...our code here...
		document.getElementById(kkc).innerHTML=(xmlhttp.responseText)
		//document.getElementById(kkc).innerText=xmlhttp.responseText
    }
  else
    {
    document.getElementById(kkc).innerHTML="数据加载失败!"
    }
  }
}
//function showInternet(address,cont){
//
//loadXMLDoc(address,cont)
//
//}

//启动对页面的请求
var tem = ""; 
function startRequest(e,itemName,thisvalue) 
{
	tem= document.getElementById("user_p_id").value; 
	deltem= document.getElementById("del_user_id").value; 
	var aa=document.getElementsByName(itemName); 
	var bb=document.getElementById('user_p_id');
	var del=document.getElementById('del_user_id');
	if(e.checked==true){ 
	tem += thisvalue+","; 
	deltem = deltem.replace(thisvalue+",",""); 
	} 
	else{ 
	deltem += thisvalue+","; 
	tem = tem.replace(thisvalue+",",""); 
	}
	bb.value=tem;
	del.value=deltem;
}

</script>
</head>
<body>
<?php
echo $message;
$ci = &get_instance ();
echo form_open ( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table id="p_g" width="855" border="0" cellpadding="0" cellspacing="0">

	<tr>
		<td colspan="2" valign="top">键名<br><?php
		echo $permission_key;
		echo form_error ( 'permission_key', '<span class="error" style="margin-left:10px;">', 
			'</span>' );
		?>
		</td>
	</tr>
	<tr>
		<td colspan="2" valign="top">权限名/说明<br><?php
		echo $permission_name;
		echo form_error ( 'permission_name', '<span class="error" style="margin-left:10px;">', 
			'</span>' );
		?>
		</td>
	</tr>
	<tr>
		<td width="94" valign="top">授权用户 <!--<font color="red">注:已经绑定用户不可操作</font>--></td>
	</tr>
	<tr>
		<td valign="top"><input id="user_name" onkeypress="if(event.keyCode==13){Javascript:change_page()}" name="user_name" type="text" value=""><input id="search" type="button" onClick="change_page()" value="账户名称搜索" name="search"></td>
	</tr>
	<tr>
		<td valign="top"><div class="fixbox xscroll">
		  <table class="fixtable fullwidth">
		    <?php
								//未增加的权限
								if (is_array ( $user )) {
									foreach ( $user as $k => $v ) {
										$user_name = $v ['user_name'];
										$disabled=$v ['disabled']." ";
										if ($v ['check'] == 1) {
											$checkbox = "  checked=checked";//disabled=disabled
										} else{
											$checkbox = "  ";
										}
										?>
		    <tr>
		      <td><?php
										echo "<input name='user_id[]' $disabled id=user_id  $checkbox  type='checkbox' onclick=startRequest(this,'aa','" . $v ['user_id'] . "')   value=" . $v ['user_id'] . ">" . mb_substr ( 
											$user_name, 0, 100, "utf-8" ) . "";
										
										?></td>
	        </tr>
		    <?php
									}
								}
								?>
	      </table>
	    </div></td>
	</tr>
	
</table>
 <input name="del_user_id" type="hidden" id="del_user_id" size="100"><br>

  <input name="user_p_id" type="hidden" id="user_p_id" size="100">
<?php

echo form_submit ( 'submitform', '完成', "id='submitform'" );
echo form_close ();
?>
<script> 
function change_page(num){
	$("#theform").submit();//提交
	return false;
}

</script>
 
</body>
</html>