<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>

<title>列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>
<?php 
	echo crumbs_nav('/数据管理/站点域名管理');
?>
<hr>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
<a href="#" onclick='add();return false;'>新建</a>&nbsp;&nbsp;&nbsp;
<br/>
<input type="hidden" name="order_type" id = "order_type" value="">
<p>排序:
						[ip]&nbsp;<a href="#" onclick="order(1);return false;">升序</a>&nbsp;|&nbsp;
							<a href="#" onclick="order(2);return false;">降序</a>
							&nbsp;&nbsp;&nbsp;&nbsp;
						[域名]&nbsp;<a href="#" onclick="order(3);return false;">升序</a>&nbsp;|&nbsp;
							<a href="#" onclick="order(4);return false;">降序</a>
							&nbsp;&nbsp;&nbsp;&nbsp;
						[根目录]&nbsp;<a href="#" onclick="order(5);return false;">升序</a>&nbsp;|&nbsp;
							<a href="#" onclick="order(6);return false;">降序</a>
							&nbsp;&nbsp;&nbsp;&nbsp;
	当前排序方式:<font color="red"><?php echo $en;?></font>
							&nbsp;&nbsp;&nbsp;&nbsp;
	
			<a href="#" onclick="change_page(0);return false;">恢复默认排序</a>				   
	</p>
<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>

<?php
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function order(num){
	$("#page_num").attr('value',0);
	$("#order_type").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function edit(v){
	show_v('编辑','<?php echo site_url("c=ipdomain&m=edit")?>&id='+v,'0','0' );
}
function add(){
	show_v('创建','<?php echo site_url("c=ipdomain&m=add")?>','0','0' );
}
function masterdel(v){
	ajax_then_submit("<?php echo site_url("c=ipdomain&m=del")?>&id="+v);
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();	
}
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}

</script>
</body>
</html>