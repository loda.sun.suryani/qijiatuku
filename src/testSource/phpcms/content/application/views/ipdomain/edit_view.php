<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  
<title>编辑</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>

<table class="p_g" width="1000" border="0">
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">IP</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'ip', 
		'id' => "ip", 
		'size' => 72,
		'value' => $record['ip'] ) );
echo form_error ( 'ip', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">域名</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'domain', 
		'id' => "domain", 
		'size' => 72,
		'value' => $record['domain'] ) );
echo form_error ( 'domain', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">根目录</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'www_root', 
		'id' => "www_root", 
		'size' => 72,
		'value' => $record['www_root'] ) );
echo form_error ( 'www_root', '<span class="error">', '</span>' );
?>
</td>
	</tr>

	
	<tr><td> </td><td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td></tr>
</table>
<?php 
echo form_close();
?>
</script>
</body>
</html>