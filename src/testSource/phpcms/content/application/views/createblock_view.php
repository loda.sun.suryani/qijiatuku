<?php $ci = get_instance();?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"  src="<?php echo base_url();?>public/js/jquery.NbspAutoComplete.1.0.js"></script>
<script>
function input_tag_changed(){
	//alert('input_tag_changed!...');
	if(console){
		console.log("input_tag_changed! ...");
	}
}
</script>
    
<title>碎片</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>
<?php echo crumbs_nav("/碎片管理/编辑碎片");?>
<hr/>
<?php
//my_debug($ci->defaults);
$the_url = modify_build_url();
if($page_id){
	$the_url = modify_build_url(array('page_id'=>$page_id));
}

echo form_open ( $the_url, array (
		'name' => "theform", 
		"id" => "theform" ) );


if($error_mesage){
	echo '<span class="error">'."$error_mesage". '</span>'; 
}
?>

<table class="p_g" width="100%" border="0">
	<tr>
		<td width="80" style="min-width:100px;">碎片名称</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'block_name', 
		'id' => "block_name", 
		"value" => $ci->field ( 'block_name' ) ) );
echo form_error ( 'block_name', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td width = "80">所属栏目</td>
		<td>
<?php 
	echo form_dropdown ( 'block_column_id', $block_column_id_select, 
			$ci->field ( "block_column_id" ), "id='block_column_id' " );
?>
		</td>
	</tr>
	<tr>
		<td valign="top">
		碎片模板</td>
		<td id="sel" valign="top">
		<div>
<?php 
echo form_radio ( array (
		'name' => 'use_tpl', 
		'id' => "use_tpl_no", 
		"value" =>'0' ,
		"checked"=> !$ci->field ( 'use_tpl' ),
		"onclick"=>'this.onchange();',
		"onchange"=>'on_use_tpl_change();',
		"style"=>"VERTICAL-ALIGN:middle;"
		));
echo "不使用模板";
echo form_radio ( array (
		'name' => 'use_tpl', 
		'id' => "use_tpl_yes", 
		"value" =>'1' ,
		"checked"=> '1'==$ci->field ( 'use_tpl' ),
		"onclick"=>'this.onchange();',
		"onchange"=>'on_use_tpl_change();',
		"style"=>"VERTICAL-ALIGN:middle;"
));
echo "使用模板";
?>
	</div>
<?php 
$extra_info = '';
if($block_tpl_info['is_locked']==1){
	$extra_info = " disabled";
}

if($ci->field ( 'use_tpl' )){
	//array_unshift($select_tplcategory_options,"请选择");
	//array_unshift($select_tpl_options,"请选择");
	echo form_dropdown ( 'block_tpl_category_id', $select_tplcategory_options, 
			$ci->field ( "block_tpl_category_id" ), "id='block_tpl_category_id' $extra_info " );
	echo form_error ( 'block_tpl_category_id', '<span class="error">', '</span>' );
	echo "&nbsp;&nbsp;";
	if($block_tpl_info['is_locked']!=1) {
		$extra_info .= " style='display:none;' ";
	}
	echo form_dropdown ( 'block_tpl_id', $select_tpl_options, 
			$ci->field ( "block_tpl_id" ), "id='block_tpl_id' $extra_info "  );
	if($block_tpl_info['is_locked']!=1){
?>
<input <?php echo $block_tpl_info['is_locked'] == 1 ? "disabled" : "" ?> name="block_tpl" value="<?php echo $ci->field ( 'block_tpl_id' ) == "0" ? "" : $select_tpl_options[$ci->field ( 'block_tpl_id' )]?>" size="40" type="text">
<?php
	} 	
	echo form_error ( 'block_tpl_id', '<span class="error">', '</span>' );
	echo "(tpl id:{$block_tpl_info['block_tpl_id']},{$block_tpl_info['block_tpl_name']})";
	echo "记录数:";
	echo $block_info_size;
	echo ',<br>';
	echo $block_tpl_description;
	echo $block_tpl_demo;
}
?>
		</td>
	</tr>
	<tr>
		<td>记录轮播</td>
		<td><div class="nob">
<?php
echo form_checkbox ( array (
		'name' => 'block_datasource_mode', 
		'id' => "block_datasource_mode", 
		"value" =>'1',
		"checked"=> (bool)$ci->field ( 'block_datasource_mode' ) ) );
?>支持记录轮播&nbsp;&nbsp;
</div>
		</td>
	</tr>
	<tr><td>设置记录</td>
	<td>
<?php 
if($ci->field('use_tpl')){
	if($ci->field('block_sql_id')){
		//如果已经设置过相关的SQL字段,就可以直接去挑选记录(而不再次设置SQL 映射字段)
		echo form_button('create_new_ds','挑选记录',
				sprintf("onclick=\"javascript:show_v('数据源','%s','0','0' )\" ",
					modify_build_url(array(
								'c'=>'datasource',
								'block_id'=>$this->input->get("block_id"),
								'sql_id'=>$ci->field('block_sql_id')
					))
				)) ;
		echo ",";
	}
	if (!$ci->field ( 'block_datasource_mode' )) {
		echo form_button('create_new_ds','设置数据库记录',
			sprintf("onclick=\"javascript:show_v('数据源','%s','0','0' )\" ",
			site_url ("c=datasource&block_id=".$this->input->get("block_id")))) ;
	}else{
		echo form_button('create_new_ds','设置数据库记录',
			sprintf("onclick=\"javascript:show_v('数据源','%s','0','0' )\" ",
			site_url ("c=datasource&multi=1&block_id=".$this->input->get("block_id")))) ;
	}
	echo ",";
	echo form_button('create_new_ds_form','通过表单录入',
			sprintf("onclick=\"javascript:show_v('数据源','%s','0','0' )\" ",
			modify_build_url(array("c"=>"datasource","m"=>"form","block_id"=>$this->input->get("block_id")) 
			)
			)
			) ;
	echo ",";
	echo form_button('create_new_ds_form','API数据源',
			sprintf("onclick=\"javascript:show_v('数据源','%s','0','0' )\" ",
			modify_build_url(array("c"=>"apidatasource","m"=>"index","block_id"=>$this->input->get("block_id")) 
			)
			)
			) ;
	echo form_button('create_new_ds_form','API分类选择商品',
			sprintf("onclick=\"javascript:show_v('数据源','%s','0','0' )\" ",
			modify_build_url(array("c"=>"mallproducts","m"=>"index","block_id"=>$this->input->get("block_id")) 
			)
			)
			) ;
	echo ",";
}
$ck_edit_link = site_url ("c=datasource&m=html");
$ck_edit_link = modify_build_url(array('block_id'=>$this->input->get("block_id")),$ck_edit_link);//修改block_id段
if($ci->field('use_tpl')){
	if($ci->field("block_tpl_id")){
		$ck_edit_link = modify_build_url(array('block_tpl_id'=>$ci->field("block_tpl_id")),$ck_edit_link);//修改block_tpl_id段
	}
}
if (!$ci->field ( 'block_datasource_mode' )) {
	echo form_button('create_new_ds','HTML片段',
			sprintf("onclick=\"javascript:show_v('编写','%s','0','0' )\" ",
				  $ck_edit_link)) ;
}else{
	$ck_edit_link = modify_build_url(array('multi'=>'1'),$ck_edit_link);//修改multi段
	echo form_button('create_new_ds','HTML片段',
			sprintf("onclick=\"javascript:show_v('编写','%s','0','0' )\" ",
				  $ck_edit_link)) ;
}
echo $alternative_ds_grid;
?>	</td>
	</tr>
	<tr>
		<td valign="top">缓存刷新</td>
		<td><div class="nob">
<?php echo form_radio ( array (
		'name' => 'refresh_mode', 
		'id' => "refresh_mode_1", 
		"value" =>'1' ,
		"checked"=> (1==$ci->field ( 'refresh_mode' ) or !$ci->field ( 'refresh_mode' )),
		"onclick"=>"this.onchange();",
		"style"=>"VERTICAL-ALIGN:middle;",
		"onchange"=>"on_refresh_mode_change(1);"
		));
?>不缓存(每次调用刷新)<br />
<?php echo form_radio ( array (
		'name' => 'refresh_mode', 
		'id' => "refresh_mode_2", 
		"value" =>'2' ,
		"checked"=> 2==$ci->field ( 'refresh_mode' ),
		"onclick"=>"this.onchange();",
		"style"=>"VERTICAL-ALIGN:middle;",
		"onchange"=>"on_refresh_mode_change(1);" 
		));
?>缓存,周期:
<?php 
echo form_input ( array (
		'name' => 'cache_day', 
		'id' => "cache_day", 
		"value" =>$ci->field ( 'cache_day' ),
		'size' => '3',
		"style"=>"VERTICAL-ALIGN:middle;"
));
echo "天";
echo form_input ( array (
		'name' => 'cache_hour', 
		'id' => "cache_hour", 
		"value" =>$ci->field ( 'cache_hour' ),
		'size' => '3',
		"style"=>"VERTICAL-ALIGN:middle;"
));
echo "小时";
echo form_input ( array (
		'name' => 'cache_minute', 
		'id' => "cache_minute", 
		"value" =>$ci->field ( 'cache_minute' ),
		'size' => '3',
		"style"=>"VERTICAL-ALIGN:middle;"
));
echo "分";
echo form_input ( array (
		'name' => 'cache_second', 
		'id' => "cache_second", 
		"value" =>$ci->field ( 'cache_second' ),
		'size' => '3',
		"style"=>"VERTICAL-ALIGN:middle;"
));
echo "秒";
?>
<br/>
<?php 
echo form_radio ( array (
		'name' => 'refresh_mode', 
		'id' => "refresh_mode_3", 
		"value" =>'3',
		"checked"=> 3==$ci->field ( 'refresh_mode' ),
		"style"=>"VERTICAL-ALIGN:middle;",
		"onclick"=>"this.blur();",
		"onchange"=>"on_refresh_mode_change(1);" 
));

?>
不过期(若数据源定义有变化,会自动刷新)<br />
<?php echo form_radio ( array (
		'name' => 'refresh_mode', 
		'id' => "refresh_mode_4", 
		"value" =>'4' ,
		"checked"=> (4==$ci->field ( 'refresh_mode' ) ),
		"onclick"=>"this.onchange();",
		"style"=>"VERTICAL-ALIGN:middle;",
		"onchange"=>"on_refresh_mode_change(1);"
		));
?>静态,对生成的内容再编辑
(
<?php 
echo form_checkbox ( array (
		'name' => 'use_richeditor', 
		'id' => 'use_richeditor', 
		'value' => '1', 
		'onchange'=>"javascript:$('#theform').submit();",
		'checked' => ( bool ) ($this->input->post ( 'use_richeditor' )) ) );

?>
使用富文本编辑器)
<br/>
<?php 

if(4==$ci->field ( 'refresh_mode' )){
	if ($this->input->post ( 'use_richeditor' )) {
		$feditor_config = array (
			'css' => "", 
			'id' => 'block_content', 
			'value' => $ci->field ( 'block_content' ), 
			'width' => '600px', 
			'height' => '200px' );
		echo $ci->editors->getedit ( $feditor_config );
	} else {
		$content = $ci->field ( 'block_content' );
		$content = htmlentities($content,ENT_NOQUOTES,'UTF-8');
		echo "
		<textarea id=\"block_content\" name=\"block_content\" 
		style=\"width:600px;height:300px;font-size:12px;\" >$content</textarea>";
	}
}
?>

</div></td>
	</tr>
	<tr>
		<td>碎片关键字</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'block_keyword', 
		'id' => "block_keyword", 
		"value" => $ci->field ( 'block_keyword' ) ) );
?>
</td>
	</tr>
	<tr>
		<td>碎片描述</td>
		<td>
<?php 
echo form_textarea ( array (
		'name' => 'block_description', 
		'id' => "block_description", 
		"value" => $ci->field ( 'block_description' ) ) );

?></td>
	</tr>
	<tr>
		<td>用户自定义变量1</td>
		<td>
<?php 
echo form_textarea ( array (
		'name' => 'user_var_1', 
		'id' => "user_var_1",
		'rows'=>'1', 
		"value" => $ci->field ( 'user_var_1' ) ) );

?><br>(在模板中用$user_var_1引用)
	</td>
	</tr>
 <tr>
   <td style="font-size:14px;  color:#000;">碎片标签</td>
   <td style="font-size:14px;  color:#000;">
   <input name="block_tags" type="hidden" id="block_tags" value="<?php $ci->field ( 'block_tags' )?>" size="50">
   <?php echo $block_tags;?></td>
 </tr>
	<tr><td> </td><td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
echo form_close ();
?>
<!--  
<button onclick='show_v("预览","<?php //echo modify_build_url(array('c'=>'block'))?>","0","0");return false;'> 预览 </a>
-->
</td></tr>
</table>

<script type="text/javascript">
document.getElementById('block_datasource_mode').onclick = function(){
	document.getElementById('block_datasource_mode').blur();
}
;
document.getElementById('block_datasource_mode').onchange = function(){
	//var node = document.getElementById('block_datasource_mode');
	//node.checked = node.checked? "":"checked";
	$("#theform").submit();//提交
};

if(document.getElementById('block_tpl_category_id')){
	document.getElementById('block_tpl_category_id').onchange = function(){
		$("#theform").submit();//提交
	};
};
if(document.getElementById('block_tpl_id')){
	document.getElementById('block_tpl_id').onchange = function(){
		$("#theform").submit();//提交
	};
};

function multids_edit(v){
	show_v('设置时间',"<?php echo site_url ( 'c=createblock&m=ds_edit&block_id=' . $this->input->get('block_id') . '&ds_id=');?>"+v,'0','0');
}
function multids_delete(v){
	show_v('删除',"<?php echo site_url ( 'c=createblock&m=ds_delete&block_id=&ds_id=');?>"+v,'0','0');
}
function show_ds_detail(v){
	show_v('详细',"<?php echo modify_build_url(array('c'=>'datasource','m'=>'html')).'&ds_id=';?>"+v,'0','0');
}

function on_refresh_mode_change(v){
	$("#theform").submit();//提交
}
function on_use_tpl_change(){
	$("#theform").submit();//提交
}
function on_ds_enable_change(v){
	$.ajax({url:"<?php echo modify_build_url(array('m'=>'ds_enable_toggle') )."&ds_id=";?>"+v,
				cache: false,
				success: function(html){
					$("#theform").submit();//提交
				}
		});
}
function on_ds_single_change(v){
	$.ajax({url:"<?php echo modify_build_url(array('m'=>'ds_single_used') )."&ds_id=";?>"+v,
				cache: false,
				success: function(html){
					$("#theform").submit();//提交
				}
		});
}


function on_ds_combine_change(v){
	$.ajax({url:"<?php echo modify_build_url(array('m'=>'ds_combine_used') )."&ds_id=";?>"+v,
				cache: false,
				success: function(html){
					$("#theform").submit();//提交
				}
		});
}
</script>
<?php
//my_debug($_POST);
//my_debug(microtime());
?>
<div class="hide_box" id="frameBox" style="width:1000px;">
	<h4><a href="javascript:void(0)" title="关闭窗口">×</a><span id="t_title"></span></h4>
	<div><iframe frameborder="0" id="add_frame" scrolling="auto" width="100%" height="500" src=""></iframe></div>
</div>
<script> 
function block_copy(v){
	//alert("<?php echo modify_build_url(array("m"=>"copy_datasource"));?>&ds_id="+v);
	$.ajax({url:"<?php echo modify_build_url(array("m"=>"copy_datasource"));?>&ds_id="+v,
			cache: false,
			success: function(html){
				$("#theform").submit();//提交
				//document.location.reload();
			}
	});
}
var ED = function(){
	return document.getElementById(arguments[0]);
};

function set_datasource_id(v){
	ED('block_datasource_id').value = v;
}

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400,
	    width:m_width,
	    height:m_height
	});
	if('0'==m_width || '0'==m_height){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
	//document.location.reload();
};

ED('frameBox').getElementsByTagName('a')[0].onclick = close_dialog;

$(document.body).ready(function(){
	$("[name='block_tpl']").NbspAutoComplete({
		location       : "<?php echo site_url ( 'c=createblock&m=getblockmodel&block_id=' . $this->input->get ( 'block_id' ) . '&cid=' . $ci->field ( "block_tpl_category_id" ) )?>",
		datatype       : "json",
		selectcount    : "scrollbar",
		scrollbarcount : 15,
		isright        : false,
		offset_y       : 0,
		finishfunc     : blocktplFinish,
		bold		   : -1,
		selectfunc     : selectFunc
	});

	$("[name='block_tpl']").blur(function(){
		$("[class='picbrowser']").hide();
	});
});

function blocktplFinish(id, value) {
	$("#block_tpl_id").find("option[value='"+id+"']").attr("selected",true);
	$("#theform").submit();
	$("[class='picbrowser']").hide();
}

function selectFunc(id) {
	var _obj = $("[name='block_tpl']");
	if($("[class='picbrowser']").size() == 0) {
		$(document.body).append("<div class='picbrowser'><img style='width:700px;' /></div>");
	}
	var _curr = $("[class='picbrowser']");
	$.get("<?php echo site_url ( 'c=createblock&m=getpicpath&block_id=' . $this->input->get ( 'block_id' ) )?>",{id: id},function(data){
		if(data != "") {
			_curr.css("top", (_obj.offset().top + _obj.outerHeight() - 100) + "px");
			_curr.css("left", (_obj.offset().left + _obj.outerWidth()) + "px");
			_curr.find("img").attr("src", data);
			_curr.show();
		} else {
			_curr.hide();
		}
	});
}
</script>
</body>
</html>