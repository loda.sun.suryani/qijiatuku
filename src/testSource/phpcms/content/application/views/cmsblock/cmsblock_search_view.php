<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>碎片模板</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/tooltip.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/page.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/css/tooltip.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php  echo crumbs_nav('/碎片模板管理');?>
<hr />
<form name="form1" method="post" action="index.php?c=cmsblock&m=block_search" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="783" border="0" cellspacing="1" cellpadding="0" style="background:#f1f1f1;">
  <tr>
    <td width="39%" height="30" bgcolor="#FFFFFF">碎片模板名称      <input type="text" name="block_tpl_name" id="block_tpl_name" value="<?php echo $block_tpl_name?>" /></td>
    <td width="33%" bgcolor="#FFFFFF">状态<?php echo $control_from?></td>
    <td width="28%" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" bgcolor="#FFFFFF">模板分类<?php echo $tpl_category_id_from?></td>
    <td bgcolor="#FFFFFF">记录数<?php echo $number_from?></td>
    <td bgcolor="#FFFFFF"><input type="submit" name="button" id="button" value="查询" /></td>
  </tr>
</table>
</form>
<table width="822" border="0" cellspacing="1" cellpadding="0" style="background:#f1f1f1;">
  <tr>
    <td height="30" bgcolor="#FFFFCC">编号</td>
    <td bgcolor="#FFFFCC">碎片模板名称</td>
    <td bgcolor="#FFFFCC">模板分类</td>
    <td bgcolor="#FFFFCC">记录数</td>
    <td bgcolor="#FFFFCC">状态</td>
    <td bgcolor="#FFFFCC">发布日期</td>
    <td bgcolor="#FFFFCC">操作</td>
  </tr>
  <?php  
  if(is_array($list)){
  foreach($list as $k=>$item){
	  if($item['control']==0){
		  $color="#f1f1f1";
		  }else{
		  $color="#FFFFFF";
		  }
	  ?>

  <tr>
    <td width="7%" bgcolor="<?php echo $color?>"><?php echo $item['block_tpl_id']?></td>
    <td width="32%" bgcolor="<?php echo $color?>"><?php echo $item['block_tpl_name']?></td>
    <td width="15%" bgcolor="<?php echo $color?>"><?php echo $item['tpl_category']?></td>
    <td width="7%" bgcolor="<?php echo $color?>"><?php echo $item['size_row']?>X<?php echo $item['size_col']?></td>
    <td width="6%" bgcolor="<?php echo $color?>"><?php if($item['control']==1){echo "已开启";}elseif($item['control']==0){echo "已禁用";}elseif($item['control']==2){echo "已使用";}?></td>
    <td width="11%" bgcolor="<?php echo $color?>"><?php echo $item['create_time']?></td>
    <td width="22%" bgcolor="<?php echo $color?>"><a href="#" class="tooltip" title="<?php echo base_url().$pic_path;?>/<?php echo $item['tpl_path']?>/<?php echo $item['demo_pic_id']?>">示例</a> | <a href="index.php?c=cmsblock&m=blockedit&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>">修改</a> | <a href="index.php?c=cmsblock&m=cmsblockdel&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要删除记录吗?"))>删除</a> | 
    <?php if($item['control']==0){?>
    <a href="index.php?c=cmsblock&m=enable&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要开启记录吗?"))>开启</a>
    <?php }else{?>
  <a href="index.php?c=cmsblock&m=disable&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要禁用记录吗?"))>禁用</a>
  <?php }?>
    </td>
  </tr>
  <?php }
  
  }?>
  <tr>
    <td colspan="7" bgcolor="#FFFFFF"><?php echo $navigation?></td>
  </tr>
</table>


</body>
</html>