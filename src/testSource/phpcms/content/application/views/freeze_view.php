<?php $ci = get_instance();?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
</head>
<body>
<br/>
<?php
	echo "当前状态:", $ci->cache->get ( "cms_config_freezed" )?"1":"0";
	echo "<br>";
	echo html_tag ( "a", "freeze!", 
		array ("id"=>"doit", "href" => "#" )  );
	echo "<br>";
	echo html_tag ( "a", "unfreeze!", 
		array ("id"=>"undo", "href" => "#" )  );
	echo "<br>";

?>

<script>
var url_do = "<?php echo modify_build_url(array("m"=>"doit")); ?>";
var url_undo = "<?php echo modify_build_url(array("m"=>"undo")); ?>";
$("#doit").click(function(){ajax_then_reload(url_do);return false;});
$("#undo").click(function(){ajax_then_reload(url_undo);return false;});
</script>
</body>
</html>