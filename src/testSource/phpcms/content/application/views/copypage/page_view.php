<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>碎片模板</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>public/js/li.js" type="text/javascript"></script>
<script language="javascript">
function CheckAll(form) 
{ 
for (var i=0;i<form.elements.length;i++) 
{ 
//alert(form);
var e = form.elements[i]; 
if (e.name != 'chkall') 
e.checked = form.chkall.checked; 
} 

} 
</script>
</head>
<body>
<form id="form1" name="form1" method="post" action="<?php echo modify_build_url ( array ('c'=>'copypage','m'=>'submit') );?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="34%">页面名称：
        <input name="new_page_name"  type="text" id="new_page_name"  value="<?php echo $new_page_info['page_name']?>"size="30" />
      <input type="button" name="submitform" id="submitform" value="复制" /></td>
    <td width="66%"></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_grid">
 <thead>
		<tr>
			<th width="3%" ><input name="chkall" type="checkbox" id="chkall" value="all" /><!--<input name="chkall" type="checkbox" id="chkall" value="all" onClick="CheckAll(this.form)" />
全选--></th>
			<th width="14%" >碎片名称</th>
			<th width="23%" >引用/复制</th>
			<th width="12%" >新建碎片名称</th>
		  <th width="5%" >区域</th>
			<th width="5%" >操作</th>
		</tr>
	</thead>
        
        
<?php 
  //print_r($list);
  if(is_array($list)){
  foreach($list as $k=>$item){
  	$tmp_block_id =$item['block_id'];
	  ?>
  <tr>
    <td class="m_b"><input name="block_id[<?php echo $tmp_block_id;?>]" checked="checked"  type="checkbox" value="<?php echo $item['block_id']?>" /><?php echo $item['block_id']?></td>
    <td class="m_m"><?php echo $item['block_name']?></td>
    <td class="m_m">
      <select name="select[<?php echo $tmp_block_id;?>]" size="2">
      <option value="copy">复制</option>
      <option value="quoted">引用</option>
      </select>
   </td>
    <td class="m_f"><div id="divObj">
      <input name="block_name_replace[<?php echo $tmp_block_id;?>]" type="text"  value="<?php echo $item['block_name']."_".date("Ymd")?>"/>
    </div></td>
    <td class="m_jl"><span class="m_m"><?php echo $item['area_id']?></span></td>
    <td class="m_jl">预览</td>
  </tr>
   <?php }
  
  }?>
     <tr>
    <td class="m_b">&nbsp;</td>
    <td class="m_m">&nbsp;</td>
    <td class="m_m">&nbsp;</td>
    <td class="m_f">&nbsp;</td>
    <td class="m_jl">&nbsp;</td>
    <td class="m_jl">&nbsp;</td>
  </tr>
</table>

</form>

</body>
</html>
<script>
$(function(){
	(function(){
		var chkall = document.getElementById('chkall');
		var btn_sub = document.getElementById('submitform');
		var chk_sel = document.getElementsByTagName('select');
		var chk_ipt = document.getElementsByTagName('input');
		var new_nam = document.getElementById('new_page_name');
		var chk_len = chk_sel.length,ary_ipt = [],ary_chk = [],ary_btn,swc_num;
		for(var i=0;i<chk_ipt.length;i++){ 
			if(chk_ipt[i].getAttribute('type') == 'text') 
				ary_ipt.push(chk_ipt[i]);
			else if(chk_ipt[i].getAttribute('type') == 'checkbox')
				ary_chk.push(chk_ipt[i]);
		}
		ary_ipt.shift();
		ary_chk.shift();
		for(var i=0;i<chk_len;i++){
			(function(_i){
				ary_chk[_i].onclick = function(){
					if(this.checked){
						ary_ipt[_i].removeAttribute('disabled');
						chk_sel[_i].removeAttribute('disabled');
					}else{
						ary_ipt[_i].setAttribute('disabled','disabled');
						chk_sel[_i].setAttribute('disabled','disabled');
						
					}
				}
			})(i);
		}
		for(var i=0;i<chk_len;i++){
			(function(_i){
				chk_sel[_i].onchange = function(){
					if(this.value == 'quoted') ary_ipt[_i].style.display = 'none';
					else if(this.value == 'copy') ary_ipt[_i].style.display = 'block';
				}
			})(i);
		}
		chkall.checked = true;
		chkall.onclick = function(){
			if(this.checked){
				for(var i=0;i<chk_len;i++){
					ary_chk[i].checked = true;
					ary_ipt[i].removeAttribute('disabled');
					chk_sel[i].removeAttribute('disabled');
				}
			}else{
				for(var i=0;i<chk_len;i++){
					ary_chk[i].checked = false;
					ary_ipt[i].setAttribute('disabled','disabled');
					chk_sel[i].setAttribute('disabled','disabled');
				}
			}
			
		}
		btn_sub.onclick = function(){
			if(new_nam.value.trim()==''){
				alert('页面名称不能为空！');
			}else{
				ary_btn = [],swc_num = 0;
				for(var i=0;i<chk_len;i++){
					if(chk_sel[i].value == 'copy' && !chk_sel[i].getAttribute('disabled')) ary_btn.push(i);
				}
				for(var i=0;i<ary_btn.length;i++){
					if(ary_ipt[ary_btn[i]].value.trim()==''){
						swc_num = 1;
						alert('不能为空');
						return false;
					}
				}
				if(swc_num == 0) document.forms["form1"].submit();
			}
		};
		String.prototype.trim=function(){ return this.replace(/(^\s*)|(\s*$)/g, "");}
	})();
});
</script>