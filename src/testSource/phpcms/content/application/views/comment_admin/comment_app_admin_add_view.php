<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加app</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
.error{color:#F00;}
-->
</style>
</head>
<body>
<?php 
	$ci = &get_instance ();
	echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table id="p_g" width="100%" border="0" cellpadding="0" cellspacing="15px">
		<tr>
		<td width="120" valign="top">
		应用名称
		</td>
		<td valign="top">
<?php
echo form_input(array(  'name'=>'app_name',
						'id'=>'app_name',
						'value'=>$row['app_name'],
						'size'=>'40'));
echo form_error('app_name', '<span class="error" style="margin-left:10px;">','</span>' );
?>
		</td>
	</tr>
		<tr>
		<td width="120" valign="top">
		备注
		</td>
		<td valign="top">
<?php
echo form_textarea(array('name'=>'app_memo',
						 'id'=>'app_memo',
						 'value'=>$row['app_memo'],
						 'cols'=>27,
						 'rows'=>3));
?>
		</td>
	</tr>
	<br/>
		<tr>
		<td width="120" valign="top">
		应用模板
		</td>
		<td valign="top">
<?php
echo form_dropdown ( 'app_tpl', $app_tpl, $row['app_tpl'], "id='app_tpl'" );
echo form_error('app_tpl', '<span class="error" style="margin-left:10px;">','</span>' );
?>
		</td>
	</tr>
	<br/>
		<tr>
		<td width="120" valign="top">
		是否对游客开放
		</td>
		<td valign="top">
		<?php
		echo form_dropdown ( 'is_open', array('1'=>'否','0'=>'是'), $row['is_open'], 
		"id='is_open' size='2' " );
		echo form_error('is_open', '<span class="error" style="margin-left:10px;">','</span>' );
		?>

		</td>
	</tr>
	<br/>
		<tr>
		<td width="120" valign="top">
		是否需要审核
		</td>
		<td valign="top">
		<?php
		echo form_dropdown ( 'is_arbitrated', array('0'=>'否','1'=>'是'), $row['is_arbitrated'], 
		"id='is_arbitrated' size='2' " );
		echo form_error('is_arbitrated', '<span class="error" style="margin-left:10px;">','</span>' );
		?>
	</td>
	</tr>
	<br/>
	<tr>
	<td width="120" valign="top">
	是否允许匿名
	</td>
		<td valign="top">
		<?php
		echo form_dropdown ( 'is_anonymous', array('1'=>'否','0'=>'是'), $row['is_anonymous'], 
		"id='is_anonymous' size='2' " );
		echo form_error('is_anonymous', '<span class="error" style="margin-left:10px;">','</span>' );
		?>
	</td>
	</tr>
	<tr>
		<td width="80" id="fn">
		</td>
		<td>
		<?php
		echo form_submit ( 'submitform', '完成', "id='submitform'" );
		?>
		</td>
		<td>&nbsp;</td>
	</tr>
</table>

<?php 
echo form_close ();
?>
</body>
</html>
