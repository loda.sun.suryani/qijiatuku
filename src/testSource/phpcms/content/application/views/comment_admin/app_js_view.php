<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
textarea {
    border-bottom: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-top: 1px solid #dcdcdc;
    border-left: 1px solid #dcdcdc;
    font-size: 12px;
    width: 630px;
    line-height: 20px;
}
button {
    cursor: pointer;
    width: auto;
    color: #2953a6;
    font-size: 12px;
    height: 20px;
    line-height: 18px;
    text-align: center;
    border-bottom: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-top: 1px solid #dcdcdc;
    border-left: 1px solid #dcdcdc;
    background-color: #fff;
}

</style>
<title>app_js</title>
</head>
<body>
<?php
$js = "<script src=\"qj.chat.js\"></script> 
$(function(){
	var location_href = location.href.substring(location.href.lastIndexOf('//')+2,location.href.length);

	var block_id='cms_comment_';//必须页面上面唯一
	var	form_id='cms_comment_';//必须页面上面唯一
	var	app_id='cms_comment_{$app_id}';//cms后台生成的app_id
	var	user_id='1';//发表咨询用户
	var	user_name='1';//登录用户的名字
	var	login_url='';//未登录转向的页面地址
	var	link_url='';//最后提交入库的cms地址
	var	shop='false';//true就是商家，false不是
	var	shop_id='1';//服务商ID,需要判断是否为服务器,否则就设为0
	//内容分页列表
	$.qjChat({
		block_id:block_id,
		form_id:form_id,
		app_id:app_id,
		user_id:user_id,//发表咨询用户
		user_name:user_name,
		link_url:link_url,//192.168.96.51
		shop:shop,//true就是商家，false不是
		shop_id:shop_id,//服务商ID
		href_url:location_href
	});
	//---引用from表单
	$.qjChat_form({
		block_id:block_id,
		form_id:form_id,
		app_id:app_id,
		user_id:user_id,//发表咨询用户
		user_name:user_name,
		link_url:link_url,//192.168.96.51
		shop_name:shop_name,//服务商名称
		shop_id:shop_id,//服务商ID
		shop:shop,//true就是商家，false不是
		href_url:location_href,
		login_url:login_url,//登陆的地址转向
		btn_pop:0//如果是0的话，就是直接引用通用留言
	});	
	//当页面有按钮弹出的话，才会有次段代码，否则可以删除
	$.qjChat_form({
		block_id:block_id,
		form_id:'cms_comment_from',
		app_id:app_id,
		user_id:user_id,//发表咨询用户
		user_name:user_name,
		link_url:link_url,//192.168.96.51
		shop_name:shop_name,//服务商名称
		shop_id:shop_id,//服务商ID
		shop:shop,//true就是商家，false不是
		href_url:location_href,
		login_url:login_url,//登陆的地址转向
		btn_pop:'btn_cl'//是按钮状态，id=btn_cl
	});
});
</script>";
echo form_textarea(array('name'=>'jscode',
						 'id'=>'jscode',
						 'value'=>"$js",
						 'rows'=>'12'));
?>
<br/>
<br/>
<?php
echo form_button (array( 'name'=>'copycode',
						 'id'=>'copecode',
						 'content'=>'复制到粘贴板',
						 'value'=> 'true',
						 'onclick'=>"copyCode();return false;"));
?>
</body>
<script type="text/javascript">
function copyCode(obj){
var testCode=document.getElementById("jscode").value;
if(copy2Clipboard(testCode)!=false){
	alert("生成的代码已经复制到粘贴板，你可以使用Ctrl+V 贴到需要的地方去了哦！  ");
}
}
copy2Clipboard =  function(txt){
	if(window.clipboardData){
		window.clipboardData.clearData();
		window.clipboardData.setData("Text",txt);
	}else if(navigator.userAgent.indexOf("Opera")!=-1){
		window.location=txt;
	}else if(window.netscape){
		try{netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
	}catch(e){
		alert("您的firefox安全限制限制您进行剪贴板操作，请打开’about:config’将signed.applets.codebase_principal_support’设置为true’之后重试，相对路径为firefox根目录/greprefs/all.js");
		return false;
	}
var clip=Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
if(!clip)
return;
var trans=Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
if(!trans)
return;
trans.addDataFlavor('text/unicode');
var str=new Object();var len=new Object();
var str=Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
var copytext=txt;str.data=copytext;trans.setTransferData("text/unicode",str,copytext.length*2);
var clipid=Components.interfaces.nsIClipboard;
if(!clip)
return false;
clip.setData(trans,null,clipid.kGlobalClipboard);}}
</script>
</html>

