<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>销售商后台</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/com_comment/css/base.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/com_comment/css/reset.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/com_comment/css/consult.css" type="text/css" />
<style type="text/css">
<!--
/*分页 page*/
.page{ height:28px; margin:10px 0; overflow:hidden;float:right;}
.page a,
.page span{ display:block; float:left; margin:0 5px; line-height:22px;}
.page a{ border:1px solid #bbb; padding:0px 7px;}
.page a:hover{ text-decoration:none; color:#09F;border:1px solid #09F;}
.page a.cur_page{background-color:#09F; color:#fff; font-weight:bold;border:1px solid #09F;}
.page .txt{ width:38px; height:14px;border:1px solid #bbb; text-align:center; color:#999;}
.page span.no_style{ color:#ccc;border:1px solid #ccc; padding:0px 7px;}
-->
</style>
</head>
<body id="constr">


      <div class="main_inner">       
        <div class="tabs">
                   <ul>
				<?php
					echo form_open ( modify_build_url(), array (
															'name' => "theform", 
															"id" => "theform" ) );
					echo form_hidden ( "page", $this->input->post ( 'page' ) );
	
					if (count ( $select_reply_options )) {
						foreach ( $select_reply_options as $k => $v ) {	
							echo $v;
						}
					};?>		
		</ul>
        </div>			
		<div class="tab_border"></div>
        <div class="consult_search">
            <div class="consult_time"><em>咨询时间：</em>从<input type="text" name="start_time" id="startDate" class="txt datein"  onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="<?php echo $this->input->get_post('start_time');?>">
            											到<input type="text" name="end_time" id="" class="txt datein"  onClick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="<?php echo $this->input->get_post('end_time');?>"></div>
            <div class="cond_filter"><em>关键词：</em>
            	<?php 
					echo form_dropdown( 'key_type', array('0'=>'请选择','1'=>'咨询店铺名称','2'=>'咨询内容'), $this->input->get_post('key_type'));           	
            	?>

              <input type="text" name="key_value" id="" size="15" value="<?php echo $this->input->get_post('key_value');?>">&nbsp;<button class="submit" type="button" onClick="javascript:on_submit_click();return false;">搜索</button>
            </div>
         </div>				                
		<div class="tab_center">
            <div class="consult_list">
          	<table>
            	<colgroup>
                	<col width="100">
                	<col width="550">
                	<col width="150">
                </colgroup>
            	<thead>
                	<tr>
                		<th>咨询店铺名称</th>
                        <th>咨询内容</th>
                        <th>咨询时间/回复时间</th>
                	</tr>
                </thead>
                <tbody>
     			<?php 
     				foreach ($grid as $k=>$v){
                    	echo "<tr>";
                    	if($v['shop_name']){
                    		echo "<td>{$v['shop_name']}</td>";
                    	}else{               
                    		echo "<td>测试商户</td>";
                    	} 
     					echo "<td><div class=\"detail\">{$v['comment_content']}</div></td>";
     					echo "<td>{$v['create_time']}</td>";
     					echo "</tr>";     					
						if($v['comment_reply']&& $v['is_arbitrated']<2){
							echo "<tr class=\"reply_yet\">";
     						echo "<td colspan=\"3\"><p class=\"reply_content\"><em class=\"shop_reply\">回复内容：</em><span>{$v['comment_reply']}</span></p><span class=\"reply_date\">{$v['comment_reply_time']}</span></td>";
     						echo "</tr>";
     					}
     					if($v['comment_reply']&& $v['is_arbitrated']== 2){
							echo "<tr class=\"reply_yet\">";
     						echo "<td colspan=\"3\"><p class=\"reply_content\"><em class=\"shop_reply\">服务商回复：</em><span>已删除</span></p></td>";
     						echo "</tr>";
     					}     					
     				}
     				?>                                                        	
                 </tbody>
            </table>
          </div>	
		</div>			
</div>
<div class = "page">
<?php echo $getpageinfo;
	  echo form_close()
?>
</div>
<iframe id="iframeA" name="iframeA" src="" width="0" height="0" style="display:none;" ></iframe> 
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"  src="<?php  echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
//alert($("#constr").outerHeight(true));
$(function(){
	//alert($("#constr").outerHeight(true));
	$("#constr").attr('rel',$("#constr").outerHeight(true)+250);
});
function sethash(){
	hashH = $("#constr").attr('rel');//获取自身高度
	urlC = "http://fuwu.jia.com/A.html"; //设置iframeA的
	document.getElementById("iframeA").src=urlC+"#"+hashH; //将高度作为参数传
}
window.onload=sethash;
</script>
<script type="text/javascript">
function on_submit_click(){
		document.getElementById('page').value = '';
		$("#theform").submit();
}
</script>	

</body>
</html>