<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>服务商后台</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/com_comment/css/base.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/com_comment/css/reset.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/com_comment/css/consult.css" type="text/css" />
<style type="text/css">
<!--
/*分页 page*/
.page{ height:28px; margin:10px 0; overflow:hidden;float:right;}
.page a,
.page span{ display:block; float:left; margin:0 5px; line-height:22px;}
.page a{ border:1px solid #bbb; padding:0px 7px;}
.page a:hover{ text-decoration:none; color:#09F;border:1px solid #09F;}
.page a.cur_page{background-color:#09F; color:#fff; font-weight:bold;border:1px solid #09F;}
.page .txt{ width:38px; height:14px;border:1px solid #bbb; text-align:center; color:#999;}
.page span.no_style{ color:#ccc;border:1px solid #ccc; padding:0px 7px;}
-->
#popService08{width:240px;height:140px;background:#F1F1F1;border:1px solid #bbb; z-index:99999999;text-align:center;display:none;_display:none;}
#popService08 .consult_barT{  padding-top:40px; font-size:14px; font-weight:700;padding-bottom:20px;}
#shaService08{z-index:99;background:#000;opacity:0.3;filter:alpha(opacity=30);-moz-opacity:0.3; left:0; top:0;display:none;_display:none;}
.add_comment_cancel {background: none; border: 0;color: #666;cursor: pointer;height: 24px;line-height: 24px;text-align: center; width: 40px;}
.cms_comment_submit {background:#eaeaea;cursor: pointer;height: 24px;line-height: 24px;text-align: center;width: 70px;border:1px solid #ddd;}
.cms_comment_submit:hover,.add_comment_cancel:hover{ text-decoration:underline; color:#a40000;}
</style>
</head>
<body id="constr">
      <div class="main_inner">       
        <div class="tabs">
        	<ul>
				<?php
					echo form_open ( modify_build_url(), array (
															'name' => "theform", 
															"id" => "theform" ) );
					echo form_hidden ( "page", $this->input->post ( 'page' ) );
	
					if (count ( $select_reply_options )) {
						foreach ( $select_reply_options as $k => $v ) {	
							echo $v;
						}
					};?>		
		</ul>
        </div>			
		<div class="tab_border"></div>
        <div class="consult_search">
            <div class="consult_time"><em>咨询时间：</em>从<input type="text" name="start_time" id="startDate" class="txt datein"  onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="<?php echo $this->input->get_post('start_time');?>">
            											到<input type="text" name="end_time" id="" class="txt datein"  onClick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="<?php echo $this->input->get_post('end_time');?>"></div>
            <div class="cond_filter"><em>关键词：</em>
            	<?php 
					echo form_dropdown( 'key_type', array('0'=>'请选择','1'=>'销售商名称','2'=>'咨询内容'), $this->input->get_post('key_type'));           	
            	?>

              <input type="text" name="key_value" id="" size="15" value="<?php echo $this->input->get_post('key_value');?>"><button class="submit" type="button" onClick="javascript:on_submit_click();return false;">搜索</button>
            </div>
         </div>			
		<!--全部咨询-->
		<div class="tab_center">
            <div class="consult_list">
          	<table>
            	<colgroup>
                	<col width="100">
                	<col width="480">
                	<col width="150">
                	<col width="70">
                </colgroup>
            	<thead>
                	<tr>
                		<th>销售商名称</th>
                        <th>咨询内容</th>
                        <th>咨询时间/回复时间</th>
                        <th>操作</th>
                	</tr>
                </thead>
                <tbody>
                	<?php 	
                		foreach ($grid as $k=>$v){
                			echo "<tr>";
                			if($v['user_name'] && $v['is_anonymous'] == 0){
                				echo "<td>{$v['user_name']}<input type=\"hidden\" value=\"{$v['comment_id']}\" name=\"id\"></td>";
                			}else{
                				echo "<td>匿名<input type=\"hidden\" value=\"{$v['comment_id']}\" name=\"id\"></td>";
                			}
                			echo "<td><div class=\"detail\" style='overflow:hidden;'>{$v['comment_content']}</div></td>";
                			echo "<td>{$v['create_time']}</td>";
                			if($v['reply_update'] == 0){
                				echo "<td class=\"info_show\"><div class=\"actions\"><p>
                        				<a class=\"add\" href=\"javascript:\">回复</a>
                        				</p></div></td>";
                				echo "</tr>";
                				echo "<tr style=\"display:none\" class=\"reply_edit\">";
                				echo "<td colspan=\"4\">";
                				echo "<div class=\"reply_ctr\">";
                				echo "<input type=\"hidden\" value=\"{$v['comment_id']}\" name=\"id\">";
                				echo "<textarea class=\"reply_area\" id=\"content\" name=\"content\"></textarea>
	                                	<p class=\"count\">您还可以输入<em>200</em>字</p>
	                                	<p class=\"reply_btm\"><a href='javascript:void(0);' class=\"replyForm\">提交</a>  <a class=\"canceled\" href=\"javascript:\">取消</a></p>	                        
                            		 </div>
                        			</td>
                    				</tr>";         
                			}
                			if($v['reply_update'] == 1 && $v['is_arbitrated']!= 2){
                				echo "<td rowspan=\"1\" class=\"info_show reply_tg\"><div class=\"actions\"><p>
                        				<a class=\"modify\" href=\"javascript:\">修改回复</a>
                        				</p><a class=\"del\" href=\"javascript:\" rel=\"{$v['comment_id']}\">删除</a></div></td>";
                				echo "</tr>";
                				echo "<tr style=\"display:none\" class=\"reply_edit_m\">
                    					<td colspan=\"4\" class=\"reply_tg\">
                        					<div class=\"reply_ctr\">
                        	        			<input type=\"hidden\" value=\"{$v['comment_id']}\" name=\"id\">
	                            				<textarea class=\"reply_area\" id=\"\" name=\"content\">{$v['comment_reply']}</textarea>
	                               				<p class=\"count\">您还可以输入<em>200</em>字</p>
	                               				 <span class=\"warn\">您的回复内容只能修改一次！</span>
	                                			<p class=\"reply_btm\"><a href='javascript:void(0);' class=\"reply_modify\">提交</a>  <a class=\"canceled_m\" href=\"javascript:\">取消</a></p>
	                        
                            				</div>
                        				</td>
                   					 </tr>";
                			}
                			
                		    if($v['reply_update'] == 2 && $v['is_arbitrated']!= 2){
                				echo "<td rowspan=\"1\" class=\"info_show reply_tg\"><div class=\"actions\">
                							<a class=\"del\" href=\"javascript:\" rel=\"{$v['comment_id']}\">删除</a></div></td>";
                				echo "</tr>";
                			}
                			
                			if($v['reply_update']==1 && $v['is_arbitrated']!= 2){
                				echo "<tr class=\"reply_yet\">
                    					<td colspan=\"4\"><p class=\"reply_content\"><en class=\"shop_reply\">回复内容：</en><span class='span_reply'>{$v['comment_reply']}</span></p><span class=\"reply_date\">{$v['comment_reply_time']}</span></td>
                     				</tr>";               				
                			}
                		    if($v['reply_update']==2 && $v['is_arbitrated']!= 2){
                				echo "<tr class=\"reply_yet\">
                    					<td colspan=\"4\"><p class=\"reply_content\"><en class=\"shop_reply\">回复内容：</en><span class='span_reply'>{$v['comment_reply']}</span></p><span class=\"reply_date\">更新于:{$v['comment_reply_time']}</span></td>
                     				</tr>";               				
                			}
                		    if( $v['reply_update']>= 1 && $v['is_arbitrated'] == 2 ){
                				echo "<tr class=\"reply_yet\">
                    					<td colspan=\"4\"><p class=\"reply_content\"><en class=\"shop_reply\">回复内容：</en><span>已删除<span></td>
                     				</tr>";               				
                			}                			    
                		}
            
 ?>                                                                     	
                                                                                        </tbody>
            </table>
          </div>				
		</div>
</div>
<div class = "page">
<?php echo $getpageinfo;
      echo form_close();?>	
</div>
<div id="popService08">
    <div class="consult_barT">确认删除吗?</div>
    <input type="button" value="确定" class="cms_comment_submit confir_btn" />
    <input type="button" value="取消" class="add_comment_cancel cloose_btn" />
</div>
<div id="shaService08"></div>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"  src="<?php  echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
//alert($("#constr").outerHeight(true));
$(function(){
	//alert($("#constr").outerHeight(true));
	$("#constr").attr('rel',$("#constr").outerHeight(true)+250);
});
function sethash(){
	hashH = $("#constr").attr('rel');//获取自身高度
	urlC = "http://fuwu.jia.com/A.html"; //设置iframeA的
	document.getElementById("iframeA").src=urlC+"#"+hashH; //将高度作为参数传
}
window.onload=sethash;
</script>
<script type="text/javascript">
function on_submit_click(){
		document.getElementById('page').value = '';
		$("#theform").submit();
}
</script>
<script type="text/javascript">
$(document).ready(function(){
        $(".tab_center").hide();
		$(".tab_center").eq(0).show();
		$(".tabs li").click(function(){		        
		        var y = $(".tabs li").index(this);
				$(this).children("a").addClass("cur");
				$(this).siblings("li").children("a").removeClass("cur");
				$(".tab_center").eq(y).show();
				$(".tab_center").eq(y).siblings(".tab_center").hide();
		});
});	
</script>	
<script type="text/javascript">
	$(document).ready(function(){
	   var reg = /^<( *)(option|head|input|iframe|link|meta|script|a|textarea)/;   
	   var IETEST6 = navigator.appVersion.indexOf("MSIE 6")>-1;	

		$(".add").live("click",function(){	
			$(".reply_edit").hide();			
			$(this).parents("tr").next(".reply_edit").show();
			$(".reply_edit_m").hide();
			$(".reply_area").focus();
	    })
	    $(".modify").live("click",function(){				
			$(".reply_edit_m").hide();
			$(this).parents("tr").next(".reply_edit_m").show();
			$(this).parents("tr").next(".reply_yet").hide();
			$(this).parents("tr").find("td .actions").hide();
			$(".reply_edit").hide();
			$(".reply_area").focus();
	    })
	    $(".canceled").live("click",function(){		
	        $(".reply_area").val("");		
			$(".reply_edit").hide();
	    })
	      $(".canceled_m").live("click",function(){		        			
			$(".reply_edit_m").hide();
			$(".actions").show();
	    })
	    $(".reply_ctr .replyForm").click(function(){
			var _parent_b = $(this).parents('tr.reply_edit');
			var _parent =_parent_b.next('tr.reply_yet').find('span.span_reply');
			var content_b = $(this).parents("tr").find("td textarea[name='content']");
	    	var content = content_b.val();
	    	var id=$(this).parents("tr").find("td input[name='id']").val();  //id
	    	content = $.trim(content);
	    	if(!content || reg.test(content)){
	    		 alert("内容不能为空");
                 return false;
	    	}
	    	if(content.length<5){
			
	    		 alert("您输入的字数不能少于4个字");
                 return false;
	    	}
	    	if(content.length>200){
	    		content = content.substr(0,200);
				leng_th=(content.length)-200;
				 //alert("您输入的字符应控制在200字符以内!");
                 return false;
	    	}
	    	if (id != '' && content != '')
			  {
			  	$(".reply_ctr p #replyForm").attr("disabled","true");
				
			    $.ajax
		        ({
		            type: "POST",
		            url: "<?php echo site_url('c=message&m=fuwu_reply');?>",
		            data: "id="+id+"&content="+escape(content),//
		            dataType: "text",
		            cache: false,
		            success: function(data)
		            {	   
		            	if(data == 1){
							_parent.html(content);
							_parent_b.hide();
							content_b.val('');
							window.location.reload();
						} else{
							alert('插入失败');
						}  
		            }
		        });
				//window.location.reload();
			  }			
	    })
	    
	     $(".reply_ctr .reply_modify").click(function(){
			var _parent_b = $(this).parents('tr.reply_edit_m');
			var _parent =_parent_b.next('tr.reply_yet').find('span.span_reply');
			var content_b = $(this).parents("tr").find("td textarea[name='content']");
	    	var content = content_b.val();
	    	var id=$(this).parents("tr").find("td input[name='id']").val();  //id
	    	content = $.trim(content);
	    	if(!content  || reg.test(content)){
	    		 alert("内容不能为空");
                 return false;
	    	}
	    	if(content.length<5){
	    		 alert("您输入的字数不能少于4个字");
                 return false;
	    	}
	    	if(content.length>200){
				leng_th=(content.length)-200;
	    		content = content.substr(0,200);
				 //alert("您输入的字符应控制在200字符以内!");
                 return false;
	    	}
	    	//$(".reply_area").attr("readonly",false);return;
	    	if (id != '' && content != '' && content.length<=200)
			  { 
			 // alert(id)
			   $.ajax
		        ({
		            type: "POST",
		            url: "<?php echo site_url('c=message&m=fuwu_reply');?>",
		            data: "id="+id+"&content="+escape(content),//
		            dataType: "text",
		            cache: false,
		            success: function(data)
		            {	   
					 	if(data == 1){
							_parent.html(content);
							_parent_b.hide();
							content_b.val('');
						} else{
							alert('插入失败');
						}    
		            }
		        });
				//window.location.reload();
			  }			
	    })
	    if(IETEST6){
			 $(".del").live("click",function(){				
				 var id=$(this).parents("tr").find("td input[name='id']").val();  //id
				 var  r = confirm('确定删除该咨询信息吗？');
				 if ( id!="" && r)
				  {
					$.ajax
					({
						type: "POST",
						url: "<?php echo site_url('c=message&m=fuwu_del');?>",
						data: "id="+id,
						dataType: "text",
						cache: false,
						success: function(text)
						{
							window.location.reload();
						}
					});
				  }			
			})
		}
	     $(".submit").click(function(){
	     	var query = $("#query").val();
	     	var field = $('#field').val(); 
	     	if(field == '' && query){
	     		alert("请选择查找类型");
	     		return false;
	     	}
	     })

    $(".reply_edit_m,.reply_yet").prev().find("td").addClass("reply_tg");	    
		
	var reply_area_content = $(".reply_area");
	if(reply_area_content.length>0)
	{
		var count=function(str){
			var sum=0;
			if (typeof(str) == "undefined" || str == '') {
				return sum;
			} else{  
			    len=str.length;
			}
			if(str){
				for( var i=0;i<len;i++){
				/*	if(str.charCodeAt(i)>255){
						sum=sum+2;
					}else{
						sum++;
					}
					*/
					sum++;
				}
				return sum;
			}else{
				return 0;
			}
		};
	}
		var oin=$(".reply_ctr .reply_area"),
		    oc=$(".count"), 
		    os=$(".reply_ctr button");
			
		if((200-count(oin.val()))>0){
			oc.html("您还可以输入<em>"+parseInt((200-count(oin.val())))+"<\/em>字");
			os.attr("disabled",false);
		}else{
			oc.html("<span style='color:red'>已超出<em class='overbrim'>"+parseInt((count(oin.val())-200))+"<\/em>字<\/span>");
			os.attr("disabled",true);
		};
		
		oin.bind("keydown keyup focus",function(){					
			var $this = $(this);
			var index=oin.index($this);

			var cha=(200-count($this.val()));
			if(cha>=0){
				os.eq(index).disabled=false;
				oc.eq(index).html("您还可以输入<em>"+parseInt((200-count(oin.eq(index).val())))+"<\/em>字");
				os.attr("disabled",false);
			}else{
				oc.eq(index).html("<span style='color:red'>已超出<em class='overbrim'>"+parseInt((count(oin.eq(index).val())-200))+"<\/em>字<\/span>");
				os.eq(index).attr("disabled",true);
			}
            var t = this;
            var h = parseInt(t.style.height, 10);
            var minHeight=20;
            var maxHeight=180;
            var sh= t.scrollHeight;
            var nav=navigator.userAgent;

            if (t.scrollTop == 0) {
                t.scrollTop = 1;
            }
            if (t.scrollTop > 0) {
                if (sh < maxHeight) {
                    t.style.height = sh + "px";
                } else {
                    t.style.overflowY = "scroll";
                }
            } else {
                t.style.overflowY = "hidden";
                if (nav.indexOf("IE") > 0) {
                    t.style.height = t.scrollHeight+ "px";
                } else {

                    if (h > minHeight) {
                        t.style.height = h - minHeight + "px";
                        t.scrollTop = 1;
                        if (t.scrollTop > 0) {
                            t.style.height = h + "px";
                        }
                    }
                }

            }
		});
		
	});
	
	
	
	
$(function(){
	  var IETEST6 = navigator.appVersion.indexOf("MSIE 6")>-1;		   
	if(!IETEST6){	   
		for(var i=0;i<$("a.del").length;i++){
			
			(function(_i){
				$("a.del:eq("+_i+")").live('click',function(){
														
								var _this = $(this).attr('rel');
								popShow(_this);	
									$('#popService08').show();
									$('#shaService08').show();
									
											
												
												});
			
			})(i);
			
		}
	}
	function popShow(t){
		
		var p = 'popService08',s = 'shaService08';
		  var IETEST6 = navigator.appVersion.indexOf("MSIE 6")>-1;	
		var tW = $('#'+p).width()/2,tH = $('#'+p).height()/2;
		getFunctionData();																		
		if(!IETEST6){
			$id(p).style.position = 'fixed';$id(s).style.position = 'fixed';
		}else{
			$id(p).style.position = 'absolute';$id(s).style.position = 'absolute';
			window.onscroll = function(){ 
				$id(p).style.top = $(window).height()/2 + $(window).scrollTop() - tH + 'px'; 								
			}
		}
		window.onresize = function(){getFunctionData();}
	
		function getFunctionData(){
			var wW = $(window).width();
			var wH = IETEST6 ? document.body.scrollHeight : $(window).height();
			var pW = wW/2 - tW;
			
			var pH = IETEST6 ? $(window).height()/2 + $(window).scrollTop() - tH : wH/2 - tH;
			$id(s).style.width = wW + 'px';
			$id(s).style.height = wH + 'px';
			//alert(pW+"|"+pH+"|"+$(window).scrollTop());
			$id(p).style.left = pW + 'px';
			$id(p).style.top = pH + 'px';
		}
		
		$($id(p)).find('input.cloose_btn')[0].onclick = function(){$id(s).style.display = 'none';$id(p).style.display = 'none';}
		$($id(p)).find('input.confir_btn')[0].onclick = function(){
			  $.ajax
		        ({
		            type: "POST",
		            url: "<?php echo site_url('c=message&m=fuwu_del');?>",
		            data: "id="+t,
		            dataType: "text",
		            cache: false,
		            success: function(text)
		            {
		              	//window.location.reload();
						$("#theform").submit();
		            }
		        });
		}
		function $id(id){ return document.getElementById(id); }
	}
});	
	
</script>
<iframe id="iframeA" name="iframeA" src="" width="0" height="0" style="display:none;" ></iframe> 



</body>
</html>
