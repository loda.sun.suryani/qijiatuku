<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>页面模板名称</title>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url();?>public/css/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
</head>
<body>
<form name="form1" method="post" action="<?php echo modify_build_url();?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td width="17%" class="m_1">名称</td>
    <td width="32%" class="m_2"><label>
      <input name="page_tpl_name" type="text" id="page_tpl_name" value="<?php echo $campage_array['page_tpl_name'];?>" size="30">
    <?php echo form_error('page_tpl_name', '<div class="error">', '</div>'); ?></label></td>
    <td width="51%" class="m_3"></td>
  </tr>
   <tr>
    <td class="m_4">页面区域</td>
    <td class="m_5"><label>
      <input type="text" name="area_count" id="area_count" size="4" value="<?php if($campage_array['area_count']>0){echo $campage_array['area_count'];}?>"/><?php echo form_error('area_count', '<div class="error">', '</div>'); ?>
    </label></td>
    <td class="m_6">&nbsp;</td>
   </tr>
   <tr>
     <td class ="m_19">设计人</td>
     <td class ="m_20"><input id="tags" value="<?php echo $campage_array['author_name']; ?>" /><input type="text" style="display:none" id="author_id" name="author_id" value="<?php echo $campage_array['author_id']; ?>" /><?php echo form_error('author_id', '<div class="error">', '</div>'); ?></td>
     <td class ="m_21">&nbsp;</td>
   </tr>
  <tr>
    <td class="m_19">页面样式<?php echo form_error('tpl_content', '<div class="error">', '</div>'); ?></td>
    <td class="m_20">
      <textarea name="tpl_content"  rows="15" id="tpl_content" style="font-size:12px;" ><?php echo $campage_array['tpl_content']?></textarea>
	<?php //echo $this->ckeditor->replace("tpl_content")?>  
    </td>
    <td class="m_21"><font style="padding:15px;color:#00F">控制区域说明</font><br /><span style="padding:15px;">
    模板{{ &nbsp;&nbsp;}}标记,</span><br />
    <span style="padding:15px;">{{$area_1}},area代表区域,<br /></span>
     <span style="padding:15px;">链接符'_',<br /></span>
     <span style="padding:15px;">数字'1'代表页面区域的第一个位置,<br /></span>
     <span style="padding:15px;">数字'2'代表页面区域的第二个位置,<br /></span>
     <span style="padding:15px;">.....<br /></span>
     </td>
  </tr>
</table>
</form>


<script>
$(function() {
	$.complete({
		searchId: '#tags',
		valueId: '#author_id',
		url:'<?php echo modify_build_url(array ('c' => 'cmspage','m' => 'tags_search'));?>'
	});
});
</script>
</body>
</html>