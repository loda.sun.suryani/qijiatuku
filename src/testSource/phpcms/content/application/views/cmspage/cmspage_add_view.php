<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>页面模板名称</title>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
 <script language="javascript">   
 function CheckForm(ObjForm) {
	 
  if(ObjForm.page_tpl_name.value == '') {
    alert('页面模板名称不能为空！');
    ObjForm.page_tpl_name.focus();
    return false;
  }
  
   if(ObjForm.area_count.value == '') {
    alert('页面区域不能为空！');
    ObjForm.area_count.focus();
    return false;
  }

}
 </script>

<link href="<?php echo base_url();?>public/css/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
<script src="<?php echo base_url();?>public/js/jquery-ui-1.8.11.custom.min.js"></script>
</head>
<body>
<?php  echo crumbs_nav('/页面模板管理/添加页面模板');?>
<hr />
<form name="form1" method="post" action="index.php?c=cmspage&m=cmspage_add&page_tpl_id=<?php echo $page_tpl_id?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td width="14%" class="m_1">名称</td>
    <td width="67%" class="m_2"><label>
      <input name="page_tpl_name" type="text" id="page_tpl_name" value="<?php echo set_value('page_tpl_name')?>" size="30">
    <?php echo form_error('page_tpl_name', '<div class="error">', '</div>'); ?></label></td>
    <td width="19%" class="m_3" style="color:#F00;">*</td>
  </tr>
   <tr>
    <td class="m_4">页面区域</td>
    <td class="m_5"><label>
      <input type="text" name="area_count" id="area_count" size="4" value="<?php echo set_value('area_count')?>"/><?php echo form_error('area_count', '<div class="error">', '</div>'); ?>
    </label></td>
    <td class="m_6" style="color:#F00;">*</td>
   </tr>
  <tr>
    <td class="m_7">上传页面示例图片</td>
    <td class="m_8"><?php echo $demo_pic_id?></a>

    </td>
    <td class="m_9"></td>
  </tr>
  <tr>
    <td class="m_10">小图预览放大</td>
    <td class="m_11" id="message_demo_pic_id" ></td>
    <td class="m_12" >&nbsp;</td>
  </tr>
  <tr>
    <td class="m_13">版式</td>
    <td class="m_14"><?php echo $layout_pic_id?></td>
    <td class="m_15" style="color:#F00;"></td>
  </tr>
  <tr>
    <td class="m_16">版式示例</td>
    <td class="m_17" id="message_layout_pic_id"></td>
    <td class="m_18" style="color:#F00;">&nbsp;</td>
  </tr>
   <tr>
    <td class="m_22">设计人</td>
    <td class="m_23"><input id="tags" /><input type="text" style="display:none" id="author_id" name="author_id" /><?php echo form_error('author_id', '<div class="error">', '</div>'); ?></td>
    <td class="m_24" style="color:#F00;">*</td>
  </tr>
  <tr>
    <td class="m_19">页面样式<?php echo form_error('tpl_content', '<div class="error">', '</div>'); ?></td>
    <td class="m_20">
      <textarea name="tpl_content" cols="100" rows="15" id="tpl_content" style="font-size:12px;" ><?php echo set_value('tpl_content')?></textarea>
	<?php //echo $this->ckeditor->replace("tpl_content")?>  
    </td>
    <td class="m_21"><font style="padding:15px;color:#00F">控制区域说明</font><br /><span style="padding:15px;">
    模板{{ &nbsp;&nbsp;}}标记,</span><br />
    <span style="padding:15px;">{{$area_1}},area代表区域,<br /></span>
     <span style="padding:15px;">链接符'_',<br /></span>
     <span style="padding:15px;">数字'1'代表页面区域的第一个位置,<br /></span>
     <span style="padding:15px;">数字'2'代表页面区域的第二个位置,<br /></span>
     <span style="padding:15px;">.....<br /></span>
     </td>
  </tr>
 
  <tr>
    <td class="m_22">&nbsp;</td>
    <td class="m_23"><label>
      <input type="submit" name="submit" id="submit" value="确定">
      <input type="button" onclick="location.href='index.php?c=cmspage&m=index';return false" value="取消">
    </label></td>
    <td class="m_24">&nbsp;</td>
  </tr>
</table>
</form>



<script>
$(function() {
	$.complete({
		searchId: '#tags',
		valueId: '#author_id',
		url:'<?php echo modify_build_url(array ('c' => 'cmspage','m' => 'tags_search'));?>'
	});
});
</script>
</body>
</html>
