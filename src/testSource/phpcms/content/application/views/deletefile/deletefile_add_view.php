<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>删除(静态)文件</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
	
}

-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/page.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="form1" method="post" action="<?php echo modify_build_url()?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))">
<table width="100%" border="0" cellspacing="1" cellpadding="0" style="background:#f1f1f1;">
  <tr>
    <td bgcolor="#FFFFFF"><font color="red">*</font>站点</td>
    <td bgcolor="#FFFFFF"><?php  echo form_dropdown ( 'web', $web,set_value('web'), "id='web'" );// onchange='on_app_id_change()'?></td>
    <td bgcolor="#FFFFFF"><?php echo form_error('web', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td width="11%" bgcolor="#FFFFFF"><font color="red">*</font>URL</td>
    <td width="63%" bgcolor="#FFFFFF"><input name="weburl" type="text" id="weburl" size="30" value="<?php echo set_value('weburl') ?>" />
      必须以斜线<font color="red">(/)</font>开头;以<font color="blue">(.html)</font>结尾 </td>
    <td width="26%" bgcolor="#FFFFFF"><?php echo form_error('weburl', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">&nbsp;</td>
    <td bgcolor="#FFFFFF"><label>
      <input type="submit" name="submit" id="submit" value="保存"/></label></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
</form>
<script> 
function on_app_id_change(){
	document.getElementById('page_num').value = '';
	$("#theform").submit();
}
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function enable_content(v){
	$.ajax({url:"<?php
	echo site_url ( 'c=message&m=enable_content&id=' );
	?>"+v,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
		}
});	
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}

</script>
</body>
</html>