<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<title>写入文件</title>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>

<table class="p_g" width="100%" border="0">
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">IP</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'client_ip', 
		'id' => "client_ip", 
		'size' => 72,
		'value' => $record['client_ip'] ) );
echo form_error ( 'client_ip', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td valign = "top">绝对路径</td>
		<td>
<?php 
echo form_input ( array (
		'name' => 'path', 
		'id' => 'path',
		'size' => 72, 
		'value' => $record['path'] ) );
echo "必须以斜线<font color=\"red\">(/)</font>开头";

?>
</td>
	</tr>
	<tr>
		<td valign = "top">内容</td>
		<td>
<?php 
echo form_textarea ( array (
		'name' => 'content', 
		'id' => 'content', 
		"value" => $record['content'] ) );
echo form_error ( 'content', '<span class="error">', '</span>' );

?></td>
	</tr>



	
	<tr><td> </td><td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td></tr>
</table>
<?php 
echo form_close();
?>
</body>
</html>