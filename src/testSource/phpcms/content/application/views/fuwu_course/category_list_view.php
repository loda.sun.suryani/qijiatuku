<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>

<title>categroy_list</title>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php 
	echo crumbs_nav('/数据管理/服务培训');
?>
<hr>
<?php
echo form_open ( modify_build_url(), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
课程类目
<?php 
echo form_input ( array (
		'name' => 'category_name', 
		'id' => "category_name",
		'size' => 20,
		'autocomplete'=>'off',
		'value'=>$ci->field ( 'category_name' ) ) );
echo nbs(5);
echo form_submit ( 'search', '查询', "id='search'" );
echo nbs(5);
?>
<input type="button" onclick="edit_category();return false;" value="新建类目" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" onclick="location.href='<?php echo modify_build_url(array('m'=>'course_list','id'=>'0'));?>';return false" value="全部课程" />

<br/>
<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>
<br/>
<br/>
<br/>
<?php
echo form_close ();
?>


<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}

function edit_category(v){
	//alert(v);
	show_v('编辑','<?php echo site_url("c=fuwu_course&m=edit_category");?>&id='+v,'0','0' );
}
function del_category(v){
	show_v('','<?php echo site_url("c=fuwu_course&m=del_category");?>&id='+v,'0','0' );
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}
</script>
</body>
</html>