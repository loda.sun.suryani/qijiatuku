<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"  src="<?php echo base_url();?>public/js/jquery-ui-1.8.11.custom.min.js"></script>
<title>course_list</title>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php 
	echo crumbs_nav('/数据管理/服务培训/课件列表');
?>
<hr>
<?php
echo form_open ( modify_build_url(), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
课件标题
<?php 
echo form_input ( array (
		'name' => 'course_title', 
		'id' => "course_title",
		'size' => 20,
		'autocomplete'=>'off',
		'value'=>$ci->field ( 'course_title' ) ) );
echo nbs(5);
echo form_submit ( 'search', '查询', "id='search'" );
echo nbs(5);
?>
<?php
	if($category_id !==0){
		echo "<input type=\"button\" onclick=\"upload_course( $category_id );return false;\" value=\"上传课件\" />";
		echo nbs(5);
	} 


?>
<?php
echo form_close ();
?>
<br/>
<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>
<br/>
<br/>
<br/>

<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function upload_course(v){
	//alert(v);
	show_v('上传课件','<?php echo site_url("c=fuwu_course&m=upload_course");?>&id='+v,'0','0' );
}
function edit_course(v){
	//alert(v);
	show_v('上传课件','<?php echo site_url("c=fuwu_course&m=upload_course");?>&course_id='+v,'0','0' );
}
function course_move_up(v,k){
	$.ajax({url:"<?php 
		echo site_url ( 'c=fuwu_course&m=course_move_up');
		?>&id="+v+"&cid="+k,
				cache: false,
				success: function(html){
					$("#theform").submit();//提交
				}
		});		
}
function course_move_down(v,k){
	$.ajax({url:"<?php
		echo site_url ( 'c=fuwu_course&m=course_move_down' );
		?>&id="+v+"&cid="+k,
				cache: false,
				success: function(html){
					$("#theform").submit();//提交
				}
		});
	
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}
</script>
</body>
</html>