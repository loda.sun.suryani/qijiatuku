<?php
if (! function_exists ( 'echo_nl' )) {
	function echo_nl() {
		echo "\n";
	}
}
if (! function_exists ( 'get_ip' )) {
	function get_ip() {
		return ip2long ( $_SERVER ["REMOTE_ADDR"] );
	}
}
if (! function_exists ( 'read_ip' )) {
	function read_ip($ip) {
		return long2ip ( $ip );
	}
}
function get_ip_char() {
	return $_SERVER ["REMOTE_ADDR"];
}
//循环目录下的所有文件
function del_fileunder_dir($dirName) {
	if ($handle = opendir ( "$dirName" )) {
		while ( false !== ($item = readdir ( $handle )) ) {
			if ($item != "." && $item != "..") {
				if (is_dir ( "$dirName/$item" )) {
					del_fileunder_dir ( "$dirName/$item" );
				} else {
					unlink ( "$dirName/$item" );
					//echo "成功删除文件： $dirName/$item<br />\n";
				}
			}
		}
		closedir ( $handle );
		rmdir ( $dirName );
	}
}
//删除多级目录及其文件
if (! array_key_exists ( 'deldir', $GLOBALS )) {
	function deldir($dir, $type = true) {
		$n = 0;
		if (is_dir ( $dir )) {
			if ($dh = opendir ( $dir )) {
				while ( ($file = readdir ( $dh )) !== false ) {
					//.svn 忽略 svn 版本控制信息 
					if ($file == '.' or $file == '..' or $file == '.svn') {
						continue;
					}
					if (is_file ( $dir . $file )) {
						unlink ( $dir . $file );
						$n ++;
					}
					if (is_dir ( $dir . $file )) {
						deldir ( $dir . $file . '/' );
						if ($type) {
							$n ++;
							rmdir ( $dir . $file . '/' );
						}
					}
				}
			}
			closedir ( $dh );
		}
		return $n;
	}
}
/*
* 权限接口使用
*/
if (! array_key_exists ( 'validation_check', $GLOBALS )) {
	function validation_check($uid, $func) {
		if (! $uid) {
			echo "无权限：登录超时/$uid/";
			exit ();
		}
		$ci = & get_instance ();
		$ci->load->library ( 'CommonCache', '', 'cache' );
		$cache_time = 60;
		$Cache_get = $ci->cache->get ( "perm_" . $uid . "_" . $func );
		
		if ($Cache_get) {
			return $Cache_get;
		}
		$ci->load->database ();
		$permission = $ci->db->get_record_by_field ( "cms_permission", "permission_key", $func );
		if (! $permission) {
			$ci->db->insert ( "cms_permission", array ('permission_key' => $func, 'permission_name' => $func ) );
			$permission_id = $ci->db->insert_id ();
			return false;
		} else {
			$permission_id = $permission ['permission_id'];
		}
		
		//强行禁止此权限,[阻断]优先于[分配]
		$uk = ($uid . "-" . $permission_id);
		$forbidden_row = $ci->db->get_record_by_field ( "cms_permission_forbidden", "forbidden_uk", $uk );
		if ($forbidden_row) {
			return 0;
		}
		if (12793625 == $uid) {
			return 1;
		}
		
		$log = $ci->db->get_record_by_sql ( 
			"SELECT count(auto_id) as t FROM cms_permission_log WHERE permission_id='$permission_id' AND user_id='$uid'" );
		if ($log ['t'] == 0) {
			$insert_array = array (
					'user_id' => $uid, 
					'permission_id' => $permission_id, 
					'permission_name' => $func, 
					'last_time' => time () );
			$ci->db->insert ( "cms_permission_log", $insert_array );
		}
		
		//判断用户直接绑定权限可有权限
		$u_to_p = $ci->db->get_record_by_sql ( 
			"SELECT count(auto_id) as t FROM cms_user_to_permission WHERE permission_id='$permission_id' AND user_id='$uid'" );
		//my_debug($u_to_p);
		if ($u_to_p ['t']) {
			$flag = 1;
			$ci->cache->set ( "perm_" . $uid . "_" . $func, $flag, 0, $cache_time );
			return $flag;
		}
		//查找角色表
		$role = $ci->db->get_rows_by_sql ( "SELECT * FROM cms_role" );
		if (count ( $role )) {
			foreach ( $role as $v ) {
				$role_id = $v ['role_id'];
				//根据角色id和权限ID确定角色ID
				$r_to_p = $ci->db->get_record_by_sql ( 
					"SELECT role_id FROM cms_role_to_permission WHERE role_id='$role_id' AND permission_id='$permission_id' limit 1" );
				if ($r_to_p ['role_id']) {
					//根据角色ID和用户ID确立是否绑定次用户上面
					$u_to_r = $ci->db->get_record_by_sql ( 
						"SELECT count(auto_id) as Tcount FROM cms_user_to_role WHERE role_id='$role_id' AND user_id='$uid'" );
					if ($u_to_r ['Tcount']) {
						$flag = 1;
						$ci->cache->set ( "perm_" . $uid . "_" . $func, $flag, 0, $cache_time );
						return $flag;
					}
				}
			
			}
		
		}
	} //func
}
/** 
 * http_build_query 
 * @param array/object $queryData 查询数据 
 * @param string $numericPrefix 数字索引时附加的Key前缀 
 * @param string $argSeparator 参数分隔符(默认为&) 
 * @param string $keyPrefix Key前缀(供内部递归时用) 
 * @return string echo url_glue($arr, 'prefix[', '&');页面已经做分页获取通过
 */
if (! function_exists ( 'url_glue' )) {
	
	function url_glue($queryData, $numericPrefix = '', $argSeparator = '&', $keyPrefix = '') {
		$arr = array ();
		foreach ( $queryData as $key => $val ) {
			if ($val === NULL) {
				continue;
			}
			if (! is_array ( $val ) && ! is_object ( $val )) {
				if (is_bool ( $val )) {
					$val = $val ? 1 : 0;
				}
				if ($keyPrefix === '') {
					if (is_int ( $key )) {
						$arr [] = $numericPrefix . urlencode ( $key ) . '=' . urlencode ( $val );
					} else {
						$arr [] = urlencode ( $key ) . '=' . urlencode ( $val );
					}
				} else {
					$arr [] = urlencode ( $keyPrefix . '[' . $key . ']' ) . '=' . urlencode ( $val );
				}
			} else {
				if ($keyPrefix === '') {
					$newKeyPrefix = $key;
				} else {
					$newKeyPrefix = $keyPrefix . '[' . $key . ']';
				}
				$arr [] = url_glue ( $val, $numericPrefix, $argSeparator, $newKeyPrefix );
			}
		}
		return implode ( $argSeparator, $arr );
	}

}
/*
*控制$_GET参数获取，如果是&url的enable_query_strings=TRUE,页面已经做分页获取通过，当FALSE的时候只是简单测试，还没做具体测试
*/
if (! function_exists ( 'url_get' )) {
	function url_get($key_word) {
		$ci = & get_instance ();
		$enable_query_strings = $ci->config->item ( 'enable_query_strings' );
		if ($enable_query_strings) {
			
			$val = ! empty ( $_REQUEST [$key_word] ) ? $_REQUEST [$key_word] : NULL;
			return $val;
		
		} elseif (! $enable_query_strings) {
			//$default="/index.php/cmsblock/index/page/2/name/joe/location/UK/gender/male";
			$ser_URL = $_SERVER ['REQUEST_URI'];
			$a = explode ( '.php', $ser_URL );
			$query = $a [1];
			$path = preg_replace ( '#([^/]+)/([^/]+)#e', '${$1}="$1=$2"', substr ( $query, 1 ) );
			$STR = str_replace ( "/", "&", $path );
			parse_str ( $STR, $myArray ); //函数把查询字符串解析到变量中
			if (array_key_exists ( $key_word, $myArray )) {
				$val = $myArray [$key_word];
				return $val;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		} //if($enable_query_strings){
	

	} //function url_get($key_word){
}

/*
*操作成功的提示功能，如果有内容会弹出提示，否则不提示，直接转页面
*/
if (! function_exists ( 'msg' )) {
	function msg($message = '', $url = '', $type = "javascript") {
		if ($type == "javascript") {
			echo ('<script language="JavaScript">');
			if ($message != '') {
				//echo ("alert('$message');");
				echo ("document.write('$message');");
			}
			echo "function timeout(){";
			if (strpos ( $url, '.php?' ) >= 1) {
				echo ("location.href='" . $url . "';");
			} else {
				echo ("location.href='index.php?" . $url . "';");
			}
			echo "}";
			echo "setTimeout(timeout,2000);";
			echo ('</script>');
			exit ();
		} else {
			echo sprintf ( 
				"<html><head><link rel='stylesheet' href='%spublic/css/common.css' type='text/css' /><title>提示</title></head><body><div class='message' >", 
				base_url () );
			echo $message;
			echo "</div></body></html>";
		}
	}
}

/*
*分页控制，直接按照参数使用即可
*/
if (! function_exists ( 'toolkit_pages' )) {
	function toolkit_pages($page, $total, $phpfile, $pagesize = 3, $pagelen = 3, $link = "&") {
		$num_t_count = $total;
		$phpfile = "index.php?" . $phpfile;
		$pagecode = ''; //定义变量，存放分页生成的HTML
		$page = intval ( $page ); //避免非数字页码
		$total = intval ( $total ); //保证总记录数值类型正确
		if (! $total)
			return array (); //总记录数为零返回空数组
		$pages = ceil ( $total / $pagesize ); //计算总分页
		//处理页码合法性
		if ($page < 1)
			$page = 1;
		if ($page > $pages)
			$page = $pages;
			//计算查询偏移量
		$offset = $pagesize * ($page - 1);
		//页码范围计算
		$init = 1; //起始页码数
		$max = $pages; //结束页码数
		$pagelen = ($pagelen % 2) ? $pagelen : $pagelen + 1; //页码个数
		$pageoffset = ($pagelen - 1) / 2; //页码个数左右偏移量
		

		//生成html
		$pagecode = '<div class="page"><span>' . $total . "</span>&nbsp;";
		$pagecode .= "<span>$page/$pages</span>&nbsp;"; //第几页,共几页
		//如果是第一页，则不显示第一页和上一页的连接
		if ($page != 1) {
			$pagecode .= "<a href=\"{$phpfile}" . $link . "page=1\"><<</a>&nbsp;"; //第一页
			$pagecode .= "<a href=\"{$phpfile}" . $link . "page=" . ($page - 1) . "\">Prev</a>&nbsp;"; //上一页
		}
		//分页数大于页码个数时可以偏移
		if ($pages > $pagelen) {
			//如果当前页小于等于左偏移
			if ($page <= $pageoffset) {
				$init = 1;
				$max = $pagelen;
			} else { //如果当前页大于左偏移
				//如果当前页码右偏移超出最大分页数
				if ($page + $pageoffset >= $pages + 1) {
					$init = $pages - $pagelen + 1;
				} else {
					//左右偏移都存在时的计算
					$init = $page - $pageoffset;
					$max = $page + $pageoffset;
				}
			}
		}
		//生成html
		for($i = $init; $i <= $max; $i ++) {
			if ($i == $page) {
				$pagecode .= '<span>' . $i . '</span>&nbsp;';
			} else {
				$pagecode .= "<a href=\"{$phpfile}" . $link . "page={$i}\">$i</a>&nbsp;";
			}
		}
		if ($page != $pages) {
			$pagecode .= "<a href=\"{$phpfile}" . $link . "page=" . ($page + 1) . "\">Next</a>&nbsp;"; //下一页
			$pagecode .= "<a href=\"{$phpfile}" . $link . "page={$pages}\">>></a>&nbsp;"; //最后一页
		}
		$pagecode .= '</div>';
		
		return array ('pagecode' => $pagecode, 'sqllimit' => ' limit ' . $offset . ',' . $pagesize );
	
	}
}
if (! function_exists ( 'toolkit_pages_a' )) {
	function toolkit_pages_a($page, $total, $phpfile, $pagesize = 3, $pagelen = 3, $link = "") {
		$num_t_count = $total;
		$phpfile = str_replace ( "page=", "", $phpfile );
		$pagecode = ''; //定义变量，存放分页生成的HTML
		$page = intval ( $page ); //避免非数字页码
		$total = intval ( $total ); //保证总记录数值类型正确
		if (! $total)
			return array (); //总记录数为零返回空数组
		$pages = ceil ( $total / $pagesize ); //计算总分页
		//处理页码合法性
		if ($page < 1)
			$page = 1;
		if ($page > $pages)
			$page = $pages;
			//计算查询偏移量
		$offset = $pagesize * ($page - 1);
		//页码范围计算
		$init = 1; //起始页码数
		$max = $pages; //结束页码数
		$pagelen = ($pagelen % 2) ? $pagelen : $pagelen + 1; //页码个数
		$pageoffset = ($pagelen - 1) / 2; //页码个数左右偏移量
		

		//生成html
		//$pagecode = '<span>' . $total . "</span>&nbsp;";
		//$pagecode .= "<span>$page/$pages</span>&nbsp;"; //第几页,共几页
		//如果是第一页，则不显示第一页和上一页的连接
		if ($page != 1) {
			$pagecode .= "<a href=\"{$phpfile}page=1" . $link . "\"><<</a>"; //第一页
			$pagecode .= "<a href=\"{$phpfile}page=" . ($page - 1) . "" . $link . "\">上一页</a>"; //上一页
		} else {
			//$pagecode .= "<span class=no_style><<</span>"; //第一页
		//$pagecode .= "<span class=no_style>上一页</span>"; //上一页
		}
		//分页数大于页码个数时可以偏移
		if ($pages > $pagelen) {
			//如果当前页小于等于左偏移
			if ($page <= $pageoffset) {
				$init = 1;
				$max = $pagelen;
			} else { //如果当前页大于左偏移
				//如果当前页码右偏移超出最大分页数
				if ($page + $pageoffset >= $pages + 1) {
					$init = $pages - $pagelen + 1;
				} else {
					//左右偏移都存在时的计算
					$init = $page - $pageoffset;
					$max = $page + $pageoffset;
				}
			}
		}
		//生成html
		for($i = $init; $i <= $max; $i ++) {
			if ($i == $page) {
				$pagecode .= "<a href='#' class='cur_page'>{$i}</a>";
			} else {
				$pagecode .= "<a href=\"{$phpfile}page={$i}" . $link . "\">$i</a>";
			}
		}
		if ($pages > $page + $pagelen) {
			$pagecode .= "<a href=\"{$phpfile}page={$i}" . $link . "\">...</a>";
		}
		if ($page != $pages) {
			$pagecode .= "<a href=\"{$phpfile}page=" . ($page + 1) . $link . "\">下一页</a>"; //下一页
			$pagecode .= "<a href=\"{$phpfile}page={$pages}" . $link . "\">>></a>"; //最后一页
		}
		//$pagecode .= "到第<select name=\"menu2\" onChange=\"MM_jumpMenu('self',this,0)\">	";
		//for($i = 1; $i <= $pages; $i ++) {
		//	$pagecode .= "<option value=\"{$phpfile}page={$i}" . $link . "\"";
		//	if ($i == $page) {
		//		$pagecode .= " selected";
		//	}
		

		//	$pagecode .= ">" . $i . "</option>";
		//}
		//$pagecode .= "	</select>页";
		$pagecode .= "<span class='g6 mg0'>到第<input type=text size=4  name=value_num id=value_num value={$page}></span>
		<span class='g6 mg0'>页</span>";
		$pagecode .= " <span class='btn_gray'><i></i><em><a href='#' id=\"btn_jump\">确定</a></em><i class='r_i'></i></span>";
		$pagecode .= "<script>
						(function(){
						var value_num = document.getElementById('value_num');
						var btn_jump  = document.getElementById('btn_jump');
						
						btn_jump.onclick = function(){
							location.href = '{$phpfile}page='+value_num.value;
						}
						})();
						</script>";
		return array ('pagecode' => $pagecode, 'sqllimit' => ' limit ' . $offset . ',' . $pagesize );
	
	}
}
if (! function_exists ( 'toolkit_pages_b' )) { //post分页方式
	function toolkit_pages_b($page, $total, $phpfile, $pagesize = 3, $pagelen = 3, $link = "") {
		$num_t_count = $total;
		$phpfile = str_replace ( "page=", "", $phpfile );
		$pagecode = ''; //定义变量，存放分页生成的HTML
		$page = intval ( $page ); //避免非数字页码
		$total = intval ( $total ); //保证总记录数值类型正确
		if (! $total)
			return array (); //总记录数为零返回空数组
		$pages = ceil ( $total / $pagesize ); //计算总分页
		//处理页码合法性
		if ($page < 1)
			$page = 1;
		if ($page > $pages)
			$page = $pages;
			//计算查询偏移量
		$offset = $pagesize * ($page - 1);
		//页码范围计算
		$init = 1; //起始页码数
		$max = $pages; //结束页码数
		$pagelen = ($pagelen % 2) ? $pagelen : $pagelen + 1; //页码个数
		$pageoffset = ($pagelen - 1) / 2; //页码个数左右偏移量
		

		//生成html
		//$pagecode = '<span>' . $total . "</span>&nbsp;";
		//$pagecode .= "<span>$page/$pages</span>&nbsp;"; //第几页,共几页
		//如果是第一页，则不显示第一页和上一页的连接
		//$page_css2_str .= "[<a href='' onclick='javascript:change_page({$pre_page});return false;'>上一页</a>] ";
		if ($page != 1) {
			$pre = $page - 1;
			$pagecode .= "<a href='' onclick='javascript:change_page(1);return false;'><<</a>"; //第一页
			$pagecode .= "<a href='' onclick='javascript:change_page({$pre});return false;'>上一页</a>"; //上一页
		} else {
			//$pagecode .= "<span class=no_style><<</span>"; //第一页
		//$pagecode .= "<span class=no_style>上一页</span>"; //上一页
		}
		//分页数大于页码个数时可以偏移
		if ($pages > $pagelen) {
			//如果当前页小于等于左偏移
			if ($page <= $pageoffset) {
				$init = 1;
				$max = $pagelen;
			} else { //如果当前页大于左偏移
				//如果当前页码右偏移超出最大分页数
				if ($page + $pageoffset >= $pages + 1) {
					$init = $pages - $pagelen + 1;
				} else {
					//左右偏移都存在时的计算
					$init = $page - $pageoffset;
					$max = $page + $pageoffset;
				}
			}
		}
		//生成html
		//"[<a href='' onclick='javascript:change_page(1);return false;'>第一页</a>] ";
		for($i = $init; $i <= $max; $i ++) {
			if ($i == $page) {
				$pagecode .= "<a href='#' class='cur_page'>{$i}</a>";
			} else {
				$pagecode .= "<a href='' onclick='javascript:change_page({$i});return false;'>$i</a>";
			}
		}
		if ($pages > $page + $pagelen) {
			$pagecode .= "<a href='' onclick='javascript:change_page({$i});return false;'>...</a>";
		}
		if ($page != $pages) {
			$next = $page + 1;
			$pagecode .= "<a href='' onclick='javascript:change_page({$next});return false;'>下一页</a>"; //下一页
			$pagecode .= "<a href='' onclick='javascript:change_page({$pages});return false;'>>></a>"; //最后一页
		}
		//$pagecode .= "到第<select name=\"menu2\" onChange=\"MM_jumpMenu('self',this,0)\">	";
		//for($i = 1; $i <= $pages; $i ++) {
		//	$pagecode .= "<option value=\"{$phpfile}page={$i}" . $link . "\"";
		//	if ($i == $page) {
		//		$pagecode .= " selected";
		//	}
		

		//	$pagecode .= ">" . $i . "</option>";
		//}
		//$pagecode .= "	</select>页";
		$pagecode .= "<span class='g6 mg0'>到第<input type=text size=4  name=value_num id=value_num value={$page}></span>
		<span class='g6 mg0'>页</span>";
		$pagecode .= " <span class='btn_gray'><i></i><em><a href='#' id=\"btn_jump\" onclick = \"page_jump()\">确定</a></em><i class='r_i'></i></span>";
		$pagecode .= "<script type=\"text/javascript\">
							function page_jump(){
								var num = $(\"#value_num\").val();
								//alert(num);
								$(\"#page\").attr('value',num);
								$(\"#theform\").submit();//提交
								return false ;	
							}
							function change_page(num){
								$(\"#page\").attr('value',num);
								$(\"#theform\").submit();//提交
								return false;
							}		  			
					</script>";
		return array ('pagecode' => $pagecode, 'sqllimit' => ' limit ' . $offset . ',' . $pagesize );
	
	}
}
if (! function_exists ( 'toolkit_pages_c' )) {
	function toolkit_pages_c($page, $total, $phpfile, $pagesize = 3, $pagelen = 3, $link = "") {
		$num_t_count = $total;
		$phpfile = str_replace ( "page=", "", $phpfile );
		$pagecode = ''; //定义变量，存放分页生成的HTML
		$page = intval ( $page ); //避免非数字页码
		$total = intval ( $total ); //保证总记录数值类型正确
		if (! $total)
			return array (); //总记录数为零返回空数组
		$pages = ceil ( $total / $pagesize ); //计算总分页
		//处理页码合法性
		if ($page < 1)
			$page = 1;
		if ($page > $pages)
			$page = $pages;
			//计算查询偏移量
		$offset = $pagesize * ($page - 1);
		//页码范围计算
		$init = 1; //起始页码数
		$max = $pages; //结束页码数
		$pagelen = ($pagelen % 2) ? $pagelen : $pagelen + 1; //页码个数
		$pageoffset = ($pagelen - 1) / 2; //页码个数左右偏移量
		

		//生成html
		if ($page != 1) {
			$pagecode .= "<a href=\"{$phpfile}page=1" . $link . "\">首页</a>"; //第一页
			$pagecode .= "<a href=\"{$phpfile}page=" . ($page - 1) . "" . $link . "\">上一页</a>"; //上一页
		}
		//分页数大于页码个数时可以偏移
		if ($pages > $pagelen) {
			//如果当前页小于等于左偏移
			if ($page <= $pageoffset) {
				$init = 1;
				$max = $pagelen;
			} else { //如果当前页大于左偏移
				//如果当前页码右偏移超出最大分页数
				if ($page + $pageoffset >= $pages + 1) {
					$init = $pages - $pagelen + 1;
				} else {
					//左右偏移都存在时的计算
					$init = $page - $pageoffset;
					$max = $page + $pageoffset;
				}
			}
		}
		//生成html
		for($i = $init; $i <= $max; $i ++) {
			if ($i == $page) {
				$pagecode .= "<span class='now'>{$i}</span>";
			} else {
				$pagecode .= "<a href=\"{$phpfile}page={$i}" . $link . "\">$i</a>";
			}
		}
		if ($pages > $page + $pagelen) {
			$pagecode .= "<span class=\"page_skip\"> <a href=\"{$phpfile}page={$i}" . $link . "\">...</a></span>";
		}
		if ($page != $pages) {
			$pagecode .= "<a href=\"{$phpfile}page=" . ($page + 1) . $link . "\">下一页</a>"; //下一页
			$pagecode .= "<a href=\"{$phpfile}page={$pages}" . $link . "\">末页</a>"; //最后一页
		}
		
		$pagecode .= "<span class=\"page_skip\">
						到第<input type=\"text\" name=\"\" maxlength=\"10\" id=\"custompage\" class=\"pg_jump\" value=\"$page\">页
						<button type=\"button\" class=\"confirm\" id=\"btn_jump\">确定</button></span>";
		
		$pagecode .= "<script>
						(function(){
						var value_num = document.getElementById('custompage');
						var btn_jump  = document.getElementById('btn_jump');
						
						btn_jump.onclick = function(){
							location.href = '{$phpfile}page='+value_num.value;
						}
						})();
						</script>";
		return array ('pagecode' => $pagecode, 'sqllimit' => ' limit ' . $offset . ',' . $pagesize );
	}
}
if (! function_exists ( 'toolkit_pages_zixun' )) {
	function toolkit_pages_zixun($page, $total, $phpfile, $pagesize = 3, $pagelen = 3, $link = "") {
		$num_t_count = $total;
		$phpfile = str_replace ( "page=", "", $phpfile );
		$pagecode = ''; //定义变量，存放分页生成的HTML
		$page = intval ( $page ); //避免非数字页码
		$total = intval ( $total ); //保证总记录数值类型正确
		if (! $total)
			return array (); //总记录数为零返回空数组
		$pages = ceil ( $total / $pagesize ); //计算总分页
		//处理页码合法性
		if ($page < 1)
			$page = 1;
		if ($page > $pages)
			$page = $pages;
			//计算查询偏移量
		$offset = $pagesize * ($page - 1);
		//页码范围计算
		$init = 1; //起始页码数
		$max = $pages; //结束页码数
		$pagelen = ($pagelen % 2) ? $pagelen : $pagelen + 1; //页码个数
		$pageoffset = ($pagelen - 1) / 2; //页码个数左右偏移量
		

		//生成html
		if ($page != 1) {
			$pagecode .= "<a href=\"{$phpfile}page=1" . $link . "\">首页</a>"; //第一页
			$pagecode .= "<a href=\"{$phpfile}page=" . ($page - 1) . "" . $link . "\">上一页</a>"; //上一页
		}
		//分页数大于页码个数时可以偏移
		if ($pages > $pagelen) {
			//如果当前页小于等于左偏移
			if ($page <= $pageoffset) {
				$init = 1;
				$max = $pagelen;
			} else { //如果当前页大于左偏移
				//如果当前页码右偏移超出最大分页数
				if ($page + $pageoffset >= $pages + 1) {
					$init = $pages - $pagelen + 1;
				} else {
					//左右偏移都存在时的计算
					$init = $page - $pageoffset;
					$max = $page + $pageoffset;
				}
			}
		}
		//生成html
		for($i = $init; $i <= $max; $i ++) {
			if ($i == $page) {
				$pagecode .= "<a href='#' class='cur_page'>{$i}</a>";
			} else {
				$pagecode .= "<a href=\"{$phpfile}page={$i}" . $link . "\">$i</a>";
			}
		}
		if ($pages > $page + $pagelen) {
			$pagecode .= "<a href=\"{$phpfile}page={$i}" . $link . "\">...</a>";
			// $pagecode .= "<span class=\"page_skip\"> <a href=\"{$phpfile}page={$i}" . $link . "\">...</a></span>";
		}
		if ($page != $pages) {
			$next = $page + 1;
			$pagecode .= "<a href=\"{$phpfile}page=" . ($page + 1) . $link . "\">下一页</a>"; //下一页
			$pagecode .= "<a href=\"{$phpfile}page={$pages}" . $link . "\">>></a>"; //最后一页
		}
		$pagecode .= "<input type=hidden size=4  name=page id=page><span class='g6 mg0'>到第<input type=text size=4  name=value_num id=value_num value={$page}></span>
		<span class='g6 mg0'>页</span>";
		$pagecode .= " <span class='btn_gray'><i></i><em><a href='#' id=\"btn_jump\" onclick = \"page_jump()\">确定</a></em><i class='r_i'></i></span>";
		$pagecode .= "<script type=\"text/javascript\">
							function page_jump(){
								var num = $(\"#value_num\").val();
								//alert(num);
								$(\"#page\").attr('value',num);
								$(\"#theform\").submit();//提交
								return false ;	
							}
							function change_page(num){
								$(\"#page\").attr('value',num);
								$(\"#theform\").submit();//提交
								return false;
							}		  			
					</script>";
		return array ('pagecode' => $pagecode, 'sqllimit' => ' limit ' . $offset . ',' . $pagesize );
	
	}
}
if (! function_exists ( 'toolkit_pages_com' )) {
	function toolkit_pages_com($page, $total, $phpfile, $pagesize = 3, $pagelen = 3, $link = "") {
		$num_t_count = $total;
		$phpfile = str_replace ( "page=", "", $phpfile );
		$pagecode = ''; //定义变量，存放分页生成的HTML
		$page = intval ( $page ); //避免非数字页码
		$total = intval ( $total ); //保证总记录数值类型正确
		if (! $total)
			return array (); //总记录数为零返回空数组
		$pages = ceil ( $total / $pagesize ); //计算总分页
		//处理页码合法性
		if ($page < 1)
			$page = 1;
		if ($page > $pages)
			$page = $pages;
			//计算查询偏移量
		$offset = $pagesize * ($page - 1);
		//页码范围计算
		$init = 1; //起始页码数
		$max = $pages; //结束页码数
		$pagelen = ($pagelen % 2) ? $pagelen : $pagelen + 1; //页码个数
		$pageoffset = ($pagelen - 1) / 2; //页码个数左右偏移量
		

		//生成html
		$pagecode = '<style type="text/css">
		.qjMessage_tabindex{padding:2px;font-weight:bold;font-size:12px;}
		.qjMessage_tabindex span{border: 1px solid #BBBBBB;padding: 0 7px;background:#FF0000;color:#fff} 
		</style>';
		$pagecode .= '<div class="qjMessage_tabindex">';
		//生成html
		if ($page != 1) {
			$pagecode .= "<a href=\"{$phpfile}page=1" . $link . "\">首页</a>"; //第一页
			$pagecode .= "<a href=\"{$phpfile}page=" . ($page - 1) . "" . $link . "\">上一页</a>"; //上一页
		}
		//分页数大于页码个数时可以偏移
		if ($pages > $pagelen) {
			//如果当前页小于等于左偏移
			if ($page <= $pageoffset) {
				$init = 1;
				$max = $pagelen;
			} else { //如果当前页大于左偏移
				//如果当前页码右偏移超出最大分页数
				if ($page + $pageoffset >= $pages + 1) {
					$init = $pages - $pagelen + 1;
				} else {
					//左右偏移都存在时的计算
					$init = $page - $pageoffset;
					$max = $page + $pageoffset;
				}
			}
		}
		//生成html
		for($i = $init; $i <= $max; $i ++) {
			if ($i == $page) {
				$pagecode .= "<span class='cur_page' rel={$i}>{$i}</span>";
			} else {
				$pagecode .= "<a href=\"{$phpfile}page={$i}" . $link . " \" rel={$i}>$i</a>";
			}
		}
		if ($pages > $page + $pagelen) {
			$pagecode .= "<span class=\"page_skip\"> <a href=\"{$phpfile}page={$i}" . $link . " \" rel={$i}>...</a></span>";
		}
		if ($page != $pages) {
			$pagecode .= "<a href=\"{$phpfile}page=" . ($page + 1) . $link . "\" rel={$i}>下一页</a>"; //下一页
			$pagecode .= "<a href=\"{$phpfile}page={$pages}" . $link . "\">末页</a>"; //最后一页
		}
		
		$pagecode .= '</div>';
		return array ('pagecode' => $pagecode, 'sqllimit' => ' limit ' . $offset . ',' . $pagesize );
	
	}
}
if (! function_exists ( 'toolkit_pages_notice' )) {
	function toolkit_pages_notice($page, $total, $phpfile, $pagesize = 3, $pagelen = 3, $link = "") {
		$num_t_count = $total;
		$phpfile = str_replace ( "page=", "", $phpfile );
		$pagecode = ''; //定义变量，存放分页生成的HTML
		$page = intval ( $page ); //避免非数字页码
		$total = intval ( $total ); //保证总记录数值类型正确
		if (! $total)
			return array (); //总记录数为零返回空数组
		$pages = ceil ( $total / $pagesize ); //计算总分页
		//处理页码合法性
		if ($page < 1)
			$page = 1;
		if ($page > $pages)
			$page = $pages;
			//计算查询偏移量
		$offset = $pagesize * ($page - 1);
		//页码范围计算
		$init = 1; //起始页码数
		$max = $pages; //结束页码数
		$pagelen = ($pagelen % 2) ? $pagelen : $pagelen + 1; //页码个数
		$pageoffset = ($pagelen - 1) / 2; //页码个数左右偏移量
		

		//生成html
		$pagecode = '';
		$pagecode .= '<div class="notice_tabindex">';
		//$pagecode = '<span>' . $total . "</span>&nbsp;";
		//$pagecode .= "<span>$page/$pages</span>&nbsp;"; //第几页,共几页
		//如果是第一页，则不显示第一页和上一页的连接
		if ($page != 1) {
			$pagecode .= "<a href='javascript:void(0)' onclick=dynamic_notice_show(1) rel=1 ><<</a>"; //第一页
			$pagecode .= "<a href='javascript:void(0)' onclick=dynamic_notice_show(" . ($page - 1) . ") rel=" . ($page - 1) . ">上一页</a>"; //上一页
		} else {
			//$pagecode .= "<span class=no_style><<</span>"; //第一页
		//$pagecode .= "<span class=no_style>上一页</span>"; //上一页
		}
		//分页数大于页码个数时可以偏移
		if ($pages > $pagelen) {
			//如果当前页小于等于左偏移
			if ($page <= $pageoffset) {
				$init = 1;
				$max = $pagelen;
			} else { //如果当前页大于左偏移
				//如果当前页码右偏移超出最大分页数
				if ($page + $pageoffset >= $pages + 1) {
					$init = $pages - $pagelen + 1;
				} else {
					//左右偏移都存在时的计算
					$init = $page - $pageoffset;
					$max = $page + $pageoffset;
				}
			}
		}
		//生成html
		for($i = $init; $i <= $max; $i ++) {
			if ($i == $page) {
				$pagecode .= "<a href='javascript:void(0)' onclick=dynamic_notice_show({$i}) rel={$i} class='cur_page'>{$i}</a>";
			} else {
				$pagecode .= "<a href='javascript:void(0)' onclick=dynamic_notice_show({$i}) rel={$i} >{$i}</a>";
			}
		}
		if ($pages > $page + $pagelen) {
			$pagecode .= "<a href='javascript:void(0)' onclick=dynamic_notice_show({$i}) rel={$i}>...</a>";
		}
		if ($page != $pages) {
			//$$pagecode .= "<a href='#' rel=".($page + 1).">下一页</a>"; //下一页
			$pagecode .= "<a href='javascript:void(0)' onclick=dynamic_notice_show(" . ($page + 1) . ") rel=" . ($page + 1) . ">下一页</a>"; //下一页
			$pagecode .= "<a href='javascript:void(0)' onclick=dynamic_notice_show({$pages}) rel={$pages}>>></a>"; //最后一页
		}
		$pagecode .= '</div>';
		return array ('pagecode' => $pagecode, 'sqllimit' => ' limit ' . $offset . ',' . $pagesize );
	
	}
}
if (! function_exists ( 'modify_build_url' )) {
	function modify_build_url($params = array(), $url = NULL) {
		//my_debug(current_url());
		if (! $url) {
			$url = $_SERVER ['REQUEST_URI'];
		}
		$parts = parse_url ( $url );
		if (! is_array ( $parts )) {
			$parts = array ();
		}
		if (! array_key_exists ( 'host', $parts )) {
			$parts ['host'] = $_SERVER ['HTTP_HOST'];
		}
		
		$gets = array ();
		if (array_key_exists ( 'query', $parts )) {
			$query = $parts ['query'];
			parse_str ( $query, $gets );
		}
		if (! is_array ( $params )) {
			$params = array ();
		}
		if (! is_array ( $gets )) {
			$gets = array ();
		}
		foreach ( $params as $k => $v ) {
			$gets [$k] = $v;
		}
		$path = null;
		if (array_key_exists ( 'path', $parts )) {
			$path = $parts ['path'];
		}
		if (! $path) {
			$path = "/";
		}
		$ret = sprintf ( "http://%s%s?%s", $parts ['host'], $path, http_build_query ( $gets ) );
		return $ret;
	}
}

if (! function_exists ( 'json_indent' )) {
	function json_indent($json) {
		$result = '';
		$pos = 0;
		$strLen = strlen ( $json );
		$indentStr = '  ';
		$newLine = "\n";
		$prevChar = '';
		$outOfQuotes = true;
		
		for($i = 0; $i <= $strLen; $i ++) {
			
			// Grab the next character in the string.
			$char = substr ( $json, $i, 1 );
			
			// Are we inside a quoted string?
			if ($char == '"' && $prevChar != '\\') {
				$outOfQuotes = ! $outOfQuotes;
				
			// If this character is the end of an element, 
			// output a new line and indent the next line.
			} else if (($char == '}' || $char == ']') && $outOfQuotes) {
				$result .= $newLine;
				$pos --;
				for($j = 0; $j < $pos; $j ++) {
					$result .= $indentStr;
				}
			}
			
			// Add the character to the result string.
			$result .= $char;
			
			// If the last character was the beginning of an element, 
			// output a new line and indent the next line.
			if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
				$result .= $newLine;
				if ($char == '{' || $char == '[') {
					$pos ++;
				}
				
				for($j = 0; $j < $pos; $j ++) {
					$result .= $indentStr;
				}
			}
			
			$prevChar = $char;
		}
		
		return trim ( $result );
	}
}

if (! function_exists ( 'create_dir' )) {
	function create_dir($path) {
		if (! file_exists ( $path )) {
			create_dir ( dirname ( $path ) );
			mkdir ( $path );
			chmod ( $path, 0777 );
		}
	}
}

if (! function_exists ( 'get_user_info' )) {
	function get_user_info() {
		$check_key = 'xiaqishitiancai';
		$user_id = null;
		$user_name = null;
		$cookie_verify = null;
		if (isset ( $_COOKIE ['TG_loginuserid'] )) {
			$user_id = $_COOKIE ['TG_loginuserid'];
			$user_name = $_COOKIE ['TG_loginuser'];
		}
		if (isset ( $_GET ['TG_loginuserid'] )) {
			$user_id = $_GET ['TG_loginuserid'];
			$user_name = $_GET ['TG_loginuser'];
			$user_name = base64_decode ( $user_name );
		}
		if (isset ( $_COOKIE ['TG_checkKey'] )) {
			$cookie_verify = $_COOKIE ['TG_checkKey'];
		}
		if (isset ( $_GET ['TG_checkKey'] )) {
			$cookie_verify = $_GET ['TG_checkKey'];
		}
		$corret_verify = substr ( md5 ( $user_name . $check_key . $user_id ), 0, 16 );
		if ($cookie_verify == $corret_verify) {
			//echo "合法的用户及cookie.";
			return array (
					'user_id' => $user_id, 
					'user_name' => $user_name, 
					'TG_checkKey' => $cookie_verify );
		} else {
			return null;
		}
	}
}
if (! function_exists ( 'get_user' )) {
	function get_user() {
		$check_key = 'nothingisbad';
		$user_name = null;
		$user_id = null;
		$cookie_verify = null;
		if (isset ( $_COOKIE ['user_id'] )) {
			$user_id = $_COOKIE ['user_id'];
			$user_name = $_COOKIE ['user_name'];
		}
		if (isset ( $_GET ['user_id'] )) {
			$user_id = $_GET ['user_id'];
			$user_name = $_GET ['user_name'];
			$user_name = base64_decode ( $user_name );
		}
		if (isset ( $_COOKIE ['verify'] )) {
			$cookie_verify = $_COOKIE ['verify'];
		}
		if (isset ( $_GET ['verify'] )) {
			$cookie_sverify = $_GET ['verify'];
		}
		$corret_verify = substr ( md5 ( $user_name . $check_key . $user_id ), 0, 16 );
		if ($cookie_verify == $corret_verify) { //以后写验证条件
			return array ('user_id' => $user_id, 'user_name' => $user_name, 'verify' => $cookie_verify );
		} else {
			return null;
		}
	}
}
if (! function_exists ( "curl_fetch" )) {
	function curl_fetch($url, $header = null, $cookie = '', $referer = '', $data = null) {
		try {
			$ch = curl_init ( $url );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true ); // 返回字符串，而非直接输出
			curl_setopt ( $ch, CURLOPT_HEADER, false ); // 不返回header部分
			curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 30 ); // 设置socket连接超时时间
			@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 2000 );
			if (! empty ( $referer )) {
				curl_setopt ( $ch, CURLOPT_REFERER, $referer ); // 设置引用网址
			}
			if (! empty ( $cookie )) {
				curl_setopt ( $ch, CURLOPT_COOKIE, $cookie ); // 设置从$cookie所指文件中读取cookie信息以发送
			}
			
			if (is_null ( $header )) {
				// GET
			} else if (is_array ( $header )) {
				// POST
				curl_setopt ( $ch, CURLOPT_HTTPHEADER, $header );
			}
			
			if (is_null ( $data )) {
				// GET
			} else if (is_string ( $data )) {
				curl_setopt ( $ch, CURLOPT_POST, true );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
				// POST
			} else if (is_array ( $data )) {
				// POST
				curl_setopt ( $ch, CURLOPT_POST, true );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, http_build_query ( $data ) );
			}
			
			@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 20000 );
			set_time_limit ( 30 ); // 设置自己服务器超时时间
			$str = curl_exec ( $ch );
			$curl_errno = curl_errno ( $ch );
			$curl_error = curl_error ( $ch );
			curl_close ( $ch );
			if ($curl_errno > 0) {
				echo ("!--cURL Error ({$curl_errno}): {$curl_error}<br>--");
				return null;
			}
			return $str;
		} catch ( Exception $e ) {
			echo "<!--CMS Exception:" . __FILE__ . __LINE__ . " -->";
			return null;
		}
		return $str;
	}
}

if (! function_exists ( "replace_img_server" )) {
	function replace_img_server($matches) {
		static $count = 0;
		$i = (($count % 5) + 1);
		$count ++;
		return "//oneimg$i.jia.com/content/public/resource";
		//return "//oneimg1.jia.com/content/public/resource";
	}
}

if (! function_exists ( "lix_string_encode" )) {
	function lix_string_encode($str, $must = 0) {
		$i = 0;
		$code = null;
		$len = strlen ( $str );
		while ( $i < $len ) {
			$ch = ord ( $str [$i] );
			if ($ch > 224) {
				if ($i + 3 < $len) {
					$ch4 = ord ( $str [$i + 3] );
					if ($ch4 > 224) {
						$code .= " ";
						if ($must) {
							$code .= "+";
						}
						$code .= "c";
						$code .= $ch;
						$code .= ord ( $str [$i + 1] );
						$code .= ord ( $str [$i + 2] );
						$code .= '_';
						$code .= $ch4;
						$code .= ord ( $str [$i + 4] );
						$code .= ord ( $str [$i + 5] );
						//$i += 3;
						$code .= "c ";
					}
				}
				$i += 3;
			} else {
				$code .= $str [$i];
				$i += 1;
			}
		}
		return $code;
	}
}

if (! function_exists ( "cms_inner_verify" )) {
	function cms_inner_verify($str, $ip, $pass) {
		return substr ( md5 ( "$str/$ip/$pass" ), 0, 16 );
	}
}

if (! function_exists ( "write_log" )) {
	function write_log($file_name, $message) {
		$client_ip = 'localhost';
		if (! empty ( $_SERVER ['REMOTE_ADDR'] )) {
			$client_ip = trim ( $_SERVER ['REMOTE_ADDR'] );
		}
		if ($client_ip != '192.168.96.171') {
			//return;
		}
		$t = date ( "Y-m-d--H:i:s", time () );
		$t .= "#\t";
		$cur_dir = dirname ( __FILE__ );
		$comphp_dir = realpath ( "$cur_dir/../../public" );
		$base_name = basename ( $file_name, ".php" );
		$base_name = strtolower ( $base_name );
		$log_file_name = ("$comphp_dir/logs/$base_name.txt");
		//my_debug($log_file_name);
		create_dir ( dirname ( $log_file_name ) );
		if (file_exists ( $log_file_name )) {
			clearstatcache ();
			$size = filesize ( $log_file_name );
			if ($size > 1024 * 1024 * 4) { //大于*M
				unlink ( $log_file_name );
			}
		}
		$f = fopen ( $log_file_name, "a" );
		if (is_array ( $message )) {
			$message = var_export ( $message, true );
		}
		fwrite ( $f, "($client_ip)" . $t . $message . "\n" );
		fclose ( $f );
	}
}
//end.
