<?php

if (! function_exists ( 'table_grid_surround_braces' )) {
	function _datagrid_surround_braces(&$str) {
		$str = '{' . $str . '}';
	}
}

if (! function_exists ( 'datagrid_action' )) {
	/**
	 * Mencetak daftar aksi yang bisa dilakukan
	 *
	 * @param array $actions	daftar aksi
	 * @param array $rowitem	row
	 */
	function datagrid_action($actions, $rowitem) {
		$output = '';
		
		foreach ( $actions as $action ) {
			// parsing url
			$url = $action ['url'];
			$keys = array_keys ( $rowitem );
			$values = array_values ( $rowitem );
			array_walk ( $keys, '_datagrid_surround_braces' );
			$url = str_replace ( $keys, $values, $url );
			
			$output .= '<a href="' . str_replace ( $keys, $values, $url ) . '">' . $action ['text'] . '</a>&nbsp;&nbsp;';
		}
		
		echo '<td>' . $output . '</td>';
	}
}

if (! function_exists ( 'datagrid_checkbox' )) {
	/**
	 * Mencetak kolom untuk checkbox
	 *
	 * @param string $grid_id				nama data grid
	 * @param array  $rowitem				row
	 * @param string $grid_checkbox_column	nama kolom sebagai nilai checkbox
	 */
	function datagrid_checkbox($grid_id, $rowitem, $grid_checkbox_column) {
		echo '<td><input type="checkbox" name="' . $grid_id . '[]" class="targetCheck" value="' . $rowitem [$grid_checkbox_column] . '" /></td>';
	}
}

if (! function_exists ( 'datagrid_data' )) {
	/**
	 * Mencetak baris
	 *
	 * @param array $rowitem			row yang siap dicetak
	 * @param array $grid_row_column	daftar nama kolom yang akan dicetak
	 */
	function datagrid_data($rowitem, $grid_row_column) {
		if (is_array ( $grid_row_column )) {
			foreach ( $grid_row_column as $row_column ) {
				//$tdClass = is_numeric ( $rowitem [$row_column] ) ? " class='aright_{$row_column} '" : '';
				$tdClass = " class='aright_{$row_column}'";
				echo '<td' . $tdClass . '>' . $rowitem [$row_column] . '</td>' . "\n";
			}
		}
	}
}

if (! function_exists ( 'datagrid_heading' )) {
	/**
	 * Mencetak kolom data grid
	 *
	 * @param array $grid_heading
	 */
	function datagrid_heading($grid_heading) {
		if (is_array ( $grid_heading )) {
			foreach ( $grid_heading as $heading ) {
				$attr = isset ( $heading ['attribute'] ) || ! empty ( $heading ['attribute'] ) ? ' ' . $heading ['attribute'] . ' ' : ' ';
				echo '<th' . $attr . '>' . $heading ['text'] . '</th>';
			}
		}
	}
}

if (! function_exists ( 'datagrid_paginate_date' )) {
	/**
	 * Mengambil data tertentu untuk keperluan pagination
	 *
	 * @param array $data
	 * @param int	$currentPage
	 * @param int	$perPage
	 * @return array data yang sudah terfilter
	 */
	function datagrid_paginate_data($data, $currentPage, $perPage) {
		// minimal halaman 1
		if ($currentPage <= 0)
			$currentPage = 1;
		$indexStart = 0;
		$indexEnd = 0;
		$totalRows = count ( $data );
		
		// hitung jumlah halaman
		$numPages = ceil ( $totalRows / $perPage );
		
		// cari tahu mulai index keberapa data diambil
		$indexEnd = ($perPage * $currentPage) - 1;
		$indexStart = ($indexEnd - $perPage) + 1;
		
		// looping data
		$newdata = array ();
		for($i = $indexStart; $i <= $indexEnd; $i ++) {
			// data sudah tidak ada berarti selesai
			if (isset ( $data [$i] ) == FALSE) {
				break;
			}
			
			$newdata [] = $data [$i];
		}
		unset ( $data );
		return $newdata;
	}
}
?>
