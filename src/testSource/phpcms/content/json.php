<?php
header ( "Content-type:text/html;charset=utf-8" );
?>
<textarea cols=150 rows=25>
<?php
/*
$param_config = array (
		"table_name" => array (
				'description' => "表名", 
				"default" => "news", 
				"type" => "select", 
				"options" => array ("cms_news" => "新闻表", "goods" => "产品表" ) ), 
		"field_1" => array (
				'description' => "条件字段", 
				"default" => "category_id", 
				"type" => "select", 
				"options" => array ("category_id" => "新闻分类ID" ) ), 
		"value_1" => array ('description' => "条件取值", "default" => "2", "type" => "text", "rule" => "required|numeric" ), 
		"field_2" => array ('description' => "排序字段", "default" => "auto_id", "type" => "text" ), 
		"field_2_order" => array (
				'description' => "排序方式", 
				"default" => "desc", 
				"type" => "select", 
				"options" => array ("asc" => "升序", "desc" => "降序" ) ) );
$param_config = array (
		"ap_id" => array (
				'description' => "广告区ID", 
				"default" => "253", 
				"type" => "select", 
				"rule" => "required|numeric", 
				"options" => array (
						"253" => "首页顶部banner", 
						"254" => "首页图片切换区", 
						"255" => "热门团购活动", 
						"256" => "首页疯狂抢购", 
						"257" => "首页设计施工促销", 
						"258" => "首页设计施工flash", 
						"259" => "首页设计施工文字", 
						"260" => "首页家居软装促销", 
						"261" => "首页家居软装flash", 
						"262" => "首页家居软装文字", 
						"263" => "首页婚庆促销", 
						"264" => "首页婚庆flash", 
						"265" => "首页婚庆文字", 
						"266" => "首页推荐品牌小图", 
						"267" => "首页专题推荐", 
						"268" => "建材团购活动", 
						"269" => "装修团购活动", 
						"270" => "居家团购活动", 
						"271" => "婚庆团购活动", 
						"272" => "上海首页疯狂抢购", 
						"273" => "自定义标题1", 
						"274" => "自定义标题2", 
						"275" => "自定义标题3", 
						"276" => "首页热门搜索", 
						"309" => "首页疯狂抢购（新）", 
						"325" => "右上侧文字", 
						"370" => "横幅banner" ) ) );
$param_config = array (
		"id" => array (
				'description' => "店铺ID", 
				"default" => "1", 
				"type" => "text", 
				"rule" => "required|numeric"
 ) );
$param_config = array (
		
		'message_id' => '信息ID', 
		'shop_id' => '店铺ID', 
		'shop_name' => '店铺名', 
		'pro_id' => '', 
		'pro_name' => '', 
		'category_id' => '分类ID', 
		'uid' => '用户ID', 
		'uname' => '用户名', 
		'manage_id' => '管理员ID', 
		'manage_name' => '管理员名字', 
		'crm_id' => 'CRM(客户关系管理系统)ID', 
		'ucare' => '', 
		'content' => '内容', 
		'puttime' => '建立时间', 
		'message_del' => '消息-for-删除', 
		'message_state' => '消息-for-状态', 
		'message_top' => '消息-for-顶部', 
		'reply_id' => '回复ID', 
		'reply_from' => '回复来源', 
		'reply_edit_name' => '', 
		'reply_edit_time' => '', 
		'is_reply_again' => '', 
		'add_ip' => 'IP地址', 
		'areaflag' => '地区号', 
		'star_role_id' => '星级', 
		'manager_type' => '管理员类型' )

;

*/
/*$param_config = array (
		'notice_id' => '自增ID', 
		'create_time' => '创建日期', 
		'city_id' => '城市' );*/
$param_config = array (
		"table_name" => array (
				'description' => "新闻公告", 
				"default" => "data_notice", 
				"type" => "select", 
				"options" => array ("data_notice" => "新闻公告") ), 
		"field_1" => array (
				'description' => "条件字段", 
				"default" => "city_id", 
				"type" => "select", 
				"options" => array ("city_id" => "城市(city_id)" ) ), 
		"value_1" => array ('description' => "条件取值", "default" => "shanghai", "type" => "text", "rule" => "required" ), 

		"field_2" => array (
				'description' => "排序字段", 
				"default" => "notice_id", 
				"type" => "select", 
				"options" => array ("notice_id" => "新闻id","create_time"=>"发布日期" ) ), 
		"field_2_order" => array (
				'description' => "排序方式", 
				"default" => "desc", 
				"type" => "select", 
				"options" => array ("asc" => "升序", "desc" => "降序" ) ) );
echo json_encode ( $param_config );



?>
</textarea>
<?php
//phpinfo ();
?>