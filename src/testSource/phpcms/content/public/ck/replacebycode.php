<?php
session_start(); 
$_SESSION['id']='0000';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
-->
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<title>Replace Textarea by Code &mdash; CKEditor Sample</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
	<script type="text/javascript" src="ckeditor.js"></script>
	<script src="_samples/sample.js" type="text/javascript"></script>
    
	<link href="_samples/sample.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<form action="_samples/sample_posteddata.php" method="post">
		<p>
			<label for="editor2">
				Editor 2:</label>
			<textarea cols="80" id="editor2" name="editor2" rows="10">&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href="http://ckeditor.com/"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;</textarea>
			<script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				var res=CKEDITOR.replace( 'editor2'); 
				//,{filebrowserBrowseUrl :'../ckfinder/ckfinder.html'}//直接通过权限载入页面中
				res.config.contentsCss = ['/css/a.css', '/css/b.css'];
				//res.addCss( 'p{border: #F00 solid 1px;}'  ); 
				


			//]]>
			</script>
            
            CKEDITOR

		</p>
		<p>
			<input type="submit" value="Submit" />
		</p>
	</form>
	<div id="footer">
		<hr />
		<p>
			CKEditor - The text editor for the Internet - <a class="samples" href="http://ckeditor.com/">http://ckeditor.com</a>
		</p>
		<p id="copy">
			Copyright &copy; 2003-2011, <a class="samples" href="http://cksource.com/">CKSource</a> - Frederico
			Knabben. All rights reserved.
		</p>
	</div>
</body>
</html>
